#include "BroadZBinning.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

/* LAPACK - DGESV prototype */
extern "C" void dgesv_( int* n, int* nrhs, double* a, int* lda, 
                        int* ipiv, double* b, int* ldb, int* info );

BroadZBinning::~BroadZBinning()
{	
	if (residualRedshiftBins) {
		for (int i=0;i<binZ->getNumBins();i++) {
			if (residualRedshiftBins[i]) {
				delete residualRedshiftBins[i];
				residualRedshiftBins[i]=NULL;
			}
		}
		freeArray1(residualRedshiftBins);
	}
		
	if (this->binZ)
		delete this->binZ;	
		
	if (entries) {				
		for (int i=0;i<this->numEntries;i++) 
			if (entries[i]) {
				delete entries[i];		
				entries[i]=NULL;
			}	
		freeArray1(entries);	
	}	
}
	
void BroadZBinning::setMinEntriesBin(int minEntriesBin) {
	
	int *countZ;
	int *numEnt;
	Bisection *bi;
	char s[MAX_LENGTH];
	
	double* tentativeZ;
	double minZ,maxZ;	
	int minZi,maxZi;
	int count;
	int binNr;
	
	minZ=0;
	maxZ=0;
	minZi=0;
	maxZi=0;
				
	this->minEntriesBin=minEntriesBin;			
				
	callocArray1(countZ,binSpecZ->getLength());this->binSpecZ=binSpecZ;	
	bi = new Bisection(binSpecZ->getLength(),binSpecZ->getZ());
		
	for (int i=0;i<numEntries;i++) {
		countZ[bi->getNearestIndex(entries[i]->getSpecZ())]++;
	}		
				
	// find minimum and maximum
	for (int zi=0;zi<binSpecZ->getLength();zi++) {
		if (countZ[zi]>0) {
			minZi=zi;
			minZ=binSpecZ->getZ(zi);
			break;
		}
	}
	for (int zi=binSpecZ->getLength()-1;zi>=0;zi--) {
		if (countZ[zi]>0) {
			maxZi=zi;
			maxZ=binSpecZ->getZ(zi);
			break;
		}
	}
	sprintf(s,"minZ=%g maxZ=%g",minZ,maxZ);
	if (m_out)
		m_out->println(s,2);	
	
	// fill tentativeBins
	allocArray1(tentativeZ,maxZi-minZi+1);	
	tentativeZ[0]=minZ-0.0001;
	count=0;
	binNr=1;
	for (int zi=0;zi<binSpecZ->getLength();zi++) {
		count+=countZ[zi];
		if (count>=minEntriesBin) {
			tentativeZ[binNr]=binSpecZ->getZ(zi);
			binNr++;
			count=0;
		}
	}		
	// put last bin (which has by construction less then minEntriesBin entries)
	// to the previous bin	
	binNr--;
	tentativeZ[binNr]=maxZ;
		
	// have now binNr bins going from 
	// - first bin tentativeZ[0]...tentativeZ[1]
	// - last bin tentative[binNr-1]..tentativeZ[binNr]
	if (binZ)
		delete binZ;
	binZ = new BinningZNumber();
	binZ->set(binNr+1, tentativeZ);
	allocArray1(numEnt,binZ->getLength()-1);	
	
	// check
	for (int zi=0;zi<binZ->getNumBins();zi++) {
		count=0;
		for (int i=0;i<numEntries;i++) {
			minZ=binZ->getZ(zi);			
			maxZ=binZ->getZ(zi+1);			
			if (entries[i]->getSpecZ()>minZ && entries[i]->getSpecZ()<=maxZ) {
				count++;
			}
		}
		numEnt[zi]=count;
		sprintf(s,"binIndex=%d minZ=%g maxZ=%g count=%d",zi,minZ,maxZ,count);
		if (m_out)
			m_out->println(s,2);
	}
	
	this->binZ->setNumEntries(numEnt);
	
	freeArray1(tentativeZ);
	freeArray1(countZ);
	freeArray1(numEnt);
		
}	

void BroadZBinning::setzBins(BinningZ *binZ) 
{
	int *numEnt;
	double minZ;
	double maxZ;
	int count;
	int invalidCount;
	char s[MAX_LENGTH];		
	
	if (this->binZ)
		delete this->binZ;
	this->binZ = new BinningZNumber();
	this->binZ->copy(binZ);
	
	allocArray1(numEnt,binZ->getNumBins());	
	
	for (int zi=0;zi<binZ->getNumBins();zi++) {
		count=0;
		invalidCount=0;
		minZ=0;
		maxZ=0;
		for (int i=0;i<numEntries;i++) {
			minZ=this->binZ->getZ(zi);			
			maxZ=this->binZ->getZ(zi+1);			
			if (entries[i]->getSpecZ()>minZ && entries[i]->getSpecZ()<=maxZ) {
				if (entries[i]->getValid()==1)
				  count++;
				else
				  invalidCount++;
			}	
		}
		numEnt[zi]=count;
		sprintf(s,"binIndex=%d minZ=%g maxZ=%g count(valid)=%d invalid=%d",zi,minZ,maxZ,count,invalidCount);
		if (m_out)
			m_out->println(s,2);
	}	
	
	this->binZ->setNumEntries(numEnt);
	freeArray1(numEnt);
}
	
void BroadZBinning::correctTemplatesFromOrig()
{					
	// create individual red shift bins and do analysis there
	double *zmin;
	double *zmax;
	allocArray1(zmin,binZ->getNumBins());
	allocArray1(zmax,binZ->getNumBins());
	for (int i=0;i<binZ->getNumBins();i++) {
		zmin[i]=binZ->getZ(i);
		zmax[i]=binZ->getZ(i+1);
	}
	
	this->templateContainer->createTemplateBins(binZ->getNumBins(),zmin,zmax);
	
	freeArray1(zmin);
	freeArray1(zmax);		
				
	// creating red shift bins
	if (residualRedshiftBins) {
		for (int i=0;i<binZ->getNumBins();i++) {
			if (residualRedshiftBins[i])
				delete residualRedshiftBins[i];
		}
		freeArray1(residualRedshiftBins);
	}			
	allocArray1(residualRedshiftBins,binZ->getNumBins());	
	
	for (int i=0;i<binZ->getNumBins();i++) {
					
		// now fill each redshift bin
		residualRedshiftBins[i] = new ResidualRedshiftBin(numFilters, filters, binSpecZ, templateContainer, i,binZ->getZ(i),binZ->getZ(i+1));
		residualRedshiftBins[i]->setIterationID(iterationID);
		residualRedshiftBins[i]->set(m_out);
		residualRedshiftBins[i]->setOutputpath(outputpath);
		residualRedshiftBins[i]->setSigma(sigma);
		residualRedshiftBins[i]->setRho(rho);
		residualRedshiftBins[i]->setigAbsorption(igAbsorption);
		residualRedshiftBins[i]->setPunishGradient(punishGradient);
		residualRedshiftBins[i]->setParameterModes(rescaleWithMesh,rescaleWithBands,rescaleWithFlux);
		residualRedshiftBins[i]->setRhoSigmaLoad(loadRhoSigma,rhoSigmaLoadBase);
		residualRedshiftBins[i]->setRhoSigmaSave(saveRhoSigma,rhoSigmaSaveBase);
		residualRedshiftBins[i]->setRhoSigmaFiles(rhoSigmaFiles);
		residualRedshiftBins[i]->setEntries(numEntries,entries);				
		residualRedshiftBins[i]->correct();		
	}			
}

void BroadZBinning::correctTemplates()
{											
	// creating red shift bins
	if (residualRedshiftBins) {
		for (int i=0;i<binZ->getNumBins();i++) {
			if (residualRedshiftBins[i])
				delete residualRedshiftBins[i];
		}
		freeArray1(residualRedshiftBins);
	}			
	allocArray1(residualRedshiftBins,binZ->getNumBins());	
	
	for (int i=0;i<binZ->getNumBins();i++) {
					
		// now fill each redshift bin
		residualRedshiftBins[i] = new ResidualRedshiftBin(numFilters, filters, binSpecZ, templateContainer,i,binZ->getZ(i),binZ->getZ(i+1));
		residualRedshiftBins[i]->setIterationID(iterationID);
		residualRedshiftBins[i]->set(m_out);
		residualRedshiftBins[i]->setOutputpath(outputpath);
		residualRedshiftBins[i]->setSigma(sigma);
		residualRedshiftBins[i]->setRho(rho);
		residualRedshiftBins[i]->setigAbsorption(igAbsorption);
		residualRedshiftBins[i]->setPunishGradient(punishGradient);
		residualRedshiftBins[i]->setParameterModes(rescaleWithMesh,rescaleWithBands,rescaleWithFlux);	
		residualRedshiftBins[i]->setRhoSigmaLoad(loadRhoSigma,rhoSigmaLoadBase);
		residualRedshiftBins[i]->setRhoSigmaSave(saveRhoSigma,rhoSigmaSaveBase);
		residualRedshiftBins[i]->setRhoSigmaFiles(rhoSigmaFiles);
		residualRedshiftBins[i]->setEntries(numEntries,entries);		
		residualRedshiftBins[i]->correct();		
	}			
}

void BroadZBinning::set(Likelihood *like)
{
	// allocate fitting entries	
	if (entries) {		
		for (int i=0;i<numEntries;i++) {	
			if (entries[i])
				delete entries[i];
		}
		freeArray1(entries);
	}	
	numEntries=like->getNumEntries();  
	allocArray1(entries,numEntries);		
	
	// do likelihood fit	
	int bestT=0;
	double bestZ=0;
	double template_norm=0;
	double chi2=0;
	double chi2Tot=0;
	int valid=0;
	int numUsedFilters;
	int *flux_valid=NULL;
	double *flux_entries=NULL;
	double *flux_errors=NULL;
	int failed;
	char s[MAX_LENGTH];
				
	// do fitting and create fitting entries
	for (int j=0;j<numEntries;j++) {
		if ((j+1)%1000==0) {
			if ((j+1)%10000==0) 
				sprintf(s,"%d",j+1);
			else
				sprintf(s,".");
			if (m_out)
				m_out->print(s,1);
		}							
						
		failed=like->calcLikelihood(j);					
		
		if (failed==0) {			
			valid=1;
			
			bestZ=like->getBinZ()->getZ(like->getBestZi());
			
			// want the respective original(!) type of the best fitted template
			// (important if best fitted template is a corrected template),
			// i.e. corrected templates are just here to help deciding which original 
			// template fits given entry best.
			bestT=templateContainer->typeOfTemplate(like->getBestT());			
			
			// the following uses the best template. However, we
			// need the values from the corresponding "original" template
			//template_norm=like->getBestTemplateNorm();
			
			if (like->getRightZMode()) {			
			  template_norm=like->getTemplateNormZ()[bestT];						
			  chi2=like->getChi2Z()[bestT];
			} else {
			  template_norm=like->getTemplateNorm()[bestT][like->getBestZi()];						
			  chi2=like->getChi2()[bestT][like->getBestZi()];				
			}
			
			chi2Tot=like->getBest_Chi2(); // can be different from chi2 if best fitting template is a corrected template
			
			flux_valid=like->getActive();
			flux_entries=like->getFluxEntries();
			flux_errors=like->getFluxErrors();
			numUsedFilters=like->getNumUsedFilters();
		} else {;	
			valid=0;
			bestZ=like->getSpecZ();
			bestT=-1;
			template_norm=-1;
			chi2=-1;
			chi2Tot=-1;
			numUsedFilters=-1;
		}
				
		entries[j] = new FittedEntry(bestZ, bestT, template_norm, chi2, chi2Tot, valid, numUsedFilters, 
		                             this->numFilters, flux_entries, flux_errors, flux_valid);						 
	}		
	
	// now go over the entries again and set all entries invalid which have a chi2 (for the number of filters used)
	// larger than that given by the user percentile	
	std::vector< std::vector<double> > chi2Values(numFilters+1,std::vector<double>(0));
	for (int j=0;j<numEntries;j++) {
	  if (entries[j]->getValid()==1) // i.e. if not failed
	    chi2Values[entries[j]->getNumUsedFilters()].push_back(entries[j]->getChi2Tot());	
	}
	std::vector<double> percUserFilter(numFilters+1);
	for (int i=0;i<=numFilters;i++) {
	  if (chi2Values[i].size()>0) {	  	
	  	sort(chi2Values[i].begin(),chi2Values[i].end());	  		  
	  	percUserFilter[i] = *(chi2Values[i].begin()+int(chi2Values[i].size()*this->percCut));
	  }	  
	}
	for (int j=0;j<numEntries;j++) {		
	  if (entries[j]->getValid()!=1) 
	    continue;		
	  if (entries[j]->getChi2Tot() > percUserFilter[entries[j]->getNumUsedFilters()]) {      
	    entries[j]->setValid(0);
	    continue;
	  }
	  if (entries[j]->getNumUsedFilters()>3 && (entries[j]->getChi2Tot()>chi2ReducedCut*(entries[j]->getNumUsedFilters()-3)) ) {
	    entries[j]->setValid(0);	   
	  }
	}	
}

void BroadZBinning::load_sigma2_from_file(std::string filename,double *sigma2,int length) {
  ConfFile *rhoSigmaFile; 
  char*** defEntries;
  int*	defEntriesNr;
  int 	defEntryLines;
  char s[MAX_LENGTH];
  
  rhoSigmaFile = new ConfFile(filename.c_str());    
  rhoSigmaFile->setOutputHandler(this->m_out);	  
  rhoSigmaFile->read();	
  //rhoSigmaFile->printTotalStatus();
  rhoSigmaFile->getEntries(defEntries,defEntriesNr,defEntryLines);
  for (int i=0;i<defEntryLines;i++) {
  	if (i>=length || defEntriesNr[i]<2) {
  	  sprintf(s,"Error in file %s",filename.c_str());
  	  EXIT(s);
  	}
    sigma2[i]=atof(defEntries[i][0]);
  }
}

void BroadZBinning::load_rho2_from_file(std::string filename,double *rho2,int length) {  
  ConfFile *rhoSigmaFile; 
  char*** defEntries;
  int*	defEntriesNr;
  int 	defEntryLines;
  char s[MAX_LENGTH];
  
  rhoSigmaFile = new ConfFile(filename.c_str());    
  rhoSigmaFile->setOutputHandler(this->m_out);	  
  rhoSigmaFile->read();	
  //rhoSigmaFile->printTotalStatus();
  rhoSigmaFile->getEntries(defEntries,defEntriesNr,defEntryLines);
  for (int i=0;i<defEntryLines;i++) {
  	if (i>=length || defEntriesNr[i]<2) {
  	  sprintf(s,"Error in file %s",filename.c_str());
  	  EXIT(s);
  	}
    rho2[i]=atof(defEntries[i][1]);
  }
}

ResidualRedshiftBin::~ResidualRedshiftBin() {

	if (residualRedshiftTypeBins) {
		for (int i=0;i<numRedshiftTypeBins;i++) {
			if (residualRedshiftTypeBins[i]) {
				delete residualRedshiftTypeBins[i];
				residualRedshiftTypeBins[i]=NULL;
			}
		}
		freeArray1(residualRedshiftTypeBins);
	}					
};

void ResidualRedshiftBin::setEntries(int numEntries, FittedEntry **entries)
{
	int count;
	char s[MAX_LENGTH];
	
	// first compute number of entries in the red shift bin	
	
	count=0;
	for (int i=0;i<numEntries;i++) {								
		if (entries[i]->getValid() && entries[i]->getSpecZ()>=this->minZ && entries[i]->getSpecZ()<this->maxZ) 			
			count++;			
	}	
	this->numEntries=count;
		
	freeArray1(this->entries);
	allocArray1(this->entries,this->numEntries);
	
	// now grep the right entries
	count=0;
	for (int i=0;i<numEntries;i++) {	
		if (entries[i]->getValid() && entries[i]->getSpecZ()>=this->minZ && entries[i]->getSpecZ()<this->maxZ)
			this->entries[count++] = new FittedEntry(entries[i]);
	}	
	sprintf(s,"** Redshift Bin created: minZ=%g maxZ=%g numEntries=%d",this->minZ,this->maxZ,this->numEntries);
	if (m_out)
		m_out->println(s,1);
}

void ResidualRedshiftBin::correct() 
{				
	// create redshift-type bins
	if (residualRedshiftTypeBins) {
		for (int i=0;i<numRedshiftTypeBins;i++) {
			if (residualRedshiftTypeBins[i])
				delete residualRedshiftTypeBins[i];
		}
		freeArray1(residualRedshiftTypeBins);
	}				
		
	this->numRedshiftTypeBins=templateContainer->getTemplateBins()[binID]->getNumBasicTemplates();
	allocArray1(residualRedshiftTypeBins,numRedshiftTypeBins);
	
	// separate entries according to their types 
	for (int t=0;t<numRedshiftTypeBins;t++) {			
		
		residualRedshiftTypeBins[t] = new ResidualRedshiftTypeBin(numFilters, filters, binSpecZ, templateContainer, binID, minZ, maxZ, t);
		residualRedshiftTypeBins[t]->setIterationID(iterationID);
		residualRedshiftTypeBins[t]->set(m_out);
		residualRedshiftTypeBins[t]->setOutputpath(outputpath);
		residualRedshiftTypeBins[t]->setSigma(sigma);
		residualRedshiftTypeBins[t]->setRho(rho);
		residualRedshiftTypeBins[t]->setigAbsorption(igAbsorption);
		residualRedshiftTypeBins[t]->setPunishGradient(punishGradient);
		residualRedshiftTypeBins[t]->setParameterModes(rescaleWithMesh,rescaleWithBands,rescaleWithFlux);
		residualRedshiftTypeBins[t]->setRhoSigmaLoad(loadRhoSigma,rhoSigmaLoadBase);
		residualRedshiftTypeBins[t]->setRhoSigmaSave(saveRhoSigma,rhoSigmaSaveBase);
		residualRedshiftTypeBins[t]->setRhoSigmaFiles(rhoSigmaFiles);		
		residualRedshiftTypeBins[t]->setEntries(numEntries, entries);	
		residualRedshiftTypeBins[t]->saveStats();
		residualRedshiftTypeBins[t]->correct();
	}
	
	// update log-interpolated templates
	templateContainer->resetLogInterTemplates(binID+1);	

	// update lin-interpolated templates
	templateContainer->resetLinInterTemplates(binID+1);	
}

void ResidualRedshiftTypeBin::setEntries(int numEntries, FittedEntry **entries)
{
	int count;
	char s[MAX_LENGTH];
	
	if (! templateContainer) {
		cerr << "templateContainer not set in ResidualRedshiftTypeBin" << endl;
		exit(1);
	}		
	
	// first determine number of entries in type bin	
	count=0;
	for (int i=0;i<numEntries;i++) {								
		if (templateContainer->getNearestBasicTemplateCode(entries[i]->getBestT())==basicTypeID)
			count++;			
	}	
	this->numEntries=count;	
	
	freeArray1(this->entries);
	allocArray1(this->entries,this->numEntries);
	
	count=0;		
	for (int i=0;i<numEntries;i++) {
		if (templateContainer->getNearestBasicTemplateCode(entries[i]->getBestT())==basicTypeID)
			this->entries[count++] = new FittedEntry(entries[i]);			
	}
	
	sprintf(s,"  Redshift Type Bin created for basic type=%d numEntries=%d",basicTypeID,this->numEntries);
	if (m_out)
		m_out->println(s,1);	
}

void ResidualRedshiftTypeBin::print() {	
	for (int i=0;i<numEntries;i++) {		
		entries[i]->print();
		cout << endl;
	}			
}	

void ResidualRedshiftTypeBin::saveStats() {
	std::ofstream ofile;
	char s[512];
	sprintf(s,"%s/resEntries_it%d_zBin%d_typeBin%d.dat",outputpath.c_str(),iterationID,binID,basicTypeID);
	ofile.open(s);	
	ofile << "# Template Improvement - Results for individual catalog entries" << std::endl;
	ofile << "# The columns with * refer to the values obtained from the 'unmodified/original' template which" << std::endl;
	ofile << "# corresponds to but might differ from the actual best fitting template (possibly an improved template)" << std::endl;
	ofile << "# specz bestType* templateNorm* chi2* chi2 validity usedFilters" << std::endl;
	for (int i=0;i<numEntries;i++) 
	  entries[i]->appendToFile(ofile);
	ofile.close();
	ofile.clear();
}

void ResidualRedshiftTypeBin::correct()
{	
	char s[MAX_LENGTH];
	
	int meshLength=this->filters[0]->getMeshedValues()->getlength();
	Template **basicTemplates=this->templateContainer->getTemplateBins()[this->binID]->getBasicTemplates();
	int numBasicTemplates=this->templateContainer->getTemplateBins()[this->binID]->getNumBasicTemplates();
	
	if (numEntries<1)
		return;						
	
	// ----------------------- average templates ------------------------------------------------ //
	
	// create averaged version of all relevant basic templates
	// this is done because otherwise the integral is not approximated good enough
	// first do it for the basic template mit basicTypeID	
	
	// calculate ft[l_i]=(f_l^e(l_i+1)+f_l^e(l_i))/2
	xyData *basic_temp_meshedValues=basicTemplates[this->basicTypeID]->getMeshedValues();	
	double *basic_temp_averaged;
	allocArray1(basic_temp_averaged,meshLength-1);
	for (int l=0;l<meshLength-1;l++)
		basic_temp_averaged[l]=(basic_temp_meshedValues->getY()[l]+basic_temp_meshedValues->getY()[l+1])/2.;
		
	// for later		
	double *basic_temp_averaged_corrected;	
	allocArray1(basic_temp_averaged_corrected,meshLength-1);
		
	// now determine the possible neighbouring basic templates (if I use interpolation)
	int basicLowID,basicHighID;
	double *basic_low_temp_averaged=NULL;
	double *basic_high_temp_averaged=NULL;		
	basicLowID=basicTypeID-1;	
	basicHighID=basicTypeID+1;
	if (basicHighID>=numBasicTemplates)
		basicHighID=-1;
	
	if (basicLowID>=0) {
		xyData *basic_low_temp_meshedValues=this->templateContainer->getBasicTemplates()[basicLowID]->getMeshedValues();
					
		allocArray1(basic_low_temp_averaged,meshLength-1);
		for (int l=0;l<meshLength-1;l++)
			basic_low_temp_averaged[l]=(basic_low_temp_meshedValues->getY()[l]+basic_low_temp_meshedValues->getY()[l+1])/2.;				
	}
	if (basicHighID>=0) {
		xyData *basic_high_temp_meshedValues=this->templateContainer->getBasicTemplates()[basicHighID]->getMeshedValues();		
		
		allocArray1(basic_high_temp_averaged,meshLength-1);
		for (int l=0;l<meshLength-1;l++)
			basic_high_temp_averaged[l]=(basic_high_temp_meshedValues->getY()[l]+basic_high_temp_meshedValues->getY()[l+1])/2.;				
	}
	
	// ----------------------- set sigma & rho ------------------------------------------------ //
	
	// setting sigma2[k] (external parameter)
	// avoids to strong deviation from original template		
	double *sigma2;
	double *rho2=NULL;
	allocArray1(sigma2,meshLength-1);
	
	if (! loadRhoSigma) { // not loaded from file, hence use values given by --sigma, --rho or the default values
						
	  if (rescaleWithMesh) { 									  
	    for (int l=0;l<meshLength-1;l++) {		  
		  sigma2[l]=(this->sigma)*(this->sigma)/(basic_temp_meshedValues->getX()[l+1]-basic_temp_meshedValues->getX()[l]);
		  if (sigma2[l]<1E-6)
		    sigma2[l]=1E-6;
	    }
	  } else {						  // this is the conventional definition as used in (Feldmann et al. 2006)
	    for (int l=0;l<meshLength-1;l++)
		  sigma2[l]=(this->sigma)*(this->sigma);				
	  }
		
	  if (rescaleWithFlux==1) { 
	    for (int l=0;l<meshLength-1;l++) {		  
		  sigma2[l]*=basic_temp_averaged[l];
		  if (sigma2[l]<1E-6)
		    sigma2[l]=1E-6;
	    }
	  } else if (rescaleWithFlux==2) {
	    for (int l=0;l<meshLength-1;l++) {		  
		  sigma2[l]*=basic_temp_averaged[l]*basic_temp_averaged[l];
		  if (sigma2[l]<1E-6)
		    sigma2[l]=1E-6;
	    }  	  
	  }
	
	  // setting rho2[k] (external parameter)
	  // avoid to strong gradients in the improved template to develop	 
	  if (this->punishGradient) {
		allocArray1(rho2,meshLength-1);
		
		if (rescaleWithMesh) {
		  for (int l=0;l<meshLength-1;l++) {
			rho2[l]=(this->rho)*(this->rho)*(basic_temp_meshedValues->getX()[l+1]-basic_temp_meshedValues->getX()[l]);
			if (rho2[l]<1E-6)
			  rho2[l]=1E-6;
		  }
		} else { 					// this is the conventional definition as used in (Feldmann et al. 2006)
		  for (int l=0;l<meshLength-1;l++)
			rho2[l]=(this->rho)*(this->rho);				
		}
		
		if (rescaleWithFlux==1) {
		  for (int l=0;l<meshLength-1;l++) {
			rho2[l]*=basic_temp_averaged[l];
			if (rho2[l]<1E-6)
			  rho2[l]=1E-6;
		  }
		} else if (rescaleWithFlux==2) {
		  for (int l=0;l<meshLength-1;l++) {
			rho2[l]*=basic_temp_averaged[l]*basic_temp_averaged[l];
			if (rho2[l]<1E-6)
			  rho2[l]=1E-6;
		  }	
		}			
	  }
	  
	}  else { // load from file			
		// check whether there is a specific file given for this template
		if (strcmp(rhoSigmaFiles[basicTypeID],"")!=0) { // such a file is given
			sprintf(s,"%s/%s",this->outputpath.c_str(),rhoSigmaFiles[basicTypeID]);
		} else {
		    sprintf(s,"%s/%s",this->outputpath.c_str(),rhoSigmaLoadBase.c_str());			
		}		
		load_sigma2_from_file(s,sigma2,meshLength-1);
		if (punishGradient) {
		  allocArray1(rho2,meshLength-1);
		  load_rho2_from_file(s,rho2,meshLength-1);
		}		  		
	}
			  
	if (saveRhoSigma && iterationID==0 && binID==0) {
	  ofstream ofile;
	  sprintf(s,"%s/%s_typeBin%d.dat",this->outputpath.c_str(),rhoSigmaSaveBase.c_str(),basicTypeID);	  	  	 
	  ofile.open(s);
          ofile << "# BasicTemplateID = " << basicTypeID << std::endl;
	  ofile << "# sigma^2 rho^2 index wavelength" << std::endl;	  
	  if (punishGradient) {
	    for (int l=0;l<meshLength-1;l++) {	  
	      ofile <<  sigma2[l] << " " << rho2[l] << " "
	              << l << " " << (basic_temp_meshedValues->getX()[l]+basic_temp_meshedValues->getX()[l+1])/2. << std::endl; 	              
	    }
	  } else {	  	
	  	for (int l=0;l<meshLength-1;l++) {	  
	      ofile <<  sigma2[l] << " " << "inf" << " "
	              << l << " " << (basic_temp_meshedValues->getX()[l]+basic_temp_meshedValues->getX()[l+1])/2. << std::endl; 	              
	    }
	  }
	  ofile.close();
	}			
	
	// -----------------------compute input for linear solver ------------------------------------------------ //
	
	// calculating matrix elements M[l][k], nu[l]
	double *M, *nu,*xsi;
	double chi2_orig=0;	
	int N1 = meshLength-1;
	int N2 = 1;
	int *pivot, ok;
	callocArray1(M,N1*N1);
	callocArray1(nu,N1);
	allocArray1(pivot,N1);
	
	IGAbsorption *iga = new IGAbsorption();	// create IG-Absorption object
	iga->setAbsorptionMode(this->igAbsorption);
	iga->setLambda(basic_temp_meshedValues);
	
	xyData *f_meshedValues;
	xyData *absorption=new xyData();
	
	double* fNorm;
        allocArray1(fNorm,numFilters);
        for(int f=0; f<numFilters; f++) {
          f_meshedValues=this->filters[f]->getMeshedValues();   
          fNorm[f] = filters[f]->getFrequencyNormalization(f_meshedValues);
        }
	
	for (int i=0;i<numEntries;i++) {
		
		double *fvalue=entries[i]->getFlux();
	     	double *fe=entries[i]->getFluxErrors();
		int *fvalid=entries[i]->getFluxValid();
		
		double shift=(1+entries[i]->getSpecZ());	
		int origType=entries[i]->getBestT();
		
		int numValidFilters=0;
		if (rescaleWithBands) { // determine number of valid filters
		  for (int f=0;f<numFilters;f++) {
		    if (! fvalid[f])
		       continue;	
		     numValidFilters++;
		  }
		} 
		
		for (int f=0;f<numFilters;f++) {
		
			if (! fvalid[f]) // invalid entries correspond to delta_n = inf -> drop out of the sum
				continue;	
			
			// compute c[lambda] & g (see Feldmann et al. 2006)
			
			f_meshedValues=this->filters[f]->getMeshedValues();			
			absorption->copy(f_meshedValues,0,meshLength-1); // y value is set in getOpticalDepth
			iga->getOpticalDepth(absorption,entries[i]->getSpecZ());
			
			// Note: I integrate \int f(x) dx in logspace i.e. \int f(x) x dln(x)
			// The approximation is for the former: \int f(x) dx ~ sum(k) (f(x_{k+1})+f(x_k))/2 * (x_{k+1}-x_k)
			//                  and for the latter: \int f(x)xdln(x) ~ (x_{k+1}*f(x_{k+1})+x_k*f(x_k))/2 * (ln(x_{k+1}-ln(x_k))
			
			double *c;
			allocArray1(c,N1);
			double g = 0;
				
			for (int l=0;l<meshLength-1;l++) {										
				double lambda1=f_meshedValues->getX()[l];
				double lambda2=f_meshedValues->getX()[l+1];
				double abs1=absorption->getY()[l];
				double abs2=absorption->getY()[l+1];
				
				double aux=shift*shift*entries[i]->getTemplateNorm()/fNorm[f]*
						   (lambda2*lambda2*f_meshedValues->calcYValue(lambda2*shift)+
						    lambda1*lambda1*f_meshedValues->calcYValue(lambda1*shift))/2.*
						    (log(f_meshedValues->getX()[l+1])-log(f_meshedValues->getX()[l]))*
						    (abs1+abs2)/2.;
						    
				if (templateContainer->getTemplateKind(origType)==0) { 		// basic template
					c[l] = aux;
					g  += basic_temp_averaged[l]*aux;
				} else if (templateContainer->getTemplateKind(origType)==1) { 	// log interpolated template	
					
					// get interpolation weight
					double weight;				
					templateContainer->getBasicTemplateWeight(origType,weight);		
		
					// template interpolated between basicTypeID template and base2 template
					// obtain base2 number from origType number
					int base2=templateContainer->getSecondNearestBasicTemplateCode(origType);
					
					if (basic_temp_averaged[l]<=1E-20) {
						c[l] = 0.;
					} else if (base2==basicLowID) {
					        c[l] = pow(basic_low_temp_averaged[l],1-weight) * pow(basic_temp_averaged[l],weight-1) * weight * aux;  
						g += pow(basic_low_temp_averaged[l],1-weight) * pow(basic_temp_averaged[l],weight) * aux;	
					} else {
						c[l] = pow(basic_high_temp_averaged[l],1-weight) * pow(basic_temp_averaged[l],weight-1) * weight * aux;	
					        g += pow(basic_high_temp_averaged[l],1-weight) * pow(basic_temp_averaged[l],weight) * aux;
					}
						
				} else {							// lin interpolated template
		
		        		// get interpolation weight	
					double weight;
					templateContainer->getBasicTemplateWeight(origType,weight);		
						
					// template interpolated between basicTypeID template and base2 template
					// obtain base2 number from origType number
					int base2=templateContainer->getSecondNearestBasicTemplateCode(origType);
							
			    		if (basic_temp_averaged[l]<=1E-20) {
			      			c[l] = 0.;
			    		} else if (base2==basicLowID) {
			      			c[l] = weight * aux;
						g += (basic_low_temp_averaged[l]*(1-weight) + basic_temp_averaged[l]*weight)*aux;
					} else {
					        c[l] = weight * aux;
					        g += (basic_high_temp_averaged[l]*(1-weight) + basic_temp_averaged[l]*weight)*aux;
					}
			  	}
		        }
			g = fvalue[f] - g;
			
			// fill M

                        for (int k=0;k<meshLength-1;k++) {
		           for (int l=0;l<meshLength-1;l++) {
                     
		              double aux=c[k]*c[l]/(fe[f]*fe[f]);
			      
			      if (rescaleWithBands)
				 aux/=numValidFilters;
			      
			      M[k*(meshLength-1)+l]+=aux;
			   }
			 }
			 
			 // fill nu
			 
			 for (int l=0;l<meshLength-1;l++) {
			       
			       double aux=c[l]*g/(fe[f]*fe[f]);
			       
			       if (rescaleWithBands)
				  aux/=numValidFilters;
				  
			       nu[l]+=aux;	  
			 }
			 
			 // chi2
			 
			 for (int l=0;l<meshLength-1;l++) {
			       
			       double aux=g*g/(fe[f]*fe[f]);
			       
			       if (rescaleWithBands)
				  aux/=numValidFilters;
				  
			       chi2_orig+=aux;	  
			 }
			 
			 
			 free(c);
			 
		}
	}
	
	for (int k=0;k<meshLength-1;k++) {
		for (int l=0;l<meshLength-1;l++) {
		     M[k*(meshLength-1)+l]/=numEntries;
		}
		nu[k]/=numEntries;
	}
	chi2_orig/=numEntries;
	
	for (int l=0;l<meshLength-1;l++) {
	        for (int k=0;k<meshLength-1;k++) {				
			
			double aux=0.;
			if (this->punishGradient) {				
				if (l==k) {					
				    if (k>0) {
					aux=1./sigma2[k]+1./rho2[k]+1./rho2[k-1];
				    } else {
				    	aux=1./sigma2[k]+1./rho2[k];
				    }
				} else if (l<k && l==k-1) {
					aux=-1./rho2[k-1];					
				} else if (l>k && l==k+1) {				
					aux=-1./rho2[k];
				} 	
			} else {
				if (l==k)
				  aux=1./sigma2[k];
			}
			
			M[k*(meshLength-1)+l]+=aux;  /* matrix is transposed because LAPACK uses FORTRAN ordering */
		}
	}	
	
	freeArray1(fNorm);
	delete absorption;
	delete iga;
	
	sprintf(s,"Before current correction step : chi^2 = %g", chi2_orig);
	this->m_out->println(s,3);							
	
	// ----------- solve linear equation ------------------------------------------------------- //
	
	dgesv_(&N1, &N2, M, &N1, pivot, nu, &N1, &ok); 
	xsi = nu;
	freeArray1(M);
	freeArray1(pivot);

	if (ok>0) {
	  sprintf(s,"Template Optimization [dgesv]: No solution could be computed");
	  this->m_out->println(s,2);
	  WARNING("You should not use the result from this iteration");
	} else if (ok < 0) {
	  sprintf(s,"Template Optimization [dgesv]: Argument %d has illegal value",-ok);
	  this->m_out->println(s,2);
	  WARNING("You should not use the result from this iteration");
	}
	
	// ----------- correct template & clean up ------------------------------------------------ //
	
	// first projecting xsi back to original mesh
	// then adding it
	
	double *x_average;
	double *xsi_meshed;
	double *temp_y;
	double *temp_x=basic_temp_meshedValues->getX();
	
	allocArray1(x_average,meshLength-1);	
	allocArray1(xsi_meshed,meshLength-1);
	allocArray1(temp_y,meshLength);
	
	for (int l=0;l<meshLength-1;l++) {
		x_average[l]=(temp_x[l]+temp_x[l+1])/2;
		xsi_meshed[l]=xsi[l];
	}		
	
	freeArray1(xsi); nu=NULL;
	
	LinearApprox *linApprox = new LinearApprox(meshLength-1, x_average, xsi_meshed);	
	linApprox->getValues(meshLength,temp_x,temp_y);	
	delete linApprox;	
	
	freeArray1(x_average);
	freeArray1(xsi_meshed);
		
	// now change the flux values (take corrected values)	
	int setToZero=0;
	for (int l=0;l<meshLength;l++) {
		temp_y[l]+=basic_temp_meshedValues->getY()[l];	
		if (temp_y[l]<0) {
			if (l>0 || l<meshLength-1)
				setToZero=1;
			temp_y[l]=0;
		}
	}
	if (setToZero) {
		WARNING("An optimized template contains negative flux values -> Set to zero.");
	}		
	
	xyData *temp_meshedValues = new xyData();	
	temp_meshedValues->set(meshLength,temp_x,temp_y);
	basicTemplates[this->basicTypeID]->setValues(temp_meshedValues);	
	basicTemplates[this->basicTypeID]->setMeshedValues(temp_meshedValues);				
				
	freeArray1(temp_y);						
	delete temp_meshedValues;
	
	#if 0	
	print();		
	#endif
			
	freeArray1(sigma2);	
	if (this->punishGradient) {
		freeArray1(rho2);	
	}
	freeArray1(basic_temp_averaged);
	freeArray1(basic_temp_averaged_corrected);
	if (basicLowID>=0) {		
		freeArray1(basic_low_temp_averaged);
	}
	if (basicHighID>=0) {				
		freeArray1(basic_high_temp_averaged);
	}
}

double ResidualRedshiftTypeBin::calc_chi2(int gridLength, int numFilters, int numEntries, 
				 double *sk_new, double *sk_old, double *sigma2, 
				 double **flux_new, double **flux_meas, double **flux_error)
{
	double chi2=0.;
	double aux;
	
	aux=0;
	for (int i=0;i<gridLength;i++) 
		aux+=(sk_new[i]-sk_old[i])*(sk_new[i]-sk_old[i])/sigma2[i];
	chi2+=aux;
	
	aux=0;
	for (int i=0;i<numEntries;i++) {
		for (int f=0;f<numFilters;f++) {
			aux+=(flux_new[i][f]-flux_meas[i][f])*(flux_new[i][f]-flux_meas[i][f]) / (flux_error[i][f]*flux_error[i][f]);
		}
	}
	aux/=numEntries;
	chi2+=aux;
		
	return chi2;					
}

double ResidualRedshiftTypeBin::calc_chi2(int gridLength, int numFilters, int numEntries, 
				 double *sk_new, double *sk_old, double *sigma2, double *rho2,
				 double **flux_new, double **flux_meas, double **flux_error)
{
	double chi2=0.;
	double aux;
	
	aux=0;
	for (int i=0;i<gridLength;i++) 
		aux+=(sk_new[i]-sk_old[i])*(sk_new[i]-sk_old[i])/sigma2[i];
	chi2+=aux;
	
	aux=0;
	for (int i=0;i<gridLength-1;i++) 
		aux+=(sk_new[i+1]-sk_new[i]-sk_old[i+1]+sk_old[i])*(sk_new[i+1]-sk_new[i]-sk_old[i+1]+sk_old[i])
			/rho2[i];
	chi2+=aux;
	
	aux=0;
	for (int i=0;i<numEntries;i++) {
		for (int f=0;f<numFilters;f++) {
			aux+=(flux_new[i][f]-flux_meas[i][f])*(flux_new[i][f]-flux_meas[i][f]) / (flux_error[i][f]*flux_error[i][f]);
		}
	}
	aux/=numEntries;
	chi2+=aux;
		
	return chi2;					
}
