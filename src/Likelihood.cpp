#include "Likelihood.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

Likelihood::Likelihood()
{
	binZ=NULL;
	flux=NULL;
	fluxRestriction=NULL;
	likelihood=NULL;
	likelihoodMarginT=NULL;
	likelihoodMarginZ=NULL;
	likelihoodZ=NULL;	
	template_norm=NULL;
	template_normZ=NULL;
	templates=NULL;
	catalog=NULL;
	chi2=NULL;
	chi2Z=NULL;	
	minT_chi2=NULL;
	minz_chi2=NULL;
	f_OT=NULL;
	f_TT=NULL;
	flux_entries=NULL;
	flux_errors=NULL;
	flux_invalid=NULL;
	residuals=NULL;
	MBMode=0;	
	m_out=NULL;
	failed=0;
	rightZMode=0;
	chi2Add=0.;
	chi2Mult=1.;
	specZ=-1;
	haveSpecZ=0;
	active=NULL;
	templateContainer=NULL;
	templateModus=0;
	allocated=0;
	allocatedZ=0;		
	templateBFluxNormalized=NULL;
	filterFluxActive=NULL;
	
#ifdef MEASTIME	 
	rTime1=rTime2=rTime3=rTime4=rTime5=rTime6=rTime7=rTime8=rTime9=rTime10=rTime11=rTime12=0;	
#endif	
}

Likelihood::~Likelihood()
{
	if (binZ)
		delete binZ;	
	freeArray1(templates);
	if (allocated==1)
		freeArrays();
	if (allocatedZ==1)
		freeArraysZ();
	if (templateBFluxNormalized) 
	  freeArray1(templateBFluxNormalized);
	if (filterFluxActive)
	  freeArray1(filterFluxActive);
}

void Likelihood::resize()
{	
	if (binZ)
		delete binZ;
	binZ=NULL;
	flux=NULL;
	fluxRestriction=NULL;		
	MBMode=0;
	chi2Add=0.;
	chi2Mult=1.;
	specZ=-1;
	if (allocated==1)
		freeArrays();
	if (allocatedZ==1)
		freeArraysZ();
	freeArray1(templates);
	if (templateBFluxNormalized)
	  freeArray1(templateBFluxNormalized);	
	if (filterFluxActive)
	  freeArray1(filterFluxActive); 	
}

#ifdef PARRAY
void Likelihood::set(double ***flux, const BinningZ *binZ, FluxRestriction * fluxRestriction, 
					 Catalog *catalog, TemplateContainer *templateContainer, int templateModus)
#else
void Likelihood::set(double *flux, const BinningZ *binZ, FluxRestriction * fluxRestriction, 
					 Catalog *catalog, TemplateContainer *templateContainer, int templateModus)
#endif					 
{
	char s[512];
	resize();
	this->binZ=new BinningZ();
	this->binZ->copy(binZ);
	this->flux=flux;
	this->fluxRestriction=fluxRestriction;
	this->templateModus=templateModus;
	this->templateContainer=templateContainer;
	this->catalog=catalog;
	this->numEntries=catalog->getLineNumber();	
	this->numFilters=catalog->getnumFilterBands();		
	
	if (templateModus==0) { // use only original (not corrected) templates		
		numTemplates=templateContainer->getNumOrigTemplates();			
		allocArray1(templates,numTemplates);
		for (int i=0;i<numTemplates;i++)
			templates[i]=templateContainer->getOrigTemplate(i);
		if (m_out) {
		  sprintf(s,"Using %d templates (orig)",templateContainer->getNumOrigTemplates());
		  m_out->println(s,2);
	    }	
	} else {				// use original + corrected templates
		numTemplates=templateContainer->getNumAllTemplates();
		allocArray1(templates,numTemplates);
		for (int i=0;i<numTemplates;i++) 
			templates[i]=templateContainer->getAllTemplate(i);									
		if (m_out) {
		  sprintf(s,"Using %d templates (orig + improved)",templateContainer->getNumAllTemplates());
		  m_out->println(s,2);
	   }
	}
	
	// store rest-frame B-filter for each template to avoid repetitive calculations
	if (templateBFluxNormalized)
	  freeArray1(templateBFluxNormalized);	  	
	allocArray1(templateBFluxNormalized,numTemplates);
	for (int i=0;i<numTemplates;i++)
	  templateBFluxNormalized[i]=templates[i]->getBFluxNormalized();
	
	// mark all filters inactive for which no flux exists for any template-redshift pair.
	if (filterFluxActive)
	  freeArray1(filterFluxActive); 
	allocArray1(filterFluxActive,numFilters);	
	for (int f=0;f<numFilters;f++)
	  filterFluxActive[f]=1;
#ifdef PARRAY	  
	for (int t=0;t<numTemplates;t++) {
	  for (int zi=0;zi<this->binZ->getLength();zi++) {
	    double *fluxp = flux[t][zi];
	      for (int f=0;f<numFilters;f++) {
	        if (fluxp[f]<0) 			  
		   filterFluxActive[f]=0;
	      }
	   }
	}	
#else
	for (int ii=0;ii<numTemplates*(this->binZ->getLength())*numFilters;ii++) {	  
	  if (flux[ii]<0) 			  
		   filterFluxActive[ii%numFilters]=0;	   
	}
#endif	  
}

void Likelihood::allocateArraysBoth()
{
	allocArray1(flux_entries,numFilters);
	allocArray1(flux_errors,numFilters);
	allocArray1(flux_invalid,numFilters);	
	allocArray1(active,numFilters);		// whether certain filter bands are active or ignored		
	allocArray1(residuals,numFilters);
	allocArray1(residual_errors,numFilters);
}

void Likelihood::freeArraysBoth()
{
	freeArray1(flux_entries);
	freeArray1(flux_errors);
	freeArray1(flux_invalid);
	freeArray1(active);	
	freeArray1(residuals);
	freeArray1(residual_errors);
}

void Likelihood::allocateArrays()
{
	allocateArraysBoth();
	allocArray2(likelihood,numTemplates,this->binZ->getLength());
	allocArray1(likelihoodMarginZ,this->binZ->getLength());
	allocArray1(likelihoodMarginT,numTemplates);
	allocArray2(chi2,numTemplates,this->binZ->getLength());
	allocArray1(minT_chi2,this->binZ->getLength());
	allocArray1(minz_chi2,numTemplates);	
	allocArray2(template_norm,numTemplates,this->binZ->getLength());	
	allocArray2(f_OT,numTemplates,this->binZ->getLength());
	allocArray2(f_TT,numTemplates,this->binZ->getLength());		
	this->allocated=1;
}

void Likelihood::freeArrays() 
{				
	freeArraysBoth();
	freeArray2(likelihood,numTemplates);	
	freeArray1(likelihoodMarginZ);
	freeArray1(likelihoodMarginT);
	freeArray2(chi2,numTemplates);
	freeArray1(minT_chi2);
	freeArray1(minz_chi2);
	freeArray2(template_norm,numTemplates);	
	freeArray2(f_OT,numTemplates);	
	freeArray2(f_TT,numTemplates);
	this->allocated=0;				
}

void Likelihood::allocateArraysZ()
{
	allocateArraysBoth();
	allocArray1(likelihoodZ,numTemplates);
	allocArray1(chi2Z,numTemplates);	
	allocArray1(template_normZ,numTemplates);		
	this->allocatedZ=1;
}

void Likelihood::freeArraysZ()
{
	freeArraysBoth();
	freeArray1(likelihoodZ);
	freeArray1(chi2Z);
	freeArray1(template_normZ);	
	this->allocatedZ=0;
}

void Likelihood::calcErrors()
{
	double aux;			
	int foundIndex;
	int aux_index;
	double aux_integ;
	double *z=this->binZ->getZ();
	xyData * data = new xyData();		
		
	if (allocated==0)
		allocateArrays();
		
	// filling entries with maximal numbers
	for (int t=0;t<numTemplates;t++) {					
		minz_chi2[t]=DBL_MAX;	
	}
	for (int zi=0;zi<binZ->getLength();zi++) {								
		minT_chi2[zi]=DBL_MAX;
	}
		
	// calculate minimal chi^2 for each z and T lines, respectively in order to estimate errors
	for (int t=0;t<numTemplates;t++) {					
		for (int zi=0;zi<binZ->getLength();zi++) {		
			aux=chi2[t][zi];
			if (minT_chi2[zi]>aux)
				minT_chi2[zi]=aux;
			if (minz_chi2[t]>aux)
				minz_chi2[t]=aux;
		}
	}
	
	//------------ errors for redshift ----------------------------
	
	if (binZ->getLength()<1)
		EXIT("Need at least one point in redshift grid to calculate errors for redshift estimate");
	
	data->set(binZ->getLength(),z,minT_chi2);
	
	double zmin=z[0];
	double zmax=z[binZ->getLength()-1];

	//------- errors using constant chi2 boundaries	
	
	// z 1-parameter 1-sigma min		
	zmin_68=data->getXValueLeft(min_chi2+1.0,best_zi,foundIndex);
	foundIndex++;			
	// z 2-parameter 1-sigma min
	zmin2_68=data->getXValueLeft(min_chi2+2.3,foundIndex,foundIndex);			
	foundIndex++;	
	// z 1-parameter 2-sigma min
	zmin_95=data->getXValueLeft(min_chi2+4.00,foundIndex,foundIndex);
	foundIndex++;	
	// z 2-parameter 2-sigma min
	zmin2_95=data->getXValueLeft(min_chi2+6.17,foundIndex,foundIndex);
	foundIndex++;	
	
	// z 1-parameter 1-sigma max
	zmax_68=data->getXValueRight(min_chi2+1.0,best_zi,foundIndex);			
	// z 2-parameter 1-sigma max
	zmax2_68=data->getXValueRight(min_chi2+2.3,foundIndex,foundIndex);		
	// z 1-parameter 2-sigma max
	zmax_95=data->getXValueRight(min_chi2+4.00,foundIndex,foundIndex);	
	// z 2-parameter 2-sigma max
	zmax2_95=data->getXValueRight(min_chi2+6.17,foundIndex,foundIndex);													
	
	//------ using 0.68 and 0.95 percentiles of redshift as errors
	
	// over marginalized likelihood function (which is per def. normalized)
	data->set(binZ->getLength(),z,likelihoodMarginZ);	
															
	aux_index=0; // use this to speed up the percentile calculation (reuse of index)
	aux_integ=0; // use this to speed up the percentile calculation (reuse of integral)																
	
	// NB. -> don't change the order of the percentile calculation	
	int percentile=data->getPercentileLow(0.95,1.,aux_index,aux_integ);
	if (percentile==-1)
		zminmz_95=zmin;
	else
		zminmz_95=z[percentile];
	
	percentile=data->getPercentileLow(0.68,1.,aux_index,aux_integ);
	if (percentile==-1)
		zminmz_68=zmin;
	else
		zminmz_68=z[percentile];											
		
	aux_index=binZ->getLength()-1;
	aux_integ=0; 					
		
	percentile=data->getPercentileHigh(0.95,1.,aux_index,aux_integ);
	if (percentile==-1)
		zmaxmz_95=zmax;
	else
		zmaxmz_95=z[percentile];	
	
	percentile=data->getPercentileHigh(0.68,1.,aux_index,aux_integ);
	if (percentile==-1)
		zmaxmz_68=zmax;
	else
		zmaxmz_68=z[percentile];				
		
	// over likelihood dist of best (after marginalization) template type
	data->set(binZ->getLength(),z,likelihood[bestMarginT_t]);
	
	// note that likelihood[bestMarginT_t] is not normalized to 1 after integration over z
	// but the result of this integration is likelihoodT[bestMarginT_t]
	// -> have to normalize it -> give norm as parameter to percentile function
	
	aux=likelihoodMarginT[bestMarginT_t]; // normalization of likelihood
	aux_index=0; // use this to speed up the percentile calculation (reuse of index)
	aux_integ=0; // use this to speed up the percentile calculation (reuse of integral)																				
	
	percentile=data->getPercentileLow(0.95,aux,aux_index,aux_integ);
	if (percentile==-1)
		zminmt_95=zmin;
	else
		zminmt_95=z[percentile];
	
	percentile=data->getPercentileLow(0.68,aux,aux_index,aux_integ);
	if (percentile==-1)
		zminmt_68=zmin;
	else
		zminmt_68=z[percentile];													
	
	aux_index=binZ->getLength()-1;
	aux_integ=0; 								
	
	percentile=data->getPercentileHigh(0.95,aux,aux_index,aux_integ);
	if (percentile==-1)
		zmaxmt_95=zmax;
	else
		zmaxmt_95=z[percentile];		
		
	percentile=data->getPercentileHigh(0.68,aux,aux_index,aux_integ);
	if (percentile==-1)
		zmaxmt_68=zmax;
	else
		zmaxmt_68=z[percentile];	
																												
	//----------- errors for template types -----------------------	
		
	double Tmin=templateContainer->getMinTemplateCode(templateContainer->binOfTemplate(best_t));
	double Tmax=templateContainer->getMaxTemplateCode(templateContainer->binOfTemplate(best_t));
	double *templateCode;	
	int numTempBin=templateContainer->getNumOrigTemplates(templateContainer->binOfTemplate(best_t));		
	int tempBinMinIndex=templateContainer->getMinIndex(templateContainer->binOfTemplate(best_t));
	
	//------- errors using constant chi2 boundaries	
			
	allocArray1(templateCode,numTempBin);	
	for (int i=0;i<numTempBin;i++)
		templateCode[i]=i;
	data->set(numTempBin,templateCode,minz_chi2+tempBinMinIndex);		
	
	int proj_best_t=templateContainer->typeOfTemplate(best_t);		
	
	// t 1-parameter 1-sigma min	
	Tmin_68=data->getXValueLeft(min_chi2+1.0,proj_best_t,foundIndex)+Tmin;
	foundIndex++;	
	// t 2-parameter 1-sigma min	
	Tmin2_68=data->getXValueLeft(min_chi2+2.3,foundIndex,foundIndex)+Tmin;	
	foundIndex++;	
	// t 1-parameter 2-sigma min
	Tmin_95=data->getXValueLeft(min_chi2+4.0,foundIndex,foundIndex)+Tmin;
	foundIndex++;
	// t 2-parameter 2-sigma min
	Tmin2_95=data->getXValueLeft(min_chi2+6.17,foundIndex,foundIndex)+Tmin;
	foundIndex++;
	
	// t 1-parameter 1-sigma max
	Tmax_68=data->getXValueRight(min_chi2+1.0,proj_best_t,foundIndex)+Tmin;	
	if (foundIndex==-1)
		foundIndex=numTempBin-1;	
	// t 2-parameter 1-sigma max
	Tmax2_68=data->getXValueRight(min_chi2+2.3,foundIndex,foundIndex)+Tmin;
	if (foundIndex==-1)
		foundIndex=numTempBin-1;	
	// t 1-parameter 2-sigma max
	Tmax_95=data->getXValueRight(min_chi2+4.00,foundIndex,foundIndex)+Tmin;
	if (foundIndex==-1)
		foundIndex=numTempBin-1;	
	// t 2-parameter 2-sigma max
	Tmax2_95=data->getXValueRight(min_chi2+6.17,foundIndex,foundIndex)+Tmin;			
	if (foundIndex==-1)
		foundIndex=numTempBin-1;	

	//------ using 0.68 and 0.95 percentiles of template type as errors
	
	// over marginalized likelihood function (which is normalized over ALL templates, but
	// we consider only the templates within the given template bin)
	data->set(numTempBin,templateCode,likelihoodMarginT+tempBinMinIndex);
	aux=0;
	for (int i=tempBinMinIndex;i<tempBinMinIndex+numTempBin;i++) {
		aux+=likelihoodMarginT[i]; // normalization of likelihood
	}
															
	aux_index=0; // use this to speed up the percentile calculation (reuse of index)
	aux_integ=0; // use this to speed up the percentile calculation (reuse of integral)																
	
	// NB. -> don't change the order of the percentile calculation	
	percentile=data->getPercentileLow(0.95,aux,aux_index,aux_integ);
	if (percentile==-1)
		Tminmt_95=Tmin;
	else
		Tminmt_95=templateCode[percentile]+Tmin;
	
	percentile=data->getPercentileLow(0.68,aux,aux_index,aux_integ);
	if (percentile==-1)
		Tminmt_68=Tmin;
	else
		Tminmt_68=templateCode[percentile]+Tmin;											
		
	aux_index=numTempBin-1;
	aux_integ=0;		
		
	percentile=data->getPercentileHigh(0.95,aux,aux_index,aux_integ);
	if (percentile==-1)
		Tmaxmt_95=Tmax;
	else
		Tmaxmt_95=templateCode[percentile]+Tmin;	
	
	percentile=data->getPercentileHigh(0.68,aux,aux_index,aux_integ);
	if (percentile==-1)
		Tmaxmt_68=Tmax;
	else
		Tmaxmt_68=templateCode[percentile]+Tmin;;				
		
	// over likelihood dist of best (after marginalization) redshift
	double *likelihoodBestZProjT;
	allocArray1(likelihoodBestZProjT,numTempBin);
	aux=0;
	for (int i=0;i<numTempBin;i++) {
		aux+=likelihoodBestZProjT[i]=likelihood[i][bestMarginZ_zi];
	}	
	
	data->set(numTempBin,templateCode,likelihoodBestZProjT);
		
	aux_index=0; // use this to speed up the percentile calculation (reuse of index)
	aux_integ=0; // use this to speed up the percentile calculation (reuse of integral)																				
	
	percentile=data->getPercentileLow(0.95,aux,aux_index,aux_integ);
	if (percentile==-1)
		Tminmz_95=Tmin;
	else
		Tminmz_95=templateCode[percentile]+Tmin;
	
	percentile=data->getPercentileLow(0.68,aux,aux_index,aux_integ);
	if (percentile==-1)
		Tminmz_68=Tmin;
	else
		Tminmz_68=templateCode[percentile]+Tmin;													
	
	aux_index=numTempBin-1;
	aux_integ=0; 								
	
	percentile=data->getPercentileHigh(0.95,aux,aux_index,aux_integ);
	if (percentile==-1)
		Tmaxmz_95=Tmax;
	else
		Tmaxmz_95=templateCode[percentile]+Tmin;		
		
	percentile=data->getPercentileHigh(0.68,aux,aux_index,aux_integ);
	if (percentile==-1)
		Tmaxmz_68=Tmax;
	else
		Tmaxmz_68=templateCode[percentile]+Tmin;	
		  
	delete data;	
	freeArray1(templateCode);
	freeArray1(likelihoodBestZProjT);			
}			

void Likelihood::calcErrorsRightZ()
{	
	int aux_index;
	double aux_integ;						
	int foundIndex;
	xyData * data = new xyData();	
	
	if (allocated==0)
		allocateArrays();					
	
	zmin_68=zmin_95=zmax_68=zmax_95=this->binZ->getZ(this->best_zi);
	zmin2_68=zmin2_95=zmax2_68=zmax2_95=zmin_68;
	zminmz_68=zminmz_95=zmaxmz_68=zmaxmz_95=zmin_68;
	zminmt_68=zminmt_95=zmaxmt_68=zmaxmt_95=zmin_68;			
	
	double Tmin=templateContainer->getMinTemplateCode(templateContainer->binOfTemplate(best_t));
	double Tmax=templateContainer->getMaxTemplateCode(templateContainer->binOfTemplate(best_t));	
	double *templateCode;
	int numTempBin=templateContainer->getNumOrigTemplates(templateContainer->binOfTemplate(best_t));		
	int tempBinMinIndex=templateContainer->getMinIndex(templateContainer->binOfTemplate(best_t));		
	
	allocArray1(templateCode,numTempBin);
	
	for (int i=0;i<numTempBin;i++)
		templateCode[i]=i;
	data->set(numTempBin,templateCode,chi2Z+tempBinMinIndex);
	
	//------- errors using constant chi2 boundaries				
	
	int proj_best_t=templateContainer->typeOfTemplate(best_t);	
	
	// t 1-parameter 1-sigma min	
	Tmin_68=data->getXValueLeft(min_chi2+1.0,proj_best_t,foundIndex)+Tmin;
	foundIndex++;	
	// t 2-parameter 1-sigma min	
	Tmin2_68=data->getXValueLeft(min_chi2+2.3,foundIndex,foundIndex)+Tmin;	
	foundIndex++;	
	// t 1-parameter 2-sigma min
	Tmin_95=data->getXValueLeft(min_chi2+4.0,foundIndex,foundIndex)+Tmin;
	foundIndex++;
	// t 2-parameter 2-sigma min
	Tmin2_95=data->getXValueLeft(min_chi2+6.17,foundIndex,foundIndex)+Tmin;
	foundIndex++;
	
	// t 1-parameter 1-sigma max
	Tmax_68=data->getXValueRight(min_chi2+1.0,proj_best_t,foundIndex)+Tmin;	
	if (foundIndex==-1)
		foundIndex=numTempBin-1;	
	// t 2-parameter 1-sigma max
	Tmax2_68=data->getXValueRight(min_chi2+2.3,foundIndex,foundIndex)+Tmin;
	if (foundIndex==-1)
		foundIndex=numTempBin-1;	
	// t 1-parameter 2-sigma max
	Tmax_95=data->getXValueRight(min_chi2+4.00,foundIndex,foundIndex)+Tmin;
	if (foundIndex==-1)
		foundIndex=numTempBin-1;	
	// t 2-parameter 2-sigma max
	Tmax2_95=data->getXValueRight(min_chi2+6.17,foundIndex,foundIndex)+Tmin;			
	if (foundIndex==-1)
		foundIndex=numTempBin-1;	
		
	//------ using 0.68 and 0.95 percentiles of template type as errors
	
	// using likelihood dist of given redshift
	
	data->set(numTempBin,templateCode,likelihoodZ);
		
	aux_index=0; // use this to speed up the percentile calculation (reuse of index)
	aux_integ=0; // use this to speed up the percentile calculation (reuse of integral)																				
	
	int percentile=data->getPercentileLow(0.95,1.,aux_index,aux_integ);
	if (percentile==-1)
		Tminmz_95=Tminmt_95=Tmin;
	else
		Tminmz_95=Tminmt_95=templateCode[percentile]+Tmin;
	
	percentile=data->getPercentileLow(0.68,1.,aux_index,aux_integ);
	if (percentile==-1)
		Tminmz_68=Tminmt_68=Tmin;
	else
		Tminmz_68=Tminmt_68=templateCode[percentile]+Tmin;													
	
	aux_index=numTempBin-1;
	aux_integ=0; 								
	
	percentile=data->getPercentileHigh(0.95,1.,aux_index,aux_integ);
	if (percentile==-1)
		Tmaxmz_95=Tmaxmt_95=Tmax;
	else
		Tmaxmz_95=Tmaxmt_95=templateCode[percentile]+Tmin;		
		
	percentile=data->getPercentileHigh(0.68,1.,aux_index,aux_integ);
	if (percentile==-1)
		Tmaxmz_68=Tmaxmt_68=Tmax;
	else
		Tmaxmz_68=Tmaxmt_68=templateCode[percentile]+Tmin;	
	
	delete data;	
	freeArray1(templateCode);
			
}			

int Likelihood::calcLikelihood(int entryIndex)
{	
	if 	(rightZMode && catalog->isSpecZ())
		return calcLikelihoodRightZ(entryIndex);
	else
		return calcLikelihoodFull(entryIndex);				
}

int Likelihood::calcLikelihood(int entryIndex, double specZ)
{	
	if 	(rightZMode)
		return calcLikelihoodRightZ(entryIndex, specZ);
	else
		return calcLikelihoodFull(entryIndex);		
}

int Likelihood::calcLikelihoodRightZ(int entryIndex)
{	
	int returnValue;
	
	if (catalog->isSpecZ()) {
		specZ=catalog->getRowSpecZ(entryIndex,0);
		haveSpecZ=1;		
	} else {
		specZ=-1;
		haveSpecZ=0;
		failed=1;
	}
				
	if (haveSpecZ)
		returnValue=calcLikelihoodRightZ(entryIndex, specZ);
	else
		returnValue=1;
		
	return returnValue;		
}

int Likelihood::calcLikelihoodRightZ(int entryIndex, double specZ)
{
	double f_OOZ;
	double *f_OTZ,*f_TTZ;
	double *i_flux_errors_squared;
	double aux;
	int *excludeZ;
	int specZi;
	char s[MAX_LENGTH];
	//unsigned numZ=binZ->getLength();
	
	Bisection *bi;
	this->entryIndex=entryIndex;		
	failed=0;		
		
	if (allocatedZ==0)
		allocateArraysZ();
			
	// map specZ to nearest redshift we have in our grid	
	if (specZ > binZ->getZ(binZ->getLength()-1)) {
		WARNING("WARNING: Spectroscopic redshift is larger than the allowed z-range, set it to largest allowed value");
		specZi=binZ->getLength()-1;
	} else if (specZ<binZ->getZ(0)) {
		WARNING("WARNING: Spectroscopic redshift is smaller than the allowed z-range, set it to smallest allowed value");
		specZi=0;
	} else {
		bi = new Bisection(binZ->getLength(),binZ->getZ());
		specZi=bi->getNearestIndex(specZ);
		delete bi;
	}
	
	callocArray1(excludeZ,numTemplates);	
	allocArray1(f_OTZ,numTemplates);
	allocArray1(f_TTZ,numTemplates);
	
	// obtain catalog entries
	catalog->getRowFlux(entryIndex,flux_entries);
	catalog->getRowErrorFlux(entryIndex,flux_errors);
	catalog->getRowInvalidFlux(entryIndex,flux_invalid);				

	// set to zero	
	f_OOZ=0.;		
	for (int t=0;t<numTemplates;t++) {	
		f_OTZ[t]=0.;
		f_TTZ[t]=0.;
	}
				
	// active[filternr] says whether a filterband should be ignored (active==0)		
	for (int f=0;f<numFilters;f++) 
		active[f]=1;
			
	// invalid says whether fitting results for entry are sensible or not
	invalid=0;
		
	// numUsedFilters says how many filterbands are actually used
	numUsedFilters=numFilters;
		
	// check entries, set active and numUsedFilters
	for (int t=0;t<numTemplates;t++) 		
	  for (int f=0;f<numFilters;f++) 
#ifdef PARRAY		
	    if (((int)flux_invalid[f]) || flux[t][specZi][f]<0) 
#else
	    if (((int)flux_invalid[f]) || flux[(t*numZ+specZi)*numFilters+f]<0)
#endif			
	      active[f]=0;
	      
	for (int f=0;f<numFilters;f++) 
		if (! active[f])
			numUsedFilters--;
					
	// Do not make an estimation if there are only 3 filterbands or less available
	if (numUsedFilters<MINFILTERBANDS)	{		
		freeArray1(excludeZ);	
		if (m_out) {
			sprintf(s,"Exclude entry since numFilter=%d < %d",numUsedFilters,MINFILTERBANDS);
			m_out->println(s,2);
		}	
		freeArray1(f_OTZ);
		freeArray1(f_TTZ);
		failed=1;
		return 1;
	}
	
	// calculate f_OT and f_TT (loog e.g. Benitez 2000)
	// ignore certain filterbands, set active status	
	allocArray1(i_flux_errors_squared,numFilters);		
	
	for (int f=0;f<numFilters;f++) 
		i_flux_errors_squared[f]=1./(flux_errors[f]*flux_errors[f]);

#ifdef PARRAY
	for (int t=0;t<numTemplates;t++) {
		double *fluxp = flux[t][specZi];	
		double fOTZadd = 0.;
		double fTTZadd = 0.;
		for (int f=0;f<numFilters;f++) {
			if (active[f]==0) 														
				continue;												  
			fOTZadd+=flux_entries[f]*fluxp[f]*i_flux_errors_squared[f];					
			fTTZadd+=fluxp[f]*fluxp[f]*i_flux_errors_squared[f];					
			
			#ifdef FULLVERBOSE
			if (m_out) {
				sprintf(s,"[t,z]=[%d,%g] f_OT=%g f_TT=%g",t,specZ,f_OTZ[t],f_TTZ[t]);
				m_out->println(s,4);					
			}				
			#endif			
		}
		f_OTZ[t]+=fOTZadd;
		f_TTZ[t]+=fTTZadd;
	}	
#else
	for (int t=0;t<numTemplates;t++) {
		unsigned aux_index = (t*numZ+specZi)*numFilters;
		double fOTZadd = 0.;
		double fTTZadd = 0.;
		for (int f=0;f<numFilters;f++) {
			if (active[f]==0) 														
				continue;												  
			fOTZadd+=flux_entries[f]*flux[aux_index+f]*i_flux_errors_squared[f];					
			fTTZadd+=flux[aux_index+f]*flux[aux_index+f]*i_flux_errors_squared[f];			
			#ifdef FULLVERBOSE
			if (m_out) {
				sprintf(s,"[t,z]=[%d,%g] f_OT=%g f_TT=%g",t,specZ,f_OTZ[t],f_TTZ[t]);
				m_out->println(s,4);					
			}				
			#endif			
		}
		f_OTZ[t]+=fOTZadd;
		f_TTZ[t]+=fTTZadd;
	}

#endif	
		
	// calculate f_OO (look e.g. Benitez 2000)
	// ignore not active filterbands
	for (int f=0;f<numFilters;f++) {
		if (active[f]) {									
			f_OOZ+=flux_entries[f]*flux_entries[f]*i_flux_errors_squared[f];									
		}
	}
	if (m_out) {
		sprintf(s,"f_OO=%g",f_OOZ);
		m_out->println(s,4);		
	}
	
	// calculate template normalization 
	for (int t=0;t<numTemplates;t++) 							
		template_normZ[t]=f_OTZ[t]/f_TTZ[t];
				
	// setting some default values
	best_zi=specZi;
	best_t=0;
	min_chi2=DBL_MAX;		
	int best_t_lir=0;
	double min_chi2_lir=DBL_MAX;	
		
	// mark templates invalid if they violate fluxRestriction 
	for (int t=0;t<numTemplates;t++) {				
		//if ( (MBMode==2) && fluxRestriction->cuttingFast(specZi,template_normZ[t],templates[t]->getBFluxNormalized()) )
		if ( (MBMode==2) && fluxRestriction->cuttingFast(specZi,template_normZ[t],templateBFluxNormalized[t]) )
			excludeZ[t]=1;		
	}			
		
	// look for minimal chi^2, set chi2
	int best_found=0;	
	for (int t=0;t<numTemplates;t++) {																							
		aux=f_OOZ-f_OTZ[t]*f_OTZ[t]/f_TTZ[t];
		chi2Z[t]=aux;
		
		// don't use this chi^2 if this template is excluded
		if (excludeZ[t]) 					
			continue;									
			
		if (min_chi2>aux) {
																
			best_found=1;
			min_chi2=aux;										
			best_t=t;
			if (m_out) {
				sprintf(s,"[t,z]=[%d,%g] curr_chi2=%g",t,specZ,aux);
				m_out->println(s,4);												
			}
		}								
		
		if (min_chi2_lir>aux && specZ>=templates[t]->getMinZ() && specZ<=templates[t]->getMaxZ()) {
			
			min_chi2_lir=aux;
			best_t_lir=t;			
			if (m_out) {
				sprintf(s,"Locale in range: [t,z]=[%d,%g] curr_chi2=%g",t,specZ,aux);
				m_out->println(s,4);												
			}		
		}
									
	}	
	
	// now choose either the minimal chi^2 where templates are in range
	// or take the global minimum of chi^2
	if ((min_chi2+chi2Add)*chi2Mult>=min_chi2_lir) {
		min_chi2=min_chi2_lir;
		best_t=best_t_lir;
	}
	
		
	// marks entry as invalid if some criteria are fulfilled
	if (MBMode==1) {			
		//if (fluxRestriction->cuttingFast(best_zi,template_normZ[best_t],templates[best_t]->getBFluxNormalized())) {
		if (fluxRestriction->cuttingFast(best_zi,template_normZ[best_t],templateBFluxNormalized[best_t])) {
			invalid=1;	// data point invalid if absolute magnitude in B band 
						// is not within some limit
		}
	}
	if (best_found==0)
		invalid=1;
							
	// calculate not-normalized likelihood function and normalization factor
	// could not do that above since need min_chi2
	// normalization s.t. \sum dT likelihoodZ[t] = 1
	// not that some (z,T) pairs are excluded 	
	norm=0.0;	
	for (int t=0;t<numTemplates;t++) {						
		if (excludeZ[t]) {
			likelihoodZ[t]=0.0;
			chi2Z[t]=DBL_MAX;						
		} else {					
			likelihoodZ[t]=exp((min_chi2-chi2Z[t])/2);
			norm+=likelihoodZ[t];
		}		
	}

	//normalize
	for (int t=0;t<numTemplates;t++) {
		likelihoodZ[t]/=norm;
	}
		
	if (m_out) {
		sprintf(s,"norm=%g",norm);
		m_out->println(s,4);
	}				


	// set values which are equal having a fixed z to ensure a uniform output format
	bestMarginZ_zi=bestMarginT_zi=best_zi;
	bestMarginZ_t=bestMarginT_t=best_t;

	// template_norm of best fit
	best_template_norm=template_normZ[best_t];	
	
	// residuals
#ifdef PARRAY	
	for (int f=0;f<numFilters;f++) {
	  residuals[f]=g_fluxToMag(best_template_norm*flux[best_t][specZi][f])-g_fluxToMag(flux_entries[f]);
	}
#else		
	unsigned aux_index=(best_t*numZ+specZi)*numFilters;
	for (int f=0;f<numFilters;f++) {
	  residuals[f]=g_fluxToMag(best_template_norm*flux[aux_index+f])-g_fluxToMag(flux_entries[f]);
	}
#endif	
	
	freeArray1(i_flux_errors_squared);	
	freeArray1(excludeZ);
	freeArray1(f_OTZ);
	freeArray1(f_TTZ);	

    if (invalid)
	  failed=1;

    return failed;	
}

int Likelihood::calcLikelihoodFull(int entryIndex)				
{		
	double aux;	
	char **exclude;		
	char s[MAX_LENGTH];
	double *dz,*z;	
	double *i_flux_errors_squared;	
	double *i_flux_errors_squared_flux_entries;	
	int numZ=binZ->getLength();
	xyData *data;	
	
	#ifdef MEASTIME	 
	 	 times( &start );		 	 
	#endif

	if (allocated==0)
		allocateArrays();					
			
	allocArray1(i_flux_errors_squared,numFilters);	
	allocArray1(i_flux_errors_squared_flux_entries,numFilters);		
	callocArray2(exclude,numTemplates,numZ);	
	
	#ifdef MEASTIME	 
		times ( &end );	
		TIMEADD( rTime1 );				
		times( &start );	  
	#endif	
	
	z=this->binZ->getZ();
	dz=this->binZ->getdZ();
	this->entryIndex=entryIndex;	
	failed=0;
	
	if (catalog->isSpecZ()) {
		specZ=catalog->getRowSpecZ(entryIndex,0);
		haveSpecZ=1;
	} else {
		specZ=-1;
		haveSpecZ=0;
	}
	
	// obtain catalog entries
	catalog->getRowFlux(entryIndex,flux_entries);
	catalog->getRowErrorFlux(entryIndex,flux_errors);
	catalog->getRowInvalidFlux(entryIndex,flux_invalid);	
	
	#ifdef MEASTIME	 
		  times ( &end );	
		  TIMEADD( rTime2 );					  
		  times( &start );		  
	#endif	
				
	// set to zero	
	f_OO=0.;		
	for (int t=0;t<numTemplates;t++) {
		for (int zi=0;zi<numZ;zi++) {
			f_OT[t][zi]=0.;
			f_TT[t][zi]=0.;
		}
	}
				
	// active[filternr] says whether a filterband should
	// be ignored (active==0), first assume all given filters
	// are active
	for (int f=0;f<numFilters;f++) 
		active[f]=1;
			
	// invalid says whether fitting results for entry are sensible or not
	invalid=0;
		
	// numUsedFilters says how many filterbands are actually used
	numUsedFilters=numFilters;				
		
	// check entries, set active and numUsedFilters		
	
	for (int f=0;f<numFilters;f++)
	  if (flux_invalid[f] || (! filterFluxActive[f]) )
	    active[f]=0;	
				
	for (int f=0;f<numFilters;f++) 
		if (! active[f])
			numUsedFilters--;
					
	// Do not make an estimation if there are only 3 filterbands or less available
	if (numUsedFilters<MINFILTERBANDS)	{	
		freeArray2(exclude,numTemplates);		
		if (m_out) {
			sprintf(s,"Exclude entry since numFilter=%d < %d",numUsedFilters,MINFILTERBANDS);
			m_out->println(s,2);
		}		
		freeArray1(i_flux_errors_squared);
		freeArray1(i_flux_errors_squared_flux_entries);					
		failed=1;
		return 1;
	}
	
	#ifdef MEASTIME
		  times ( &end );	
		  TIMEADD( rTime3 );		    
		  times( &start );		 
	#endif	
				
	// calculate f_OT and f_TT (look e.g. at Benitez 2000)
	// ignore certain filterbands, set active status						
	for (int f=0;f<numFilters;f++) 
		i_flux_errors_squared[f]=1./(flux_errors[f]*flux_errors[f]);
		
	for (int f=0;f<numFilters;f++) 
		i_flux_errors_squared_flux_entries[f]=i_flux_errors_squared[f]*flux_entries[f];

#ifdef PARRAY	
	for (int t=0;t<numTemplates;t++) {					
		for (int zi=0;zi<numZ;zi++) {
			double fOTadd = 0.;
			double fTTadd = 0.;
			double *fluxp = flux[t][zi];
			for (int f=0;f<numFilters;f++) {
				if (active[f]==0) 														
					continue;				
			    fOTadd+=fluxp[f]*i_flux_errors_squared_flux_entries[f];
			    fTTadd+=fluxp[f]*fluxp[f]*i_flux_errors_squared[f];
			    
			    #ifdef FULLVERBOSE
				  if (m_out) {
					sprintf(s,"[t,z]=[%d,%g] f_OT=%g f_TT=%g",t,binZ->getZ(zi),f_OT[t][zi],f_TT[t][zi]);
					m_out->println(s,4);					
				  }
				#endif				
			}
			f_OT[t][zi]+=fOTadd;
			f_TT[t][zi]+=fTTadd;
		}
	}
#else
	for (int t=0;t<numTemplates;t++) {					
		for (int zi=0;zi<numZ;zi++) {
			double fOTadd = 0.;
			double fTTadd = 0.;
			unsigned aux_index = (t*numZ+zi)*numFilters;
			for (int f=0;f<numFilters;f++) {
				if (active[f]==0)													
					continue;				
			    fOTadd+=flux[aux_index+f]*i_flux_errors_squared_flux_entries[f];
			    fTTadd+=flux[aux_index+f]*flux[aux_index+f]*i_flux_errors_squared[f];
			    
			    #ifdef FULLVERBOSE
				  if (m_out) {
					sprintf(s,"[t,z]=[%d,%g] f_OT=%g f_TT=%g",t,binZ->getZ(zi),f_OT[t][zi],f_TT[t][zi]);
					m_out->println(s,4);					
				  }
				#endif				
			}
			f_OT[t][zi]+=fOTadd;
			f_TT[t][zi]+=fTTadd;
		}
	}


#endif	
		
	// calculate f_OO (look e.g. Benitez 2000)
	// ignore not active filterbands
	for (int f=0;f<numFilters;f++) {
		if (active[f]) 								
			f_OO+=flux_entries[f]*flux_entries[f]*i_flux_errors_squared[f];	
	}	
	if (m_out) {
		sprintf(s,"f_OO=%g",f_OO);
		m_out->println(s,4);		
	}
	
	#ifdef MEASTIME	  	 
		  times ( &end );	
		  TIMEADD( rTime4 );		  	  
		  times( &start );	  
	#endif	
	
	// calculate template normalization 
	for (int t=0;t<numTemplates;t++) 					
		for (int zi=0;zi<numZ;zi++) 	
			template_norm[t][zi]=f_OT[t][zi]/f_TT[t][zi];
			
	#ifdef MEASTIME	  
		  times ( &end );	
		  TIMEADD( rTime5 );		 		  
		  times( &start );	  
	#endif	
				
	// calculate minimum of chi^2 (brute force)	
	// zLimit=z[binZ->getLength()-1];
	// fluxRestriction->getZLimit(zLimit,flux_entries);						
	
	best_zi=0;	
	best_t=0;
	min_chi2=DBL_MAX;		
	int best_zi_lir=0;
	int best_t_lir=0;
	double min_chi2_lir=DBL_MAX;	
	
		
	// mark (zi,t) pairs invalid if they violate fluxRestriction 
	// or templates are out of their allowed range
	for (int t=0;t<numTemplates;t++) {
		for (int zi=0;zi<numZ;zi++) {			
		  // if ( (MBMode==2) && fluxRestriction->cuttingFast(zi,template_norm[t][zi],templates[t]->getBFluxNormalized()) ) 
		   if ( (MBMode==2) && fluxRestriction->cuttingFast(zi,template_norm[t][zi],templateBFluxNormalized[t]) ) 
		     exclude[t][zi]=1;	
		}		
	}
	

	#ifdef MEASTIME	  
		  times ( &end );	
		  TIMEADD( rTime6 );				  
		  times( &start );	  
	#endif
	
	int best_found=0;		
	for (int t=0;t<numTemplates;t++) {					
	
		// look for minimal chi^2, set chi2	
		for (int zi=0;zi<numZ;zi++) {																			
			aux=f_OO-f_OT[t][zi]*f_OT[t][zi]/f_TT[t][zi];
			chi2[t][zi]=aux;		
															
			// don't use this chi^2 as a solution if this template-redshift pair is excluded
			if (exclude[t][zi])
				continue;
				
			// 	if aux is a new global minimum
			if (min_chi2>aux) {								
													
				best_found=1;
				min_chi2=aux;				
				best_zi=zi;			
				best_t=t;								
				
				if (m_out) {
					sprintf(s,"[t,z]=[%d,%g] curr_chi2=%g",t,binZ->getZ(zi),aux);					
					m_out->println(s,4);												
				}
			}															
		}
		
		// look for local minimum of chi^2 s.t. redshift lies in allowed range
		for (int zi=0;zi<numZ;zi++) {
		
			// don't use this chi^2 if this template-redshift pair is excluded
			if (exclude[t][zi])
				continue;
			
			// if aux is a locale minimum and template is in allowed redshift range
			if (min_chi2_lir > chi2[t][zi] && z[zi]>=templates[t]->getMinZ() && z[zi]<templates[t]->getMaxZ()
			    && leftUp(0,chi2[t],zi) && rightUp(binZ->getLength()-1,chi2[t],zi) ) {
			    				 
			 	min_chi2_lir=chi2[t][zi];
			 	best_zi_lir=zi;
			 	best_t_lir=t; 
			 	
			 	if (m_out) {
					sprintf(s,"Lokal in Range: [t,z]=[%d,%g] curr_chi2=%g",t,binZ->getZ(zi),chi2[t][zi]);
					m_out->println(s,4);												
				}
			}
		}
		
	}		
						
	#ifdef MEASTIME	  
		  times ( &end );	
		  TIMEADD( rTime7 );		  
		  times( &start );	  
	#endif
	
	// now choose either the best lokal minima of chi^2 where templates are in range
	// or take the global minimum of chi^2
	if ((min_chi2+chi2Add)*chi2Mult>min_chi2_lir) {
		best_zi=best_zi_lir;
		best_t=best_t_lir;
		min_chi2=min_chi2_lir;
	}
		
	// mark entry as invalid if some criteria are fulfilled
	if (MBMode==1) {
		//if (fluxRestriction->cutting(best_zi,template_norm[best_t][best_zi],templates[best_t]->getBFluxNormalized())) {
		if (fluxRestriction->cutting(best_zi,template_norm[best_t][best_zi],templateBFluxNormalized[best_t])) {
			invalid=1; // data point invalid if absolute magnitude in B band is not within some limit
		}
	}
	if (best_found==0)
		invalid=1;				
		
	// calculate not-normalized likelihood function and normalization factor
	// could not do that above since need min_chi2
	// normalization s.t. \sum dT \int dz likelihood[t][z] = 1
	// note that some (z,T) pairs are excluded 	
	norm=0.0;	
	for (int t=0;t<numTemplates;t++) {						
		if (exclude[t][0]) {
			likelihood[t][0]=0.0;
			chi2[t][0]=DBL_MAX;						
		} else {					
			likelihood[t][0]=exp((min_chi2-chi2[t][0])/2);
		}
		for (int zi=1;zi<numZ;zi++) {													
			if (exclude[t][zi]) {				
				likelihood[t][zi]=0.0;
				norm+=dz[zi-1]*(likelihood[t][zi]+likelihood[t][zi-1]);		
				chi2[t][zi]=DBL_MAX;				
				continue;
			}
			// DBL_MIN=1E-37 -> if (min_chi2-chi2)/2 < -80 set likelihood ~ exp(...) directly to zero
			if (chi2[t][zi] > min_chi2+160) {
				likelihood[t][zi]=0.0;
				continue;
			}			
			likelihood[t][zi]=exp((min_chi2-chi2[t][zi])/2); 		// not normalized likelihood
			norm+=dz[zi-1]*(likelihood[t][zi]+likelihood[t][zi-1]);									
		}
	}
	norm=norm/2.0;		// from integration
	
	if (numZ==1)	
		norm=1.;		// special case, just one z-value
		
	#ifdef MEASTIME	 
		  times ( &end );	
		  TIMEADD( rTime8 );				   
		  times( &start );	  
	#endif
	
	if (m_out) {
		sprintf(s,"norm=%g",norm);
		m_out->println(s,4);
	}
		
	// normalize likelihood function
	for (int t=0;t<numTemplates;t++) {			
		for (int zi=0;zi<numZ;zi++) {
			if (exclude[t][zi]) {
				likelihood[t][zi]=0;
				continue;
			}							
			likelihood[t][zi]/=norm;			
		}
	}
	
	#ifdef MEASTIME	  
		  times ( &end );	
		  TIMEADD( rTime9 );				   
		  times( &start );	  
	#endif

	#if 0
	// test whether problem with normalization
	data = new xyData();
	double testnorm=0;
	for (int t=0;t<numTemplates;t++) {		
		data->set(binZ->getLength(),binZ->getZ(),likelihood[t]);
		testnorm+=data->integrate();
	}
	delete data;
	cout << "Integral over Likelihood = " << testnorm << endl;	
	#endif

	// template_norm of best fit
	best_template_norm=template_norm[best_t][best_zi];	
	
	// calculate marginalized likelihood functions
	for (int zi=0;zi<numZ;zi++) 
		likelihoodMarginZ[zi]=0;		
	for (int t=0;t<numTemplates;t++) {		
		for (int zi=0;zi<numZ;zi++) {
			likelihoodMarginZ[zi]+=likelihood[t][zi];			
		}
	}
	data = new xyData();	
	for (int t=0;t<numTemplates;t++) {			
		data->set(numZ,z,likelihood[t]);
		likelihoodMarginT[t]=data->integrate();
	}
	delete data;
	
	#ifdef MEASTIME	  
		  times ( &end );	
		  TIMEADD( rTime10 );				    
		  times( &start );	  
	#endif
	
	// find best redshift z of marginalized likelihood and,
	// keeping z fixed, the best fit template type
	aux=0.;
	bestMarginZ_zi=0;
	for (int zi=0;zi<numZ;zi++) {
		if (aux<likelihoodMarginZ[zi]) { 
			aux=likelihoodMarginZ[zi];	
			bestMarginZ_zi=zi;
		}
	}
	aux=0.;
	bestMarginZ_t=0;
	for (int t=0;t<numTemplates;t++) {
		if (aux<likelihood[t][bestMarginZ_zi]) {
			 aux=likelihood[t][bestMarginZ_zi];
			 bestMarginZ_t=t;
		}
	}
	
	// find best template type t of marginalized likelihood and,
	// keeping t fixed, the best fit redshift	
	aux=0.;
	bestMarginT_t=0;
	for (int t=0;t<numTemplates;t++) {
		if (aux<likelihoodMarginT[t]) {
			 aux=likelihoodMarginT[t];
			 bestMarginT_t=t;
		}
	}
	aux=0.;
	bestMarginT_zi=0;
	for (int zi=0;zi<numZ;zi++) {
		if (aux<likelihood[bestMarginT_t][zi]) {
			 aux=likelihood[bestMarginT_t][zi];
			 bestMarginT_zi=zi;
		}
	}	
	
	#ifdef MEASTIME	 
		  times ( &end );	
		  TIMEADD( rTime11 );				  
		  times( &start );	 			  
	#endif
			
	// residuals
#ifdef PARRAY
	for (int f=0;f<numFilters;f++) {
		residuals[f]=g_fluxToMag(best_template_norm*flux[best_t][best_zi][f])-g_fluxToMag(flux_entries[f]);		
	}
#else
	unsigned aux_index = (best_t*numZ+best_zi)*numFilters;
	for (int f=0;f<numFilters;f++) {
		residuals[f]=g_fluxToMag(best_template_norm*flux[aux_index+f])-g_fluxToMag(flux_entries[f]);
	}
#endif	
	
	#ifdef MEASTIME	 
		  times ( &end );	
		  TIMEADD( rTime12 );				   
	#endif
	
	freeArray1(i_flux_errors_squared);
	freeArray1(i_flux_errors_squared_flux_entries);
	freeArray2(exclude,numTemplates);	
	
	#ifdef MEASTIME		
		if (entryIndex==numEntries-1) {
			cout << "\nFor " << numEntries << " entries: \n";
			cout << "Alloc memory " << rTime1 << "s" << endl;	
			cout << "Obtain catalog entries : " << rTime2 << "s" << endl;
			cout << "Calculate active filters, numUsedFilters : " << rTime3 << "s" << endl;
			cout << "Calculate f_TT, f_OT, f_OO, numUsedFilters : " << rTime4 << "s" << endl;
			cout << "Calculate template normalization : " << rTime5 << "s" << endl;
			cout << "Calculate exclude array : " << rTime6 << "s" << endl;
			cout << "Search minimal chi2 : " << rTime7 << "s" << endl; 
			cout << "Calculate likelihood & norm of likelihood: " << rTime8 << "s" << endl;
			cout << "Normalize likelihood: " << rTime9 << "s" << endl;
			cout << "Calculate marginalized likelihood functions: " << rTime10 << "s" << endl;
			cout << "Find optimal z/t of marginalized likelihood functions: " << rTime11 << "s" << endl;
			cout << "calculate residuals: " << rTime12 << "s" << endl;
		}		
	#endif
	
	if (invalid)
	  failed=1;				
	  
	return failed;				
}

void Likelihood::smoothingLikelihood(double smoothZ, double smoothT)
{
	xyData *data;	
	double *trans_likelihood;
	double *templateCode;
	char s[MAX_LENGTH];
	
	data = new xyData();
			
	sprintf(s,"Smoothing Likelihood: Sigma in redshift direction %g, Sigma in template direction %g",smoothZ,smoothT);
	m_out->println(s,2);
	
	if (smoothZ>0) {		
		sprintf(s,"Smoothing in redshift direction");
		m_out->println(s,1);		
		
		for (int t=0;t<numTemplates;t++) {
			data->set(binZ->getLength(),binZ->getZ(),likelihood[t]);
			data->convolveGaussian(smoothZ);			
			data->getCopyY(likelihood[t]);		
		}		
	}
	
	if (smoothT>0) {
		sprintf(s,"Smoothing in template direction");
		m_out->println(s,1);
						
		for (int zi=0;zi<binZ->getLength();zi++) {			
			// for orig-bin		
			allocArray1(trans_likelihood,templateContainer->getNumOrigTemplates());								
			for (int t=0;t<templateContainer->getNumOrigTemplates();t++) {
				trans_likelihood[t]=likelihood[t][zi];
			}						
			allocArray1(templateCode,templateContainer->getNumOrigTemplates());		
			templateContainer->getPhysicalTemplateCode_Orig(0, templateCode);			
			data->set(templateContainer->getNumOrigTemplates(),templateCode,trans_likelihood);
			freeArray1(templateCode);
			data->convolveGaussian(smoothT);
			data->getCopyY(trans_likelihood);
			for (int t=0;t<templateContainer->getNumOrigTemplates();t++) {
				likelihood[t][zi]=trans_likelihood[t];
			}	
			freeArray1(trans_likelihood);
			// for the other template bins
			for (int i=0;i<templateContainer->getNumTemplateBins();i++) {
				allocArray1(trans_likelihood,templateContainer->getNumOrigTemplates());	
				for (int t=0;t<templateContainer->getNumOrigTemplates(i+1);t++) {
					trans_likelihood[t]=likelihood[templateContainer->getMinIndex(i+1)+t][zi];
				}										
				allocArray1(templateCode,templateContainer->getNumOrigTemplates(i+1));		
				templateContainer->getPhysicalTemplateCode_Orig(i+1, templateCode);			
				data->set(templateContainer->getNumOrigTemplates(i+1),templateCode,trans_likelihood);
				freeArray1(templateCode);
				data->convolveGaussian(smoothT);
				data->getCopyY(trans_likelihood);
				for (int t=0;t<templateContainer->getNumOrigTemplates(i+1);t++) {
					likelihood[templateContainer->getMinIndex(i+1)+t][zi]=trans_likelihood[t];
				}									
				freeArray1(trans_likelihood);										
			}				
		}								
	} 		
	
	delete data;
	
}

void Likelihood::printLikelihoodHeader(char *filename)
{
	ofstream ofile;
	ofile.open(filename);
	CHECK(ofile, filename);
	
	if (rightZMode) {				
		ofile << "# This files encodes the likelihood in the following way:" << endl;		
		ofile << "# Each line corresponds to one data entry from the catalog at the right red shift." << endl;
		ofile << "# Each line consists of several columns. Each column corresponds to the likelihood for a certain template." << endl;		
	} else {		
		ofile << "# This files encodes the likelihood in the following way:" << endl;
		ofile << "# The file consists of blocks separated by a blank line." << endl;
		ofile << "# Each block corresponds to one data entry from the catalog." << endl;
		ofile << "# Each block consists of several lines. Each line corresponds to the likelihood for a fixed template." << endl;
		ofile << "# Each line consists of several columns. Each column corresponds to the likelihood at a certain red shift." << endl;
		ofile << "# likelihood[template][redshift]=P(fluxes of entry|template,redshift)" << endl;							
	}
	
	if (MBMode==2)
			ofile << "# M_B restriction used (MBMode=" << MBMode << ") " << this->fluxRestriction->getMBLow() 
				  << " < M_B < " << this->fluxRestriction->getMBHigh() << endl;			
	
	ofile.close();
	ofile.clear();
}

void Likelihood::printLikelihood(char *filename)
{
	ofstream ofile;		
		
	//   calculate and print complete likelihood function			
	ofile.open(filename,ios::app);
	CHECK(ofile, filename);		
	
	if (rightZMode) {
		if (this->failed==0 && this->invalid==0) {		
			for (int t=0;t<numTemplates;t++) {						
				ofile << likelihoodZ[t] << " ";
			}
		} else {		
			for (int t=0;t<numTemplates;t++) {						
				ofile << "0.0 ";
			}
		}								
	} else {
		if (this->failed==0 && this->invalid==0) {
			for (int t=0;t<numTemplates;t++) {
				for (int zi=0;zi<binZ->getLength();zi++) 
					ofile << likelihood[t][zi] << " ";									
				ofile << endl;
			}
			ofile << endl;
		} else {
			for (int t=0;t<numTemplates;t++) {
				for (int zi=0;zi<binZ->getLength();zi++) 
					ofile << "0.0 ";									
				ofile << endl;
			}
			ofile << endl;				
		}	
	}
				
	ofile.close();
	ofile.clear();
}

void Likelihood::printLikelihoodSingle(char *filename)
{
	ofstream ofile;	
	double *z;
	z=binZ->getZ();	
		
	//   calculate and print complete likelihood function			
	ofile.open(filename);
	CHECK(ofile, filename);
	
	if (rightZMode) {		
		ofile << "# Each line corresponds to the likelihood for a fixed template at the right red shift." << endl;
		ofile << "# template likelihood[template]=likelihood[template][spectroscopic_redshift]" << endl;
		if (MBMode==2)
			ofile << "# M_B restriction used (MBMode=" << MBMode << ")" << endl;	
		if (this->failed==0 && this->invalid==0) {		
			for (int t=0;t<numTemplates;t++) {						
				ofile << t << " " << likelihoodZ[t] << endl;
			}
		} else {
			for (int t=0;t<numTemplates;t++) {						
				ofile << t << " 0.0" << endl;
			}	
		}							
	} else {
		#if 0
  		  ofile << "# Each line corresponds to the likelihood for a fixed template." << endl;
		  ofile << "# Each line consists of several columns. Each column corresponds to the likelihood at a certain red shift." << endl;
		  ofile << "# likelihood[template][redshift]=P(fluxes of entry|template,redshift)" << endl;							
		  if (MBMode==2)
			  ofile << "# M_B restriction used (MBMode=" << MBMode << ")" << endl;	
		
		  if (this->failed==0 && this->invalid==0) {
			for (int t=0;t<numTemplates;t++) {
				for (int zi=0;zi<binZ->getLength();zi++) 
					ofile << likelihood[t][zi] << " ";									
				ofile << endl;
			}
			ofile << endl;
		  } else {
			for (int t=0;t<numTemplates;t++) {
				for (int zi=0;zi<binZ->getLength();zi++) 
					ofile << "0.0 ";									
				ofile << endl;
			}
			ofile << endl;				
		  }	
		#else
		  ofile << "# template redshift likelihood[template][redshift]=P(fluxes of entry|template,redshift)" << endl;							
		  if (MBMode==2)
			  ofile << "# M_B restriction used (MBMode=" << MBMode << ")" << endl;	
		
		  if (this->failed==0 && this->invalid==0) {
			for (int t=0;t<numTemplates;t++) {
				for (int zi=0;zi<binZ->getLength();zi++) 
					ofile << t << " " << z[zi] << " " << likelihood[t][zi] << endl;
				ofile << endl;
			}
			ofile << endl;
		  } else {
			for (int t=0;t<numTemplates;t++) {
				for (int zi=0;zi<binZ->getLength();zi++) 
					ofile << t << " " << z[zi] << " 0.0 ";
				ofile << endl;
			}
			ofile << endl;				
		  }		  		  
		#endif
	}
	
	ofile.close();
	ofile.clear();		
}

void Likelihood::printLikelihoodMarginTHeader(char *filename)
{
	ofstream ofile;
	ofile.open(filename);
	CHECK(ofile, filename);
	if (MBMode==2)
		ofile << "# M_B restriction used (MBMode=" << MBMode << ")" << endl;	   	
	ofile << "# This files encodes the likelihood marginalized over red shifts in the following way:" << endl;		
	ofile << "# Each line corresponds to one data entry from the catalog." << endl;
	ofile << "# Each line consists of several columns. Each column corresponds to the likelihood for a certain template." << endl;
	ofile << "# likelihood[template] = \\int d(red shift) likelihood[template][redshift]" << endl;		
	ofile.close();
	ofile.clear();	
}

void Likelihood::printLikelihoodMarginT(char *filename)
{
	ofstream ofile;
	double *z;
	xyData *data;
	
	z=binZ->getZ();	
		
	ofile.open(filename,ios::app);
	CHECK(ofile, filename);
	
	
	if (this->failed==0 && this->invalid==0) {
		data = new xyData();	
		for (int t=0;t<numTemplates;t++) {			
			data->set(binZ->getLength(),z,likelihood[t]);		
			ofile << data->integrate() << " ";
		}
		delete data;
	} else {		
		for (int t=0;t<numTemplates;t++) {						
			ofile << "0.0 ";
		}
	}
	
	ofile << endl;
	ofile.close();
	ofile.clear();
}


void Likelihood::printLikelihoodMarginTRightZ(char *filename)
{
	ofstream ofile;
	double *z;
	//xyData *data;
	double sum=0;
	
	z=binZ->getZ();	
		
	ofile.open(filename,ios::app);
	CHECK(ofile, filename);
	
	
	if (this->failed==0 && this->invalid==0) {
		//data = new xyData();	
		for (int t=0;t<numTemplates;t++) {			
		//	data->set(binZ->getLength(),z,likelihoodZ[t]);		
		//	ofile << data->integrate() << " ";
		ofile << likelihoodZ[t] << " ";
		sum+=likelihoodZ[t];
		}
		//delete data;
	} else {		
		for (int t=0;t<numTemplates;t++) {						
			ofile << "0.0 ";
		}
	}
	
	ofile << endl;
	ofile.close();
	ofile.clear();
	
	cout << "sum of likelihoodT: "<< sum<< endl; 
}

void Likelihood::printLikelihoodMarginTSingle(char *filename)
{
	ofstream ofile;
	double *z;
	xyData *data;
	
	z=this->binZ->getZ();
	
	ofile.open(filename);
	CHECK(ofile, filename);
	if (MBMode==2)
		ofile << "# M_B restriction used (MBMode=" << MBMode << ")" << endl;
	ofile << "# template likelihood[template]=\\int d(red shift) likelihood[template][redshift]" << endl;	
	
	if (this->failed==0 && this->invalid==0) {
		data = new xyData();
		for (int t=0;t<numTemplates;t++) {			
			data->set(binZ->getLength(),z,likelihood[t]);		
			ofile << t << " " << data->integrate() << endl;
		}
		delete data;
	} else {
		for (int t=0;t<numTemplates;t++) {						
			ofile << t << " 0.0" << endl;
		}	
	}	
	ofile.close();
	ofile.clear();					
}

void Likelihood::printLikelihoodMarginZHeader(char *filename)
{
	ofstream ofile;
	ofile.open(filename);
	CHECK(ofile, filename);
	if (MBMode==2)
		ofile << "# M_B restriction used (MBMode=" << MBMode << ")" << endl;	   	
	ofile << "# This files encodes the likelihood marginalized over template types in the following way:" << endl;		
	ofile << "# Each line corresponds to one data entry from the catalog." << endl;
	ofile << "# Each line consists of several columns. Each column corresponds to the likelihood at a certain red shift." << endl;
	ofile << "# likelihood[redshift] = \\sum_{templates} likelihood[template][redshift]" << endl;		
	ofile.close();
	ofile.clear();	
}

void Likelihood::printLikelihoodMarginZ(char *filename)
{
	ofstream ofile;
	double aux;	
		
	ofile.open(filename,ios::app);
	CHECK(ofile, filename);
	for (int zi=0;zi<binZ->getLength();zi++) {		
		if (this->failed==0 && this->invalid==0) {
			aux=0;
			for (int t=0;t<numTemplates;t++) {									
				aux+=likelihood[t][zi];			
			}
			ofile << aux << " ";
		} else {
			ofile << "0.0 ";
		}
	}
	ofile << endl;
	ofile.close();
	ofile.clear();
}

void Likelihood::printLikelihoodMarginZSingle(char *filename)
{
	ofstream ofile;
	double *z;
	double aux;
	
	z=this->binZ->getZ();	
	ofile.open(filename);
	CHECK(ofile, filename);
	if (MBMode==2)
		ofile << "# M_B restriction used (MBMode=" << MBMode << ")" << endl;
	ofile << "# redshift likelihood[redshift]=\\sum_{templates}likelihood[template][redshift]" << endl;	
	for (int zi=0;zi<binZ->getLength();zi++) {
		if (this->failed==0 && this->invalid==0) {
			aux=0;		
			for (int t=0;t<numTemplates;t++) {									
				aux+=likelihood[t][zi];			
			}
			ofile << z[zi] << " " << aux << endl;
		} else {
			ofile << z[zi] << " 0.0" << endl;
		}		
	}
	ofile.close();
	ofile.clear();					
}

void Likelihood::printLikelihoodMarginZPercentileHeader(char *filename)
{
	ofstream ofile;
	ofile.open(filename);
	CHECK(ofile, filename);
	ofile << "# The likelihood[redshift] = \\sum_{templates} likelihood[template][redshift] normalized to one" << endl;		
	ofile << "# each line corresponds to a galaxy, the percentiles are given as 0.0001, 0.01, 0.02, ..., 0.98, 0.99, 0.9999" << endl;
	ofile.close();
	ofile.clear();	
}

void Likelihood::printLikelihoodMarginZPercentile(char *filename)
{
	ofstream ofile;
	double *likeSum,*z;
	xyData *data;
	double aux,percentile,limit;
		
	ofile.open(filename,ios::app);
	CHECK(ofile, filename);
	ofile.precision(PERCENTILE_PRECISION);
	
	if (this->failed==0 && this->invalid==0) {		
		
		allocArray1(likeSum,binZ->getLength());	
		z=binZ->getZ();
	
		// first calculate marginalized likelihood				
		for (int zi=0;zi<binZ->getLength();zi++) {
			aux=0;			
			for (int t=0;t<numTemplates;t++) {									
				aux+=likelihood[t][zi];				
			}
			likeSum[zi]=aux;
			//cout << "likeSum[" << zi << "]=" << aux << " zSum=" << z[zi] << endl;
		}
		data = new xyData();
		data->set(binZ->getLength(),z,likeSum);
		data->cumulate();
	
		// find percentiles
		// first percentile 0.0001
		percentile=data->getXValue(0.0001);
		ofile << percentile << " ";
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {
			limit=i*0.01;
			percentile=data->getXValue(limit);
			ofile << percentile << " ";
		}	
		// last percentile 0.9999
		percentile=data->getXValue(0.9999);
		ofile << percentile << endl;
	
		freeArray1(likeSum);	
		delete data;
		
	} else {
		
		// first percentile 0.0001
		ofile << "0.0 ";
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {			
			ofile << "0.0 ";
		}	
		// last percentile 0.9999
		ofile << "0.0 " << endl;
		
	}

	ofile.close();
	ofile.clear();		
}


void Likelihood::printLikelihoodMarginZPercentileSingle(char *filename)
{
	ofstream ofile;
	double *likeSum,*z;
	xyData *data;
	double aux,percentile,limit;
		
	ofile.open(filename);
	CHECK(ofile, filename);
	ofile.precision(PERCENTILE_PRECISION);
	
	if (this->failed==0 && this->invalid==0) {
	
		allocArray1(likeSum,binZ->getLength());	
		z=binZ->getZ();
	
		// first calculate marginalized likelihood				
		for (int zi=0;zi<binZ->getLength();zi++) {
			aux=0;			
			for (int t=0;t<numTemplates;t++) {									
				aux+=likelihood[t][zi];				
			}
			likeSum[zi]=aux;			
		}
		data = new xyData();
		data->set(binZ->getLength(),z,likeSum);
		data->cumulate();
	
		// find percentiles
		// first percentile 0.0001
		percentile=data->getXValue(0.0001);
		ofile << "0.0001 " << percentile << "\n";
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {
			limit=i*0.01;
			percentile=data->getXValue(limit);
			ofile << limit << " " << percentile << "\n";
		}	
		// last percentile 0.9999
		percentile=data->getXValue(0.9999);
		ofile << "0.9999 " << percentile << endl;
	
		freeArray1(likeSum);	
		delete data;
		
	} else {
		
		// first percentile 0.0001
		ofile << "0.0001 0.0\n";
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {			
			limit=i*0.01;
			ofile << limit << " 0.0\n";
		}
		// last percentile 0.9999
		ofile << "0.9999 0.0" << endl;
		
	}
	
	ofile.close();
	ofile.clear();		
}

void Likelihood::printLikelihoodMarginZPercentileHeader(char *filename_MarginZ, char *filename_BestT)
{
	ofstream ofile;
	
	ofile.open(filename_MarginZ);
	CHECK(ofile, filename_MarginZ);
	ofile << "# The likelihood[redshift] = \\sum_{templates} likelihood[template][redshift] normalized to one" << endl;		
	ofile << "# each line corresponds to a galaxy, the percentiles are given as 0.0001, 0.01, 0.02, ..., 0.98, 0.99, 0.9999" << endl;
	ofile.close();
	ofile.clear();	
	
	ofile.open(filename_BestT);
	CHECK(ofile, filename_BestT);
	ofile << "# Each entry corresponds to the best-fit template associated with the respective redshift in the percentile file" << endl;
	ofile << "# each line corresponds to a galaxy, look into the marginalized-likelihood percentile file for more information" << endl;
	ofile.close();
	ofile.clear();	
}

void Likelihood::printLikelihoodMarginZPercentile(char *filename_MarginZ, char *filename_BestT)
{
	ofstream ofile;
	ofstream ofile2;
	
	double *likeSum,*z;
	xyData *data;
	double aux,percentile,limit;
		
	ofile.open(filename_MarginZ,ios::app);
	CHECK(ofile, filename_MarginZ);
	ofile.precision(PERCENTILE_PRECISION);
	
	ofile2.open(filename_BestT,ios::app);
	CHECK(ofile2, filename_BestT);	
	
	if (this->failed==0 && this->invalid==0) {		
		
		allocArray1(likeSum,binZ->getLength());	
		z=binZ->getZ();
	
		// first calculate marginalized likelihood				
		for (int zi=0;zi<binZ->getLength();zi++) {
			aux=0;			
			for (int t=0;t<numTemplates;t++) {									
				aux+=likelihood[t][zi];				
			}
			likeSum[zi]=aux;			
		}
		data = new xyData();
		data->set(binZ->getLength(),z,likeSum);
		data->cumulate();
	
		// find percentiles
		// first percentile 0.0001
		percentile=data->getXValue(0.0001);		
		ofile << percentile << " ";		
		ofile2 << getBestT(data->getNearestIndex(percentile)) << " ";
		
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {
			limit=i*0.01;
			percentile=data->getXValue(limit);
			ofile << percentile << " ";
			ofile2 << getBestT(data->getNearestIndex(percentile)) << " ";
		}	
		// last percentile 0.9999
		percentile=data->getXValue(0.9999);
		ofile << percentile << endl;
		ofile2 << getBestT(data->getNearestIndex(percentile)) << endl;
	
		freeArray1(likeSum);
		delete data;
		
	} else {
		
		// first percentile 0.0001
		ofile << "0.0 ";
		ofile2 << "-1 ";
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {			
			ofile << "0.0 ";
			ofile2 << "-1 ";
		}	
		// last percentile 0.9999
		ofile << "0.0 " << endl;
		ofile2 << "-1 " << endl;
		
	}

	ofile.close();
	ofile.clear();
	
	ofile2.close();
	ofile2.clear();		
}


void Likelihood::printLikelihoodMarginZPercentileSingle(char *filename_MarginZ, char *filename_BestT)
{
	ofstream ofile;
	ofstream ofile2;
	
	double *likeSum,*z;
	xyData *data;
	double aux,percentile,limit;
		
	ofile.open(filename_MarginZ);
	CHECK(ofile, filename_MarginZ);
	ofile.precision(PERCENTILE_PRECISION);
	
	ofile2.open(filename_BestT);
	CHECK(ofile2, filename_BestT);	
	
	if (this->failed==0 && this->invalid==0) {
	
		allocArray1(likeSum,binZ->getLength());	
		z=binZ->getZ();
	
		// first calculate marginalized likelihood				
		for (int zi=0;zi<binZ->getLength();zi++) {
			aux=0;			
			for (int t=0;t<numTemplates;t++) {									
				aux+=likelihood[t][zi];				
			}
			likeSum[zi]=aux;
			//cout << "likeSum[" << zi << "]=" << aux << " zSum=" << z[zi] << endl;
		}
		data = new xyData();
		data->set(binZ->getLength(),z,likeSum);
		data->cumulate();
	
		// find percentiles
		// first percentile 0.0001
		percentile=data->getXValue(0.0001);
		ofile << "0.0001 " << percentile << "\n";
		ofile2 << "0.0001 " << getBestT(data->getNearestIndex(percentile)) << "\n";
		
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {
			limit=i*0.01;
			percentile=data->getXValue(limit);
			ofile << limit << " " << percentile << "\n";
			ofile2 << limit << " " << getBestT(data->getNearestIndex(percentile)) << "\n";
		}	
		// last percentile 0.9999
		percentile=data->getXValue(0.9999);
		ofile << "0.9999 " << percentile << endl;
		ofile2 << "0.9999 " << getBestT(data->getNearestIndex(percentile)) << endl;
	
		freeArray1(likeSum);
		delete data;
		
	} else {
		
		// first percentile 0.0001
		ofile << "0.0001 0.0\n";
		ofile2 << "0.0001 -1\n";
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {			
			limit=i*0.01;
			ofile << limit << " 0.0\n";
			ofile2 << limit << " -1\n";
		}	
		// last percentile 0.9999
		ofile << "0.9999 0.0" << endl;
		ofile2 << "0.9999 -1" << endl;
		
	}
	
	ofile.close();
	ofile.clear();	
		
	ofile2.close();
	ofile2.clear();	
}
void Likelihood::printFluxesHeader(char *filename)
{
	ofstream ofile;
	ofile.open(filename);
	CHECK(ofile,filename);
	ofile << "# filter_average_wavelength filter_pivot_wavelength filter_flux filter_flux_error template_flux template_norm filter_mag template_mag\n";
	ofile << "# wavelength in Angstroem, flux (densities) per unit wavelength" << endl;
	ofile.close();
	ofile.clear();
}
	
void Likelihood::printFluxes(char *filename, Filter **filters)
{
	double pivotWavelength;
	double pivotWavelength2;
	ofstream ofile;
	
	ofile.open(filename,ios::app);
	CHECK(ofile,filename);
	for (int f=0;f<numFilters;f++) {
			
		pivotWavelength=filters[f]->getPivotWavelength();
		pivotWavelength2=pivotWavelength*pivotWavelength;				
		
		if (this->failed==0 && active[f]!=0 && this->invalid==0) {
								
			ofile << filters[f]->getAverageWavelength() << " "
			      << pivotWavelength << " " 			
				  << flux_entries[f]/pivotWavelength2 << " " 
				  << flux_errors[f]/pivotWavelength2 << " "			 
#ifdef PARRAY				  
				  << best_template_norm*flux[best_t][best_zi][f]/pivotWavelength2 << " "
#else
				  << best_template_norm*flux[best_t*binZ->getLength()*numFilters+best_zi*numFilters+f]/pivotWavelength2 << " "
#endif				  
				  << best_template_norm << " "
				  << g_fluxToMag(flux_entries[f]) << " "
#ifdef PARRAY				  
				  << g_fluxToMag(best_template_norm*flux[best_t][best_zi][f]) << " " << endl;
#else
				  << g_fluxToMag(best_template_norm*flux[best_t*binZ->getLength()*numFilters+best_zi*numFilters+f]) << " " << endl;
#endif				  
		} else {
			ofile << filters[f]->getAverageWavelength() << " "
			      << pivotWavelength << " "
				  << "-1.0 "
				  << "-1.0 "
				  << "-1.0 "
				  << "-1.0 "
				  << "-1.0 " 
				  << "-1.0 " << endl;
		}			   
	}			
	ofile.close();					
	ofile.clear();
}

void Likelihood::printResidualHeader(char *filename)
{
	
	ofstream ofile;
	ofile.open(filename);
	CHECK(ofile,filename);
	ofile << "# filter_average_wavelength filter_mag template_mag error_mag (for each filter)" << endl;		
	ofile.close();
	ofile.clear();	
}

void Likelihood::printResidual(char *filename, Filter **filters)
{
	ofstream ofile;			
		
	ofile.open(filename,ios::app);
	CHECK(ofile,filename);
#ifdef PARRAY	
	for (int f=0;f<numFilters;f++) {					
			
		ofile << filters[f]->getAverageWavelength() << " ";
		if (this->failed==0 && this->active[f]!=0 && this->invalid==0) {	// filter invalid or likelihood calc failed or invalid (due to MB cut)
			ofile  << g_fluxToMag(flux_entries[f]) << " "
			  	   << g_fluxToMag(best_template_norm*flux[best_t][best_zi][f]) << " "
		      	   << g_fluxToMagError(flux_errors[f]/flux_entries[f]) << " ";
		} else
			ofile << "99.0 99.0 99.0 ";
	}
#else
	unsigned aux_index = (best_t*binZ->getLength()+best_zi)*numFilters;
	for (int f=0;f<numFilters;f++) {					
			
		ofile << filters[f]->getAverageWavelength() << " ";
		if (this->failed==0 && this->active[f]!=0 && this->invalid==0) {	// filter invalid or likelihood calc failed or invalid (due to MB cut)
			ofile  << g_fluxToMag(flux_entries[f]) << " "
			  	   << g_fluxToMag(best_template_norm*flux[aux_index+f]) << " "
		      	   << g_fluxToMagError(flux_errors[f]/flux_entries[f]) << " ";
		} else
			ofile << "99.0 99.0 99.0 ";
	}
#endif		
	ofile << endl;
	ofile.close();					
	ofile.clear();
}

void Likelihood::printPhotoZHeader(ofstream & ofile)
{
	ofile << "# Output of " << PHOTOZ << " version " << VERSION << endl;
	ofile << "# For questions refer to " << REFERENCE << endl;
	ofile << "#" << endl;	
	ofile << "# Explanation of the columns:" << endl;
	
	ofile << "# z_cat ... redshift given in the catalog (e.g. a spectroscopic redshift)" << endl;	
	ofile << "# usedFilters .. number of used filter bands" << endl;	
	
	ofile << "# z_phot ... redshift corresponding to maximum likelihood" << endl;
	ofile << "# T_phot ... template type corresponding to maximum likelihood" << endl;
	ofile << "# a ... template normalization factor" << endl;
	ofile << "# chi^2 ... chi2^2 of (z_phot,T_phot), which is the mimimum chi^2 value" << endl;
	ofile << "# M_B ... calculated absolute magnitude in the B band using calculated T_phot,z_phot" << endl;	
	ofile << "# D_L ... luminosity distance at z_phot in Mpc" << endl;

	ofile << "# z_photz ... redshift corresponding to maximum of marginalized (over type) likelihood" << endl;	
	ofile << "# T_photz ... best fitting template type given z_photmz" << endl;			
	ofile << "# az ... template normalization factor corresponding to (z_photmz,T_photmz)" << endl;
	ofile << "# chi^2z ... chi2^2 of (z_photmz,T_photmz)" << endl;
	ofile << "# M_Bz ... calculated absolute magnitude in the B band using calculated T_photmz,z_photmz" << endl;	
	ofile << "# D_Lz ... luminosity distance at z_photmz in Mpc" << endl;		
	
	ofile << "# z_phott ... best fitting redshift given T_photmt" << endl;	
	ofile << "# T_phott ... template type corresponding to maximum of marginalized (over redshift) likelihood" << endl;			
	ofile << "# at ... template normalization factor corresponding to (z_photmt,T_photmt)" << endl;
	ofile << "# chi^2t ... chi2^2 of (z_photm,T_photm)" << endl;
	ofile << "# M_Bt ... calculated absolute magnitude in the B band using calculated T_photm,z_photm" << endl;	
	ofile << "# D_Lt ... luminosity distance at z_photm in Mpc" << endl;	
	
	ofile << "# z_ph2_L0.68 z_ph2_H0.68 ... 2-parameter limits of confidence area (using Delta_chi2=2.3, corresponding to conf. level 0.68)" << endl;
	ofile << "# z_ph2_L0.95 z_ph2_H0.95 ... 2-parameter limits of confidence area (using Delta_chi2=6.17, corresponding to conf. level 0.95)" << endl;
	
	ofile << "# z_ph_L0.68 z_ph_H0.68 ... 1-parameter limits of confidence area (using Delta_chi2=1.0, corresponding to conf. level 0.68)" << endl;
	ofile << "# z_ph_L0.95 z_ph_H0.95 ... 1-parameter limits of confidence area (using Delta_chi2=4.07, corresponding to conf. level 0.95)" << endl;
	
	ofile << "# z_phz_L0.68 z_phz_H0.68 ... limits of confidence area (percentiles corresponding to conf. level 0.68) of marginalized redshift estimate" << endl;
	ofile << "# z_phz_L0.95 z_phz_H0.95 ... limits of confidence area (percentiles corresponding to conf. level 0.95) of marginalized redshift estimate" << endl;
	
	ofile << "# z_pht_L0.68 z_pht_H0.68 ... limits of confidence area (percentiles corresponding to conf. level 0.68) of redshift using best (marginalized) type" << endl;
	ofile << "# z_pht_L0.95 z_pht_H0.95 ... limits of confidence area (percentiles corresponding to conf. level 0.95) of redshift using best (marginalized) type" << endl;
	
	ofile << "# T_ph2_L0.68 T_ph2_H0.68 ... 2-parameter limits of confidence area (using Delta_chi2=2.3, corresponding to conf. level 0.68)" << endl;
	ofile << "# T_ph2_L0.95 T_ph2_H0.95 ... 2-parameter limits of confidence area (using Delta_chi2=6.17, corresponding to conf. level 0.95)" << endl;
	
	ofile << "# T_ph_L0.68 T_ph_H0.68 ... 1-parameter limits of confidence area (using Delta_chi2=1.0, corresponding to conf. level 0.68)" << endl;
	ofile << "# T_ph_L0.95 T_ph_H0.95 ... 1-parameter limits of confidence area (using Delta_chi2=4.07, corresponding to conf. level 0.95)" << endl;
	
	ofile << "# T_phz_L0.68 T_phz_H0.68 ... limits of confidence area (percentiles corresponding to conf. level 0.68) of marginalized type estimate" << endl;
	ofile << "# T_phz_L0.95 T_phz_H0.95 ... limits of confidence area (percentiles corresponding to conf. level 0.95) of marginalized type estimate" << endl;
	
	ofile << "# T_pht_L0.68 T_pht_H0.68 ... limits of confidence area (percentiles corresponding to conf. level 0.68) of type using best (marginalized) redshift" << endl;
	ofile << "# T_pht_L0.95 T_pht_H0.95 ... limits of confidence area (percentiles corresponding to conf. level 0.95) of type using best (marginalized) redshift" << endl;
	
	ofile << "#";

    // 2 + 3*6 + 8*4 = 52
    for (int i=0;i<9;i++)
    	ofile << setw(MYSETW-2) << "(" << i+1 << ")";
    for (int i=9;i<52;i++)
        ofile << setw(MYSETW-3) << "(" << i+1 << ")";	
		
	ofile << endl << "#";
	
	ofile << setw(MYSETW-1) << "zcat";
	ofile << setw(MYSETW) << "usedFilters";
	ofile << setw(MYSETW) << "z_phot";
	ofile << setw(MYSETW) << "T_phot";
	ofile << setw(MYSETW) << "a";
	ofile << setw(MYSETW) << "chi2^2";
	ofile << setw(MYSETW) << "M_B";
	ofile << setw(MYSETW) << "D_L";

	ofile << setw(MYSETW) << "z_photz";
	ofile << setw(MYSETW) << "T_photz";
	ofile << setw(MYSETW) << "az";
	ofile << setw(MYSETW) << "chi2^2z";
	ofile << setw(MYSETW) << "M_Bz";
	ofile << setw(MYSETW) << "D_Lz";
	
	ofile << setw(MYSETW) << "z_phott";
	ofile << setw(MYSETW) << "T_phott";
	ofile << setw(MYSETW) << "at";
	ofile << setw(MYSETW) << "chi2^2t";
	ofile << setw(MYSETW) << "M_Bt";
	ofile << setw(MYSETW) << "D_Lt";
	
	ofile << setw(MYSETW) << "z_ph2_L0.68";
	ofile << setw(MYSETW) << "z_ph2_H0.68";
	ofile << setw(MYSETW) << "z_ph2_L0.95";
	ofile << setw(MYSETW) << "z_ph2_H0.95";
	ofile << setw(MYSETW) << "z_ph_L0.68";
	ofile << setw(MYSETW) << "z_ph_H0.68";
	ofile << setw(MYSETW) << "z_ph_L0.95";
	ofile << setw(MYSETW) << "z_ph_H0.95";
	
	ofile << setw(MYSETW) << "z_phz_L0.68";
	ofile << setw(MYSETW) << "z_phz_H0.68";
	ofile << setw(MYSETW) << "z_phz_L0.95";
	ofile << setw(MYSETW) << "z_phz_H0.95";
	ofile << setw(MYSETW) << "z_pht_L0.68";
	ofile << setw(MYSETW) << "z_pht_H0.68";
	ofile << setw(MYSETW) << "z_pht_L0.95";
	ofile << setw(MYSETW) << "z_pht_H0.95";
	
	ofile << setw(MYSETW) << "T_ph2_L0.68";
	ofile << setw(MYSETW) << "T_ph2_H0.68";
	ofile << setw(MYSETW) << "T_ph2_L0.95";
	ofile << setw(MYSETW) << "T_ph2_H0.95";
	ofile << setw(MYSETW) << "T_ph_L0.68";
	ofile << setw(MYSETW) << "T_ph_H0.68";
	ofile << setw(MYSETW) << "T_ph_L0.95";
	ofile << setw(MYSETW) << "T_ph_H0.95";
	
	ofile << setw(MYSETW) << "T_phz_L0.68";
	ofile << setw(MYSETW) << "T_phz_H0.68";
	ofile << setw(MYSETW) << "T_phz_L0.95";
	ofile << setw(MYSETW) << "T_phz_H0.95";
	ofile << setw(MYSETW) << "T_pht_L0.68";
	ofile << setw(MYSETW) << "T_pht_H0.68";
	ofile << setw(MYSETW) << "T_pht_L0.95";
	ofile << setw(MYSETW) << "T_pht_H0.95";
		
	ofile << endl;		
}


void Likelihood::printPhotoZ(ofstream & ofile)
{	
	
	//std::cout << " FAILED= "<< failed << " INVALID=" << invalid << std::endl;			
	if (failed==0 && this->invalid==0) {
		
		if (! rightZMode) {			
			calcErrors();		
		} else {
			calcErrorsRightZ();
		}
			
		if (catalog->isSpecZ()) 
			ofile << setprecision(6) << setw(MYSETW) << specZ;
		else 
			ofile << setw(MYSETW) << "-1";
		ofile << setw(MYSETW) << numUsedFilters;		
		
		if (! rightZMode) {				
			
				fprintfMLGroup(ofile, best_zi, best_t);
				fprintfMLGroup(ofile, bestMarginZ_zi, bestMarginZ_t);
				fprintfMLGroup(ofile, bestMarginT_zi, bestMarginT_t);		
		
		} else {
			
				fprintfMLGroupZ(ofile, best_zi,best_t);
				fprintfMLGroupZ(ofile, bestMarginZ_zi, bestMarginZ_t);
				fprintfMLGroupZ(ofile, bestMarginZ_zi, bestMarginT_t); 
	    }	
			
		fprintfMLErrGroup(ofile, zmin2_68, zmax2_68, zmin2_95, zmax2_95);
		fprintfMLErrGroup(ofile, zmin_68, zmax_68, zmin_95, zmax_95);
		fprintfMLErrGroup(ofile, zminmz_68, zmaxmz_68, zminmz_95, zmaxmz_95);
		fprintfMLErrGroup(ofile, zminmt_68, zmaxmt_68, zminmt_95, zmaxmt_95);
			
		fprintfMLErrGroup(ofile, Tmin2_68, Tmax2_68, Tmin2_95, Tmax2_95);
		fprintfMLErrGroup(ofile, Tmin_68, Tmax_68, Tmin_95, Tmax_95);
		fprintfMLErrGroup(ofile, Tminmz_68, Tmaxmz_68, Tminmz_95, Tmaxmz_95);
		fprintfMLErrGroup(ofile, Tminmt_68, Tmaxmt_68, Tminmt_95, Tmaxmt_95);		

				
		ofile << "\n";				
		
	} else {
		
		if (catalog->isSpecZ()) 
			ofile << setprecision(6) << setw(MYSETW) << specZ;
		else 
			ofile << setw(MYSETW) << "-1";
		ofile << setw(MYSETW) << numUsedFilters;

        // 3*6 + 8*4 = 50
        for (int i=0;i<50;i++) {					
			ofile << setprecision(6) << setw(MYSETW) << "-1";
		}				
		ofile << "\n";
	}
	
}

Prior::Prior()
{
 	likelihood=NULL; 	
 	templateContainer=NULL;
 	prior=NULL;
 	priorBestT=NULL;
 	posterior=NULL;
 	posteriorMarginZ=NULL;
 	posteriorMarginT=NULL; 	
 	posteriorBestT=NULL; 	
 	numberCountML=NULL;
 	numberCountMLBestT=NULL;
 	out=NULL;
 	savePriorIterative=0;
 	maxIterations=1;
 	smoothZ=0.03;
 	smoothT=0;
 	chi2ReducedCut=1e100;
}

Prior::~Prior()
{		
	freeArray2(prior,numTemplates);
	freeArray1(priorBestT);
	freeArray2(posterior,numTemplates);
	freeArray1(posteriorMarginZ);
	freeArray1(posteriorMarginT);
	freeArray1(posteriorBestT);
	freeArray2(numberCountML,numTemplates);
	freeArray1(numberCountMLBestT);
}

void Prior::setFlatPrior()
{
	double aux;
	double zmax,zmin;
	
	freeArray2(prior,numTemplates);
	allocArray2(prior,numTemplates,numZ);
	
	// set initial prior (uniform)	
	zmax=likelihood->getBinZ()->getZ(likelihood->getBinZ()->getLength()-1);
	zmin=likelihood->getBinZ()->getZ(0);
	aux=(zmax-zmin)*numTemplates;
			
	for (int t=0;t<numTemplates;t++)
		for (int zi=0;zi<numZ;zi++)
			prior[t][zi]=1/aux;					
}

void Prior::setFlatPriorBestT()
{
	double aux;
	
	freeArray1(priorBestT);
	allocArray1(priorBestT,numZ);
		
	// set initial prior (uniform)	
	aux=likelihood->getBinZ()->getZ(likelihood->getBinZ()->getLength()-1)-likelihood->getBinZ()->getZ(0);
	for (int zi=0;zi<numZ;zi++)
		priorBestT[zi]=1/aux;		
}

void Prior::calcPriorBestT() 
{
	double *like;
	double etaAbsDiff,prior_norm,*eta,etaDiff;
	double etaRMS;
	int numIter;
	char s[MAX_LENGTH];	
	double *dz,*z;
	int numEntries;
	xyData *data1, *data2;
	
	out->print("Calculating prior (best template): ",1);

	dz=likelihood->getBinZ()->getdZ();
	z=likelihood->getBinZ()->getZ();
		
	allocArray1(eta,numZ);								
	freeArray1(numberCountMLBestT);
	callocArray1(numberCountMLBestT,numZ);
					
	etaAbsDiff=1000;
	numIter=0;	
	
	data1 = new xyData();
	data2 = new xyData();
	
	while ((etaAbsDiff>0.2) && numIter<maxIterations ) {
		
			cout << "Iteration: " << numIter+1 << endl;
			
			if (savePriorIterative) {
			  sprintf(s,"%sBestT_l%d.dat",priorfilename,numIter);
			  savePriorBestT(s);			  
			}
			
			// reset eta													
			for (int zi=0;zi<numZ;zi++) 
				eta[zi]=0;	
				
			numEntries=0;
			for (int i=0;i<likelihood->getNumEntries();i++) {
				
				// continue	with next entry if no likelihood could be calculated
				if (likelihood->calcLikelihood(i)!=0) {					
					continue;
				}
								
				if (likelihood->getNumUsedFilters()>3 && 
				    likelihood->getBest_Chi2() > chi2ReducedCut*(likelihood->getNumUsedFilters()-3))
					continue; 				    																								
				
				// don't believe the prior estimation above a redshift of PRIOR_MAX_Z
				//if (z[likelihood->getBestZi()]>PRIOR_MAX_Z)
				//	continue;
						
				numEntries++;	
				
				// fill the maximum likelihood number count array
				// just needed in first iteration since it does not change
				if (numIter==0)
					numberCountMLBestT[likelihood->getBestZi()]++;
					
				like=likelihood->getLikelihoodBestT();
			
				// calculate weighted likelihood (normalization)	
				// note the extra factor of dz, this stems from the facts
				// that we a) want pdfs which are normalized (integration)
				// and b) the red shift bin size is not constant				
				prior_norm=0;				
				data1->set(numZ,z,priorBestT);
				data2->set(numZ,z,like);
				(*data1)*data2;
				prior_norm=data1->integrate();												
				
				#if 0
				cout << "Entry " << i << " of total=" << likelihood->getNumEntries() << " good=" << numEntries 
					 << " prior_norm=" << prior_norm << endl;
				#endif
				
				// calculate eta
				for (int zi=0;zi<numZ;zi++) 
					eta[zi]+=like[zi]/prior_norm;											
			}
			
			// normalize eta
				for (int zi=0;zi<numZ;zi++) 
					eta[zi]/=numEntries;	
			
			if (SAVEETA==1) {
				sprintf(s,"%sBestT_ETA_l%d.dat",priorfilename,numIter);
				saveEtaBestT(s,eta);										
			}
			
			// update prior			
			for (int zi=0;zi<numZ;zi++) {
				if (INVERSEETA==0)					
					priorBestT[zi]*=eta[zi];
				else
					priorBestT[zi]/=eta[zi];
				if (priorBestT[zi]!=priorBestT[zi]) 
					WARNING("NAN in prior calculation encountered");				
			}
			
			// smoothing prior
			if (smoothPrior)
				this->smoothingPriorBestT();								
				
			// calculating eta
			etaAbsDiff=0;
			etaRMS=0;	
			etaDiff=0;	
			for (int zi=0;zi<numZ;zi++) {
				etaRMS+=(eta[zi]-1)*(eta[zi]-1);
				if (fabs(eta[zi]-1)>etaAbsDiff)						
					etaDiff=eta[zi]-1;
					etaAbsDiff=fabs(etaDiff);
			}
			etaRMS=sqrt(etaRMS/numZ);	
				
			
			cout << " -- RMS(eta-1)= " << etaRMS << endl;
			cout << " -- Worst eta = " << 1+etaDiff << endl;						
								
			numIter++;															
	}
	if (numIter==maxIterations) {
		sprintf(s," -- maximum number (%d) of iterations reached -- ",maxIterations);
		out->println(s,1);
	}	
	
	#if 1
		sprintf(s,"%s_MLcountBestT.dat",priorfilename);
		saveMLBestT(s,numberCountMLBestT);
	#endif			
	
	freeArray1(eta);
	delete data1;
	delete data2;
	
	out->println(" done",1);
}

void Prior::calcPrior() 
{
	double **like,
		   **eta,
		   **eta2m1,
		   prior_norm,
		   etaRMS,
		   aux;	
	int numIter;
	char s[MAX_LENGTH];	
	double *dz,*z;
	int numEntries;
	xyData *data1, *data2;
	Chi2InvTable chi2InvTable;
	
	out->println("Calculating prior: ",1);

	dz=likelihood->getBinZ()->getdZ();
	z=likelihood->getBinZ()->getZ();
	
	allocArray2(eta,numTemplates,numZ);
	freeArray2(numberCountML,numTemplates);
	callocArray2(numberCountML,numTemplates,numZ);
	
	allocArray2(eta2m1,numTemplates,numZ);
				
	numIter=0;
	
	data1 = new xyData();
	data2 = new xyData();
	
	while (numIter<maxIterations) {
		
			sprintf(s,"Iteration: %d",numIter+1);
			out->println(s,1);
						
			if (savePriorIterative) {
			  sprintf(s,"%s_l%d.dat",priorfilename,numIter);
			  savePrior(s);
			  sprintf(s,"%sMarginT_l%d.dat",priorfilename,numIter);
			  savePriorMarginT(s);
			  sprintf(s,"%sMarginZ_l%d.dat",priorfilename,numIter);
			  savePriorMarginZ(s);
			}
			
			// reset eta										
			for (int t=0;t<numTemplates;t++) 
				for (int zi=0;zi<numZ;zi++) 
					eta[t][zi]=0;	
					
			numEntries=0;
			for (int i=0;i<likelihood->getNumEntries();i++) {
				
				// continue	with next entry if no likelihood could be calculated
				if (likelihood->calcLikelihood(i)!=0)
					continue;																				
				
				// if the fit was bad, than we do not include this fit in the
				// prior estimation 				
				//if ((likelihood->getNumUsedFilters()< MINFILTERBANDS) || 
				//	likelihood->getBest_Chi2()>chi2InvTable.getChi2(likelihood->getNumUsedFilters()-3))
				//	continue;
				
				if (likelihood->getNumUsedFilters()>3 && 
				    likelihood->getBest_Chi2() > chi2ReducedCut*(likelihood->getNumUsedFilters()-3))
					continue;
				
				// don't believe the prior estimation above a redshift of PRIOR_MAX_Z
				//if (z[likelihood->getBestZi()]>PRIOR_MAX_Z)
				//	continue;
					
				// just a test eliminate spikes (ie. to "good" fits)
				//if (likelihood->getNorm()<dz[0]*20)
				//	continue;
				
				// count the entry 
				numEntries++;
				
				// fill the maximum likelihood number count array
				// just needed in first iteration since it does not change
				if (numIter==0)
					numberCountML[likelihood->getBestT()][likelihood->getBestZi()]++;
				
				// smoothing likelihood
				//likelihood->smoothingLikelihood(smoothZ,smoothT);
				
				like=likelihood->getLikelihood();				
			
				// calculate weighted likelihood (normalization)	
				// note the extra factor of dz, this stems from the facts
				// that we a) want pdfs which are normalized (integration)
				// and b) the red shift bin size is not constant								
				prior_norm=0;
				for (int t=0;t<numTemplates;t++) {
					data1->set(numZ,z,prior[t]);
					data2->set(numZ,z,like[t]);
					(*data1)*data2;
					prior_norm+=data1->integrate();
				}																								
				
				// calculate eta										
				for (int t=0;t<numTemplates;t++) 
					for (int zi=0;zi<numZ;zi++) {
						if (like[t][zi]>0)
							eta[t][zi]+=like[t][zi]/(prior_norm);					
					}									
					
			}						
			
			// normalize eta by revelant number of entries
			if (numEntries>0) {
				for (int t=0;t<numTemplates;t++) 
					for (int zi=0;zi<numZ;zi++) 
						eta[t][zi]/=numEntries;	
			}
			
			if (savePriorIterative && SAVEETA==1) {
				sprintf(s,"%s_ETA_l%d.dat",priorfilename,numIter);
				saveEta(s,eta);
				sprintf(s,"%sMarginZ_ETA_l%d.dat",priorfilename,numIter);
			  	saveEtaMarginZ(s,eta);									
			}
									
			// update prior			
			for (int t=0;t<numTemplates;t++) 
				for (int zi=0;zi<numZ;zi++) {
					if (INVERSEETA==0)					
						prior[t][zi]*=eta[t][zi];
					else
						prior[t][zi]/=eta[t][zi];
					if (prior[t][zi]!=prior[t][zi]) 
						WARNING("NAN in prior calculation encountered");					
				}								
			
			// smoothing prior
			if (smoothPrior)
				this->smoothingPrior(numIter);			
			
			// evaluating eta										
			for (int t=0;t<numTemplates;t++) {
				for (int zi=0;zi<numZ;zi++) {					
					aux=(eta[t][zi]-1);
					eta2m1[t][zi]=aux*aux;					
				}
			}
			
			// calculate root mean square of (eta-1)			
			aux=0;
			for (int t=0;t<numTemplates;t++) {
				data1->set(numZ,z,eta2m1[t]);								
				aux+=data1->integrate();
			}	
			
			double diffZ;
			if (numZ>1) 
				diffZ=z[numZ-1]-z[0];
			else
				diffZ=1;
			etaRMS=sqrt(aux/(numTemplates*diffZ));							
			
			sprintf(s,"-- RMS(eta-1)=%g --",etaRMS);
			out->println(s,1);			
											
			numIter++;															
	}
	if (numIter==maxIterations) {
		sprintf(s," -- maximum number (%d) of iterations reached -- ",maxIterations);
		out->println(s,1);
	}	
	
	#if 1
		sprintf(s,"%s_MLcount.dat",priorfilename);
		saveML(s,numberCountML);
	#endif
	
	/* normalizing the prior */
	prior_norm=0;
	for (int t=0;t<numTemplates;t++) {
		data1->set(numZ,z,prior[t]);			
		prior_norm+=data1->integrate();
	}	
	double inv_prior_norm=1/prior_norm;
	
	for (int t=0;t<numTemplates;t++) 
		for (int zi=0;zi<numZ;zi++) 								
			prior[t][zi]*=inv_prior_norm;					
	
	delete data1;
	delete data2;
	freeArray2(eta,numTemplates);
	freeArray2(eta2m1,numTemplates);
	out->println(" done",1);
}

void Prior::smoothingPrior()
{
	smoothingPrior(0);
}

void Prior::smoothingPrior(int numIter)
{
	xyData *data;	
	double *trans_prior;
	double *templateCode;
	char s[MAX_LENGTH];
	
	double prior_norm;	
	double *z;
	double aux,aux2;
	//int indexLeft, indexRight;
	
	double *smoothing_sigma=NULL;
	double **smoothing_sigma_2D=NULL;
	
	double *aux_smoothing_sigma;
	double *zmean;
	xyData *data1;
	
	data1 = new xyData();
	
	data = new xyData();	
			
	sprintf(s,"Smoothing: Sigma in redshift direction %g, Sigma in template direction %g",this->smoothZ,this->smoothT);
	out->println(s,1);
	
	z=likelihood->getBinZ()->getZ();
	
	if (this->smoothZ>0) {
		sprintf(s,"Smoothing in redshift direction");
		out->println(s,1);		
		
		// calculate "natural smoothing", i.e. assume that stronger smoothing 
		// is needed at higher redshifts due to the fact that the likelihood error
		// is larger (should scale like err*(1+z))	
		if (smoothZMode==1) {						
		  allocArray1(smoothing_sigma,numZ);		  		 
		  for (int i=0;i<numZ;i++)
			smoothing_sigma[i]=(1+z[i])*this->smoothZ;			
		} else if (smoothZMode==2) {				  		 
		  allocArray2(smoothing_sigma_2D,numTemplates,numZ);
		  allocArray1(aux_smoothing_sigma,numZ);
		  allocArray1(zmean,numZ);
		  for (int t=0;t<numTemplates;t++) {
		  	/*			
			for (int i=0;i<numZ;i++) {
					// look for strictly smaller redshift bin that contains at least one entry
					indexLeft=leftIndexNotNull(0,numberCountML[t],i);
					// look for larger redshift bin that contains at least one entry
					indexRight=rightIndexNotNull(numZ-1,numberCountML[t],i);
					aux=(1+z[i])*this->smoothZ;
					aux2=z[indexRight]-z[indexLeft];
					if (aux<aux2)
						smoothing_sigma_2D[t][i]=aux2;
					else
						smoothing_sigma_2D[t][i]=aux;				
					cout << "smoothing_sigma_2D[" << t << "][" << i << "]=" << smoothing_sigma_2D[t][i] << endl;
			}*/
			int indexStart=0;
			int indexCount=0;
			int indexRun;
			while (indexStart<numZ) {
				// find first non-zero entry in numberCountML to the right
				indexRun=indexStart+1;
				while (indexRun<numZ && numberCountML[t][indexRun]==0) { 
					indexRun++;
				}
				if (indexRun==numZ) {
					zmean[indexCount]=(z[indexStart]+z[indexRun-1])/2.;
					aux=(1+zmean[indexCount])*this->smoothZ;
					aux2=(z[indexRun-1]-z[indexStart]);					
				} else {
					zmean[indexCount]=(z[indexStart]+z[indexRun])/2.;
					aux=(1+zmean[indexCount])*this->smoothZ;
					aux2=(z[indexRun]-z[indexStart]);					
				}				
				//cout << "indexRun=" << indexRun << "z=" << z[indexRun] << "zmean=" << zmean[indexCount] << "aux=" << aux << " aux2=" << aux2 << endl;
				if (aux<aux2)
					aux_smoothing_sigma[indexCount]=aux2;
				else
					aux_smoothing_sigma[indexCount]=aux;
				indexCount++;
				indexStart=indexRun;
			} 
			
			//cout << "indexCount=" << indexCount << endl;	
			//cout << "numZ=" << numZ << endl;
			
			if (indexCount==0) {
				aux=z[numZ-1]-z[0];
				for (int i=0;i<numZ;i++) 
					smoothing_sigma_2D[t][i]=aux;
			} else {		
				
			  // add right end to get an extra point and avoid extrapolation 
			  zmean[indexCount]=z[numZ-1];			  
			  aux=(1+zmean[indexCount])*this->smoothZ;
			  aux2=aux_smoothing_sigma[indexCount-1];
			  if (aux<aux2)
				aux_smoothing_sigma[indexCount]=aux2;
			  else
				aux_smoothing_sigma[indexCount]=aux;
			  indexCount++;
			  
			  // interpolate
			  data1->set(indexCount,zmean,aux_smoothing_sigma);
			  data1->calcYValues(numZ,z,smoothing_sigma_2D[t]);
			}
			
			sprintf(s,"%s_SmoothingSigma_l%d.dat",priorfilename,numIter);
			saveEta(s,smoothing_sigma_2D);
			//for (int i=0;i<numZ;i++) 
				//cout << "smoothing_sigma_2D[" << t << "][" << i << "]=" << smoothing_sigma_2D[t][i] << endl;
		  }
		  freeArray1(aux_smoothing_sigma);
		  freeArray1(zmean);
		}
		
		/* calculate norm of prior */
		prior_norm=0;
		for (int t=0;t<numTemplates;t++) {
			data1->set(numZ,z,prior[t]);			
			prior_norm+=data1->integrate();
		}		
		
		cout << "prior_norm before smoothing: " << prior_norm << endl;				
		
		for (int t=0;t<numTemplates;t++) {
			data->set(numZ,likelihood->getBinZ()->getZ(),prior[t]);
							
			if (smoothZMode==1) { 		 // if smoothing length is adaptive
				data->convolveGaussian(smoothing_sigma);
			} else if (smoothZMode==2) { // if smoothing length is adaptive and 
										 // template dependent -> reduce poisson noise
				data->convolveGaussian(smoothing_sigma_2D[t]);
			} else { 					 // if smoothing length is fixed
			  data->convolveGaussian(this->smoothZ);		
			}
			
			data->getCopyY(prior[t]);
		}
		
		/* calculate norm of prior */
		prior_norm=0;
		for (int t=0;t<numTemplates;t++) {
			data1->set(numZ,z,prior[t]);			
			prior_norm+=data1->integrate();
		}	
		
		cout << "prior_norm after smoothing: " << prior_norm << endl;
		
		if (smoothZMode==1)
			freeArray1(smoothing_sigma);		
		else if (smoothZMode==2)
			freeArray2(smoothing_sigma_2D,numTemplates);				
	}
	
	if (this->smoothT>0) {
		sprintf(s,"Smoothing in template direction");
		out->println(s,1);
		
		for (int zi=0;zi<numZ;zi++) {	
			// for orig-bin						
			allocArray1(trans_prior,templateContainer->getNumOrigTemplates());
			for (int t=0;t<templateContainer->getNumOrigTemplates();t++) {
				trans_prior[t]=prior[t][zi];
			}						
			allocArray1(templateCode,templateContainer->getNumOrigTemplates());		
			templateContainer->getPhysicalTemplateCode_Orig(0, templateCode);			
			data->set(templateContainer->getNumOrigTemplates(),templateCode,trans_prior);
			freeArray1(templateCode);
			data->convolveGaussian(smoothT);
			data->getCopyY(trans_prior);
			for (int t=0;t<templateContainer->getNumOrigTemplates();t++) {
				prior[t][zi]=trans_prior[t];
			}	
			freeArray1(trans_prior);
			// for the other template bins
			for (int i=0;i<templateContainer->getNumTemplateBins();i++) {
				allocArray1(trans_prior,templateContainer->getNumOrigTemplates(i+1));
				for (int t=0;t<templateContainer->getNumOrigTemplates(i+1);t++) {
					trans_prior[t]=prior[templateContainer->getMinIndex(i+1)+t][zi];
				}										
				allocArray1(templateCode,templateContainer->getNumOrigTemplates(i+1));		
				templateContainer->getPhysicalTemplateCode_Orig(i+1, templateCode);			
				data->set(templateContainer->getNumOrigTemplates(i+1),templateCode,trans_prior);
				freeArray1(templateCode);
				data->convolveGaussian(smoothT);
				data->getCopyY(trans_prior);
				for (int t=0;t<templateContainer->getNumOrigTemplates(i+1);t++) {
					prior[templateContainer->getMinIndex(i+1)+t][zi]=trans_prior[t];
				}	
				freeArray1(trans_prior);																		
			}
		}												
	} 
		
	delete data1;
		
	delete data;
	
}

void Prior::smoothingPriorBestT()
{
	xyData *data;		
	char s[MAX_LENGTH];
	
	double *smoothing_sigma=NULL;
	double *z;
	double aux,aux2;
	int indexLeft,indexRight;
	
	data = new xyData();	
		
	sprintf(s,"Smoothing: Sigma in redshift direction %g",this->smoothZ);
	out->println(s,1);
	
	 z=likelihood->getBinZ()->getZ();
	 
	if (this->smoothZ>0) {
		sprintf(s,"Smoothing in redshift direction");
		out->println(s,1);				
		data->set(numZ,likelihood->getBinZ()->getZ(),priorBestT);
		
		if (smoothZMode==1) {						
		  allocArray1(smoothing_sigma,numZ);		  		 
		  for (int i=0;i<numZ;i++)
			smoothing_sigma[i]=(1+z[i])*this->smoothZ;			
		} else if (smoothZMode==2) {		
		   allocArray1(smoothing_sigma,numZ);		    			
		   for (int i=0;i<numZ;i++) {
			 // look for strictly smaller redshift bin that contains at least one entry
			 indexLeft=leftIndexNotNull(0,numberCountMLBestT,i);
			 // look for larger redshift bin that contains at least one entry
			 indexRight=rightIndexNotNull(numZ-1,numberCountMLBestT,i);
			 aux=(1+z[i])*this->smoothZ;
			 aux2=z[indexRight]-z[indexLeft];
			 if (aux<aux2)
			   smoothing_sigma[i]=aux2;
			 else
			   smoothing_sigma[i]=aux;				
		  }
		}
				
		if (smoothZMode==1 || smoothZMode==2)
			data->convolveGaussian(smoothing_sigma);			
		else 
			data->convolveGaussian(this->smoothZ);			
		
		data->getCopyY(priorBestT);
				
		if (smoothZMode==1 || smoothZMode==2)
			freeArray1(smoothing_sigma);		
	}
			
	delete data;
	
}
void Prior::loadPrior(char *filename)
{
	PriorFile *pf;
	pf = new PriorFile(filename,likelihood->getBinZ(),numTemplates);
	
	freeArray2(prior,numTemplates);
	allocArray2(prior,numTemplates,numZ);
	pf->load(prior);
	
	delete pf;			
}

void Prior::savePrior(char *filename)
{	
	PriorFile *pf;
	pf = new PriorFile(filename,likelihood->getBinZ(),numTemplates);
	pf->save(prior);
	delete pf;
}

void Prior::loadPriorBestT(char *filename)
{
	PriorFile *pf;
	pf = new PriorFile(filename,likelihood->getBinZ(),numTemplates);
	
	freeArray1(priorBestT);
	allocArray1(priorBestT,numZ);
	pf->load1D(priorBestT);		
		
	delete pf;
}

void Prior::savePriorBestT(char *filename)
{	
	PriorFile *pf;
	pf = new PriorFile(filename,likelihood->getBinZ(),numTemplates);
	pf->save1D(priorBestT);
	delete pf;
}
void Prior::savePriorMarginT(char *filename)
{
	double *z;
	xyData *data;
	ofstream ofile;	
	
	z=likelihood->getBinZ()->getZ();	
	ofile.open(filename);
	CHECK(ofile,filename);
		
	ofile << " # template \\sum_{redshift} prior[template][redshift]" << endl;
	
	data = new xyData();
	for (int t=0;t<numTemplates;t++) {			
		data->set(numZ,z,prior[t]);		
		ofile << t << " " << data->integrate() << endl;
	}
	delete data;
		
	ofile.close();
	ofile.clear();	
}

void Prior::savePriorMarginZ(char *filename)
{
	double *z,aux;
	ofstream ofile;
	
	z=likelihood->getBinZ()->getZ();
	ofile.open(filename);
	CHECK(ofile,filename);
	
	ofile << " # redshift \\sum_{templates} prior[template][redshift]" << endl;
	for (int zi=0;zi<numZ;zi++) {
		aux=0;
		for (int t=0;t<numTemplates;t++) 
			aux+=prior[t][zi];		
		ofile << z[zi] << " " << aux << endl;	
	}	
	ofile.close();
	ofile.clear();								
}

void Prior::savePriorMarginZPercentile(char *filename)
{
	ofstream ofile;
	double *sum,*z;
	xyData *data;
	double aux,percentile,limit;
		
	ofile.open(filename,ios::app);
	CHECK(ofile, filename);
	ofile.precision(PERCENTILE_PRECISION);		
		
	allocArray1(sum,likelihood->getBinZ()->getLength());	
	z=likelihood->getBinZ()->getZ();
	
	// first calculate marginalized prior
	for (int zi=0;zi<likelihood->getBinZ()->getLength();zi++) {
		aux=0;			
		for (int t=0;t<numTemplates;t++) {									
			aux+=prior[t][zi];				
		}
		sum[zi]=aux;
			
	}
	data = new xyData();
	data->set(likelihood->getBinZ()->getLength(),z,sum);
	data->cumulate();
	
	// find percentiles
	// first percentile 0.0001
	percentile=data->getXValue(0.0001);
	ofile << percentile << " ";
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {
			limit=i*0.01;
			percentile=data->getXValue(limit);
			ofile << percentile << " ";
		}	
	// last percentile 0.9999
	percentile=data->getXValue(0.9999);
	ofile << percentile << endl;
		
	freeArray1(sum);	
	delete data;			
	
	ofile.close();
	ofile.clear();	
}

void Prior::saveMarginZPercentileSingle(ofstream & ofile,int zeroLine) 
{	
	double *z;
	xyData *data;
	double percentile,limit;		
	
	if (zeroLine==0) {		
		z=likelihood->getBinZ()->getZ();
			
		data = new xyData();
		data->set(likelihood->getBinZ()->getLength(),z,posteriorMarginZ);
		data->cumulate();
	
		// find percentiles
		// first percentile 0.0001
		percentile=data->getXValue(0.0001);
		ofile << "0.0001 " << percentile << "\n";
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {
			limit=i*0.01;
			percentile=data->getXValue(limit);
			ofile << limit << " " << percentile << "\n";
		}	
		// last percentile 0.9999
		percentile=data->getXValue(0.9999);
		ofile << "0.9999 " << percentile << endl;
		
		delete data;
		
	} else { // if posteriorMarginZ is not valid -> print line with zeros
		
		// first percentile 0.0001
		ofile << "0.0001 0.0\n";
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {			
			limit=i*0.01;
			ofile << limit << " 0.0\n";
		}
		// last percentile 0.9999
		ofile << "0.9999 0.0" << endl;
	}			
}

void Prior::saveMarginZPercentileSingle(ofstream & ofile, ofstream & ofile2, int zeroLine) 
{	
	double *z;
	xyData *data;
	double percentile,limit;		
	
	if (zeroLine==0) {		
		z=likelihood->getBinZ()->getZ();
			
		data = new xyData();
		data->set(likelihood->getBinZ()->getLength(),z,posteriorMarginZ);
		data->cumulate();
	
		// find percentiles
		// first percentile 0.0001
		percentile=data->getXValue(0.0001);
		ofile << "0.0001 " << percentile << "\n";
		ofile2 << "0.0001 " << likelihood->getBestT(data->getNearestIndex(percentile)) << "\n";
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {
			limit=i*0.01;
			percentile=data->getXValue(limit);
			ofile << limit << " " << percentile << "\n";
			ofile2 << limit << " " << likelihood->getBestT(data->getNearestIndex(percentile)) << "\n";
		}	
		// last percentile 0.9999
		percentile=data->getXValue(0.9999);
		ofile << "0.9999 " << percentile << endl;
		ofile2 << "0.9999 " << likelihood->getBestT(data->getNearestIndex(percentile)) << endl;
		
		delete data;
		
	} else { // if posteriorMarginZ is not valid -> print line with zeros
		
		// first percentile 0.0001
		ofile << "0.0001 0.0\n";
		ofile2 << "0.0001 -1\n";
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {			
			limit=i*0.01;
			ofile << limit << " 0.0\n";
			ofile2 << limit << " -1\n";
		}
		// last percentile 0.9999
		ofile << "0.9999 0.0" << endl;
		ofile2 << "0.9999 -1" << endl;
	}			
}

void Prior::saveBestTZPercentileSingle(ofstream & ofile,int zeroLine) 
{	
	double *z;
	xyData *data;
	double percentile,limit;		
	
	if (zeroLine==0) {		
		z=likelihood->getBinZ()->getZ();
			
		data = new xyData();
		data->set(likelihood->getBinZ()->getLength(),z,posteriorBestT);
		data->cumulate();
	
		// find percentiles
		// first percentile 0.0001
		percentile=data->getXValue(0.0001);
		ofile << "0.0001 " << percentile << "\n";
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {
			limit=i*0.01;
			percentile=data->getXValue(limit);
			ofile << limit << " " << percentile << "\n";
		}	
		// last percentile 0.9999
		percentile=data->getXValue(0.9999);
		ofile << "0.9999 " << percentile << endl;
		
		delete data;
		
	} else { // if posteriorMarginZ is not valid -> print line with zeros
		
		// first percentile 0.0001
		ofile << "0.0001 0.0\n";
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {			
			limit=i*0.01;
			ofile << limit << " 0.0\n";
		}
		// last percentile 0.9999
		ofile << "0.9999 0.0" << endl;
	}			
}

void Prior::saveMarginZPercentile(char *filename, int zeroLine)
{
	ofstream ofile;
	double *z;
	xyData *data;
	double percentile,limit;
		
	ofile.open(filename,ios::app);
	CHECK(ofile, filename);
	ofile.precision(PERCENTILE_PRECISION);		
	
	if (zeroLine==0) {		
		z=likelihood->getBinZ()->getZ();
			
		data = new xyData();
		data->set(likelihood->getBinZ()->getLength(),z,posteriorMarginZ);
		data->cumulate();
	
		// find percentiles
		// first percentile 0.0001
		percentile=data->getXValue(0.0001);
		ofile << percentile << " ";
		
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {
			limit=i*0.01;
			percentile=data->getXValue(limit);
			ofile << percentile << " ";			
		}	
		// last percentile 0.9999
		percentile=data->getXValue(0.9999);
		ofile << percentile << endl;		
		
		delete data;
		
	} else { // if posteriorMarginZ is not valid -> print line with zeros
		
		// first percentile 0.0001
		ofile << "0.0 ";		
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {			
			ofile << "0.0 ";			
		}
		// last percentile 0.9999
		ofile << "0.0 " << endl;		
	}
	
	ofile.close();
	ofile.clear();	
	
}

void Prior::saveMarginZPercentile(char *filename_MarginZ, char *filename_BestT, int zeroLine)
{
	ofstream ofile;
	ofstream ofile2;
	double *z;
	xyData *data;
	double percentile,limit;
		
	ofile.open(filename_MarginZ,ios::app);
	CHECK(ofile, filename_MarginZ);
	ofile.precision(PERCENTILE_PRECISION);		
	
	ofile2.open(filename_BestT,ios::app);
	CHECK(ofile2, filename_BestT);	
	
	if (zeroLine==0) {		
		z=likelihood->getBinZ()->getZ();
			
		data = new xyData();
		data->set(likelihood->getBinZ()->getLength(),z,posteriorMarginZ);
		data->cumulate();
	
		// find percentiles
		// first percentile 0.0001
		percentile=data->getXValue(0.0001);		
		
		ofile << percentile << " ";
		ofile2 << likelihood->getBestT(data->getNearestIndex(percentile)) << " ";
		
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {
			limit=i*0.01;
			percentile=data->getXValue(limit);
			ofile << percentile << " ";
			ofile2 << likelihood->getBestT(data->getNearestIndex(percentile)) << " ";
		}	
		// last percentile 0.9999
		percentile=data->getXValue(0.9999);
		ofile << percentile << endl;
		ofile2 << likelihood->getBestT(data->getNearestIndex(percentile)) << endl;
		
		delete data;
		
	} else { // if posteriorMarginZ is not valid -> print line with zeros
		
		// first percentile 0.0001
		ofile << "0.0 ";
		ofile2 << "-1 ";
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {			
			ofile << "0.0 ";
			ofile2 << "-1 ";
		}
		// last percentile 0.9999
		ofile << "0.0 " << endl;
		ofile2 << "-1 " << endl;
	}
	
	ofile.close();
	ofile.clear();	
	
	ofile2.close();
	ofile2.clear();		
	
}

void Prior::saveBestTZPercentile(char *filename, int zeroLine)
{
	ofstream ofile;
	double *z;
	xyData *data;
	double percentile,limit;
		
	ofile.open(filename,ios::app);
	CHECK(ofile, filename);
	ofile.precision(PERCENTILE_PRECISION);		
	
	if (zeroLine==0) {		
		z=likelihood->getBinZ()->getZ();
			
		data = new xyData();
		data->set(likelihood->getBinZ()->getLength(),z,posteriorBestT);
		data->cumulate();
	
		// find percentiles
		// first percentile 0.0001
		percentile=data->getXValue(0.0001);
		ofile << percentile << " ";
		
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {
			limit=i*0.01;
			percentile=data->getXValue(limit);
			ofile << percentile << " ";			
		}	
		// last percentile 0.9999
		percentile=data->getXValue(0.9999);
		ofile << percentile << endl;		
		
		delete data;
		
	} else { // if posteriorMarginZ is not valid -> print line with zeros
		
		// first percentile 0.0001
		ofile << "0.0 ";		
		// from 0.01 to 0.99
		for (int i=1;i<100;i++) {			
			ofile << "0.0 ";			
		}
		// last percentile 0.9999
		ofile << "0.0 " << endl;		
	}
	
	ofile.close();
	ofile.clear();	
	
}
void Prior::saveEta(char *filename,double **eta)
{
	double *z;
	ofstream ofile;
	
	z=likelihood->getBinZ()->getZ();
	ofile.open(filename);
	CHECK(ofile,filename);
	
	ofile << " # template redshift  eta[template][redshift]" << endl;
	for (int t=0;t<numTemplates;t++) 
		for (int zi=0;zi<numZ;zi++) 
			ofile << t << " " << z[zi] << " " << eta[t][zi] << endl;				
	ofile.close();
	ofile.clear();								
}

void Prior::saveML(char *filename,int **f)
{
	double *z;
	ofstream ofile;
	
	z=likelihood->getBinZ()->getZ();
	ofile.open(filename);
	CHECK(ofile,filename);
	
	ofile << " # template redshift  numberCount[template][redshift]" << endl;
	for (int t=0;t<numTemplates;t++) 
		for (int zi=0;zi<numZ;zi++) 
			ofile << t << " " << z[zi] << " " << f[t][zi] << endl;				
	ofile.close();
	ofile.clear();								
}
void Prior::saveEtaBestT(char *filename,double *eta)
{
	double *z;
	ofstream ofile;
	
	z=likelihood->getBinZ()->getZ();
	ofile.open(filename);
	CHECK(ofile,filename);
	
	ofile << " # redshift  eta[best_template][redshift]" << endl; 
	for (int zi=0;zi<numZ;zi++) 
		ofile << z[zi] << " " << eta[zi] << endl;				
	ofile.close();
	ofile.clear();								
}

void Prior::saveMLBestT(char *filename,int *f)
{
	double *z;
	ofstream ofile;
	
	z=likelihood->getBinZ()->getZ();
	ofile.open(filename);
	CHECK(ofile,filename);
	
	ofile << " # redshift  numberCount[best_template][redshift]" << endl; 
	for (int zi=0;zi<numZ;zi++) 
		ofile << z[zi] << " " << f[zi] << endl;				
	ofile.close();
	ofile.clear();								
}

void Prior::saveEtaMarginZ(char *filename,double **eta)
{
	double *z,aux;
	ofstream ofile;
	
	z=likelihood->getBinZ()->getZ();
	ofile.open(filename);
	CHECK(ofile,filename);
	
	ofile << " # redshift \\sum_{templates} eta[template][redshift]" << endl;
	for (int zi=0;zi<numZ;zi++) {
		aux=0;
		for (int t=0;t<numTemplates;t++) 
			aux+=eta[t][zi];		
		ofile << z[zi] << " " << aux << endl;	
	}	
	ofile.close();
	ofile.clear();								
}

int Prior::calcPosterior(int entryIndex)
{	
	xyData *data;	
	int failed;
	double aux,
		   **like,
		   i_posterior_norm;
	double *z=likelihood->getBinZ()->getZ();
	xyData *data1,*data2;
	
	failed=likelihood->calcLikelihood(entryIndex);				
	
	if (failed)
		return failed;
			
	like=likelihood->getLikelihood();
	data1 = new xyData();
	data2 = new xyData();
										
	// calculate inverse of norm of 2D-posterior					
	i_posterior_norm=0;
	for (int t=0;t<numTemplates;t++) {
		data1->set(numZ,z,prior[t]);
		data2->set(numZ,z,like[t]);
		(*data1)*data2;
		i_posterior_norm+=data1->integrate();
	}
	i_posterior_norm=1./i_posterior_norm;			
	
	delete data1;
	delete data2;
	
	// calculate 2D-posterior					
	for (int t=0;t<numTemplates;t++) {
		for (int zi=0;zi<numZ;zi++) {
			posterior[t][zi]=prior[t][zi]*like[t][zi]*i_posterior_norm;		
		}
	}			
	
	// calculate posterior marginalized over types	
	for (int zi=0;zi<numZ;zi++) 
		posteriorMarginZ[zi]=0.;
	for (int t=0;t<numTemplates;t++) {
		for (int zi=0;zi<numZ;zi++) {	
			posteriorMarginZ[zi]+=posterior[t][zi];
		}
	}	

	// calculate posterior marginalized over redshift
	data = new xyData();	
	for (int t=0;t<numTemplates;t++) {			
		data->set(numZ,z,posterior[t]);
		posteriorMarginT[t]=data->integrate();
	}
	delete data;				
	
	// find redshift-template pair with highest posterior
	aux=0.;
	best_t=-1;
	best_zi=-1;
	for (int t=0;t<numTemplates;t++) {
		for (int zi=0;zi<numZ;zi++) {
			if (aux<posterior[t][zi])	{
				aux=posterior[t][zi];
				best_t=t;
				best_zi=zi;
			}
		}
	}	
	
	// find best redshift z of marginalized posterior and,
	// keeping z fixed, the template type with highest posterior
	aux=0.;
	bestMarginZ_zi=-1;
	for (int zi=0;zi<numZ;zi++) {
		if (aux<posteriorMarginZ[zi]) { 
			aux=posteriorMarginZ[zi];	
			bestMarginZ_zi=zi;
		}
	}
	if (bestMarginZ_zi==-1)
		return 1;
	
	aux=0.;
	bestMarginZ_t=-1;
	for (int t=0;t<numTemplates;t++) {
		if (aux<posterior[t][bestMarginZ_zi]) {
			 aux=posterior[t][bestMarginZ_zi];
			 bestMarginZ_t=t;
		}
	}
	
	// find best template type t of marginalized likelihood and,
	// keeping t fixed, the redshift with highest posterior
	aux=0.;
	bestMarginT_t=-1;
	for (int t=0;t<numTemplates;t++) {
		if (aux<posteriorMarginT[t]) {
			 aux=posteriorMarginT[t];
			 bestMarginT_t=t;
		}
	}
	if (bestMarginT_t==-1)
		return 1;
	
	aux=0.;	
	bestMarginT_zi=-1;	
	for (int zi=0;zi<numZ;zi++) {
		if (aux<posterior[bestMarginT_t][zi]) {
			 aux=posterior[bestMarginT_t][zi];
			 bestMarginT_zi=zi;
		}
	}		
	
	if (best_t==-1 || best_zi==-1 || bestMarginZ_t==-1 || bestMarginT_zi==-1)
		return 1;
	
	return 0;
}

void Prior::calcPosteriorErrors()
{
	double aux;			
	int aux_index;
	double aux_integ;
	double *z=likelihood->getBinZ()->getZ();
	xyData * data = new xyData();			
	int numZ = likelihood->getBinZ()->getLength();
	
	//------------ errors for redshift ----------------------------
	
	if (numZ<1)
		EXIT("Need at least one point in redshift grid to calculate errors for redshift estimate");		
	
	double zmin=z[0];
	double zmax=z[numZ-1];

	//------- errors using constant chi2 boundaries	(not implemented and would also make no sense;
	//        It would be worth, however, to implement conf. intervals from constant posterior regions)
	
	zmin_68=zmin2_68=zmin_95=zmin2_95=-1;
	zmax_68=zmax2_68=zmax_95=zmax2_95=-1;
		
	//------ using 0.68 and 0.95 percentiles of redshift as errors
	
	// over marginalized posterior (which is per def. normalized)
	data->set(numZ,z,posteriorMarginZ);	
															
	aux_index=0; // use this to speed up the percentile calculation (reuse of index)
	aux_integ=0; // use this to speed up the percentile calculation (reuse of integral)																
	
	// NB. -> don't change the order of the percentile calculation	
	int percentile=data->getPercentileLow(0.95,1.,aux_index,aux_integ);
	if (percentile==-1)
		zminmz_95=zmin;
	else
		zminmz_95=z[percentile];
	
	percentile=data->getPercentileLow(0.68,1.,aux_index,aux_integ);
	if (percentile==-1)
		zminmz_68=zmin;
	else
		zminmz_68=z[percentile];											
		
	aux_index=numZ-1;
	aux_integ=0; 					
		
	percentile=data->getPercentileHigh(0.95,1.,aux_index,aux_integ);
	if (percentile==-1)
		zmaxmz_95=zmax;
	else
		zmaxmz_95=z[percentile];	
	
	percentile=data->getPercentileHigh(0.68,1.,aux_index,aux_integ);
	if (percentile==-1)
		zmaxmz_68=zmax;
	else
		zmaxmz_68=z[percentile];				
		
	// over posterior of best (after marginalization) template type
	data->set(numZ,z,posterior[bestMarginT_t]);
	
	// note that posterior[bestMarginT_t] is not normalized to 1 after integration over z
	// but the result of this integration is posteriorMarginT[bestMarginT_t]
	// -> have to normalize it -> give norm as parameter to percentile function
	
	aux=posteriorMarginT[bestMarginT_t]; // normalization of posterior
	aux_index=0; // use this to speed up the percentile calculation (reuse of index)
	aux_integ=0; // use this to speed up the percentile calculation (reuse of integral)																				
	
	percentile=data->getPercentileLow(0.95,aux,aux_index,aux_integ);
	if (percentile==-1)
		zminmt_95=zmin;
	else
		zminmt_95=z[percentile];
	
	percentile=data->getPercentileLow(0.68,aux,aux_index,aux_integ);
	if (percentile==-1)
		zminmt_68=zmin;
	else
		zminmt_68=z[percentile];													
	
	aux_index=numZ-1;
	aux_integ=0; 								
	
	percentile=data->getPercentileHigh(0.95,aux,aux_index,aux_integ);
	if (percentile==-1)
		zmaxmt_95=zmax;
	else
		zmaxmt_95=z[percentile];		
		
	percentile=data->getPercentileHigh(0.68,aux,aux_index,aux_integ);
	if (percentile==-1)
		zmaxmt_68=zmax;
	else
		zmaxmt_68=z[percentile];	
																												
	//----------- errors for template types -----------------------	
		
	double Tmin=templateContainer->getMinTemplateCode(templateContainer->binOfTemplate(best_t));
	double Tmax=templateContainer->getMaxTemplateCode(templateContainer->binOfTemplate(best_t));
	double *templateCode;	
	int numTempBin=templateContainer->getNumOrigTemplates(templateContainer->binOfTemplate(best_t));		
	int tempBinMinIndex=templateContainer->getMinIndex(templateContainer->binOfTemplate(best_t));
	
	allocArray1(templateCode,numTempBin);	
	for (int i=0;i<numTempBin;i++)
		templateCode[i]=i;
			
	//------- errors using constant chi2 boundaries	
	
	Tmin_68=Tmin2_68=Tmin_95=Tmin2_95=-1;
	Tmax_68=Tmax2_68=Tmax_95=Tmax2_95=-1;						

	//------ using 0.68 and 0.95 percentiles of template type as errors
	
	// over marginalized posterior function (which is normalized over ALL templates, but
	// we consider only the templates within the given template bin)
	data->set(numTempBin,templateCode,posteriorMarginT+tempBinMinIndex);
	aux=0;
	for (int i=tempBinMinIndex;i<tempBinMinIndex+numTempBin;i++) {
		aux+=posteriorMarginT[i]; // normalization of posterior
	}
															
	aux_index=0; // use this to speed up the percentile calculation (reuse of index)
	aux_integ=0; // use this to speed up the percentile calculation (reuse of integral)																
	
	// NB. -> don't change the order of the percentile calculation	
	percentile=data->getPercentileLow(0.95,aux,aux_index,aux_integ);
	if (percentile==-1)
		Tminmt_95=Tmin;
	else
		Tminmt_95=templateCode[percentile]+Tmin;
	
	percentile=data->getPercentileLow(0.68,aux,aux_index,aux_integ);
	if (percentile==-1)
		Tminmt_68=Tmin;
	else
		Tminmt_68=templateCode[percentile]+Tmin;											
		
	aux_index=numTempBin-1;
	aux_integ=0;		
		
	percentile=data->getPercentileHigh(0.95,aux,aux_index,aux_integ);
	if (percentile==-1)
		Tmaxmt_95=Tmax;
	else
		Tmaxmt_95=templateCode[percentile]+Tmin;	
	
	percentile=data->getPercentileHigh(0.68,aux,aux_index,aux_integ);
	if (percentile==-1)
		Tmaxmt_68=Tmax;
	else
		Tmaxmt_68=templateCode[percentile]+Tmin;;				
		
	// over posterior of best (after marginalization) redshift
	double *posteriorBestZProjT;
	allocArray1(posteriorBestZProjT,numTempBin);
	aux=0;
	for (int i=0;i<numTempBin;i++) {
		aux+=posteriorBestZProjT[i]=posterior[i][bestMarginZ_zi];
	}		
	data->set(numTempBin,templateCode,posteriorBestZProjT);
	freeArray1(posteriorBestZProjT);			
	
	aux_index=0; // use this to speed up the percentile calculation (reuse of index)
	aux_integ=0; // use this to speed up the percentile calculation (reuse of integral)																				
	
	percentile=data->getPercentileLow(0.95,aux,aux_index,aux_integ);
	if (percentile==-1)
		Tminmz_95=Tmin;
	else
		Tminmz_95=templateCode[percentile]+Tmin;
	
	percentile=data->getPercentileLow(0.68,aux,aux_index,aux_integ);
	if (percentile==-1)
		Tminmz_68=Tmin;
	else
		Tminmz_68=templateCode[percentile]+Tmin;													
	
	aux_index=numTempBin-1;
	aux_integ=0; 								
	
	percentile=data->getPercentileHigh(0.95,aux,aux_index,aux_integ);
	if (percentile==-1)
		Tmaxmz_95=Tmax;
	else
		Tmaxmz_95=templateCode[percentile]+Tmin;		
		
	percentile=data->getPercentileHigh(0.68,aux,aux_index,aux_integ);
	if (percentile==-1)
		Tmaxmz_68=Tmax;
	else
		Tmaxmz_68=templateCode[percentile]+Tmin;	
		  
	delete data;	
	freeArray1(templateCode);
}	

void Prior::printPhotoZHeader(ofstream & ofile)
{
	ofile << "# Output of " << PHOTOZ << " version " << VERSION << endl;
	ofile << "# For questions refer to " << REFERENCE << endl;
	ofile << "#" << endl;	
	ofile << "# Explanation of the columns:" << endl;
	
	ofile << "# z_cat ... redshift given in the catalog (e.g. a spectroscopic redshift)" << endl;	
	ofile << "# usedFilters .. number of used filter bands" << endl;	
	
	ofile << "# z_phot ... redshift corresponding to maximum posterior" << endl;
	ofile << "# T_phot ... template type corresponding to maximum posterior" << endl;
	ofile << "# a ... template normalization factor" << endl;
	ofile << "# chi^2 ... chi2^2 of (z_phot,T_phot), (not necessarily the minimum chi^2 value)" << endl;
	ofile << "# M_B ... calculated absolute magnitude in the B band using calculated T_phot,z_phot" << endl;	
	ofile << "# D_L ... luminosity distance at z_phot in Mpc" << endl;

	ofile << "# z_photz ... redshift corresponding to maximum of marginalized (over type) posterior" << endl;	
	ofile << "# T_photz ... best fitting template type given z_photmz" << endl;			
	ofile << "# az ... template normalization factor corresponding to (z_photmz,T_photmz)" << endl;
	ofile << "# chi^2z ... chi2^2 of (z_photmz,T_photmz)" << endl;
	ofile << "# M_Bz ... calculated absolute magnitude in the B band using calculated T_photmz,z_photmz" << endl;	
	ofile << "# D_Lz ... luminosity distance at z_photmz in Mpc" << endl;
	
	ofile << "# z_phott ... best fitting redshift given T_photmt" << endl;	
	ofile << "# T_phott ... template type corresponding to maximum of marginalized (over redshift) posterior" << endl;			
	ofile << "# at ... template normalization factor corresponding to (z_photmt,T_photmt)" << endl;
	ofile << "# chi^2t ... chi2^2 of (z_photm,T_photm)" << endl;
	ofile << "# M_Bt ... calculated absolute magnitude in the B band using calculated T_photm,z_photm" << endl;	
	ofile << "# D_Lt ... luminosity distance at z_photm in Mpc" << endl;
	
	ofile << "# z_ph2_L0.68 z_ph2_H0.68 ... not supported yet" << endl;
	ofile << "# z_ph2_L0.95 z_ph2_H0.95 ... not supported yet" << endl;
	
	ofile << "# z_ph_L0.68 z_ph_H0.68 ... not supported" << endl;
	ofile << "# z_ph_L0.95 z_ph_H0.95 ... not supported" << endl;
	
	ofile << "# z_phz_L0.68 z_phz_H0.68 ... limits of confidence area (percentiles corresponding to conf. level 0.68) of marginalized redshift estimate" << endl;
	ofile << "# z_phz_L0.95 z_phz_H0.95 ... limits of confidence area (percentiles corresponding to conf. level 0.95) of marginalized redshift estimate" << endl;
	
	ofile << "# z_pht_L0.68 z_pht_H0.68 ... limits of confidence area (percentiles corresponding to conf. level 0.68) of redshift using best (marginalized) type" << endl;
	ofile << "# z_pht_L0.95 z_pht_H0.95 ... limits of confidence area (percentiles corresponding to conf. level 0.95) of redshift using best (marginalized) type" << endl;
	
	ofile << "# T_ph2_L0.68 T_ph2_H0.68 ... not supported yet" << endl;
	ofile << "# T_ph2_L0.95 T_ph2_H0.95 ... not supported yet" << endl;
	
	ofile << "# T_ph_L0.68 T_ph_H0.68 ... not supported yet" << endl;
	ofile << "# T_ph_L0.95 T_ph_H0.95 ... not supported yet" << endl;
	
	ofile << "# T_phz_L0.68 T_phz_H0.68 ... limits of confidence area (percentiles corresponding to conf. level 0.68) of marginalized type estimate" << endl;
	ofile << "# T_phz_L0.95 T_phz_H0.95 ... limits of confidence area (percentiles corresponding to conf. level 0.95) of marginalized type estimate" << endl;
	
	ofile << "# T_pht_L0.68 T_pht_H0.68 ... limits of confidence area (percentiles corresponding to conf. level 0.68) of type using best (marginalized) redshift" << endl;
	ofile << "# T_pht_L0.95 T_pht_H0.95 ... limits of confidence area (percentiles corresponding to conf. level 0.95) of type using best (marginalized) redshift" << endl;	
	
	ofile << "#";
	
	ofile << setw(MYSETW-1) << "zcat";
	ofile << setw(MYSETW) << "usedFilters";
	ofile << setw(MYSETW) << "z_phot";
	ofile << setw(MYSETW) << "T_phot";
	ofile << setw(MYSETW) << "a";
	ofile << setw(MYSETW) << "chi2^2";
	ofile << setw(MYSETW) << "M_B";
	ofile << setw(MYSETW) << "D_L";
	
	ofile << setw(MYSETW) << "z_photz";
	ofile << setw(MYSETW) << "T_photz";
	ofile << setw(MYSETW) << "az";
	ofile << setw(MYSETW) << "chi2^2z";
	ofile << setw(MYSETW) << "M_Bz";
	ofile << setw(MYSETW) << "D_Lz";
	
	ofile << setw(MYSETW) << "z_phott";
	ofile << setw(MYSETW) << "T_phott";
	ofile << setw(MYSETW) << "at";
	ofile << setw(MYSETW) << "chi2^2t";
	ofile << setw(MYSETW) << "M_Bt";
	ofile << setw(MYSETW) << "D_Lt";
	
	ofile << setw(MYSETW) << "z_ph2_L0.68";
	ofile << setw(MYSETW) << "z_ph2_H0.68";
	ofile << setw(MYSETW) << "z_ph2_L0.95";
	ofile << setw(MYSETW) << "z_ph2_H0.95";
	ofile << setw(MYSETW) << "z_ph_L0.68";
	ofile << setw(MYSETW) << "z_ph_H0.68";
	ofile << setw(MYSETW) << "z_ph_L0.95";
	ofile << setw(MYSETW) << "z_ph_H0.95";
	
	ofile << setw(MYSETW) << "z_phz_L0.68";
	ofile << setw(MYSETW) << "z_phz_H0.68";
	ofile << setw(MYSETW) << "z_phz_L0.95";
	ofile << setw(MYSETW) << "z_phz_H0.95";
	ofile << setw(MYSETW) << "z_pht_L0.68";
	ofile << setw(MYSETW) << "z_pht_H0.68";
	ofile << setw(MYSETW) << "z_pht_L0.95";
	ofile << setw(MYSETW) << "z_pht_H0.95";
	
	ofile << setw(MYSETW) << "T_ph2_L0.68";
	ofile << setw(MYSETW) << "T_ph2_H0.68";
	ofile << setw(MYSETW) << "T_ph2_L0.95";
	ofile << setw(MYSETW) << "T_ph2_H0.95";
	ofile << setw(MYSETW) << "T_ph_L0.68";
	ofile << setw(MYSETW) << "T_ph_H0.68";
	ofile << setw(MYSETW) << "T_ph_L0.95";
	ofile << setw(MYSETW) << "T_ph_H0.95";
	
	ofile << setw(MYSETW) << "T_phz_L0.68";
	ofile << setw(MYSETW) << "T_phz_H0.68";
	ofile << setw(MYSETW) << "T_phz_L0.95";
	ofile << setw(MYSETW) << "T_phz_H0.95";
	ofile << setw(MYSETW) << "T_pht_L0.68";
	ofile << setw(MYSETW) << "T_pht_H0.68";
	ofile << setw(MYSETW) << "T_pht_L0.95";
	ofile << setw(MYSETW) << "T_pht_H0.95";
	
	ofile << endl;		
}

void Prior::printPhotoZ(ofstream & ofile,int *printPost, int single)
{
	
	int numEntries=likelihood->getNumEntries();
	int failed;
	ofstream ofilePost,ofilePostT,ofilePostZ,ofilePostPZ,ofilePostPZT;
	ofstream ofilePostS,ofilePostTS,ofilePostZS,ofilePostPZS,ofilePostPZTS;
	char s[MAX_LENGTH];		
	
	double *z=likelihood->getBinZ()->getZ();	
	double *templatesIndex;
	
	out->print("Calculating photometric red shifts: ",1);			
		
	if (! single) {
		if (printPost[0]) {					
			ofilePost.open(filenamePost);
			CHECK(ofilePost,filenamePost);	
			ofilePost << "# This files encodes the posterior in the following way:" << endl;
			ofilePost << "# The file consists of blocks separated by a blank line." << endl;
			ofilePost << "# Each block corresponds to one data entry from the catalog." << endl;
			ofilePost << "# Each block consists of several lines. Each line corresponds to the posterior for a fixed template." << endl;
			ofilePost << "# Each line consists of several columns. Each column corresponds to the posterior at a certain red shift." << endl;
			ofilePost << "# posterior[template][redshift]=P(template,redshift|fluxes of entry)" << endl;		
		}
		
		if (printPost[1]) {
			ofilePostT.open(filenamePostT);
			CHECK(ofilePostT,filenamePostT);	
			ofilePostT << "# This files encodes the posterior marginalized over red shifts in the following way:" << endl;		
			ofilePostT << "# Each line corresponds to one data entry from the catalog." << endl;
			ofilePostT << "# Each line consists of several columns. Each column corresponds to the posterior at a certain template type." << endl;
			ofilePostT << "# posterior[template]=P(template|fluxes of entry)" << endl;		
		}
		
		if (printPost[2]) {
			ofilePostZ.open(filenamePostZ);
			CHECK(ofilePostZ,filenamePostZ);	
			ofilePostZ << "# This files encodes the posterior marginalized over template types in the following way:" << endl;		
			ofilePostZ << "# Each line corresponds to one data entry from the catalog." << endl;
			ofilePostZ << "# Each line consists of several columns. Each column corresponds to the posterior at a certain red shift." << endl;
			ofilePostZ << "# posterior[redshift]=P(redshift|fluxes of entry)" << endl;		
		}				
		
		if (printPost[3]) {
			if (printPost[4]) {
				ofilePostPZT.open(filenamePostPZT);
				CHECK(ofilePostPZT,filenamePostPZT);
				ofilePostPZT << "# Each entry corresponds to the best-fit template associated with the respective redshift in the percentile file" << endl;
				ofilePostPZT << "# each line corresponds to a galaxy, look into the marginalized-posterior percentile file for more information" << endl;				
				ofilePostPZT.close();
				ofilePostPZT.clear();						
			}
			ofilePostPZ.open(filenamePostPZ);
			CHECK(ofilePostPZ,filenamePostPZ);
			ofilePostPZ << "# The posterior[redshift] = \\sum_{templates} posterior[template][redshift] normalized to one" << endl;		
			ofilePostPZ << "# each line corresponds to a galaxy, the percentiles are given as 0.0001, 0.01, 0.02, ..., 0.98, 0.99, 0.9999" << endl;
			ofilePostPZ.close();
			ofilePostPZ.clear();
		}								
	}			
	
	allocArray1(templatesIndex,numTemplates);
	for (int t=0;t<numTemplates;t++) {
		templatesIndex[t]=t;
	}
	
	for (int i=0;i<numEntries;i++) {							
				
		if (single) {
			if (printPost[0]) {
				sprintf(s,"%s_l%d.dat",filenamePost,i);
				ofilePostS.open(s);
				CHECK(ofilePostS,s);
			}		
		
			if (printPost[1]) {
				sprintf(s,"%s_l%d.dat",filenamePostT,i);
				ofilePostTS.open(s);
				CHECK(ofilePostTS,s);
			}	
		
			if (printPost[2]) {
				sprintf(s,"%s_l%d.dat",filenamePostZ,i);
				ofilePostZS.open(s);
				CHECK(ofilePostZS,s);
			}
			
			if (printPost[3]) {
				if (printPost[4]) {
					sprintf(s,"%s_l%d.dat",filenamePostPZT,i);
					ofilePostPZTS.open(s);
					CHECK(ofilePostPZTS,s);
				}					
				sprintf(s,"%s_l%d.dat",filenamePostPZ,i);
				ofilePostPZS.open(s);
				CHECK(ofilePostPZS,s);
			}									
		}
										
		if ((i+1)%1000==0) {
			if ((i+1)%10000==0) 
				sprintf(s,"%d",i+1);
			else
				sprintf(s,".");
			out->print(s,1);
		}				
		
		failed=calcPosterior(i);
						
		if (failed==0) {
			
			// printing additional output files if specified
			if (single) {
				if (printPost[0]) 	{	// printPost
					fprintfGrid2D(ofilePostS,numTemplates,templatesIndex,numZ,z,posterior,0);
					ofilePostS.close();
					ofilePostS.clear();
				}
				if (printPost[1]) {		// printPostT
					fprintfGrid1D(ofilePostTS,numTemplates,templatesIndex,posteriorMarginT,0);
					ofilePostTS.close();
					ofilePostTS.clear();
				}
				if (printPost[2]) {		// printPostZ
					fprintfGrid1D(ofilePostZS,numZ,z,posteriorMarginZ,0);
					ofilePostZS.close();
					ofilePostZS.clear();					
				}
				if (printPost[3]) {		// printPostPZ
					if (printPost[4]) {		// printPostPZT and printPostPZ
						saveMarginZPercentileSingle(ofilePostPZS,ofilePostPZTS,0);
						ofilePostPZTS.close();
						ofilePostPZTS.clear();							
						ofilePostPZS.close();
						ofilePostPZS.clear();	
					} else {				// only printPostPZ
						saveMarginZPercentileSingle(ofilePostPZS,0);	
						ofilePostPZS.close();
						ofilePostPZS.clear();			
					}
				}				
			} else {
				if (printPost[0])
					fprintfArray2D(ofilePost,numTemplates,numZ,posterior,0);
				if (printPost[1])
					fprintfArray1D(ofilePostT,numTemplates,posteriorMarginT,0);
				if (printPost[2])
					fprintfArray1D(ofilePostZ,numZ,posteriorMarginZ,0);
				if (printPost[3]) {
					if (printPost[4]) {
						saveMarginZPercentile(filenamePostPZ,filenamePostPZT,0);
					} else {
						saveMarginZPercentile(filenamePostPZ,0);					
					}
				}
			}			
							
			calcPosteriorErrors();	
					
			if (likelihood->hasSpecZ()) 
				ofile << setprecision(6) << setw(MYSETW) << likelihood->getSpecZ();
			else 
				ofile << setw(MYSETW) << "-1";
			ofile << setw(MYSETW) << likelihood->getNumUsedFilters();		
					
			fprintfGroup(ofile, best_zi, best_t,0);
			fprintfGroup(ofile, bestMarginZ_zi, bestMarginZ_t,0);
			fprintfGroup(ofile, bestMarginT_zi, bestMarginT_t,0);				
			
			fprintfErrGroup(ofile, zmin2_68, zmax2_68, zmin2_95, zmax2_95);
			fprintfErrGroup(ofile, zmin_68, zmax_68, zmin_95, zmax_95);
			fprintfErrGroup(ofile, zminmz_68, zmaxmz_68, zminmz_95, zmaxmz_95);
			fprintfErrGroup(ofile, zminmt_68, zmaxmt_68, zminmt_95, zmaxmt_95);
			
			fprintfErrGroup(ofile, Tmin2_68, Tmax2_68, Tmin2_95, Tmax2_95);
			fprintfErrGroup(ofile, Tmin_68, Tmax_68, Tmin_95, Tmax_95);
			fprintfErrGroup(ofile, Tminmz_68, Tmaxmz_68, Tminmz_95, Tmaxmz_95);
			fprintfErrGroup(ofile, Tminmt_68, Tmaxmt_68, Tminmt_95, Tmaxmt_95);							
			
			ofile << "\n";		
						
		} else {
			
			if (single) { // let the file be empty
				if (printPost[0]) {
					ofilePostS.close();
					ofilePostS.clear();
				}
				if (printPost[1]) {
					ofilePostTS.close();
					ofilePostTS.clear();
				}
				if (printPost[2]) {
					ofilePostZS.close();
					ofilePostZS.clear();
				}	
				if (printPost[3]) {
					if (printPost[4]) {
						ofilePostPZTS.close();
						ofilePostPZTS.clear();	
					}
					ofilePostPZS.close();
					ofilePostPZS.clear();						
				}
			} else { // print lines with zeros (to preserve number of lines!)
				
				if (printPost[0]) 
					fprintfArray2D(ofilePost,numTemplates,numZ,posterior,1);
				if (printPost[1])
					fprintfArray1D(ofilePostT,numTemplates,posteriorMarginT,1);
				if (printPost[2])
					fprintfArray1D(ofilePostZ,numZ,posteriorMarginZ,1);
				if (printPost[3]) { 
					if (printPost[4]) {
						saveMarginZPercentile(filenamePostPZ,filenamePostPZT,1);	
					} else {
						saveMarginZPercentile(filenamePostPZ,1);					
					}
				}				
			}
			
			if (likelihood->hasSpecZ()) 
				ofile << setprecision(6) << setw(MYSETW) << likelihood->getSpecZ();
			else 
				ofile << setw(MYSETW) << "-1";
			ofile << setw(MYSETW) << likelihood->getNumUsedFilters();						
		
			// 3*6+8*4=50 plus 1 for U-V_color
			for (int i=0;i<51;i++) {
				ofile << setprecision(6) << setw(MYSETW) << "-1";
			}				
		
			ofile << "\n";
									
		}																		
	}	
	
	freeArray1(templatesIndex);
	
	if (printPost[0] && ofilePost) {
		ofilePost.close();
		ofilePost.clear();
	}
	if (printPost[1] && ofilePostT) {
		ofilePostT.close();
		ofilePostT.clear();
	}
	if (printPost[2] && ofilePostZ) {
		ofilePostZ.close();
		ofilePostZ.clear();
	}
	if (printPost[3] && ofilePostPZ) {
		if (printPost[4] && ofilePostPZT) {
			ofilePostPZT.close();
			ofilePostPZT.clear();			
		}		
		ofilePostPZ.close();
		ofilePostPZ.clear();
	}
}	

int Prior::calcPosteriorBestT(int entryIndex)
{		
	int failed;
	double aux,
		   *like,
		   i_posterior_norm;
	double *z=likelihood->getBinZ()->getZ();
	xyData *data1,*data2;
	
	failed=likelihood->calcLikelihood(entryIndex);				
	
	if (failed)
		return failed;
		
	like=likelihood->getLikelihoodBestT();
	best_t=likelihood->getBestT();	
	if (best_t<0)
		return 1;

	data1 = new xyData();
	data2 = new xyData();
										
	// calculate inverse of norm of 2D-posterior					
	data1->set(numZ,z,priorBestT);
	data2->set(numZ,z,like);
	(*data1)*data2;
	i_posterior_norm=1./data1->integrate();
	
	delete data1;
	delete data2;
				
	// calculate posterior						
	for (int zi=0;zi<numZ;zi++) {
		posteriorBestT[zi]=priorBestT[zi]*like[zi]*i_posterior_norm;				
	}
				
	// find redshift with highest posterior
	aux=0.;
	best_zi=-1;
	for (int zi=0;zi<numZ;zi++) {
		if (aux<posteriorBestT[zi])	{
			aux=posteriorBestT[zi];	
			best_zi=zi;
		}
	}
		
	if (best_t<0 || best_zi<0)
		return 1;
	
	return 0;
}

void Prior::calcPosteriorErrorsBestT()
{
	int percentile;	
	int aux_index;
	double aux_integ;
	double *z=likelihood->getBinZ()->getZ();
	xyData * data = new xyData();			
	int numZ = likelihood->getBinZ()->getLength();
	
	//------------ errors for redshift ----------------------------
	
	if (numZ<1)
		EXIT("Need at least one point in redshift grid to calculate errors for redshift estimate");		
	
	double zmin=z[0];
	double zmax=z[numZ-1];

	//------- errors using constant chi2 boundaries	(not implemented and would also make no sense;
	//        It would be worth, however, to implement conf. intervals from constant posterior regions)
	
	zmin_68=zmin2_68=zmin_95=zmin2_95=-1;
	zmax_68=zmax2_68=zmax_95=zmax2_95=-1;
		
	//------ using 0.68 and 0.95 percentiles of redshift as errors
	
	zminmz_95=zminmz_68=zmaxmz_95=zmaxmz_68=-1;
						
	// over posterior "bestT" of best template type (minimal chi2)
	data->set(numZ,z,posteriorBestT);
	
	// note that posterior[bestMarginT_t] is not normalized to 1 after integration over z
	// but the result of this integration is posteriorMarginT[bestMarginT_t]
	// -> have to normalize it -> give norm as parameter to percentile function
		
	aux_index=0; // use this to speed up the percentile calculation (reuse of index)
	aux_integ=0; // use this to speed up the percentile calculation (reuse of integral)																				
	
	percentile=data->getPercentileLow(0.95,1.0,aux_index,aux_integ);
	if (percentile==-1)
		zminmt_95=zmin;
	else
		zminmt_95=z[percentile];
	
	percentile=data->getPercentileLow(0.68,1.0,aux_index,aux_integ);
	if (percentile==-1)
		zminmt_68=zmin;
	else
		zminmt_68=z[percentile];													
	
	aux_index=numZ-1;
	aux_integ=0; 								
	
	percentile=data->getPercentileHigh(0.95,1.0,aux_index,aux_integ);
	if (percentile==-1)
		zmaxmt_95=zmax;
	else
		zmaxmt_95=z[percentile];		
		
	percentile=data->getPercentileHigh(0.68,1.0,aux_index,aux_integ);
	if (percentile==-1)
		zmaxmt_68=zmax;
	else
		zmaxmt_68=z[percentile];	
																												
	//----------- errors for template types -----------------------								
			
	//------- errors using constant chi2 boundaries	
	
	Tmin_68=Tmin2_68=Tmin_95=Tmin2_95=best_t;
	Tmax_68=Tmax2_68=Tmax_95=Tmax2_95=best_t;				

	//------ using 0.68 and 0.95 percentiles of template type as errors
	
	Tminmt_68=Tminmz_68=Tminmt_95=Tminmz_95=best_t;
	Tmaxmt_68=Tmaxmz_68=Tmaxmt_95=Tmaxmz_95=best_t;			
		  
	delete data;	
}	


void Prior::printPhotoZHeaderBestT(ofstream & ofile)
{
	ofile << "# Output of " << PHOTOZ << " version " << VERSION << endl;
	ofile << "# For questions refer to " << REFERENCE << endl;
	ofile << "#" << endl;	
	ofile << "# Explanation of the columns:" << endl;
	
	ofile << "# z_cat ... redshift given in the catalog (e.g. a spectroscopic redshift)" << endl;	
	ofile << "# usedFilters .. number of used filter bands" << endl;	
	
	ofile << "# z_phot ... redshift corresponding to largest posterior given the template with smallest chi2" << endl;
	ofile << "# T_phot ... the template with smallest chi2" << endl;
	ofile << "# a ... template normalization factor" << endl;
	ofile << "# chi^2 ... chi2^2 of (z_phot,T_phot), (not necessarily the minimum chi^2 value)" << endl;
	ofile << "# M_B ... calculated absolute magnitude in the B band using calculated T_phot,z_phot" << endl;	
	ofile << "# D_L ... luminosity distance at z_phot in Mpc" << endl;

	ofile << "# z_photz ... not supported" << endl;	
	ofile << "# T_photz ... not supported" << endl;			
	ofile << "# az ... not supported" << endl;
	ofile << "# chi^2z ... not supported" << endl;
	ofile << "# M_Bz ... not supported" << endl;	
	ofile << "# D_Lz ... not supported" << endl;
	
	ofile << "# z_phott ... not supported" << endl;	
	ofile << "# T_phott ... not supported" << endl;			
	ofile << "# at ... not supported" << endl;
	ofile << "# chi^2t ... not supported" << endl;
	ofile << "# M_Bt ... not supported" << endl;	
	ofile << "# D_Lt ... not supported" << endl;
	
	ofile << "# z_ph2_L0.68 z_ph2_H0.68 ... not supported yet" << endl;
	ofile << "# z_ph2_L0.95 z_ph2_H0.95 ... not supported yet" << endl;
	
	ofile << "# z_ph_L0.68 z_ph_H0.68 ... not supported" << endl;
	ofile << "# z_ph_L0.95 z_ph_H0.95 ... not supported" << endl;
	
	ofile << "# z_phz_L0.68 z_phz_H0.68 ... not supported" << endl;
	ofile << "# z_phz_L0.95 z_phz_H0.95 ... not supported" << endl;
	
	ofile << "# z_pht_L0.68 z_pht_H0.68 ... limits of confidence area (percentiles corresponding to conf. level 0.68) of redshift using best chi2 type" << endl;
	ofile << "# z_pht_L0.95 z_pht_H0.95 ... limits of confidence area (percentiles corresponding to conf. level 0.95) of redshift using best chi2 type" << endl;
	
	ofile << "# T_ph2_L0.68 T_ph2_H0.68 ... not supported" << endl;
	ofile << "# T_ph2_L0.95 T_ph2_H0.95 ... not supported" << endl;
	
	ofile << "# T_ph_L0.68 T_ph_H0.68 ... not supported" << endl;
	ofile << "# T_ph_L0.95 T_ph_H0.95 ... not supported" << endl;
	
	ofile << "# T_phz_L0.68 T_phz_H0.68 ... not supported" << endl;
	ofile << "# T_phz_L0.95 T_phz_H0.95 ... not supported" << endl;
	
	ofile << "# T_pht_L0.68 T_pht_H0.68 ... not supported" << endl;
	ofile << "# T_pht_L0.95 T_pht_H0.95 ... not supported" << endl;
	
	ofile << "#";
	
	ofile << setw(MYSETW-1) << "zcat";
	ofile << setw(MYSETW) << "usedFilters";
	ofile << setw(MYSETW) << "z_phot";
	ofile << setw(MYSETW) << "T_phot";
	ofile << setw(MYSETW) << "a";
	ofile << setw(MYSETW) << "chi2^2";
	ofile << setw(MYSETW) << "M_B";
	ofile << setw(MYSETW) << "D_L";
	
	ofile << setw(MYSETW) << "z_photz";
	ofile << setw(MYSETW) << "T_photz";
	ofile << setw(MYSETW) << "az";
	ofile << setw(MYSETW) << "chi2^2z";
	ofile << setw(MYSETW) << "M_Bz";
	ofile << setw(MYSETW) << "D_Lz";
	
	ofile << setw(MYSETW) << "z_phott";
	ofile << setw(MYSETW) << "T_phott";
	ofile << setw(MYSETW) << "at";
	ofile << setw(MYSETW) << "chi2^2t";
	ofile << setw(MYSETW) << "M_Bt";
	ofile << setw(MYSETW) << "D_Lt";
	
	ofile << setw(MYSETW) << "z_ph2_L0.68";
	ofile << setw(MYSETW) << "z_ph2_H0.68";
	ofile << setw(MYSETW) << "z_ph2_L0.95";
	ofile << setw(MYSETW) << "z_ph2_H0.95";
	ofile << setw(MYSETW) << "z_ph_L0.68";
	ofile << setw(MYSETW) << "z_ph_H0.68";
	ofile << setw(MYSETW) << "z_ph_L0.95";
	ofile << setw(MYSETW) << "z_ph_H0.95";
	
	ofile << setw(MYSETW) << "z_phz_L0.68";
	ofile << setw(MYSETW) << "z_phz_H0.68";
	ofile << setw(MYSETW) << "z_phz_L0.95";
	ofile << setw(MYSETW) << "z_phz_H0.95";
	ofile << setw(MYSETW) << "z_pht_L0.68";
	ofile << setw(MYSETW) << "z_pht_H0.68";
	ofile << setw(MYSETW) << "z_pht_L0.95";
	ofile << setw(MYSETW) << "z_pht_H0.95";
	
	ofile << setw(MYSETW) << "T_ph2_L0.68";
	ofile << setw(MYSETW) << "T_ph2_H0.68";
	ofile << setw(MYSETW) << "T_ph2_L0.95";
	ofile << setw(MYSETW) << "T_ph2_H0.95";
	ofile << setw(MYSETW) << "T_ph_L0.68";
	ofile << setw(MYSETW) << "T_ph_H0.68";
	ofile << setw(MYSETW) << "T_ph_L0.95";
	ofile << setw(MYSETW) << "T_ph_H0.95";
	
	ofile << setw(MYSETW) << "T_phz_L0.68";
	ofile << setw(MYSETW) << "T_phz_H0.68";
	ofile << setw(MYSETW) << "T_phz_L0.95";
	ofile << setw(MYSETW) << "T_phz_H0.95";
	ofile << setw(MYSETW) << "T_pht_L0.68";
	ofile << setw(MYSETW) << "T_pht_H0.68";
	ofile << setw(MYSETW) << "T_pht_L0.95";
	ofile << setw(MYSETW) << "T_pht_H0.95";
	
	ofile << endl;		
}

void Prior::printPhotoZBestT(ofstream & ofile, int printPostZ, int printPostPZ, int single)
{		
	int numEntries=likelihood->getNumEntries();
	int failed;
	ofstream ofilePostZ;
	ofstream ofilePostZS;
	ofstream ofilePostPZ;
	ofstream ofilePostPZS;	
	char s[MAX_LENGTH];		
	double *z=likelihood->getBinZ()->getZ();
		
	allocArray1(posterior,this->numZ);
	
	out->print("Calculating photometric red shifts: ",1);	
	
	if (! single) {
		if (printPostZ) {		
			ofilePostZ.open(filenamePostZ);
			CHECK(ofilePostZ,filenamePostZ);
			ofilePostZ << "# This files encodes the posterior in the following way:" << endl;		
			ofilePostZ << "# Each line corresponds to one data entry from the catalog." << endl;
			ofilePostZ << "# Each line consists of several columns. Each column corresponds to the posterior at a certain red shift." << endl;
			ofilePostZ << "# posterior[redshift]=P(redshift|fluxes of entry)" << endl;						
		}
		if (printPostPZ) {
			ofilePostPZ.open(filenamePostPZ);
			CHECK(ofilePostPZ,filenamePostPZ);
			ofilePostPZ << "# The posterior[redshift]" << endl;		
			ofilePostPZ << "# each line corresponds to a galaxy, the percentiles are given as 0.0001, 0.01, 0.02, ..., 0.98, 0.99, 0.9999" << endl;
			ofilePostPZ.close();
			ofilePostPZ.clear();
		}
	}
	
	for (int i=0;i<numEntries;i++) {							
				
		if (single) {									
		
			if (printPostZ) {
				sprintf(s,"%s_l%d.dat",filenamePostZ,i);
				ofilePostZS.open(s);
				CHECK(ofilePostZS,s);
			}
			
			if (printPostPZ) {
				sprintf(s,"%s_l%d.dat",filenamePostPZ,i);
				ofilePostPZS.open(s);
				CHECK(ofilePostPZS,s);
			}			
		}
										
		if ((i+1)%1000==0) {
			if ((i+1)%10000==0) 
				sprintf(s,"%d",i+1);
			else
				sprintf(s,".");
			out->print(s,1);
		}				
		
		failed=calcPosteriorBestT(i);
						
		if (failed==0) {
			
			// printing additional output files if specified
			if (single) {
				
				if (printPostZ) {
					fprintfGrid1D(ofilePostZS,numZ,z,posteriorBestT,0);
					ofilePostZS.close();
					ofilePostZS.clear();					
				}
				if (printPostPZ) {
					saveBestTZPercentileSingle(ofilePostPZS,0);
					ofilePostPZS.close();
					ofilePostPZS.clear();			
				}
				
			} else {				
				if (printPostZ)
					fprintfArray1D(ofilePostZ,numZ,posteriorBestT,0);
				if (printPostPZ) 
					saveBestTZPercentile(filenamePostPZ, 0);							
			}			
							
			calcPosteriorErrorsBestT();		
					
			if (likelihood->hasSpecZ()) 
				ofile << setprecision(6) << setw(MYSETW) << likelihood->getSpecZ();
			else 
				ofile << setw(MYSETW) << "-1";
			ofile << setw(MYSETW) << likelihood->getNumUsedFilters();		
					
			fprintfGroup(ofile, best_zi, best_t,0);
			fprintfGroup(ofile, bestMarginZ_zi, bestMarginZ_t,1);
			fprintfGroup(ofile, bestMarginT_zi, bestMarginT_t,1);				
			
			fprintfErrGroup(ofile, zmin2_68, zmax2_68, zmin2_95, zmax2_95);
			fprintfErrGroup(ofile, zmin_68, zmax_68, zmin_95, zmax_95);
			fprintfErrGroup(ofile, zminmz_68, zmaxmz_68, zminmz_95, zmaxmz_95);
			fprintfErrGroup(ofile, zminmt_68, zmaxmt_68, zminmt_95, zmaxmt_95);
			
			fprintfErrGroup(ofile, Tmin2_68, Tmax2_68, Tmin2_95, Tmax2_95);
			fprintfErrGroup(ofile, Tmin_68, Tmax_68, Tmin_95, Tmax_95);
			fprintfErrGroup(ofile, Tminmz_68, Tmaxmz_68, Tminmz_95, Tmaxmz_95);
			fprintfErrGroup(ofile, Tminmt_68, Tmaxmt_68, Tminmt_95, Tmaxmt_95);		
			
			ofile << "\n";		
						
		} else {
			
			if (single) { // let the file be empty				
				if (printPostZ) {
					ofilePostZS.close();
					ofilePostZS.clear();
				}	
				if (printPostPZ) {
					ofilePostPZS.close();
					ofilePostPZS.clear();						
				}
			} else { // print lines with zeros (to preserve number of lines!)
							
				if (printPostZ)
					fprintfArray1D(ofilePostZ,numZ,posteriorBestT,1);
				if (printPostPZ) 
					saveBestTZPercentile(filenamePostPZ,1);					
			}
			
			if (likelihood->hasSpecZ()) 
				ofile << setprecision(6) << setw(MYSETW) << likelihood->getSpecZ();
			else 
				ofile << setw(MYSETW) << "-1";
			ofile << setw(MYSETW) << likelihood->getNumUsedFilters();						
		
			// 3*6+8*4=50
			for (int i=0;i<50;i++) {
				ofile << setprecision(6) << setw(MYSETW) << "-1";
			}				
		
			ofile << "\n";
									
		}						
			
	}	
			
	if (printPostZ && ofilePostZ) {
		ofilePostZ.close();
		ofilePostZ.clear();
	}
	if (printPostPZ && ofilePostPZ) {
		ofilePostPZ.close();
		ofilePostPZ.clear();
	}
}	
