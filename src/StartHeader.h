#ifndef STARTHEADER_H_
#define STARTHEADER_H_

#include "globHeader.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

/* prints a header when ZEBRA is started */

class StartHeader
{
public:
	StartHeader(){};
	virtual ~StartHeader(){};
	void printHeader() {
		cout << "*************************************************************************" << endl;
		cout << "*              _                                                        *" << endl;       
		cout << "*             | |                                                       *" << endl;
	 	cout << "*  _____ _____| |__   ____ _____                                        *" << endl;
		cout <<	"* (___  ) ___ |  _ \\ / ___|____ |          by Robert Feldmann           *" << endl;
		cout << "*  / __/| ____| |_) ) |   / ___ |          written 2005-13              *" << endl;
		cout << "* (_____)_____)____/|_|   \\_____|                                       *" << endl;
		cout << "*                                                                       *" << endl;
		cout << "*                                          Institute of Astronomy       *" << endl;
		cout << "* version " << VERSION <<"            Department of Physics        *" << endl;		
		cout << "*                                              ______________  __       *" << endl;
		cout << "*                                             / ______  __  / / /       *" << endl;
		cout << "* Contributors: --contrib                    / __/   / / / /_/ /        *" << endl;
		cout << "* Help / options: --help                    / /___  / / / __  /         *" << endl;
		cout << "*                                          /_____/ /_/ /_/ /_/          *" << endl;
		cout << "*                                          Zurich, Switzerland          *" << endl;
		cout << "*************************************************************************" << endl;
	}
};

#endif /*STARTHEADER_H_*/
