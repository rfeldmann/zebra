#include <iostream>
#include "BayesSolver.h"
#include "CommandLineHandler.h"
#include "globHeader.h"
#include "StartHeader.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

using namespace std;

int main(int argc, char **argv)
{		
	OutputHandler *out = new OutputHandler();
	CommandLineHandler *clh=new CommandLineHandler(argc,argv);
	
	clh->setOutputHandler(out);	
	
	clh->readCommandLine();		
					
	// create BayesSolver
	BayesSolver *bayes = new BayesSolver(clh);
	bayes->setOutputHandler(out);
	
	if (clh->getImproveTemplate())	
		bayes->improveTemplates();	
	else
		bayes->calcPhotoZ();
			
	
	if (clh->getVerboseLevel()>0) {		
		cout << "***********************" << endl;
		cout << "* " << PHOTOZ << " has finished  *" << endl;
		cout << "***********************" << endl;
	}
		
	delete bayes;
	delete clh;	
	delete out;
}
