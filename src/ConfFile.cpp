#include "ConfFile.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

ConfFile::ConfFile(const char* filename)
{
	this->filename=(char*)malloc((strlen(filename)+1)*sizeof(char));
	strcpy(this->filename,filename);
	m_loaded=0;
	entries=NULL;
	entrynumber=NULL;
	linenumber=0;	
	headerentries=NULL;
	headerlinenumber=0;
	entryType=NULL;	
	m_out=NULL;
	totallinenumber=0;
	totalheaderlinenumber=0;
	totalcolnumber=0;
}

ConfFile::~ConfFile()
{	
	freeArray1(this->filename);	
	
	resize();
		
	closeFileHandle();
}

void ConfFile::resize()
{	
	for (int i=0;i<this->linenumber;i++) {
		if (entrynumber)	
			freeArray2(entries[i],entrynumber[i]);
	}
	freeArray1(this->entries);
	
	freeArray1(this->entrynumber);
	
	for (int i=0;i<this->headerlinenumber;i++)
		freeArray1(this->headerentries[i]);
	freeArray1(this->headerentries);
	
	freeArray1(this->entryType);
	
	linenumber=0;
	colnumber=0;
	headerlinenumber=0;
	
	m_loaded=0;
}

void ConfFile::read()
{
	ifstream infile;
	int linenumber,linenumber2,
		headerlinenumber,
		colnumber,
		entryNr;
	unsigned int maxl;	
	char s[MAX_LENGTH];
			
	resize();
	totallinenumber=0;
	totalheaderlinenumber=0;
	totalcolnumber=0;
			
	infile.open(this->filename);		
	if (! infile.good())
	{
		sprintf(s,"Could not open file %s",filename);
		EXIT(s);
	}
	
	std::string str;	
	linenumber=0;
	headerlinenumber=0;	
	while (infile.good()) {
		getline(infile,str);		
		if (str.find('#')==string::npos) {	
			vector<string> splitline; 			
			entryNr=SplitString(str, " \t\r", splitline);	
			if (entryNr!=0)  { // not an empty line
			  linenumber++;
			}	 
		} else { // header line
			headerlinenumber++;			 
		}
	}				
			
	allocArray1(this->entrynumber,linenumber);
	allocArray1(this->entries,linenumber);	
	allocArray1(this->headerentries,headerlinenumber);		
	
	infile.clear();
	infile.close();
		
	this->linenumber=linenumber;
	this->headerlinenumber=headerlinenumber;
	this->totallinenumber=linenumber;
	this->totalheaderlinenumber=headerlinenumber;
	
	infile.open(this->filename);		
	if (! infile.good())
	{
		sprintf(s,"Could not open file %s",filename);
		EXIT(s);
	}	
	colnumber=0;
	linenumber2=0;
	headerlinenumber=0;
	while (infile.good())	{
		getline(infile,str);		
		if (str.find('#')==string::npos) { // not a comment line			
			vector<string> splitline; 			
			entryNr=SplitString(str, " \t\r", splitline);
			
			#if 0
			for (vector<string>::iterator vi=splitline.begin(); vi!=splitline.end();vi++) {
				cout << *vi << endl;
			}	
			#endif
					
			if (entryNr!=0)  {  // not emptyline						
			
				if (entryNr>colnumber) colnumber=entryNr;			
				this->entrynumber[linenumber2]=entryNr;
			
				maxl=0;
				for (int i=0;i<entryNr;i++) {
					if ( splitline[i].length()>maxl )
						maxl=splitline[i].length();
				}								
			
				allocArray2(this->entries[linenumber2],entryNr,maxl+1);
		
				for (int i=0;i<entryNr;i++) {					
					strcpy(this->entries[linenumber2][i],splitline[i].c_str());
																   
					#if 0
						cout << "entries[" << linenumber2 << "][" << i
							 << "]=" << entries[linenumber2][i] << endl;
					#endif
		
				}								
				linenumber2++;
			} 
		}	else { 						// comment line found						
			
			allocArray1(this->headerentries[headerlinenumber],str.length()+1);
			strcpy(this->headerentries[headerlinenumber],str.c_str());
			headerlinenumber++;									
		}			
	}		
	
	if (linenumber != linenumber2) {
	  sprintf(s,"Problem occured while reading file %s ",this->filename);
	  EXIT(s);			
	}
	
	this->colnumber=colnumber;		
	this->totalcolnumber=colnumber;
	
	printCurrentStatus();
	
	infile.close();
	infile.clear();		
	m_loaded=1;
}

void ConfFile::closeFileHandle() {
	if (infileFrac.is_open()) {		
		infileFrac.close();
		infileFrac.clear();
	}	
	totallinenumber=0;
	totalheaderlinenumber=0;
	totalcolnumber=0;
}

void ConfFile::readFraction(int bunchlines)
{				
	closeFileHandle();
	readFractionContinue(bunchlines);		
}

void ConfFile::readFractionContinue(int bunchlines)
{	
	int linenumber,
		headerlinenumber,
		colnumber,
		entryNr;
	unsigned int maxl;	
	char s[MAX_LENGTH];
	std::string str;		
	
	resize();
	
	if (! infileFrac.is_open()) 	
		infileFrac.open(this->filename);		
	if (! infileFrac.good())
	{
		sprintf(s,"Could not open file %s / corrupt file handle",filename);
		EXIT(s);
	}		
		
	allocArray1(this->entrynumber,bunchlines);	
	allocArray1(this->entries,bunchlines);		
	allocArray1(this->headerentries,bunchlines);	
	allocArray1(this->entryType,bunchlines);
					
	colnumber=0;
	linenumber=0;
	headerlinenumber=0;
			
	while (infileFrac.good() && linenumber+headerlinenumber < bunchlines)	{
		getline(infileFrac,str);
		if (str.find('#')==string::npos) { // not a comment line			
			vector<string> splitline; 			
			entryNr=SplitString(str, " \t\r", splitline);
			
			#if 0
			for (vector<string>::iterator vi=splitline.begin(); vi!=splitline.end();vi++) {
				cout << *vi << endl;
			}	
			#endif
					
			if (entryNr!=0)  {  // not emptyline						
			
				if (entryNr>colnumber) colnumber=entryNr;			
				this->entrynumber[linenumber]=entryNr;
			
				maxl=0;
				for (int i=0;i<entryNr;i++) {
					if ( splitline[i].length()>maxl )
						maxl=splitline[i].length();
				}								
			
				allocArray2(this->entries[linenumber],entryNr,maxl+1);
		
				for (int i=0;i<entryNr;i++) {					
					strcpy(this->entries[linenumber][i],splitline[i].c_str());
																   
					#if 0
						cout << "entries[" << linenumber << "][" << i
							 << "]=" << entries[linenumber][i] << endl;
					#endif
		
				}			
				entryType[linenumber+headerlinenumber]=0;					
				linenumber++;				
			}					
			
		}	else { 						// comment line found						
			
			allocArray1(this->headerentries[headerlinenumber],str.length()+1);
			strcpy(this->headerentries[headerlinenumber],str.c_str());
			entryType[linenumber+headerlinenumber]=1;
			headerlinenumber++;
		}			
	}		
	
	this->linenumber=linenumber;
	this->headerlinenumber=headerlinenumber;
	this->colnumber=colnumber;
	
	this->totallinenumber+=linenumber;
	this->totalheaderlinenumber+=headerlinenumber;
	if (this->totalcolnumber<colnumber)
		this->totalcolnumber=colnumber;
	
	printCurrentStatus();		 	
	m_loaded=1;
}

void ConfFile::printCurrentStatus() {
	char s[MAX_LENGTH];
	if (m_out) {
		sprintf(s,"Read bunch of %s: %d line(s) (+%d header line(s)) with <= %d column(s)",
				filename,linenumber,headerlinenumber,colnumber);				
		m_out->println(s,2);
	}	
}

void ConfFile::printTotalStatus() {
	char s[MAX_LENGTH];
	if (m_out) {
		sprintf(s,"> Read %s: %d line(s) (+%d header line(s)) with <= %d column(s) <",
				filename,totallinenumber,totalheaderlinenumber,totalcolnumber);				
		m_out->println(s,1);			 		
	}	
}

int ConfFile::readComplete() {
	if (infileFrac.good())
		return 0;
	return 1;
}

void ConfFile::getQuickLineColnumber(const char *filename, int & linenumber, int & colnumber) 
{	
	ifstream infile;	
	int entryNr;
	std::string str;
	char s[MAX_LENGTH];	
	
	infile.open(this->filename);		
	if (! infile.good())
	{
		sprintf(s,"Could not open file %s",filename);
		EXIT(s);
	}
	
	linenumber=0;			
	colnumber=0;
	
	while (infile.good())	{
		getline(infile,str);
		if (str.find('#')==string::npos) { // not a comment line			
			vector<string> splitline; 			
			entryNr=SplitString(str, " \t\r", splitline);						
					
			if (entryNr!=0)  {  // not emptyline							
				if (entryNr>colnumber) colnumber=entryNr;																											   												
				linenumber++;
			}
		}		
	}		
	
	infile.close();
	infile.clear();		
}

void ConfFile::write()
{
	write("");	
}

void ConfFile::write(const char *headerline)
{
	ofstream ofile;	
	char s[MAX_LENGTH];
			
	if (! m_loaded)		
		return;
				
	ofile.open(this->filename);
	CHECK(ofile, this->filename);
	
	// print first header content
	for (int i=0;i<headerlinenumber;i++) {		
		ofile << headerentries[i] << endl;
	}
	
	// print now additional header line given as parameter
	if (headerline)
		ofile << headerline << endl;
	//ofile << "#template_name used z_min z_max" << endl;
	
	// print entries
	for (int i=0;i<linenumber;i++) {		
		for (int j=0;j<entrynumber[i];j++)
			ofile << entries[i][j] << " ";
		ofile << endl;
	}
	
	if (m_out) {
		sprintf(s,"Written %s: %d line(s) (%d header line(s)) with %d column(s)",
				filename,linenumber,headerlinenumber,colnumber);
		m_out->println(s,1);			 		
	}	
					
	ofile.close();	
	ofile.clear();		
}

void ConfFile::saveAppend(char *filename)
{
	int linenumber=0;
	int headerlinenumber=0;
	
	ofstream ofile;
	ofile.open(filename,ios::app);
	CHECK(ofile,filename);
	
	for (int i=0;i<this->linenumber+this->headerlinenumber;i++) {
		if (entryType[i]==0) { // normal entry
			for (int j=0;j<entrynumber[linenumber];j++)
				ofile << entries[linenumber][j] << " ";
			ofile << endl;
			linenumber++;
		} else { 				// header line
			ofile << headerentries[headerlinenumber] << endl;
			headerlinenumber++;
		}
	}
	
	ofile.close();
	ofile.clear();
}

void ConfFile::setEntries(char *** entries,int * entrynumber, 
						  int linenumber, char ** headerentries, int headerlinenumber)
{
	unsigned int maxl;
	resize();
	
	// 1. about entries	
	this->linenumber=linenumber;		
	allocArray1(this->entrynumber,linenumber);
	allocArray1(this->entries,linenumber);
	
	// determine maximal string length;
	maxl=0;
	for (int i=0;i<linenumber;i++) {	
		for (int j=0;j<entrynumber[i];j++) {	
			if (maxl<strlen(entries[i][j]))
				maxl=strlen(entries[i][j]);
		}
	}
	maxl++;
	
	// fill entries
	for (int i=0;i<linenumber;i++) {		
		this->entrynumber[i]=entrynumber[i];
		allocArray2(this->entries[i],entrynumber[i],maxl);	
		for (int j=0;j<entrynumber[i];j++) {			
			strcpy(this->entries[i][j],entries[i][j]);
		}		
		if (this->colnumber < entrynumber[i])
			this->colnumber=entrynumber[i];
	}			
	
	// 2. about header lines	
	this->headerlinenumber=headerlinenumber;
	allocArray1(this->headerentries,headerlinenumber);	
	
	// determine maximal string length	
	maxl=0;
	for (int i=0;i<linenumber;i++) {			
		if (maxl<strlen(headerentries[i]))
			maxl=strlen(headerentries[i]);		
	}
	maxl++;
	
	// fill header lines	
	for (int i=0;i<linenumber;i++) {		
		allocArray1(this->headerentries[i],maxl);			
		strcpy(this->headerentries[i],headerentries[i]);						
	}
	
	this->m_loaded=1;
}	

void ConfFile::getEntries(char *** & entries, int * & entrynumber, 
						  int & linenumber)
{
	entries=this->entries;
	entrynumber=this->entrynumber;
	linenumber=this->linenumber;
}	

void ConfFile::getHeaderEntries(char ** & headerentries, int & headerlinenumber )
{
	headerentries=this->headerentries;
	headerlinenumber=this->headerlinenumber;	
}
