#ifndef LIKELIHOOD_H_
#define LIKELIHOOD_H_

#include <sys/times.h>
#include <iomanip>
#include "NumericalData.h"
#include "Template.h"
#include "Catalog.h"
#include "globHeader.h"
#include "FluxFile.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

class Likelihood
{
private:

	int entryIndex;
	BinningZ *binZ;
#ifdef PARRAY	
	double ***flux; 
#else
	double *flux;
#endif	
	double *flux_entries;
	double *flux_errors;
	char *flux_invalid;	
	double **f_OT;
	double **f_TT;	
	double f_OO;
	int *active;	// whether filter is valid -> incorporates flux_invalid information
	double *residuals;
	double *residual_errors; // defined as "magnitude errors"
	FluxRestriction * fluxRestriction;
	
	TemplateContainer *templateContainer;
	int templateModus;	// 1 .. use all templates (including corrected) of templateContainer, 0 ... use only original templates
	int numTemplates;
	int numFilters;
	int numEntries;
	Template **templates;
	Catalog *catalog;
	
	OutputHandler *m_out;	
	
	double *templateBFluxNormalized;  // stores the rest-frame B-flux of all templates to avoid multiple calculation
	int *filterFluxActive;            // preselection for *active array; marks filter invalid if for one or more
					  // template-redshifts pairs no flux could be calculated
					  // used only in calcLikelihoodFull
	
	double **likelihood;
	double *likelihoodMarginZ;
	double *likelihoodMarginT;
	double *likelihoodZ;
	double **chi2;
	double *chi2Z;
	double *minT_chi2;
	double *minz_chi2;
	double min_chi2;
	double** template_norm;
	double *template_normZ;
	double best_template_norm;
	int best_t;
	int bestMarginZ_t;	// best fit type corresponding to bestMarginZ_zi
	int bestMarginT_t;	// best template type of marginalized likelihood (over redshift)
	int best_zi;
	int bestMarginZ_zi;	// best redshift z of marginalized likelihood (over types)
	int bestMarginT_zi;	// best fit redshift corresponding to bestMarginT_t			
	
	double zmin2_68;
	double zmax2_68;
	double zmin2_95;
	double zmax2_95;
	double zmin_68;
	double zmax_68;
	double zmin_95;
	double zmax_95;
	double zminmz_68;
	double zmaxmz_68;
	double zminmz_95;
	double zmaxmz_95;	
	double zminmt_68;
	double zmaxmt_68;
	double zminmt_95;
	double zmaxmt_95;
	
	double Tmin2_68;
	double Tmax2_68;
	double Tmin2_95;
	double Tmax2_95;	
	double Tmin_68;
	double Tmax_68;
	double Tmin_95;
	double Tmax_95;	
	double Tminmz_68;
	double Tmaxmz_68;
	double Tminmz_95;
	double Tmaxmz_95;	
	double Tminmt_68;
	double Tmaxmt_68;
	double Tminmt_95;
	double Tmaxmt_95;	
	
#ifdef MEASTIME	 
	float rTime1,rTime2,rTime3,rTime4, rTime5,rTime6,
	      rTime7, rTime8, rTime9, rTime10, rTime11, rTime12;
	struct tms start, end;
#endif	
	
	double norm;	
	double specZ;
	int haveSpecZ;
	int invalid;
	int numUsedFilters;		
	
	int MBMode;
	int rightZMode;
	int failed;		// likelihood calculation failed (e.g. to few bands available)
	int allocated;  // whether memory for the likelihood field (z,T) and others has already been allocated
	int allocatedZ; // whether memory for the likelihood field (bestZ,T) and others has already been allocated
	double chi2Add; // addend to best chi2 if template is used out of its redshift range
	double chi2Mult;// factor to best chi2 if template is used out of its redshift range
	
	void calcErrors();
	void calcErrorsRightZ();
	void allocateArraysBoth();
	void allocateArrays();
	void allocateArraysZ();
	void freeArraysBoth();
	void freeArrays();
	void freeArraysZ();		
	
	int calcLikelihoodRightZ(int entryIndex);		
	int	calcLikelihoodRightZ(int entryIndex, double specZ);
	int calcLikelihoodFull(int entryIndex);	
	
	int leftUp(int minIndex, double * chi2z, int index) {
		
		if (index<=minIndex)
			return 0;
			
		double aux=chi2z[index];
		if (chi2z[index-1]>aux)
			return 1;
		if (chi2z[index-1]<aux)
			return 0;
		for (int i=2;i<index+1-minIndex;i++) {
			if (chi2z[index-i]>aux)
				return 1;
			if (chi2z[index-i]<aux)
				return 0;
		}
		return 0;						
	}
	
	int rightUp(int maxIndex, double * chi2z, int index) {
		
		if (index>=maxIndex)
			return 0;
		
		double aux=chi2z[index];
		if (chi2z[index+1]>aux)
			return 1;
		if (chi2z[index+1]<aux)
			return 0;
		for (int i=2;i<maxIndex-index+1;i++) {
			if (chi2z[index+i]>aux)
				return 1;
			if (chi2z[index+i]<aux)
				return 0;
		}
		return 0;						
	}
	
	void fprintfMLGroup(ofstream & ofile, int zi, int t) {		
		double mb=templates[t]->getBFluxNormalized();		
		double tn=template_norm[t][zi];				
		ofile << setprecision(6) << setw(MYSETW) << binZ->getZ(zi);
		ofile << setprecision(6) << setw(MYSETW) << t;		
		ofile << setprecision(6) << setw(MYSETW) << tn;
		ofile << setprecision(6) << setw(MYSETW) << chi2[t][zi];
		ofile << setprecision(6) << setw(MYSETW) << fluxRestriction->getAbsoluteMagB(zi,tn,mb);
		ofile << setprecision(6) << setw(MYSETW) << fluxRestriction->getDL()[zi];
	}
	
	void fprintfMLGroupZ(ofstream & ofile, int zi, int t) {		
		double mb=templates[t]->getBFluxNormalized();		
		double tn=template_normZ[t];
				
		ofile << setprecision(6) << setw(MYSETW) << binZ->getZ(best_zi);
		ofile << setprecision(6) << setw(MYSETW) << t;		
		ofile << setprecision(6) << setw(MYSETW) << tn;
		ofile << setprecision(6) << setw(MYSETW) << chi2Z[t];
		ofile << setprecision(6) << setw(MYSETW) << fluxRestriction->getAbsoluteMagB(zi,tn,mb);
		ofile << setprecision(6) << setw(MYSETW) << fluxRestriction->getDL()[zi];
	}	
	
	void fprintfMLErrGroup(ofstream & ofile, double f1, double f2, double f3, double f4) {
		ofile << setprecision(6) << setw(MYSETW) << f1;
		ofile << setprecision(6) << setw(MYSETW) << f2;
		ofile << setprecision(6) << setw(MYSETW) << f3;
		ofile << setprecision(6) << setw(MYSETW) << f4;
	}	
	
public:
	
	Likelihood();	
	virtual ~Likelihood();
	
	void resize();
#ifdef PARRAY		
	void set(double ***flux, const BinningZ *binZ, FluxRestriction * fluxRestriction,
			 Catalog* catalog,TemplateContainer *templateContainer, int templateModus);
#else
	void set(double *flux, const BinningZ *binZ, FluxRestriction * fluxRestriction,
			 Catalog* catalog,TemplateContainer *templateContainer, int templateModus);
#endif			 
			 
	int calcLikelihood(int entryIndex);					// calculates likelihood function (as function of z,T or of T, dep. on rightZMode)
	int	calcLikelihood(int entryIndex, double specZ); 	// calculates the likelihood function for given z from catalog (as function of T)		
		
	double** getLikelihood() {						
		return likelihood;
	};
	
	double* getLikelihoodBestT() {		
		
		#if 0		
		// normalize likelihood function (not necessary, just for checking poss. problem sources)
		xyData *data = new xyData();
		double testnorm=0;
		data->set(binZ->getLength(),binZ->getZ(),likelihood[best_t]);
		testnorm=data->integrate();		
		delete data;										
		for (int zi=0;zi<binZ->getLength();zi++) {							
			likelihood[best_t][zi]/=testnorm;			
		}		
		#endif
		
		return likelihood[best_t];		
	}
	
	double getNorm() { return norm; }
	
	int getNumZ() { return binZ->getLength(); };
	int getNumTemplates() { return numTemplates; };
	int getNumEntries() { return numEntries; };
	
	BinningZ *getBinZ() { return binZ; };
	FluxRestriction *getFluxRestriction() { return fluxRestriction; };
	double **getTemplateNorm() { return template_norm; };
	double *getTemplateNormZ() { return template_normZ; };
	Template **getTemplates() { return templates; };	
	
	
	double *getResiduals() {
		return residuals;
	}
	
	double *getResidualErrors() {
		for (int i=0;i<numFilters;i++) {
			residual_errors[i]=2.5*log10(1+flux_errors[i]/flux_entries[i]);
		}
		return residual_errors;
	}		
	
	double *getFluxEntries() {
		return flux_entries;
	}
	
	double *getFluxErrors() {
		return flux_errors;
	}
	
	int *getActive() {
		return active;
	}		
	
	int getBestT(int zi) {
		// find template which fits best for given redshift
		// this could also be directly implemented into the main likelihood calculation
		// but sofar this function is seldom used and would only increase the usual 
		// calculation time
		
		// loop over all templates
		int myBestT=0;
		double myBestChi2=DBL_MAX;
		for (int t=0;t<numTemplates;t++) {
			if (chi2[t][zi]<myBestChi2) {
				myBestChi2=chi2[t][zi];
				myBestT=t;
			}
		}
		return myBestT;
	}
	
	int getBestT() {			
		return best_t;
	};
	int getBestZi() { 	
		return best_zi; 
	};
	double getBest_Chi2() { 		
		return min_chi2; 
	};
	double *getChi2Z() {
		return chi2Z;
	}
	double **getChi2() {
		return chi2;
	}
	
	double getBestTemplateNorm() {	
		return best_template_norm;	
		
	}
	int getInvalid() { 		
		return invalid; 
	};
	
	int getNumUsedFilters() { 							
		return numUsedFilters; 
	};
	
	int hasSpecZ() {
		return haveSpecZ;
	};
		
	double getSpecZ() {						
		return specZ;		
	};	
	
	int getRightZMode() {
		return rightZMode;
	}
	
	void smoothingLikelihood(double smoothZ, double smoothT);
	
	void getCollBest(double &coll_best_like, int & coll_best_zi ) {		
		double aux;
			
		coll_best_zi=0;
		coll_best_like=-1;	
		for (int zi=0;zi<binZ->getLength();zi++) {
			aux=0;
			for (int t=0;t<numTemplates;t++) 
				aux+=likelihood[t][zi];								
			if (aux > coll_best_like) {
				coll_best_like=aux;
				coll_best_zi=zi;				
			}
		}	
	}
	
	void setMBMode(int MBMode) { 
		this->MBMode = MBMode; 
	};
	
	void setRightZMode(int rightZMode) {
		this->rightZMode=rightZMode;
	}
	
	void setOutputHandler(OutputHandler *out) {
		m_out=out;
	}
	
	void setChi2Add(double chi2Add) {
		this->chi2Add=chi2Add;
	}
	
	void setChi2Mult(double chi2Mult) {
		this->chi2Mult=chi2Mult;
	}
	
	
	void printLikelihood(char *filename);
	void printLikelihoodHeader(char *filename);
	void printLikelihoodSingle(char *filename);
	void printLikelihoodMarginT(char *filename);
	void printLikelihoodMarginTRightZ(char *filename);
	void printLikelihoodMarginTHeader(char *filename);
	void printLikelihoodMarginTSingle(char *filename);
	void printLikelihoodMarginZ(char *filename);
	void printLikelihoodMarginZHeader(char *filename);
	void printLikelihoodMarginZSingle(char *filename);
	void printLikelihoodMarginZPercentileHeader(char *filename);
	void printLikelihoodMarginZPercentile(char *filename);
	void printLikelihoodMarginZPercentileSingle(char *filename);
	void printLikelihoodMarginZPercentileHeader(char *filename1, char *filename2);
	void printLikelihoodMarginZPercentile(char *filename1, char *filename2);
	void printLikelihoodMarginZPercentileSingle(char *filename1, char *filename2);
	void printFluxesHeader(char *filename);
	void printFluxes(char * filename, Filter **filters);
	void printResidualHeader(char *filename);
	void printResidual( char *filename, Filter **filters);
	void printPhotoZHeader(ofstream & ofile);
	void printPhotoZ(ofstream & ofile);	
	
};

class Prior
{
private:
	Likelihood *likelihood;			
	double	**prior,
			*priorBestT,
			**posterior,
			*posteriorMarginZ,
			*posteriorMarginT,	
			*posteriorBestT;
	int **numberCountML;
	int *numberCountMLBestT;
	OutputHandler *out;
	TemplateContainer *templateContainer;
	
	int savePriorIterative;
	int maxIterations;
	int smoothPrior;
	int smoothZMode;
	double smoothT,smoothZ;
	char priorfilename[MAX_LENGTH];
	
	double best_P;
	double chi2ReducedCut;  // don't use fits with high chi^2 (i.e. chi2 > (numFilter-3)*chi2ReducedCut)
	
	int best_t;
	int bestMarginZ_t;	// best fit type corresponding to bestMarginZ_zi
	int bestMarginT_t;	// best template type of marginalized posterior (over redshift)
	int best_zi;
	int bestMarginZ_zi;	// best redshift z of marginalized posterior (over types)
	int bestMarginT_zi;	// best fit redshift corresponding to bestMarginT_t		
	
	double zmin2_68;
	double zmax2_68;
	double zmin2_95;
	double zmax2_95;	
	double zmin_68;
	double zmax_68;
	double zmin_95;
	double zmax_95;	
	double zminmz_68;
	double zmaxmz_68;
	double zminmz_95;
	double zmaxmz_95;	
	double zminmt_68;
	double zmaxmt_68;
	double zminmt_95;
	double zmaxmt_95;
	
	double Tmin2_68;
	double Tmax2_68;
	double Tmin2_95;
	double Tmax2_95;	
	double Tmin_68;
	double Tmax_68;
	double Tmin_95;
	double Tmax_95;	
	double Tminmz_68;
	double Tmaxmz_68;
	double Tminmz_95;
	double Tmaxmz_95;	
	double Tminmt_68;
	double Tmaxmt_68;
	double Tminmt_95;
	double Tmaxmt_95;	
	
	int numTemplates;
	int numZ;
	
	char filenamePost[MAX_LENGTH];
	char filenamePostT[MAX_LENGTH];
	char filenamePostZ[MAX_LENGTH];
	char filenamePostPZ[MAX_LENGTH];
	char filenamePostPZT[MAX_LENGTH];
	
	int leftIndexNotNull(int minIndex, int *f, int index) {
		
		for (int i=index-1;i>minIndex;i--)	
			if (f[i]>0)
				return i;
				
		return minIndex;										
	}
	
	int rightIndexNotNull(int maxIndex, int *f, int index) {
		
		for (int i=index;i<=maxIndex;i++)	
			if (f[i]>0)
				return i;
				
		return maxIndex;										
	}
	
	void calcPosteriorErrors();
	void calcPosteriorErrorsBestT();
	
	void fprintfGroup(ofstream & ofile, int zi, int t, int failed) {	
		if (! failed) {	
			double mb=likelihood->getTemplates()[t]->getBFluxNormalized();
			double tn=likelihood->getTemplateNorm()[t][zi];
			ofile << setprecision(6) << setw(MYSETW) << likelihood->getBinZ()->getZ(zi);
			ofile << setprecision(6) << setw(MYSETW) << t;		
			ofile << setprecision(6) << setw(MYSETW) << tn;
			ofile << setprecision(6) << setw(MYSETW) << likelihood->getChi2()[t][zi];
			ofile << setprecision(6) << setw(MYSETW) << likelihood->getFluxRestriction()->getAbsoluteMagB(zi,tn,mb);
			ofile << setprecision(6) << setw(MYSETW) << likelihood->getFluxRestriction()->getDL()[zi];
		} else {
			ofile << setprecision(6) << setw(MYSETW) << -1;
			ofile << setprecision(6) << setw(MYSETW) << -1;		
			ofile << setprecision(6) << setw(MYSETW) << -1;
			ofile << setprecision(6) << setw(MYSETW) << -1;
			ofile << setprecision(6) << setw(MYSETW) << -1;
			ofile << setprecision(6) << setw(MYSETW) << -1;							
		}
	}
	
	void fprintfErrGroup(ofstream & ofile, double f1, double f2, double f3, double f4) {
		ofile << setprecision(6) << setw(MYSETW) << f1;
		ofile << setprecision(6) << setw(MYSETW) << f2;
		ofile << setprecision(6) << setw(MYSETW) << f3;
		ofile << setprecision(6) << setw(MYSETW) << f4;
	}
	
	void fprintfGrid2D(ofstream & ofile,int dimA,double *A,int dimB,double *B,double **C, int failed) {
		if (! failed) {
			for (int i=0;i<dimA;i++) {			
				for (int j=0;j<dimB;j++) {				
					ofile << A[i] << " " << B[j] << " " << C[i][j] << endl;
				}
				ofile << endl;
			}
		} else {
			for (int i=0;i<dimA;i++) {			
				for (int j=0;j<dimB;j++) {				
					ofile << A[i] << " " << B[j] << " 0" << endl;
				}
				ofile << endl;
			}
		}
	}
			
	void fprintfGrid1D(ofstream & ofile,int dimA,double *A,double *C, int failed) {
		if (! failed) {
			for (int i=0;i<dimA;i++) {							
				ofile << A[i]  << " " << C[i] << endl;	
			}
		} else {
			for (int i=0;i<dimA;i++) {							
				ofile << A[i]  << " 0" << endl;	
			}	
		}
	}
	
	void fprintfArray2D(ofstream & ofile,int dimA, int dimB, double **C, int failed) {			
		if (! failed) {
			for (int i=0;i<dimA;i++) {			
				for (int j=0;j<dimB;j++) {				
					ofile << C[i][j] << " ";
				}
				ofile << endl;
			}
			ofile << endl;
		} else { 
			for (int i=0;i<dimA;i++) {			
				for (int j=0;j<dimB;j++) {				
					ofile << "0 ";
				}
				ofile << endl;
			}
			ofile << endl;						
		}
	}
	
	void fprintfArray1D(ofstream & ofile,int dimA, double *C, int failed) {			
		if (! failed) {
			for (int i=0;i<dimA;i++) {			
				ofile << C[i] << " ";
			}
			ofile << endl;			
		} else {
			for (int i=0;i<dimA;i++) {			
				ofile << "0 ";
			}
			ofile << endl;						
		}
	}
	
	public:
		Prior();
		~Prior();
		void set(Likelihood *likelihood,TemplateContainer *templateContainer,char *priorfilename) {
			freeArray2(prior,numTemplates);
			this->likelihood=likelihood;
			this->templateContainer=templateContainer;
			strcpy(this->priorfilename,priorfilename);
			numTemplates=likelihood->getNumTemplates();
			numZ=likelihood->getNumZ();						
			allocArray2(prior,numTemplates,numZ);
			allocArray2(posterior,numTemplates,numZ);
			allocArray1(posteriorMarginZ,numZ);
			allocArray1(posteriorMarginT,numTemplates);	
			allocArray1(posteriorBestT,numZ);
		}		
		
		void set(OutputHandler *out) {
			this->out=out;
		}		
		
		void setSavePriorIterative(int savePriorIterative) {
			this->savePriorIterative=savePriorIterative;		
		}
		void setMaxIterations(int maxIterations) { 
			this->maxIterations=maxIterations;
		}
		void setSmoothPrior(int smoothPrior) {
			this->smoothPrior=smoothPrior;
		}
		void setSmoothT(double smoothT) {
			this->smoothT=smoothT;
		}
		void setSmoothZ(double smoothZ) {
			this->smoothZ=smoothZ;
		}				
		void setSmoothZMode(int smoothZMode) {
			this->smoothZMode=smoothZMode;	
		}
		void setFilenamePost(char *filenamePost) {
			strcpy(this->filenamePost,filenamePost);
		}		
		void setFilenamePostT(char *filenamePostT) {
			strcpy(this->filenamePostT,filenamePostT);
		}
		void setFilenamePostZ(char *filenamePostZ) {
			strcpy(this->filenamePostZ,filenamePostZ);
		}	
		void setFilenamePostPZ(char *filenamePostPZ) {
			strcpy(this->filenamePostPZ,filenamePostPZ);
		}
		void setFilenamePostPZT(char *filenamePostPZT) {
			strcpy(this->filenamePostPZT,filenamePostPZT);
		}		
		
		void setChi2ReducedCut(double chi2ReducedCut) {
			this->chi2ReducedCut=chi2ReducedCut;
		}
		
		// prior related
		void calcPrior();			
		void calcPriorBestT();
		int calcPosterior(int entryIndex);	
		int calcPosteriorBestT(int entryIndex);			
		void setFlatPrior();
		void setFlatPriorBestT();		
		void smoothingPrior();
		void smoothingPrior(int numIter);
		void smoothingPriorBestT();
		void loadPrior(char *filename);
		void loadPriorBestT(char *filename);
		void savePrior(char *filename);
		void savePriorBestT(char *filename);
		void savePriorMarginT(char *filename);
		void savePriorMarginZ(char *filename);		
		void savePriorMarginZPercentile(char *filename);		
		
		// posterior related
		void saveMarginZPercentile(char *filename, int zeroLine);		
		void saveMarginZPercentile(char *filename1, char *filename2, int zeroLine);		
		void saveMarginZPercentileSingle(ofstream & ofile, int zeroline);
		void saveMarginZPercentileSingle(ofstream & ofile1, ofstream & ofile2, int zeroline);
		
		void saveBestTZPercentile(char *filename, int zeroLine);						
		void saveBestTZPercentileSingle(ofstream & ofile, int zeroline);		
		
		// other stuff
		void saveEtaMarginZ(char *filename,double **eta);	
		void saveEta(char *filename,double **eta);
		void saveML(char *filename, int **numberCountML);
		void saveEtaBestT(char *filename, double *eta);
		void saveMLBestT(char *filename, int *numberCountML);
		
		// Bayes_ML.dat
		void printPhotoZHeader(ofstream & ofile);
		void printPhotoZ(ofstream & ofile, int *printPost, int single);			
		void printPhotoZHeaderBestT(ofstream & ofile);
		void printPhotoZBestT(ofstream & ofile, int printPostZ, int printPostPZ, int single);
};

#endif /*LIKELIHOOD_H_*/
