#ifndef FLUXFILE_H_
#define FLUXFILE_H_

#include <sstream>
#include "ConfFile.h"
#include "NumericalData.h"
#include "globHeader.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

class FluxFile : public ConfFile
{

public:
	FluxFile(const char *filename):ConfFile(filename){};
	~FluxFile(){};
#ifdef PARRAY	
	void load(double*** flux,int x, int y, int z);
	void save(double*** flux,int x, int y, int z);
	void loadStepByStep(double*** flux,int x, int y, int z);
#else
	void load(double* flux,int x, int y, int z);
	void save(double* flux,int x, int y, int z);
	void loadStepByStep(double* flux,int x, int y, int z);
#endif
};

class PriorFile : public ConfFile
{
	
private:
	BinningZ *m_binZ;
	int numTemplates;
	
public:
	PriorFile(const char *filename, BinningZ *binZ, int numTemplates):ConfFile(filename){
		m_binZ = new BinningZ();
		m_binZ->copy(binZ);	
		this->numTemplates=numTemplates;
	};
	~PriorFile(){
		delete m_binZ;
	};
	void load(double** prior);
	void load1D(double *prior);
	void save(double** prior);
	void save1D(double *prior);		
};

class TableFile : public ConfFile
{
	
private:
	double **body;	
	
	void resize() {
		freeArray2(body,linenumber);
	}

public:	
	TableFile(const char *filename) : ConfFile(filename) {
		body=NULL;		
	};	
	~TableFile() {		
		freeArray2(body,linenumber);
	};
	void load();
	void load(int bunchlines);
	void saveAppend(char *filename);
	void getbody(double ** body) {
		for (int i=0;i<linenumber;i++)
			for (int j=0;j<colnumber;j++) 
				body[i][j]=this->body[i][j];		
	}
	double **getbody() { return body; };	
};

#endif /*FLUXFILE_H_*/
