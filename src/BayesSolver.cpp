#include "BayesSolver.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/


/* ----------- public ------------------------- */

BayesSolver::BayesSolver(CommandLineHandler *clh)
{	
	this->clh=clh;	
	this->templates=NULL;
	this->filters=NULL;
	this->templateContainer=NULL;
	this->out=NULL;
	this->B_filter=NULL;
	this->catalog=NULL;	
	this->binZ=NULL;
	this->flux=NULL;
	this->like=NULL;
	this->fluxRestriction=NULL;
	this->prior=NULL;
	
}

BayesSolver::~BayesSolver()
{												
	if (this->prior)
		delete prior;
	
	if (this->like)	
		delete like;
		
	if (this->fluxRestriction)
		delete fluxRestriction;				
	
	if (this->binZ) 
		delete this->binZ;
	
	if (this->B_filter) 
		delete this->B_filter;			
			
	if (this->templateContainer)
		delete this->templateContainer;
		
	for (int i=0;i<numFilters;i++) {
		if (this->filters[i])
			delete this->filters[i];	
	}	
	freeArray1(this->filters);
	
	freeArray1(this->templates);
	
	if (this->catalog)
		delete this->catalog;
}
	
void BayesSolver::checkRhoSigmaFile(std::string filename, unsigned int basicTypeID) 
{
  int linenumber;
  int colnumber;	  
  ConfFile *rhoSigmaFile;
  std::string fullfilename=std::string(clh->getoutputpath())+"/"+filename;
  char s[MAX_LENGTH];
  
  rhoSigmaFile = new ConfFile(fullfilename.c_str());    
  rhoSigmaFile->setOutputHandler(this->out);	  
  rhoSigmaFile->getQuickLineColnumber(fullfilename.c_str(), linenumber, colnumber);
  if (colnumber < 2) {
	sprintf(s,"Rho-Sigma File %s contains less than the 2 required columns (1st sigma, 2nd rho)",filename.c_str());
	EXIT(s);
  }
  // the -1 in the following expression stems from the fact that rho and sigma live on interval-midpoints
  int aux=templateContainer->getBasicTemplates()[basicTypeID]->getxyData()->getlength()-1;  
  if (linenumber != aux) {   
	sprintf(s,"Rho-Sigma File %s contains %d line(s) instead of %d (which is the mesh-size)",
	        filename.c_str(),linenumber,aux);
	EXIT(s);
  }
	
}	

void BayesSolver::printUseRhoSigmaFile(std::string filename, unsigned int basicTypeID) 
{  
  char s[MAX_LENGTH];
  sprintf(s,"The sigma and rho definitions of file %s are used for template %s",
             filename.c_str(),templateContainer->getBasicTemplates()[basicTypeID]->getShortName());
  this->out->println(s,1);
}
 
void BayesSolver::improveTemplates()
{				
	struct tms start, end;
	struct tms startTotal, endTotal;
	float createAndLoadTime,calcFluxTime,correctingTime,totalTime;	
	char s[MAX_LENGTH];
	BroadZBinning * broadZBinning=NULL;
	char*** defEntries;
	int*	defEntriesNr;
	int 	defEntryLines;
	char defFileName[MAX_LENGTH];
	
	// starting timing
	times( &startTotal);
	times( &start);
	
	prepareA();
	prepareB();									
		
	templatesFiltersToMesh(1);	
	
	normalizeTemplates(true,true);
	
	checkTemplateRange();
			
	if (clh->getLoadRhoSigma()) {			  	 
	  // 1. check for each BASIC template whether there is a specific rhoSigma file
	  //    if yes check that this file is ok
	  // 2. if there is at least one template without specific rhoSigme file
	  // check global rhoSigma file
	  bool flag_need_global_rhoSigma=false;	  
	  for (int t=0;t<templateContainer->getNumBasicTemplates();t++) {
	  	if (strcmp(clh->getRhoSigmaFiles()[t],"")!=0) { // specific file is given
	  	  checkRhoSigmaFile(clh->getRhoSigmaFiles()[t],t);
	  	  printUseRhoSigmaFile(clh->getRhoSigmaFiles()[t],t);
	  	} else {							  // no specific file given
	  	  flag_need_global_rhoSigma=true;
	  	  printUseRhoSigmaFile(clh->getRhoSigmaLoadBase(),t);
	  	}
	  }
	  if (flag_need_global_rhoSigma) {	  	
	  	checkRhoSigmaFile(clh->getRhoSigmaLoadBase(),0);
	  }
	}
		
	// create binZ:
	double *z;
	
	// read definition file which has the following format (6 columns)
	// filter_name numberBins min_Magnitude max_Magnitude InterpolMode ExtrapolMode		
	ConfFile *defFile;
	sprintf(defFileName,"%s/%s",clh->getoutputpath(),clh->getTemplateImprovementDef());
	defFile = new ConfFile(defFileName);
	defFile->setOutputHandler(this->out);
	defFile->read();	
	defFile->printTotalStatus();
	defFile->getEntries(defEntries,defEntriesNr,defEntryLines);			
	
	allocArray1(z,defEntryLines);
	for (int i=0;i<defEntryLines;i++) {
		if (defEntriesNr[i]>=1) {
		  z[i]=atof(defEntries[i][0]);
		}
		else {
			sprintf(s,"The file %s is corrupted - need at least one column",defFileName);
			EXIT(s);	
		}		
	}
	BinningZ *template_binZ = new BinningZ();
	template_binZ->set(defEntryLines,z);
	
	freeArray1(z);
	delete defFile;
			
	
	times ( &end );
	TIME( createAndLoadTime );	
	times( &start );	
	calcFluxTime=0;
	correctingTime=0;
	
	// start iteration
	// note: iteration just means to make an new chi2 fit and take into account that
	// some entries are now assigned to a new type
	// iteration does not mean to take the latest corrected templates and do a template
	// correction on them. The template correction is not idempotent, i.e. doing it twice
	// is different from doing it once (the difference comes from an effective change (increase) 
	// in sigma. If, however, sigma is already very large then the result is not changed by 
	// subsequent iterations (except for the changing of types of some entries). Note, however, 
	// that due to the corrected templates the flux in each filter band is slightly changed thus 
	// thus leading to slightly different fits (different template_normalizations). The interaction
	// of different galaxies can thus lead to a slightly different correction result thus affecting 
	// the new templates and hence its flux...
	for (int i=0;i<clh->getCorrectionSteps();i++) {
#ifdef PARRAY				
		allocArray3(flux,numTemplates,binZ->getLength(),numFilters);	
#else
		allocArray1(flux,numTemplates*binZ->getLength()*numFilters);
#endif		
		calcFlux();		
		times( &end );
		TIMEADD( calcFluxTime );	
		
		times( &start );		
		// create Likelihood object
		if (like)
			delete like;
		like = new Likelihood();
		#if 0 // allows output of more information but makes the code considerably slower
		  like->setOutputHandler(this->out);				
		#endif  		
		// hope that works, i.e. in the first iteration it just uses the original templates
		like->set(flux, binZ, fluxRestriction, catalog, templateContainer, 1);	
		like->setRightZMode(clh->getRightZMode());
		like->setMBMode(clh->getMBMode());	
		like->setChi2Add(clh->getChi2Add());
		like->setChi2Mult(clh->getChi2Mult());
		
		// create broad z-bins of entry-objects
		if (broadZBinning)
			delete broadZBinning;
		broadZBinning = new BroadZBinning(numFilters,filters,binZ,templateContainer);	
		broadZBinning->set(this->out);
		broadZBinning->setOutputpath(clh->getoutputpath());
		broadZBinning->setSigma(clh->getSigma());
		broadZBinning->setRho(clh->getRho());
		broadZBinning->setPunishGradient(clh->getPunishGradient());		
		broadZBinning->setParameterModes(clh->getRescaleWithMesh(),clh->getRescaleWithBands(),clh->getRescaleWithFlux());				
		broadZBinning->setRhoSigmaLoad(clh->getLoadRhoSigma(),clh->getRhoSigmaLoadBase());			
		broadZBinning->setRhoSigmaSave(clh->getSaveRhoSigma(),clh->getRhoSigmaSaveBase());	
		broadZBinning->setRhoSigmaFiles(clh->getRhoSigmaFiles());
		broadZBinning->setPercCut(clh->getPercCut());
		broadZBinning->setChi2ReducedCut(clh->getChi2ReducedCut());
		broadZBinning->setIterationID(i);
		if (this->out) {		
			sprintf(s,"sigma = %g rho = %g",clh->getSigma(),clh->getRho());			
			this->out->println(s,2);	
			if (clh->getPunishGradient()) 
			  this->out->println("punishGradient = Yes",1);
			else
			  this->out->println("punishGradient = No",1);
			if (clh->getRescaleWithMesh()) 
			  this->out->println("rescale with mesh resolution = Yes",1);
			else
			  this->out->println("rescale with mesh resolution = No",1);
			if (clh->getRescaleWithBands()) 
			  this->out->println("rescale with filter band number = Yes",1);
			else
			  this->out->println("rescale with filter band number = No",1);
			 if (clh->getRescaleWithFlux()) {
			   sprintf(s,"rescale with template flux = Yes [%d]",clh->getRescaleWithFlux());
			   this->out->println(s,1);
			 } else {			    
			   this->out->println("rescale with template flux = No",1);
			 } 			  				
		}
		broadZBinning->setigAbsorption(clh->getigAbsorption());
		broadZBinning->set(like);
		// for each broad z-bin optimize each template using chi^2-repair				
		broadZBinning->setzBins(template_binZ);	
		
		if (clh->getImproveTemplateMode()==1 && i>0)
			broadZBinning->correctTemplates();
		else {		
			broadZBinning->correctTemplatesFromOrig();	// in first iteration start from orig in any case	
		}
	
		sprintf(s,"_it%d",i);
		templateContainer->writeBasic(clh->getoutputpath(),s);

#ifdef PARRAY		
		freeArray3(flux,numTemplates,binZ->getLength());
#else
		freeArray1(flux);
#endif 		
		
		// obtain all templates in order to make a next iteration
		// broadZBinning had now manipulated the templates in the templateContainer
		// want now these new templates
	
		// allocate templates
		freeArray1(templates);
		numTemplates=templateContainer->getNumAllTemplates();
		allocArray1(templates,numTemplates);
		for (int t=0;t<numTemplates;t++) {
			templates[t]=templateContainer->getAllTemplate(t);			
		}
								
		normalizeTemplates(true,true);				
		
		#if 1
		sprintf(s,"Template_corr_it%d",i);
 	    	printTemplates(s);	
		#endif					
		
		times( & end);
		TIMEADD( correctingTime );			
		times( &start );	
	}			
		
	times( &endTotal );
	TIMETOTAL ( totalTime );
	
	delete broadZBinning;
	delete template_binZ;
	
	if (clh->getVerboseLevel()>0) {
		cout << "*** TIMES:\n";
		cout << " Creating and loading objects: " << createAndLoadTime << "s\n";
		cout << " Calculating fluxes for templates: " << calcFluxTime << "s\n";
		cout << " Improving templates: " << correctingTime << "s\n";
		cout << " Total time: " << totalTime << "s" << endl;
	}		
}

void BayesSolver::calcPhotoZ()
{
		
	ofstream ofile,ofile2;
	
	struct tms start, end;
	struct tms startTotal, endTotal;
	float createAndLoadTime=0, calcFluxTime=0, maxLikeTime=0;
	float catCorrTime=0, totalTime=0;
	char s[MAX_LENGTH];
	
	// starting timing
	times( &startTotal);
	times( &start);		
	
	
	{
		if (! clh->getCalcLikelihood() &&
			! clh->getCalcBayesian() &&
			! clh->getCalcCatalogCorrection() &&
			! clh->getApplyCatalogCorrection() &&
			clh->getKcorrection() ) { 
			prepareAnoCatalog();
		} else {		
		    	prepareA();	
		}
	
		if (clh->getCalcCatalogCorrection() ||
			clh->getCalcLikelihood() ||	
			clh->getCalcBayesian() || 
			clh->getKcorrection() ) {
			prepareB();	
			if (clh->getUseMesh())				
			  templatesFiltersToMesh(0);
			normalizeTemplates(clh->getNormalizeTemplates(),clh->getUseMesh());  				
			checkTemplateRange();	
		}
		
		times ( &end );
		TIME( createAndLoadTime );				
		times ( &start);
		
		if (clh->getCalcCatalogCorrection() ||
			clh->getCalcLikelihood() ||	
			clh->getCalcBayesian() || 
			clh->getKcorrection() ) {	
#ifdef PARRAY			
			allocArray3(flux,numTemplates,binZ->getLength(),numFilters);					
#else
			allocArray1(flux,numTemplates*binZ->getLength()*numFilters);	
#endif			
			calcFlux();
		}		
		
			
		times( &end );
		TIME( calcFluxTime );
		times( &start );		 				
		
		if (clh->getKcorrection())
			calcKcorrection();				// print K-corrections for used templates
		
		// calc-catalog-correction always implies apply-catalog-correction	
		if (clh->getIterationsCatalogCorrection()) {
					
			for (int i=1;i<=clh->getIterationsCatalogCorrection();i++) {
				if (i==clh->getIterationsCatalogCorrection()) {	// last iteration has no infix
					if (clh->getCalcCatalogCorrection()) 
						calcCatalogCorrection("");									
					if (clh->getApplyCatalogCorrection() || clh->getCalcCatalogCorrection())
						applyCatalogCorrection("");
				} else {
					sprintf(s,"_%d",i);				
					if (clh->getCalcCatalogCorrection()) 
						calcCatalogCorrection(s);
					if (clh->getApplyCatalogCorrection() || clh->getCalcCatalogCorrection())
						applyCatalogCorrection(s);	
				}
			}
			
		} else {
				
			if (clh->getCalcCatalogCorrection()) 
				calcCatalogCorrection("");	// find systematic deviations in the catalog
			
			if (clh->getApplyCatalogCorrection() || clh->getCalcCatalogCorrection())
				applyCatalogCorrection("");	// apply systematic deviations to given catalog
					
		}
		
		times( &end );
		TIME( catCorrTime );
		times( &start );				
		
		if (clh->getCalcLikelihood())
			calcMaxLikelihood("");		// photo-Z estimation using maximum likelihood	
				
		if (clh->getCalcBayesian())
			calcBayesian();				// photo-Z estimation using bayesian approach
		
		times( &end );
		TIME( maxLikeTime );					
	}		
	
	if (clh->getCalcCatalogCorrection() ||
		clh->getCalcLikelihood() ||	
		clh->getCalcBayesian() || 
		clh->getKcorrection() ) {
#ifdef PARRAY		
		freeArray3(flux,numTemplates,binZ->getLength());
#else
		freeArray1(flux);
#endif		
	}
	
	times( &endTotal );
	TIMETOTAL ( totalTime );
	if (clh->getVerboseLevel()>0) {
		cout << "*** TIMES:\n";
		cout << " Creating and loading objects: " << createAndLoadTime << "s\n";
		cout << " Calculating fluxes for templates: " << calcFluxTime << "s\n";		
		if (clh->getCalcCatalogCorrection() || clh->getApplyCatalogCorrection())
			cout << " Correcting the catalog: " << catCorrTime << "s\n";
		if (clh->getCalcLikelihood() || clh->getCalcBayesian())
			cout << " Calculating red shifts: " << maxLikeTime << "s\n";
		cout << " Total time: " << totalTime << "s" << endl;
	}			
}

/* ----------- private ------------------------- */

void BayesSolver::createFilters(int numFilters, char** nameFilters)
{
	char ffilename[MAX_LENGTH+MAX_LENGTH];	
	char s[MAX_LENGTH];
	// create all filters
	if (this->out) {
		sprintf(s,"Creating %d filter(s)", numFilters);
		out->println(s,1);
	}
	filters=(Filter**)malloc(numFilters*sizeof(Filter*));
	for (int i=0;i<numFilters;i++) {
		sprintf(ffilename,"%s/%s",clh->getfilterpath(),nameFilters[i]);
		filters[i] = new Filter(ffilename,nameFilters[i]);	
		filters[i]->setOutput(out);
		if (this->out) {
			sprintf(s,"Filter: %s created",this->filters[i]->getName());
			out->println(s,2);		
		}
	}
	this->numFilters=numFilters;		
}

// load catalog 
void BayesSolver::createCatalog()
{			
	char cfilename[MAX_LENGTH];	
	char dfilename[MAX_LENGTH];				
	char s[MAX_LENGTH];
	
	if (this->out) {
		out->println("Creating catalog ... ",1);
	}
	
	sprintf(cfilename,"%s/%s",clh->getcatalogpath(),
							  clh->getnameCatalog());
	sprintf(dfilename,"%s/%s",clh->getcatalogpath(),
							  clh->getnameDefFile());							  
	catalog = new Catalog(cfilename,clh->getnameCatalog(),dfilename);
	catalog->setOutput(out);	
	catalog->setFluxType(clh->getFluxType());	
	catalog->setFluxEntries(clh->getFluxEntries());
	catalog->setkBandCorrection(clh->getKBandCorrection());
	catalog->setAllowMissingError(clh->getAllowMissingError());
	catalog->setPreserveErrorsOutput(clh->getPreserveErrorsOutput());
	catalog->setPreserveCatalogFormat(clh->getPreserveCatalogFormat());
	catalog->setMagCatalogLow(clh->getMagCatalogLow());
	catalog->setMagCatalogHigh(clh->getMagCatalogHigh());
	
	if (this->out) {
		sprintf(s,"%s created",catalog->getName());
		out->println(s,2);	
	}
	
}	

/* saves a template in file called filename
 * t is index of template, a is a multiplicative constant
 * template is printed as it is used for the chi^2 fit, i.e.
 * including IG absorption, redshifting, ...
 */
void BayesSolver::saveObservedTemplate(char *filename,int t, int zi, double a)
{
	xyData *data;
	
	data = new xyData();
	this->templates[t]->getObservedSpectrum(zi, data);
	(*data) * a;	
	ofstream ofile;
	ofile.open(filename);
	ofile << "# template spectrum as seen by an observer (including red shifting, absorption, ...)" << endl;
	ofile << "# t=" << t << " zi=" << zi << " a=" << a << endl;
	ofile.close();
	ofile.clear();			
	data->append(filename);
	delete data;
}

/* saves a template in file called filename
 * t is index of template, a is a multiplicative constant
 * the template is just redshifted, nothing else
 */
void BayesSolver::saveTemplate(char *filename,int t, int zi, double a)
{
	xyData *data;
	
	data = new xyData();
	this->templates[t]->getSpectrum(zi, data);
	(*data) * a;	
	ofstream ofile;
	ofile.open(filename);
	ofile << "# template spectrum after red shifting" << endl;
	ofile << "# t=" << t << " zi=" << zi << " a=" << a << endl;
	ofile.close();
	ofile.clear();			
	data->append(filename);
	delete data;
}

void BayesSolver::printTemplates(const char *filebase)
{
	
	char s[MAX_LENGTH];
	xyData *data;
	
	for (int t=0;t<numTemplates;t++) {
		sprintf(s,"%s/%s_l%d.dat",clh->getoutputpath(),filebase,t);
		data = new xyData();		
		this->templates[t]->getPlainSpectrum(data);		
		ofstream ofile;
		ofile.open(s);
		ofile << "# template spectrum" << endl;
		ofile << "# t=" << t << " zi=" << 0 << " a=" << 1 << endl;
		ofile.close();
		ofile.clear();			
		data->append(s);		
		delete data;
	}	
}

void BayesSolver::printFilters(const char *filebase)
{
	
	char s[MAX_LENGTH];
	xyData *data;
	
	for (int f=0;f<numFilters;f++) {
		sprintf(s,"%s/%s_l%d.dat",clh->getoutputpath(),filebase,f);
		data = new xyData();
		this->filters[f]->getTransmission(data);
		ofstream ofile;
		ofile.open(s);
		ofile << "# filter transmission curve" << endl;
		ofile << "# f=" << f << endl;
		ofile.close();
		ofile.clear();			
		data->append(s);		
		delete data;
	}	
}
void BayesSolver::calcMaxLikelihood(const char *iterationInfix)
{			
	char s[MAX_LENGTH];		
	char s2[MAX_LENGTH];
	char ofilename[MAX_LENGTH];				
	int failed;
	ofstream ofile;
	
	#ifdef MEASTIME
		float rTime;
		struct tms start, end;
	#endif
	
	if (this->out)	{
		out->print("Maximum likelihood analysis started: ",1);
		if (clh->getRightZMode())
			out->print("(fitting types only) ",1);				
	}		
			
	#ifdef MEASTIME
	  times( &start );	
	#endif		
	
	// create Likelihood object
	if (like)
		delete like;
	like = new Likelihood();
	#if 0 // allows output of more information but makes the code considerably slower
	  like->setOutputHandler(this->out);					
	#endif
	like->set(flux, binZ, fluxRestriction, catalog, templateContainer, 0);
	
	like->setMBMode(clh->getMBMode());	
	like->setRightZMode(clh->getRightZMode());
	like->setChi2Add(clh->getChi2Add());
	like->setChi2Mult(clh->getChi2Mult());
		
	// preparing file header
	sprintf(ofilename,"%s/%s%s.dat",clh->getoutputpath(),clh->getoutputbase(),iterationInfix);
	ofile.open(ofilename);
	CHECK(ofile,ofilename);
	like->printPhotoZHeader(ofile);		
		
	if (! clh->getSingle()) {
		if (clh->getLikelihood()[0]) {
			sprintf(s,"%s/Likelihood%s.dat",clh->getoutputpath(),iterationInfix);
			like->printLikelihoodHeader(s);	
		}		
		
		if (clh->getLikelihood()[1]) { //&& (! clh->getRightZMode())) {
			sprintf(s,"%s/LikelihoodMarginT%s.dat",clh->getoutputpath(),iterationInfix);
			like->printLikelihoodMarginTHeader(s);	
		}	

		if (clh->getLikelihood()[2] && (! clh->getRightZMode()) ) {
			sprintf(s,"%s/LikelihoodMarginZ%s.dat",clh->getoutputpath(),iterationInfix);
			like->printLikelihoodMarginZHeader(s);	
		}		
		
		if (clh->getLikelihood()[3] && (! clh->getRightZMode())) {
		
			if (clh->getLikelihood()[4] && (! clh->getRightZMode())) {
				sprintf(s,"%s/LikelihoodMarginZPercentile%s.dat",clh->getoutputpath(),iterationInfix);
				sprintf(s2,"%s/LikelihoodBestTPercentile%s.dat",clh->getoutputpath(),iterationInfix);
				like->printLikelihoodMarginZPercentileHeader(s,s2);
			} else {				
				sprintf(s,"%s/LikelihoodMarginZPercentile%s.dat",clh->getoutputpath(),iterationInfix);
				like->printLikelihoodMarginZPercentileHeader(s);				
			}
		}
	}		
	
	if (clh->getDump()[4]) {							
		sprintf(s,"%s/residual%s.dat",clh->getoutputpath(),iterationInfix);
		like->printResidualHeader(s);			
	}
	
	// auxiliary file, just needed by the program to transport information
	sprintf(s,"%s/aux%s.dat",clh->getoutputpath(),iterationInfix);
	printAuxHeader(s);				
	
	#ifdef MEASTIME
	  times ( &end );	
	  TIME( rTime );		
	  cout << "Init likelihood obj & writting header files : " << rTime << "s" << endl;
	  rTime=0;
	#endif		
	
	// some additional information about the chi^2 distribution, useful
	// for the user to check, e.g. whether the errors are sensible
	std::vector< std::vector<double> > chi2Values(numFilters+1,std::vector<double>(0));	
							
	// looping over all entries from the catalog
	// each entry is a number of fluxes and flux-errors corr. to bands
	for (int j=0;j<catalog->getLineNumber();j++) {
		if (this->out) {
			if ((j+1)%1000==0) {
				if ((j+1)%10000==0) 
					sprintf(s,"%d",j+1);
				else
					sprintf(s,".");
				out->print(s,1);
			}							
		}
		
	    #ifdef MEASTIME
	      times( &start );	
	    #endif								
	    
		failed=like->calcLikelihood(j);		
												
	    #ifdef MEASTIME
	      times ( &end );	
	      TIMEADD( rTime );	     
	    #endif 			
	    
	    // some additional information about the chi^2 distribution, useful
	    // for the user to check, e.g. whether the errors are sensible
	    
	    if (! failed)
	      chi2Values[like->getNumUsedFilters()].push_back(like->getBest_Chi2());
	    
		if (clh->getLikelihood()[0]) {
			// print complete likelihood function			
			if (! clh->getSingle()) {				
				sprintf(s,"%s/Likelihood%s.dat",clh->getoutputpath(),iterationInfix);
				like->printLikelihood(s);									
			} else {				
				sprintf(s,"%s/Likelihood_l%d%s.dat",clh->getoutputpath(),j,iterationInfix);
				like->printLikelihoodSingle(s);				
			}
		}
		
		if (clh->getLikelihood()[1] && (! clh->getRightZMode())) {
			// print collapsed likelihood function \int dz P(c|z,T)
			if (! clh->getSingle()) {
				sprintf(s,"%s/LikelihoodMarginT%s.dat",clh->getoutputpath(),iterationInfix);
				like->printLikelihoodMarginT(s);					
			} else {
				sprintf(s,"%s/LikelihoodMarginT_l%d%s.dat",clh->getoutputpath(),j,iterationInfix);
				like->printLikelihoodMarginTSingle(s);
			}
		}
		
		//new!
		if (clh->getLikelihood()[1] && clh->getRightZMode()) {
			sprintf(s,"%s/LikelihoodMarginT%s.dat",clh->getoutputpath(),iterationInfix);
			like->printLikelihoodMarginTRightZ(s);
		}
		
		if (clh->getLikelihood()[2] && (! clh->getRightZMode())) {
			// print collapsed likelihood function \sum_{T} P(c|z,T)										
			if (! clh->getSingle()) {
				sprintf(s,"%s/LikelihoodMarginZ%s.dat",clh->getoutputpath(),iterationInfix);
				like->printLikelihoodMarginZ(s);
			} else {
				sprintf(s,"%s/LikelihoodMarginZ_l%d%s.dat",clh->getoutputpath(),j,iterationInfix);
				like->printLikelihoodMarginZSingle(s);
			}
		}
			
		if (clh->getLikelihood()[3] && (! clh->getRightZMode())) {
			// print percentiles of collapsed likelihood function \sum_{T} P(c|z,T)								
			
			if (clh->getLikelihood()[4] && (! clh->getRightZMode())) {
				// print also best fit templates corresponding to redshifts of percentiles 
				// of collapsed likelihood function \sum_{T} P(c|z,T)	
				if (! clh->getSingle()) {				
					sprintf(s,"%s/LikelihoodMarginZPercentile%s.dat",clh->getoutputpath(),iterationInfix);
					sprintf(s2,"%s/LikelihoodBestTPercentile%s.dat",clh->getoutputpath(),iterationInfix);
					like->printLikelihoodMarginZPercentile(s,s2);
				} 
				else {
					sprintf(s,"%s/LikelihoodMarginZPercentile_l%d%s.dat",clh->getoutputpath(),j,iterationInfix);
					sprintf(s2,"%s/LikelihoodBestTPercentile_l%d%s.dat",clh->getoutputpath(),j,iterationInfix);
					like->printLikelihoodMarginZPercentileSingle(s,s2);
				}						
							
			} else {
			
				if (! clh->getSingle()) {				
					sprintf(s,"%s/LikelihoodMarginZPercentile%s.dat",clh->getoutputpath(),iterationInfix);
					like->printLikelihoodMarginZPercentile(s);
				} 
				else {
					sprintf(s,"%s/LikelihoodMarginZPercentile_l%d%s.dat",clh->getoutputpath(),j,iterationInfix);
					like->printLikelihoodMarginZPercentileSingle(s);
				}		
			}
			
		}				
				
		if (failed==0 && clh->getDump()[0]) {
			// 	  print best_fit_template_spectrum 
			sprintf(s,"%s/BestTemp_l%d%s.dat",clh->getoutputpath(),j,iterationInfix);
			saveObservedTemplate(s,like->getBestT(),like->getBestZi(),like->getBestTemplateNorm());			 		
		}
		
		if (failed==0 && clh->getDump()[1]) {
			//   print for each template the fit		
			//	 (including redshifting, IG absorption, ...)
			for (int t=0;t<numTemplates;t++) {	
				sprintf(s,"%s/Temp%d_l%d%s.dat",clh->getoutputpath(),t,j,iterationInfix);
				saveObservedTemplate(s,t,like->getBestZi(),like->getBestTemplateNorm());	
			}						
		}
		
		if (failed==0 && clh->getDump()[2]) {
			//    print for each template the fit
			//	  (including redshifting but nothing else)
			for (int t=0;t<numTemplates;t++) {	
				sprintf(s,"%s/TempNoMod%d_l%d%s.dat",clh->getoutputpath(),t,j,iterationInfix);
				saveTemplate(s,t,like->getBestZi(),like->getBestTemplateNorm());	
			}									
		}							
		
		if (failed==0 && clh->getDump()[3]) {
			// 	  print f->av.Wavelength,flux_entries[f], flux_errors[f], for
			//    each filter f			  				
			sprintf(s,"%s/Data_l%d%s.dat",clh->getoutputpath(),j,iterationInfix);			
			like->printFluxesHeader(s);
			like->printFluxes(s,filters);
		}
		
		if (clh->getDump()[4]) {				
			sprintf(s,"%s/residual%s.dat",clh->getoutputpath(),iterationInfix);
			like->printResidual(s,filters);					
		}																
		
		// auxiliary file, just needed by the program to transport information
		sprintf(s,"%s/aux%s.dat",clh->getoutputpath(),iterationInfix);
		printAux(s, failed);					
							
		// the big output table						
		sprintf(s,"%s/%s%s.dat",clh->getoutputpath(),clh->getoutputbase(),iterationInfix);
		like->printPhotoZ(ofile);				
	
	}
		
	// calculate & print chi^2 statistics (for convenience of the user)
	for (int i=0;i<=numFilters;i++) {
	  if (chi2Values[i].size()>0) {
	  	sprintf(s,"\n* Chi2-Statistics for all catalog entries with %d filters used in the ML fit",i);
	  	this->out->println(s,1);
	  	
	  	sort(chi2Values[i].begin(),chi2Values[i].end());
	  	
	  	// binNumber is at the maximum of 10 and entries/20;
	  	int binNumber = chi2Values[i].size()/20;
	  	if (binNumber<10)
	  	  binNumber=10;
	  	double perc25 = *(chi2Values[i].begin()+int(chi2Values[i].size()*0.25));
	  	double perc75 = *(chi2Values[i].begin()+int(chi2Values[i].size()*0.75));	  	
	  	double percUser = *(chi2Values[i].begin()+int(chi2Values[i].size()*clh->getPercCut()));
	  		  		  
	  	SimpleHistogram hist(chi2Values[i],binNumber, 0., percUser);
	    sprintf(s,"Write histogram file: %s/chi2Hist_numFilter%d%s.dat ... ",clh->getoutputpath(),i,iterationInfix);
	    this->out->print(s,2); 
	    sprintf(s,"%s/chi2Hist_numFilter%d%s.dat",clh->getoutputpath(),i,iterationInfix);
	  	hist.getContent()->write(s);
	  	this->out->println("done",2); 
	  	
	  	double chi2PeakY=hist.getContent()->getMaxY();
	  	int found;
	  	double chi2PeakX=hist.getContent()->getXValueRight(chi2PeakY, 0, found);
	  	double chi2Min=*min_element(chi2Values[i].begin(),chi2Values[i].end());
	  	double chi2Max=*max_element(chi2Values[i].begin(),chi2Values[i].end());
	  		  	
	  	sprintf(s,"min of chi2 is: %g, max of chi2 is: %g ",chi2Min,chi2Max);
	  	this->out->println(s,2);	  		 
	  	double av = std::accumulate(chi2Values[i].begin(),chi2Values[i].begin()+int(chi2Values[i].size()*0.9+0.5),0.)/chi2Values[i].size();
	  	double median;
	  	if (chi2Values[i].size() % 2 != 0) {
    	   median =  *(chi2Values[i].begin()+chi2Values[i].size()/2);
        } else {
       	   median = (*(chi2Values[i].begin()+chi2Values[i].size()/2-1) + *(chi2Values[i].begin()+chi2Values[i].size()/2))/2.;
        }
        sprintf(s,"10%% clipped-average of chi2 is: %g, median of chi2 is: %g, histogram peak is at: %g",av,median,chi2PeakX);
        this->out->println(s,2);
        sprintf(s,"25 percentile is: %g, 75 percentile is: %g, %3.1f percentile is: %g",perc25,perc75,clh->getPercCut()*100,percUser);
	  	this->out->println(s,1);	  	  	
	  	if (i>3) {
	  	  sprintf(s,"average (10%% clipped) of reduced chi2 is: %g",av/(i-3));
	  	  this->out->println(s,1);
	  	  if (av/(i-3)<0.5) {
	  	  	WARNING("The calculated average reduced chi2 is somewhat low (should be about 1)"); 
	  	  }
	  	  if (av/(i-3)>2) {
	  	  	WARNING("The calculated average reduced chi2 is somewhat high (should be about 1)");
	  	  }
	  	}	  	  
	  }
	}		
	
	#ifdef MEASTIME	 
	  cout << "Total runtime of calcLikelihood : " << rTime << "s" << endl;	 	  
	#endif 						
	
	ofile.close();
	ofile.clear();
	if (this->out)
		out->println(" done",1);
	
}

void BayesSolver::printAuxHeader(char *filename) {
  	ofstream ofile;
	ofile.open(filename);
	CHECK(ofile, filename);
    ofile << "# This is an auxiliary file not to be used by the user of ZEBRA\n# failed chi2 numFilterUsed" << std::endl;
    ofile.close();
    ofile.clear();
}


void BayesSolver::printAux(char *filename,int failed) {
  	ofstream ofile;
	ofile.open(filename,ios::app);
	CHECK(ofile, filename);
    ofile << failed << " " << like->getBest_Chi2() << " " << like->getNumUsedFilters() << std::endl;
    ofile.close();
    ofile.clear();
}

void BayesSolver::calcBayesian()
{
	char s[MAX_LENGTH];
	ofstream ofile;
	
	// create Likelihood object if it does not exist by now
	if (! like) {	
		like = new Likelihood();
		like->set(flux, binZ, fluxRestriction, catalog, templateContainer, 0);
		#if 0 // allows output of more information but makes the code considerably slower
		  like->setOutputHandler(this->out);					
		#endif
		like->setMBMode(clh->getMBMode());
		like->setRightZMode(clh->getRightZMode());
		like->setChi2Add(clh->getChi2Add());
		like->setChi2Mult(clh->getChi2Mult());
	}
	
	// Or calculate prior redshift distribution
	// create Prior object	
	sprintf(s,"%s/%s.dat",clh->getoutputpath(),clh->getpriorbase());
	if (prior)
		delete prior;
	prior = new Prior();
	prior->set(like,templateContainer,s);
	prior->set(out);
	prior->setChi2ReducedCut(clh->getChi2ReducedCut());
	if (clh->getsavePriorIterative()) {		
		prior->setSavePriorIterative(clh->getsavePriorIterative());
	}
		
	cerr << " setting smoothZ=" << clh->getSmoothZ() << endl;
	prior->setSmoothPrior(clh->getSmoothPrior());
	prior->setSmoothT(clh->getSmoothT());
	prior->setSmoothZ(clh->getSmoothZ());				
	prior->setSmoothZMode(clh->getSmoothZMode());	
	
	if (clh->getCalcPriorMode()==0) {	// prior in z and T
			
		// load prior
		if (clh->getloadPrior()) {
			sprintf(s,"%s/%s.dat",clh->getpriorpath(),clh->getpriorLoadBase());
			prior->loadPrior(s);
		} else
			prior->setFlatPrior();					
		
		// calculate prior
		if (clh->getCalcPrior()) {
			prior->setMaxIterations(clh->getMaxIterations());
			prior->calcPrior();
		}
			
		// save prior if this is wanted
		if (clh->getsavePrior()) {			
			sprintf(s,"%s/%s.dat",clh->getpriorpath(),clh->getpriorbase());
			prior->savePrior(s);
			sprintf(s,"%s/%sMarginT.dat",clh->getpriorpath(),clh->getpriorbase());
			prior->savePriorMarginT(s);
			sprintf(s,"%s/%sMarginZ.dat",clh->getpriorpath(),clh->getpriorbase());
			prior->savePriorMarginZ(s);		
			sprintf(s,"%s/%sMarginZPercentile.dat",clh->getpriorpath(),clh->getpriorbase());
			prior->savePriorMarginZPercentile(s);		
		}														
										
		if (clh->getPosterior()[0]) {
			sprintf(s,"%s/posterior.dat",clh->getoutputpath());
			prior->setFilenamePost(s);
		}
		if (clh->getPosterior()[1]) {
			sprintf(s,"%s/posteriorMarginT.dat",clh->getoutputpath());
			prior->setFilenamePostT(s);										
		}
		if (clh->getPosterior()[2]) {
			sprintf(s,"%s/posteriorMarginZ.dat",clh->getoutputpath());
			prior->setFilenamePostZ(s);					
		}
		if (clh->getPosterior()[3]) {
			if (clh->getPosterior()[4]) {
				sprintf(s,"%s/posteriorBestTPercentile.dat",clh->getoutputpath());
				prior->setFilenamePostPZT(s);			
			} 
			sprintf(s,"%s/posteriorMarginZPercentile.dat",clh->getoutputpath());
			prior->setFilenamePostPZ(s);			
		}
		sprintf(s,"%s/Bayes_%s.dat",clh->getoutputpath(),clh->getoutputbase());
		ofile.open(s);
		CHECK(ofile,s);
		prior->printPhotoZHeader(ofile);
		prior->printPhotoZ(ofile, clh->getPosterior(), clh->getSingle());		
		ofile.close();
		ofile.clear();		
				
	} else {						// prior just in z
			
		// load prior
		if (clh->getloadPrior()) {
			sprintf(s,"%s/%sBestT.dat",clh->getpriorpath(),clh->getpriorLoadBase());
			prior->loadPriorBestT(s);
		} else
			prior->setFlatPriorBestT();
		
		if (clh->getCalcPrior()) {
			prior->setMaxIterations(clh->getMaxIterations());
			prior->calcPriorBestT();
		}	
			
		// save prior if this is wanted
		if (clh->getsavePrior()) {			
			sprintf(s,"%s/%sBestT.dat",clh->getpriorpath(),clh->getpriorbase());
			prior->savePriorBestT(s);	
		}
				
		sprintf(s,"%s/BayesBestT_%s.dat",clh->getoutputpath(),clh->getoutputbase());
		char *filenamePost=NULL;			
		if (clh->getPosterior()[0] || clh->getPosterior()[2]) {
			allocArray1(filenamePost,MAX_LENGTH);
			sprintf(filenamePost,"%s/posteriorBestT.dat",clh->getoutputpath());
		}			
		ofile.open(s);
		CHECK(ofile,s);		
		prior->printPhotoZHeader(ofile);
		prior->printPhotoZBestT(ofile,clh->getPosterior()[2],clh->getPosterior()[3], clh->getSingle());
		freeArray1(filenamePost);		
		ofile.close();
		ofile.clear();															
	}	
}

void BayesSolver::prepareA() 
{	
	char **nameFilters;
	char s[MAX_LENGTH];
	
	// create catalog
	createCatalog();	
	catalog->readDefFile(clh->getnumFilters(),clh->getnameFilters(),
						 this->numFilters,nameFilters);	
						 				 						 	
	// create filters					
	createFilters(this->numFilters, nameFilters);
	freeArray2(nameFilters,this->numFilters);	
	
	// load catalog	
	catalog->load();
	if (this->out) {
		sprintf(s,"Catalog loaded: %s",catalog->getName());
		out->println(s,1);
	}
	
	catalog->show();
}

void BayesSolver::prepareAnoCatalog() 
{				
	// create filters					
	createFilters(clh->getnumFilters(), clh->getnameFilters());	
}

void BayesSolver::prepareB()
{
	char s[MAX_LENGTH];
	double *z,*dz;							
	
	// try to load B filter from usual filter location			
	// this filter is necessary for, e.g. normalization of the templates 
	// (when linearly interpolating) or for for the magnitude limit cuts
	 
	sprintf(s,"%s/%s",clh->getfilterpath(),clh->getBFilterName());
	if (B_filter)
		delete B_filter;
	B_filter = new Filter(s);	
	B_filter->setOutput(out);	
	if (this->out) {
		sprintf(s,"B-Filter: %s created",B_filter->getName());
		out->println(s,2);
	}
	B_filter->check();	
		
	// prepare red shift bins
	binZ = new BinningZ();
		
	binZ->set(clh->getbinZType(),clh->getZmin(),clh->getZmax(),clh->getdZ());
	z=binZ->getZ();	
	dz=binZ->getdZ();												

	// create FluxRestriction object
	fluxRestriction = new FluxRestriction(binZ,clh->getOmegaM(),clh->getOmegaL(),clh->getHubble());
	fluxRestriction->setMBLow(clh->getMBLow());
	fluxRestriction->setMBHigh(clh->getMBHigh());
	
	if (this->out) {
		sprintf(s,"%d z-values ranging from %.6f to %.6f",binZ->getLength(),z[0],z[binZ->getLength()-1]);
		out->println(s,2);
		sprintf(s,"luminosity-distance for different z - cosmological parameters OMEGA_M=%g, OMEGA_L=%g, h=%g",clh->getOmegaM(),clh->getOmegaL(),clh->getHubble());
		out->println(s,3);
		out->println("-----------------------------------",2);		
		for (int i=0;i<binZ->getLength();i++) {
			sprintf(s,"%.6f",z[i]);
			out->print(s,3);
			sprintf(s," %.6f",dz[i]);
			out->print(s,3);
			sprintf(s," %.6f",fluxRestriction->getDL()[i]);
			out->println(s,3);
		}		
	}	
	
	// create templates
	templateContainer = new TemplateContainer();
	templateContainer->setOutputHandler(this->out);
					
	TemplateOption *templateOption = new TemplateOption(clh->getnumTemplates(),
					 clh->getnameTemplates(),clh->gettemplatepath());			
	templateOption->setIGAbsorption(clh->getigAbsorption());
	templateOption->setFluxType(clh->getFluxType());
	templateOption->setMassToLightRatios(clh->getMassToLightRatios());
	templateOption->setMinZ(clh->getMinZ());
	templateOption->setMaxZ(clh->getMaxZ());
	templateOption->setBFilter(this->B_filter);	
	
	templateOption->setLogInterpolation(clh->getLogInterpolation());
	templateOption->setLogInterpolationMode(clh->getLogInterpolationMode());
	templateOption->setLinInterpolation(clh->getLinInterpolation());
	templateOption->setLinInterpolationMode(clh->getLinInterpolationMode());	
	
	templateContainer->setOptions(templateOption);
	
	delete templateOption;
	
	templateContainer->createTemplates();
	
	// allocate templates
	freeArray1(templates);	
	numTemplates=templateContainer->getNumOrigTemplates();			
	allocArray1(templates,numTemplates);
	for (int i=0;i<numTemplates;i++) {
		templates[i]=templateContainer->getOrigTemplate(i);			
	}		
	
	if (this->out) {
		sprintf(s,"Filter-name\tPivot-wavelength\tAverage-wavelength\tRMS-Width\tFWHM\tMax");
		out->println(s,2);
		for (int i=0;i<this->numFilters;i++) {
			sprintf(s,"%s\t%g\t%g\t%g\t%g\t%g",filters[i]->getName(),filters[i]->getPivotWavelength(),
					filters[i]->getAverageWavelength(),filters[i]->getRMSWidth(),
					filters[i]->getFWHM(),filters[i]->getMax());
			out->println(s,2);
		}
	}
	
		   					
	if (clh->getPrintData()[0])
		printFilters("Filter");
		
	// smooth filters   
	if (clh->getSmoothFilter()>=0) {
		
		if (this->out) {
			if (clh->getSmoothTFMode()==0)
			  sprintf(s,"Smoothing filters with fwhm = %g ... ",clh->getSmoothFilter());
			else
			  sprintf(s,"Smoothing filters with fwhm/lambda = %g ... ",clh->getSmoothFilter());
			this->out->print(s,1);
		}
		
		for (int f=0;f<numFilters;f++) 
			this->filters[f]->smoothRectangular(clh->getSmoothFilter(), clh->getSmoothTFMode() );	
			
		if (clh->getPrintData()[0]) {
			sprintf(s,"Filter_smooth");
			printFilters(s);	
		}
		
		if (this->out)
			this->out->println("done",1);
	}		
	
	if (clh->getPrintData()[1])
		printTemplates("Template");
	
	// smooth templates				
	if (clh->getSmoothTemplate()>=0) {				
		
		if (this->out) {
		        if (clh->getSmoothTFMode()==0)
			  sprintf(s,"Smoothing templates with fwhm = %g ... ",clh->getSmoothTemplate());
			else
			  sprintf(s,"Smoothing templates with fwhm/lambda = %g ... ",clh->getSmoothTemplate());
			this->out->print(s,1);
		}
		
		for (int t=0;t<numTemplates;t++) 
		        this->templates[t]->smooth( clh->getSmoothTemplate(), clh->getSmoothLimitLow(), clh->getSmoothTFMode());
			
		if (clh->getPrintData()[1]) {
			sprintf(s,"Template_smooth");
			printTemplates(s);	
		}
		
		if (this->out)
			this->out->println("done",1);
	}									

    // check whether prior-load file can be found
	if (clh->getloadPrior()) {
		sprintf(s,"%s/%s.dat",clh->getpriorpath(),clh->getpriorLoadBase());		
		ifstream infile;
		infile.open(s);
	    CHECK( infile, s );
	}

	// print table of template names and ID if this is wanted
	if (clh->getShowTable())
		templateContainer->printTemplateTable();
}

// checks whether templates are always encompassing all filters or not
// this check is done after possible putting the templates to a mesh
void BayesSolver::checkTemplateRange() {
   char s[MAX_LENGTH];
      
   // check whether templates are large enough to accommodate all filters for complete redshift range
   // the generated warning should encourage the user to extend the templates; if he does not, however,
   // the considered filterbands are simple removed from the chi2 fit. This (very simple) scheme should be
   // improved later.
   
   for (int i=0;i<numTemplates;i++) {
   	 bool template_ext=false;
   	 std::vector<int> filterIndex(0);
   	 
   	 for (int f=0;f<numFilters;f++) {
   	   if (templates[i]->getxyData()->getMinX()*(1+binZ->getZ()[binZ->getLength()-1]) > filters[f]->getxyData()->getMinX()) {
   	   	 if (template_ext==false) {
   	   	   sprintf(s,"Wavelength range problem with template %s\nPlease extend the template (or the mesh if using --use-mesh or --improve-template) into the UV\n"
   	   	             "or reduce the maximum redshift (--zmax) in order to avoid problems with the following filter(s):",templates[i]->getShortName());   	   	      	   	      	   	      	   	
   	   	   WARNING(s);
   	   	 }
   	   	 template_ext=true;
   	   	 filterIndex.push_back(f);
   	   }
   	 }   	 
   	 if (template_ext==true) {
   	    for (unsigned f=0;f<filterIndex.size();f++) {
   	    	WARNINGCONT(filters[filterIndex[f]]->getShortName());
   	    }
   	 }
   	 
   	 filterIndex.clear();
   	 template_ext=false;
   	 
   	 for (int f=0;f<numFilters;f++) {
   	   if (templates[i]->getxyData()->getMaxX()*(1+binZ->getZ()[0]) < filters[f]->getxyData()->getMaxX()) {
   	   	 if (template_ext==false) {
   	   	   sprintf(s,"Wavelength range problem with template %s\nPlease extend the template (or the mesh if using --use-mesh or --improve-template) into the IR\n"
   	   	             "or increase the minimum redshift (--zmin) in order to avoid problems with the following filter(s):",templates[i]->getShortName());   	   	
   	   	   WARNING(s);
   	   	 }
   	   	 template_ext=true;
   	   	 filterIndex.push_back(f);
   	   }
   	 }   	 
   	 if (template_ext==true) {
   	    for (unsigned f=0;f<filterIndex.size();f++) {
   	    	WARNINGCONT(filters[filterIndex[f]]->getShortName());
   	    }
   	 }   	    	    
   }	
}	

void BayesSolver::calcFlux()
{
	int loadFlux;
	char filename[MAX_LENGTH];
	char s[MAX_LENGTH];
	double *z;
	
	// if reuse is 1 try to load flux file
	// if it has same z range, filter range and template range use it, 
	// otherwise calculate fluxes through templates		
	loadFlux=clh->getloadFlux();
	z=binZ->getZ();			
	
	if ( loadFlux ) {
		sprintf(filename,"%s/%s.dat",clh->getfluxpath(),clh->getfluxbase());
		FluxFile *fluxfile = new FluxFile(filename);		
		fluxfile->loadStepByStep(flux,numTemplates,binZ->getLength(),numFilters);
		delete fluxfile;
		//is needed if best-fitting template wants to be written!
		for (int t=0;t<numTemplates;t++) {
			templates[t]->prepareCalcFlux(numFilters,filters,binZ);
		}
	} 
	
	if (this->out) {
	        sprintf(s,"Calculating template flux [%d]: ",numTemplates);
		out->print(s,1);
	}
	
	if (! loadFlux ) {
			
		for (int t=0;t<numTemplates;t++) {					
			templates[t]->prepareCalcFlux(numFilters,filters,binZ);
#ifdef PARRAY						
			templates[t]->calcFlux(flux[t]);
#else
			templates[t]->calcFlux(&flux[t*binZ->getLength()*numFilters]);
#endif			
			if (this->out) {
				if ((t+1)%10==0) {
					sprintf(s,"%d",t+1);
					out->print(s,1);
				} else
					out->print(".",1);
			}
				
			this->templates[t]->clear();
			if (this->out) {
				sprintf(s,"%s cleared",this->templates[t]->getName());
				out->println(s,3);					
			}
		}	
		if (this->out)			
			out->println("",1);
		
		if (clh->getsaveFlux()) { // save flux results
			if (this->out) 
				out->print("Exporting template fluxes ... ",1);
			sprintf(filename,"%s/%s.dat",clh->getfluxpath(),clh->getfluxbase());
			FluxFile *fluxfile = new FluxFile(filename);
			fluxfile->save(flux,numTemplates,binZ->getLength(),numFilters);	
			delete fluxfile;
			if (this->out)
				out->println("done",1);					
		}		
	
		// clear filters
		for (int f=0;f<numFilters;f++)
			filters[f]->clear();		
	}
		
	if (clh->getVerboseLevel()>2) {			
		for (int t=0;t<numTemplates;t++) {	
			for (int zi=0;zi<binZ->getLength();zi++) {
				for (int f=0;f<numFilters;f++) {
					cout << "t=" << t << " z=" << z[zi] 
#ifdef PARRAY					
						 << " flux[" << f << "]=" << flux[t][zi][f] << endl;
#else
						 << " flux[" << f << "]=" << flux[t*(binZ->getLength()*numFilters)+zi*numFilters+f] << endl;
#endif						 
				}
			}
		}
	}	
}

void BayesSolver::normalizeTemplates(bool doit, bool usemesh) {  
  if (doit) {
  	cout << "Normalizing templates" << endl;
  	if (this->out)  	  	
  	  this->out->println("Template     Filter      Norm",2);
	// normalizing templates to B band
	for (int t=0;t<numTemplates;t++) {
	  templates[t]->normalizeToFilter_values(this->B_filter);
	  if (usemesh)
	    templates[t]->normalizeToFilter_meshed(this->B_filter);	 
    }
  }	
}

// extend = 1:  templates are extended to grid 
//              even if not given there (set to zero)
// extend = 0:  templates are only meshed in the
//				region where they are given

void BayesSolver::templatesFiltersToMesh(int extend) {	
	int templateExtend=0;
	int filterExtend=0;
	char s[MAX_LENGTH];
	
	if (extend==1) {  // extend template and filters (add zeros if necessesary
	  templateExtend=1; // print warning if template really needs to be extended
	  filterExtend=2;   // don't print warning
	}
	
	// putting templates to the mesh
	for (int t=0;t<numTemplates;t++) {
		if (clh->getLinearMesh()==0) { // put to log-lambda grid			
			templates[t]->calcLogMeshedValues(clh->getMeshLambdaMin(),clh->getMeshLambdaMax(),clh->getMeshLambdaRes(),templateExtend);			
			templates[t]->setValues(templates[t]->getMeshedValues());	
		} else {					   // put to linear grid
			templates[t]->calcMeshedValues(clh->getMeshLambdaMin(),clh->getMeshLambdaMax(),clh->getMeshLambdaRes(),templateExtend);		
			templates[t]->setValues(templates[t]->getMeshedValues());
		}
	}
	
	if (numTemplates>0) {
	  sprintf(s,"Meshing templates: from %g to %g using %d mesh points",
	          (templates[0]->getMeshedValues())->getMinX(),
	          (templates[0]->getMeshedValues())->getMaxX(),
	          (templates[0]->getMeshedValues())->getlength());
	  out->println(s,2);   
	}
		
	// putting filters to the mesh, but want to restrict the extend in lambda space
	// speeding up the flux calculations
	double *y;
	double *x;
	xyData *data;
	int count;
	double minL,maxL;
	int length;
	
	for (int f=0;f<numFilters;f++) {				
		minL=filters[f]->getMinLambda();
		maxL=filters[f]->getMaxLambda();
		if (clh->getLinearMesh()==0) { // put to log-lambda grid
		  filters[f]->calcLogMeshedValues(clh->getMeshLambdaMin(),clh->getMeshLambdaMax(),clh->getMeshLambdaRes(),filterExtend);
		} else {
		  filters[f]->calcMeshedValues(clh->getMeshLambdaMin(),clh->getMeshLambdaMax(),clh->getMeshLambdaRes(),filterExtend);
		}
		
		count=0;
		length=filters[f]->getMeshedValues()->getlength();
		for (int i=0;i<length;i++) {
			if (i<length-1 && filters[f]->getMeshedValues()->getX()[i+1]<minL)
				continue;
			if (i>0 && filters[f]->getMeshedValues()->getX()[i-1]>maxL)
				break;
			count++;
		}
		allocArray1(x,count);
		allocArray1(y,count);
		
		count=0;
		for (int i=0;i<length;i++) {
			if (i<length-1 && filters[f]->getMeshedValues()->getX()[i+1]<minL)
				continue;
			if (i>0 && filters[f]->getMeshedValues()->getX()[i-1]>maxL)
				break;
			x[count]=filters[f]->getMeshedValues()->getX()[i];
			y[count]=filters[f]->getMeshedValues()->getY()[i];
			count++;
		}
		data = new xyData(count,x,y);
		filters[f]->setValues(data);
		delete data;
		freeArray1(x);
		freeArray1(y);
	}				
}

void BayesSolver::calcCatalogCorrection(const char *iterationInfix) 
{					
	char resFileName[MAX_LENGTH];	
	char defFileName[MAX_LENGTH];
	char auxFileName[MAX_LENGTH];
	char fitInfix[MAX_LENGTH];
	char s[MAX_LENGTH];
	char*** resEntries;
	int*	resEntriesNr;
	int 	resEntryLines;
	char*** defEntries;
	int*	defEntriesNr;
	int 	defEntryLines;
	char*** auxEntries;
	int*	auxEntriesNr;
	int 	auxEntryLines;	
	int numberEntries;
	
	int magColumn;   // column of original mag value from catalog
	int magTColumn;	 // column of mag value of template
	int magErrorColumn; // column of error of original mag value from catalog
	double minMagFound; // minimal magnitude for interpolation (for considered filter)
	double maxMagFound; // maximal magnitude for interpolation (for considered filter)
	double lowMag;		// minimal magnitude for extrapolation -> correctionData
	double highMag;		// maximal magnitude for extrapolation -> correctionData
	double correctionDataResolution; // resolution of correctionData object
	double levelY;			 // percentile based rejection - 1 keep all, 0 reject all
	int minimumBinEntries;   // only magnitude bins with at least minimumBinEntries are used in the fit
	
	RegressionData *regressionData;
	xyErrorData *correctionData;
	xyErrorData *data;	
	double *aux_x;
	double *aux_y;
	double *aux_x_error;
	double *aux_y_error;	
	double *aux2_x;
	int aux2;
	
	int numBins;
	int interMode;
	
	double *param,**cov;
	double chisq;
	
	int *filterMapping; // filterMapping[i]=j says that filters[i] corresponds to j-th 
						// line in catalogCorrection.def file
	int found;
	int found_index;
	
	char filename[MAX_LENGTH];
	ofstream ofile;		

	ConfFile *residualFile=NULL;
	ConfFile *defFile=NULL;
	ConfFile *auxFile=NULL;
	
	// read definition file which has the following format (at least 8 columns)
	// filter_name numberBins min_Magnitude max_Magnitude InterpolMode ExtrapolMode	low_limit high_limit	
	sprintf(defFileName,"%s/%s",clh->getoutputpath(),clh->getCatalogCorrectionDef());
	defFile = new ConfFile(defFileName);
	defFile->setOutputHandler(this->out);
	defFile->read();
	defFile->printTotalStatus();	
	defFile->getEntries(defEntries,defEntriesNr,defEntryLines);	
	
	for (int i=0;i<defEntryLines;i++) {
		if (defEntriesNr[i]<8) {
			sprintf(s,"Columns missing in %s or corrupted file!",defFileName);
			EXIT(s);
		}
	}
	
	allocArray1(filterMapping,numFilters);
											
	for (int i=0;i<numFilters;i++) {
		found=0;
		found_index=-1;
		for (int j=0;j<defEntryLines;j++) {
			if (strcmp(filters[i]->getShortName(),defEntries[j][0])==0) {
				found++;
				found_index=j;
			}
		}	
		
		if (found>1) {
			sprintf(s,"Multiple occurance of Filter %s in %s!",filters[i]->getShortName(),defFileName);
			WARNING(s);			
		} 			
		filterMapping[i]=found_index;
	}								
						
    	for (int i=0;i<numFilters;i++) {
            if (filterMapping[i]>=0) {
    	      sprintf(s,"Calculate corrections for filter %s",defEntries[filterMapping[i]][0]);
	      this->out->println(s,2);
	    }
	}
	
    std::vector< std::vector<double> > chi2Values(numFilters+1,std::vector<double>(0));
    std::vector<double> percUserFilter(numFilters+1);
    
	if (clh->getSingleFit()) { // in this mode just one fit is done for all filters simulateously
	  			   			   // hence, only one residual file is generated
	  clh->setDump(4,1);       // dump residual file
	  calcMaxLikelihood(iterationInfix);

  	  // NOW: use the fit results (residuals)
	  sprintf(resFileName,"%s/residual%s.dat",clh->getoutputpath(),iterationInfix);
	  residualFile = new ConfFile(resFileName);
	  residualFile->setOutputHandler(this->out);
	  residualFile->read();
	  residualFile->printTotalStatus();
	  residualFile->getEntries(resEntries,resEntriesNr,resEntryLines);
	  
	  // this file contains just chi2 and numUsedFilter bands for each object
	  // this file was created to avoid at this point to read in the whole ML.dat table
	  sprintf(auxFileName,"%s/aux%s.dat",clh->getoutputpath(),iterationInfix);
	  auxFile = new ConfFile(auxFileName);
	  auxFile->setOutputHandler(this->out);
	  auxFile->read();
	  auxFile->printTotalStatus();
	  auxFile->getEntries(auxEntries,auxEntriesNr,auxEntryLines);		
	 	  
	  for (int i=0;i<auxEntryLines;i++) {
	    if (! atoi(auxEntries[i][0])) // i.e. if not failed
	     chi2Values[atoi(auxEntries[i][2])].push_back(atof(auxEntries[i][1]));
	  }	  
	  for (int i=0;i<=numFilters;i++) {
	    if (chi2Values[i].size()>0) {	  	
	  	  sort(chi2Values[i].begin(),chi2Values[i].end());	  		  
	  	  percUserFilter[i] = *(chi2Values[i].begin()+int(chi2Values[i].size()*clh->getPercCut()));
	    }	  
	  }		  
    }  
	
	/*
	// this file contains just chi2 and numUsedFilter bands for each object
	// this file was created to avoid at this point to read in the whole ML.dat table
	sprintf(auxFileName,"%s/aux%s.dat",clh->getoutputpath(),iterationInfix);
	auxFile = new ConfFile(auxFileName);
	auxFile->setOutputHandler(this->out);
	auxFile->read();
	auxFile->printTotalStatus();
	auxFile->getEntries(auxEntries,auxEntriesNr,auxEntryLines);		
	 
	std::vector< std::vector<double> > chi2Values(numFilters+1,std::vector<double>(0));
	for (int i=0;i<auxEntryLines;i++) {
	  if (! atoi(auxEntries[i][0])) // i.e. if not failed
	   chi2Values[atoi(auxEntries[i][2])].push_back(atof(auxEntries[i][1]));
	}
	std::vector<double> percUserFilter(numFilters+1);
	for (int i=0;i<=numFilters;i++) {
	  if (chi2Values[i].size()>0) {	  	
	  	sort(chi2Values[i].begin(),chi2Values[i].end());	  		  
	  	percUserFilter[i] = *(chi2Values[i].begin()+int(chi2Values[i].size()*clh->getPercCut()));
	  }	  
	}	
	*/	 
			
	for (int f=0;f<numFilters;f++) {
		
		data = new xyErrorData();	
		regressionData = new RegressionData();
		correctionData = new xyErrorData();
		
		if (filterMapping[f]<0) {
		        delete data;
		        delete regressionData;
		        delete correctionData;
			continue;
		}	
		
		magColumn=1+f*4;
		magTColumn=2+f*4;
		magErrorColumn=3+f*4;	
		
		minMagFound=99.0;
		maxMagFound=-99.0;
				
		// if not in singleFit mode -- otherwise this is done for all filters at one (outside the loop)
		if (! clh->getSingleFit()) {
		
		  sprintf(fitInfix,"%s%s",iterationInfix,filters[f]->getShortName());
		  sprintf(resFileName,"%s/residual%s.dat",clh->getoutputpath(),fitInfix);
		  		 
		  // degrade current filter (increase the relative error) 
		  catalog->getFilterBands()[f]->multiplyError(clh->getDowngradingFactor());
		  
		  // calculate residuals
	  	  clh->setDump(4,1); // dump residual file
	          calcMaxLikelihood(fitInfix);

		  catalog->getFilterBands()[f]->multiplyError(1./clh->getDowngradingFactor());
		  
		  // read the residual file
		  residualFile = new ConfFile(resFileName);
		  residualFile->setOutputHandler(this->out);
		  residualFile->read();
		  residualFile->printTotalStatus();
		  residualFile->getEntries(resEntries,resEntriesNr,resEntryLines);
		  
		  // this file contains just chi2 and numUsedFilter bands for each object
	      // this file was created to avoid at this point to read in the whole ML.dat table
	      sprintf(auxFileName,"%s/aux%s.dat",clh->getoutputpath(),fitInfix);
	      auxFile = new ConfFile(auxFileName);
	      auxFile->setOutputHandler(this->out);
	      auxFile->read();
	      auxFile->printTotalStatus();
	      auxFile->getEntries(auxEntries,auxEntriesNr,auxEntryLines);		
	 	  
	 	  for (int i=0;i<auxEntryLines;i++)
	 	    std::cout << auxEntries[i][0] << " " << auxEntries[i][1] << std::endl;
	 	     
	 	  	
	 	  for (int i=0;i<numFilters;i++)
	 	    chi2Values[i].resize(0);
	 	    	 	         
	      for (int i=0;i<auxEntryLines;i++) {
	       if (! atoi(auxEntries[i][0])) // i.e. if not failed
	        chi2Values[atoi(auxEntries[i][2])].push_back(atof(auxEntries[i][1]));
	      }	  
	      
	      for (int i=0;i<numFilters;i++)
	 	    percUserFilter[i]=0;
	 	    
	      for (int i=0;i<=numFilters;i++) {
	       if (chi2Values[i].size()>0) {	  	
	  	    sort(chi2Values[i].begin(),chi2Values[i].end());	  		  
	  	    percUserFilter[i] = *(chi2Values[i].begin()+int(chi2Values[i].size()*clh->getPercCut()));
	       }
	      }					 
		}
		
		// alloc auxiliary memory
	    allocArray1(aux_x,resEntryLines);
		allocArray1(aux_y,resEntryLines);
		allocArray1(aux_x_error,resEntryLines);
		allocArray1(aux_y_error,resEntryLines);

		// obtain the residuals
		numberEntries=0;
		for (int i=0;i<resEntryLines;i++) {		
			if (resEntriesNr[i]<numFilters*4) {
				sprintf(s,"Filter entries missing in %s or corrupted file!",resFileName);				
				EXIT(s);
			}
									
			if (fabs(atof(resEntries[i][magColumn]))>=99 || atoi(auxEntries[i][0])!=0 ) {			   
			   continue;
			}
			
			if (atof(auxEntries[i][1]) > percUserFilter[atoi(auxEntries[i][2])]) {				   		   
			   continue;
			}
			
			if ((atoi(auxEntries[i][2]) > 3) && (atof(auxEntries[i][1]) > clh->getChi2ReducedCut()*(atoi(auxEntries[i][2])-3)) ) {			   
			   continue;
			}
					
			aux_x[numberEntries]=atof(resEntries[i][magColumn]);
			aux_y[numberEntries]=aux_x[numberEntries]-atof(resEntries[i][magTColumn]);
			aux_y_error[numberEntries]=aux_x_error[numberEntries]=atof(resEntries[i][magErrorColumn]);
			if (aux_x[numberEntries]<minMagFound)
			minMagFound=aux_x[numberEntries];
			if (aux_x[numberEntries]>maxMagFound)
				maxMagFound=aux_x[numberEntries];			
			numberEntries++;					
		}
		data->set(numberEntries,aux_x,aux_y,aux_x_error,aux_y_error);

		// free auxiliary memory
		freeArray1(aux_x);
		freeArray1(aux_y);
		freeArray1(aux_x_error);
		freeArray1(aux_y_error);
		
		// close residual file (if not in singleFit mode)
		if (! clh->getSingleFit()) {
		  delete residualFile;
		  delete auxFile;
		}
		
		if (this->out) {
			sprintf(s,"Filter %s with %d entries is being inspected ... ",filters[f]->getShortName(),data->getlength());
			this->out->print(s,1);
		}
											
		if (strcmp(defEntries[filterMapping[f]][2],"x")!=0)
			minMagFound=atof(defEntries[filterMapping[f]][2]);																
		
		if (strcmp(defEntries[filterMapping[f]][3],"x")!=0)
			maxMagFound=atof(defEntries[filterMapping[f]][3]);		
						
		// construct correctionData
		if (defEntriesNr[filterMapping[f]]>6 && strcmp(defEntries[filterMapping[f]][6],"x")!=0)
			lowMag=atof(defEntries[filterMapping[f]][6]);
		else
			lowMag=minMagFound;
		if (defEntriesNr[filterMapping[f]]>7 && strcmp(defEntries[filterMapping[f]][7],"x")!=0)
			highMag=atof(defEntries[filterMapping[f]][7]);
		else
			highMag=maxMagFound;
		if (defEntriesNr[filterMapping[f]]>8 && strcmp(defEntries[filterMapping[f]][8],"x")!=0)
			correctionDataResolution=atof(defEntries[filterMapping[f]][8]);
		else
			correctionDataResolution=0.05;
		if (defEntriesNr[filterMapping[f]]>9 && strcmp(defEntries[filterMapping[f]][9],"x")!=0)
			levelY=atof(defEntries[filterMapping[f]][9]);
		else
			levelY=0.954;
		if (defEntriesNr[filterMapping[f]]>10 && strcmp(defEntries[filterMapping[f]][10],"x")!=0)
			minimumBinEntries=atoi(defEntries[filterMapping[f]][10]);
		else
			minimumBinEntries=10;		
				
		aux2=(int)floor((highMag-lowMag)/correctionDataResolution+0.5)+1;
		correctionDataResolution=(highMag-lowMag)/(aux2-1.);
		allocArray1(aux2_x,aux2);	
		for (int i=0;i<aux2;i++)
			aux2_x[i]=lowMag+i*correctionDataResolution;
		correctionData->set(aux2,aux2_x);
		freeArray1(aux2_x);
		
		// fill regression object & set options
		regressionData->set(data);
			// number of magnitude bins	
		numBins=atoi(defEntries[filterMapping[f]][1]);
		regressionData->setNumBins(numBins); 
			// smallest magnitude value
		regressionData->setLeft(minMagFound);
			// largest magnitude value
		regressionData->setRight(maxMagFound);	
			// calculate y-errors from binned data (if numBins>0)
		regressionData->setWeightedY(0);
		    // set minimum number of entries for each bin to be used
		regressionData->setMinimumBinEntries(minimumBinEntries);
			// set percentile based rejection level (to eliminate outliers)
		regressionData->setLevelY(levelY);			
			// set interpolation Mode & extrapolation Mode
		interMode=atoi(defEntries[filterMapping[f]][4]);								
		regressionData->setInterpolationMode(interMode); 
		regressionData->setExtrapolationMode(atoi(defEntries[filterMapping[f]][5])); 	
		
		allocArray1(param,interMode+1);
		allocArray2(cov,interMode+1,interMode+1);
		
		// obtain fitting curve
		regressionData->getRegression(correctionData,interMode+1,param,cov,chisq);
			
		if (this->out)	
			this->out->println("done",1);				
				
		sprintf(filename,"%s/%s%s_%s.stat",clh->getoutputpath(),clh->getCatalogCorrectionBase(),iterationInfix,filters[f]->getShortName());
		if (this->out) {
			sprintf(s,"Writing into file %s",filename);
			this->out->println(s,2);			
		}
		ofile.open(filename);
		CHECK(ofile,filename);
		ofile << "#------- Fitting results ----------" << endl;
		ofile << "# chisq = " << chisq << endl;	
		ofile << "# parameters" << endl;
		for (int i=0;i<interMode+1;i++) {		
			ofile << "# c[" << i << "]=" << param[i] << endl;
		}	
		ofile << "# Covariance matrix" << endl;
		for (int i=0;i<interMode+1;i++) {				
			ofile << "# [ ";
			for (int j=0;j<interMode+1;j++) 
				ofile << cov[i][j] << " ";
			ofile << " ]" << endl;
		}						
		if (numBins>0) {
			ofile << "#------- Binning --------------" << endl;
			ofile << "# mag  res(mag-mag_Template) sigma(res)" << endl;		
			ofile.close();
			ofile.clear();			
			regressionData->getBinnedData()->append(filename,1,1,0,1);
		} else {
			ofile.close();
			ofile.clear();	
		}
		
		sprintf(filename,"%s/%s%s_%s.corr",clh->getoutputpath(),clh->getCatalogCorrectionBase(),iterationInfix,filters[f]->getShortName());
		if (this->out) {
			sprintf(s,"Writing into file %s",filename);
			this->out->println(s,2);
		}
		ofile.open(filename);
		CHECK(ofile,filename);
		ofile << "# mag  <residual(mag-mag_Template)> sigma(res)" << endl;		
		ofile.close();
		ofile.clear();		
		correctionData->append(filename,1,1,0,1);
		
		freeArray1(param);
		freeArray2(cov,interMode+1);
		
		delete data;
		delete regressionData;
		delete correctionData;		
	}	

	if (clh->getSingleFit()) {
	  delete residualFile;
	  delete auxFile;
	}
	delete defFile;
	
	freeArray1(filterMapping);
	
	//delete data;
	//delete regressionData;
	//delete correctionData;			
}	

void BayesSolver::applyCatalogCorrection(const char *iterationInfix)
{		
	ConfFile *corrFile;
	xyData *dataCorr;
	char*** corrEntries;
	int*	corrEntriesNr;
	int 	corrEntryLines;
	double *magVal,*magCorr;
	double aux;
	double aux2;
	
	char s[MAX_LENGTH];
	char filename[MAX_LENGTH];
	ifstream ifile;	

	char defFileName[MAX_LENGTH];
	char*** defEntries;
	int*	defEntriesNr;
	int 	defEntryLines;
	int *filterMapping; 		// filterMapping[i]=j says that filters[i] corresponds to j-th 
					// line in catalogCorrection.def file
	int found;
	int found_index;
	
	// read definition file to determine which filters shall be affected by the .corr files
        // only the first column (filter_name) is of interest	
	// read definition file which has the following format (6 columns)
	// filter_name numberBins min_Magnitude max_Magnitude InterpolMode ExtrapolMode		
	ConfFile *defFile;
	sprintf(defFileName,"%s/%s",clh->getoutputpath(),clh->getCatalogCorrectionDef());
	defFile = new ConfFile(defFileName);
	defFile->setOutputHandler(this->out);
	defFile->read();
	defFile->printTotalStatus();	
	defFile->getEntries(defEntries,defEntriesNr,defEntryLines);	
	
	allocArray1(filterMapping,numFilters);
											
	for (int i=0;i<numFilters;i++) {
		found=0;
		found_index=-1;
		for (int j=0;j<defEntryLines;j++) {
			if (strcmp(filters[i]->getShortName(),defEntries[j][0])==0) {
				found++;
				found_index=j;
			}
		}	
		
		if (found>1) {
			sprintf(s,"Multiple occurance of Filter %s in %s!",filters[i]->getShortName(),defFileName);
			WARNING(s);			
		} 			
		filterMapping[i]=found_index;
	}								
						
    	for (int i=0;i<numFilters;i++) {
          if (filterMapping[i]>=0) {
    	    sprintf(s,"Apply corrections to filter %s",defEntries[filterMapping[i]][0]);
	    this->out->println(s,2);
	  }
	}
	
	// check whether all .corr files exist				
	for (int f=0;f<numFilters;f++) {
	   if (filterMapping[f]>=0) {
		sprintf(filename,"%s/%s%s_%s.corr",clh->getoutputpath(),clh->getCatalogCorrectionBase(),iterationInfix,filters[f]->getShortName());		
		ifile.open(filename);
		CHECK(ifile,filename);
		ifile.close();
		ifile.clear();
	   }
	}

	for (int f=0;f<numFilters;f++) {
	  if (filterMapping[f]>=0) {
		sprintf(filename,"%s/%s%s_%s.corr",clh->getoutputpath(),clh->getCatalogCorrectionBase(),iterationInfix,filters[f]->getShortName());	
		corrFile = new ConfFile(filename);		
		corrFile->setOutputHandler(this->out);
		corrFile->read();
		corrFile->printTotalStatus();
		corrFile->getEntries(corrEntries,corrEntriesNr,corrEntryLines);
		allocArray1(magVal,corrEntryLines);
		allocArray1(magCorr,corrEntryLines);
		
		bool flag_NAN_value=false;
		bool flag_NAN_corr=false;
		for (int i=0;i<corrEntryLines;i++) {
			double aux;
			aux = atof(corrEntries[i][0]);
			if (aux!=aux)
				flag_NAN_value=true;			
			magVal[i]=aux;
			aux = atof(corrEntries[i][1]);
			if (aux!=aux)
				flag_NAN_corr=true;				
			magCorr[i]=aux;				
		}
		if (flag_NAN_value) {
		  sprintf(s,"NaN was read in column 1 in file %s\n As a last resort ZEBRA disabled corrections in filter %s!",filename,filters[f]->getShortName());		  
		  WARNING(s);
		}
		if (flag_NAN_corr) {
		  sprintf(s,"NaN was read in column 2 in file %s\n As a last resort ZEBRA disabled corrections in filter %s!",filename,filters[f]->getShortName());		  
		  WARNING(s);
		}
		
		if (flag_NAN_corr==false && flag_NAN_value==false) {
		
		  dataCorr = new xyData(corrEntryLines,magVal,magCorr);
		  for (int i=0;i<catalog->getFilterBands()[f]->getLength();i++) {
			aux=g_fluxToMag(catalog->getFilterBands()[f]->getValue()[i]);
			int outofbounds;
			aux2=dataCorr->calcYValue(aux,outofbounds);
			if (outofbounds==0)
			  aux-=aux2;
			else if (dataCorr->getlength()>0) {
			  if (outofbounds==-1)
			    aux-=dataCorr->getY()[0];
			  else 
			    aux-=dataCorr->getY()[dataCorr->getlength()-1];
			}					
			catalog->getFilterBands()[f]->getValue()[i]=g_magToFlux(aux);
		  }
		  delete dataCorr;
		}
		
		delete corrFile;
		freeArray1(magVal);
		freeArray1(magCorr);
	  }	
	}
	
	sprintf(s,"%s%s.corr",catalog->getShortName(),iterationInfix);
	catalog->save(clh->getoutputpath(),s);
	delete defFile;
	freeArray1(filterMapping);
}

void BayesSolver::calcKcorrection()
{
	char s[MAX_LENGTH];
	// calculate k-correction
	sprintf(s,"%s/%s.dat",clh->getoutputpath(),clh->getoutputbase());
	ofstream ofile;
	ofile.open(s);
	CHECK(ofile, s);
	ofile << "# Structure of the file: first column redshift" << endl;
	ofile << "# The next columns print mag[template][filter] where the filter index varies fastest" << endl;
	ofile << "# here mag[redshift z][template t][filter f] corresponds to the following flux: " << endl;
	ofile << "# flux(z,t,f)=(\\int dlambda*lambda*flux_lambda_restframe(lambda/(1+z))*filter(lambda))/(\\int dlambda/lambda*filter(lambda))" << endl;
	ofile << "# mag=-2.5*log10(f_nu)+const" << endl;
	ofile << "# The order of filters/templates is the same as in the respective .conf files" << endl;
	double * z=binZ->getZ();		
	//unsigned zlength = binZ->getLength();
	
	for (int zi=0;zi<binZ->getLength();zi++) {
		ofile << z[zi] << " ";
		for (int t=0; t<numTemplates; t++) 
			for (int f=0; f<numFilters; f++) 
#ifdef PARRAY			
				ofile << g_fluxToMag(flux[t][zi][f]) << " ";
#else
				ofile << g_fluxToMag(flux[t*zlength*numFilters+zi*numFilters+f]) << " ";
#endif				
				//ofile << flux[t][zi][f] << " ";
		ofile << endl;
	}
	ofile.close();
	ofile.clear();
	
	// calculate real k-correction
	sprintf(s,"%s/%s_real.dat",clh->getoutputpath(),clh->getoutputbase());
	ofile.open(s);
	CHECK(ofile, s);
	ofile << "# K-correction" << endl;
	ofile << "# Structure of the file: first column redshift" << endl;
	ofile << "# The next columns print 2.5log10(1+z)+(mag[template][2nd filter][z]-mag[template][1st filter][z0])" << endl;
	ofile << "# mag=-2.5*log10(f_nu)+const" << endl;	
	ofile << "# The order of filters/templates is the same as in the respective .conf files" << endl;	
	for (int zi=0;zi<binZ->getLength();zi++) {
		ofile << z[zi] << " ";
		for (int t=0; t<numTemplates; t++) 	
#ifdef PARRAY				
		  ofile << g_fluxToMag(flux[t][zi][1])-g_fluxToMag(flux[t][0][0])+2.5*log10(1+z[zi]) << " ";
#else
		  ofile << g_fluxToMag(flux[t*zlength*numFilters+zi*numFilters+1])-g_fluxToMag(flux[t*zlength*numFilters])+2.5*log10(1+z[zi]) << " ";
#endif			
		ofile << endl;
	}
	ofile.close();
	ofile.clear();		
	
	// calculate k-correction which is approx zero if pivot_wavelength of both filters are in the ration 1+z
	sprintf(s,"%s/%s_zero.dat",clh->getoutputpath(),clh->getoutputbase());
	ofile.open(s);
	CHECK(ofile, s);
	ofile << "# (-1)*(K-correction + 2.5*log10(1+z))" << endl;
	ofile << "# This K-correction is approx. zero if pivot wavelength of second filter = pivot wavelength of first filter * (1+z)" << endl;
	ofile << "# Structure of the file: first column redshift" << endl;
	ofile << "# The next columns prints for each template -5log10(1+z)+(mag[template][1st filter][z0]-mag[template][2nd filter][z])" << endl;
	ofile << "# mag=-2.5*log10(f_nu)+const" << endl;	
	ofile << "# The order of filters/templates is the same as in the respective .conf files" << endl;	
	for (int zi=0;zi<binZ->getLength();zi++) {
		ofile << z[zi] << " ";
		for (int t=0; t<numTemplates; t++) 			
#ifdef PARRAY		
		  ofile << g_fluxToMag(flux[t][0][0])-g_fluxToMag(flux[t][zi][1])-5*log10(1+z[zi]) << " ";
#else
		  ofile << g_fluxToMag(flux[t*zlength*numFilters])-g_fluxToMag(flux[t*zlength*numFilters+zi*numFilters+1])-5*log10(1+z[zi]) << " "; 
#endif		  
		ofile << endl;
	}
	ofile.close();
	ofile.clear();		
}
