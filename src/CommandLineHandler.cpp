#include "CommandLineHandler.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

CommandLineHandler::CommandLineHandler(int argc, char** argv)
{
	this->argc=argc;
	this->argv=argv;
	this->m_out=NULL;
	nameFilters=NULL;
	nameTemplates=NULL;
	massToLightRatios=NULL;
	minZ=NULL;
	maxZ=NULL;
	rhoSigmaFiles=NULL;
	// defaults;
	logInterpolation=0;
	logInterpolationMode=0;
	linInterpolation=0;
	linInterpolationMode=0;	
	fluxType=0;
	verboseLevel=1;
	fluxEntries=0;
	preserveErrorsOutput=0;
	preserveCatalogFormat=0;
	loadFlux=0;
	saveFlux=0;
	loadPrior=0;
	savePrior=0;
	savePriorIterative=0;	
	zmin=0.0;
	zmax=4.0;
	dz=0.005;
	meshLambdaMin=500;
	meshLambdaMax=55000;
	meshLambdaRes=2;
	binZType=0;	
	igAbsorption=0;
	kBandCorrection=0;
	mbMode=0;
	allowMissingError=0;
	improveTemplate=0;
	improveTemplateMode=0;	
	rescaleWithMesh=0;
	rescaleWithBands=0;
	rescaleWithFlux=0;
	loadRhoSigma=0;
	saveRhoSigma=0;
	mbLow=-22.5;
	mbHigh=-13.0;
	magCatalogLow=5;
	magCatalogHigh=35;
	percCut=0.95;
	chi2ReducedCut=10;
	omegaM=0.25;
	omegaL=0.75;
	hubble=0.7;
	smoothLimitLow=913;	
	rightZMode=0;
	useMesh=0;
	linearMesh=0;
	normalizeTemplates=0;
	correctionSteps=3;	
	calcCatalogCorrection=0;
	applyCatalogCorrection=0;
	iterationsCatalogCorrection=0;
	singleFit=0;
	downgradingFactor=4.;
	kcorrection=0;
	calcPrior=0;
	calcPriorMode=0;
	maxIterations=5;
	showTable=0;
	calcBayesian=0;
	calcLikelihood=0;
	smoothPrior=0;
	smoothZ=0.03;
	smoothZMode=0;
	smoothT=0.05;
	smoothFilter=0.;
	smoothTemplate=0.;
	smoothTFMode=0;
	sigma=1.;
	rho=0.1;
	punishGradient=0;
	single=0;
	callocArray1(dump,5);
	callocArray1(likelihood,5);
	callocArray1(posterior,5);
	callocArray1(printData,2);
	chi2Add=0.;
	chi2Mult=1.0;	
}

CommandLineHandler::~CommandLineHandler()
{	
	freeArray2(nameFilters,numFilters);
	freeArray2(nameTemplates,numTemplates);	
	freeArray2(rhoSigmaFiles,numTemplates);
	freeArray1(massToLightRatios);
	freeArray1(minZ);
	freeArray1(maxZ);
	freeArray1(dump);
	freeArray1(likelihood);
	freeArray1(posterior);
	freeArray1(printData);
}

void CommandLineHandler::readCommandLine()
{	
	char*** filterEntries;
	char*** templateEntries;
	char filterfilename[MAX_LENGTH];
	char templatefilename[MAX_LENGTH];	
	char filename[MAX_LENGTH+MAX_LENGTH];
	char base[MAX_LENGTH];
	ConfFile *filterfile;
	ConfFile *templatefile;
	int*  filterEntriesNr;
	int*  templateEntriesNr;
	int	  filterLines;
	int   templateLines;
	char s[MAX_LENGTH];
	StartHeader *sh;
	
	// set default values
	strcpy(filterfilename,"filter.conf");	
	strcpy(templatefilename,"template.conf");
	strcpy(nameCatalog,"catalog.cat");		
	strcpy(nameDefFile,"");
	strcpy(outputbase,"ML");
	strcpy(fluxbase,"templateFlux");
	strcpy(fluxpath,".");
	strcpy(priorbase,"prior");
	strcpy(priorLoadBase,"prior_load");
	strcpy(priorpath,".");
	strcpy(catalogCorrectionDef,"catalogCorrection.def");
	strcpy(catalogCorrectionBase,"catalogCorrection");
	strcpy(templateImprovementDef,"templateImprovement.def");
	strcpy(BFilterName,"B.res");
	strcpy(rhoSigmaLoadBase,"rhoSigma.dat");
	strcpy(rhoSigmaSaveBase,"rhoSigma");
	strcpy(this->filterpath,".");				// path of the filter files
	strcpy(this->templatepath,".");				// path of the templates
	strcpy(this->catalogpath,".");				// path of the catalog
	strcpy(this->outputpath,".");				// path of the outputfiles	
	strcpy(base,".");
	
	// define allowed options
	static const struct option long_options[]=
	{
		{ "filterconf", required_argument, 0, 'f' },
        { "filterpath", required_argument, 0, 'F' },
        { "templateconf", required_argument, 0, 't' },
        { "templatepath", required_argument, 0, 'T' },
        { "catalog", required_argument, 0, 'c'   },
        { "catalogpath", required_argument, 0, 'C'   },
        { "catalogdef", required_argument, 0, 'd' },
        { "outputbase", required_argument, 0, 'o'},
		{ "outputpath", required_argument, 0, 'O'},	
        { "fluxbase", required_argument, 0, 'x' },				// 10
        { "fluxpath", required_argument, 0, 'X' },
        { "priorbase", required_argument, 0, 'p' },
        { "prior-load-base", required_argument, 0, 0 },
        { "priorpath", required_argument, 0, 'P' },
        { "base", required_argument, 0, 'B'},
        { "zmin", required_argument, 0, 'z'},
        { "zmax", required_argument, 0, 'Z'},
        { "dz", required_argument, 0, 'D'},
        { "log-zBin", no_argument, 0, 0}, 
        { "omegaM", required_argument, 0, 0},
        { "omegaL", required_argument, 0, 0},
        { "hubble", required_argument, 0, 0},
        { "ig-absorption",required_argument, 0,'a'},   			// 23
        { "k-band-correction",no_argument, 0, 0},
        { "allow-missing-error",no_argument, 0, 0},            	
        { "mb-mode",required_argument,0 ,0},        
        { "mb-low", required_argument,0, 'm'},
        { "mb-high", required_argument,0, 'M'},
        { "b-filter-name",required_argument, 0, 0},
        { "sigma", required_argument, 0, 0},
        { "rho", required_argument, 0, 0},
        { "punish-gradient", no_argument, 0, 0},
        { "chi2-add", required_argument, 0, 0},
        { "chi2-mult", required_argument, 0, 0},				// 34
        { "use-mesh", no_argument, 0, 0},
        { "linear-mesh", no_argument, 0, 0},        
        { "mesh-lambda-min", required_argument, 0, 0},
        { "mesh-lambda-max", required_argument, 0, 0},
        { "mesh-lambda-res", required_argument, 0, 0},
        { "normalize-templates", no_argument, 0, 0},           
        { "rightz", no_argument, 0, 'r'},
        { "correction-steps", required_argument, 0, 0},        
        { "calc-catalog-correction", no_argument, 0, 0},
        { "apply-catalog-correction", no_argument, 0, 0},
        { "iterations-catalog-correction", required_argument, 0, 0},
		{ "single-fit", no_argument, 0, 0},
		{ "downgrading-factor", required_argument, 0, 0},
        { "kcorrection", no_argument, 0, 0},
        { "catalog-correction-def", required_argument, 0, 0},
        { "catalog-correction-base", required_argument, 0, 0}, 			// 50       
        { "template-improvement-def", required_argument, 0, 0},	
        { "improve-template", no_argument, 0 },					
        { "improve-template-mode", required_argument, 0, 0},	        
        { "rescale-with-mesh", no_argument, 0, 0},
        { "rescale-with-bands", no_argument, 0, 0},
        { "rescale-with-flux", required_argument, 0, 0},
        { "load-rho-sigma", no_argument, 0, 0},
        { "save-rho-sigma", no_argument, 0, 0},
        { "rho-sigma-load-base", required_argument, 0, 0},
        { "rho-sigma-save-base", required_argument, 0, 0},
        { "calcLikelihood", no_argument, 0, 'k'},	
 		{ "calcBayesian", no_argument, 0, 'b'},
 		{ "smoothprior", no_argument, 0, 0},
 		{ "smootht", required_argument, 0, 0},
 		{ "smoothz", required_argument, 0, 0},
 		{ "smoothz-mode", required_argument, 0, 0},
 		{ "smooth-filter", required_argument, 0, 0},			// 67
 		{ "smooth-template", required_argument, 0, 0},
 		{ "smooth-limit-low", required_argument, 0, 0},	
		{ "smooth-mode", required_argument, 0, 0}, 	
		{ "log-interpolation", required_argument, 0, 'i' },			
		{ "lin-interpolation", required_argument, 0, 'l' },				
		{ "flux-type", required_argument, 0 , 0 }, 
		{ "flux-entries", no_argument, 0, 0},
		{ "preserve-errors-output", no_argument, 0, 0},
		{ "preserve-catalog-format", no_argument, 0, 0},
		{ "mag-catalog-low",required_argument, 0, 0},
		{ "mag-catalog-high",required_argument, 0, 0},
		{ "chi2-percentile-cut",required_argument, 0, 0},
		{ "chi2-reduced-cut", required_argument, 0, 0},
		{ "load-flux", no_argument, 0, 0},
		{ "save-flux", no_argument, 0, 0},
		{ "load-prior", no_argument, 0, 0},				// 83
		{ "save-prior", no_argument, 0, 0},				
		{ "save-prior-iterative", no_argument, 0, 0},			
		{ "calc-prior", no_argument, 0, 0},						
		{ "calc-prior-mode", required_argument, 0,0},			
		{ "max-iterations", required_argument, 0, 0},			
		{ "show-table", no_argument, 0, 0},
		{ "dump", required_argument, 0, 0},		
		{ "likelihood", required_argument, 0, 0},
		{ "posterior", required_argument, 0, 0},
		{ "single", no_argument, 0, 0},					// 93
		{ "print-data", required_argument, 0, 0},			
		{ "verbose", required_argument, 0, 'v' },				
		{ "contrib", no_argument, 0, 0 },						
		{ "help",  no_argument, 0, 'h' },						
		{ "usage", no_argument, 0, 'u' },
        {0}
  	};
  	
  	static const char* option_help[]=
  	{
  		"File defining used filters - default filter.conf",
  		"Directory containing the defined filters - default .",
  		"File defining used templates - default template.conf",
  		"Directory containing defined templates - default .",
  		"File name of the catalog file - no default",
  		"Directory containing catalog file - default .",
  		"File defining the catalog entries - default [catalogbase].def",
  		"Output file name - default ML (maximum likelihood) or Bayes_ML (Bayesian, if -b)",
  		"Directory to which output is written - default .",
  		"File storing the fluxes through templates - default templateFlux",
  		"Directory containing the template flux file - default .",
  		"Basename of file storing the prior P(T,z) - default prior",
  		"Basename of file containing the prior to load - default prior_load",
  		"Directory containing the prior file - default .",
  		"Directory, overwrites -F -T -C -O defaults",
  		"Lower limit of investigated red shift range - default 0.0",
  		"Upper limit of investigated red shift range - default 4.0",
  		"Difference between lowest red shift bins - default 0.005",
  		"Use binning in log(1+z) instead of binning linear in z (default)",  
  		"Omega_Matter - default 0.25",
  		"Omega_Lambda - default 0.75",
  		"Hubble constant in 100 Km/s/Mpc - default 0.7",  		
  		"Intergalactic absorption according to Madau (1,2), Meiksin (4,5,6,7), mixed (3) or no absorption (0) - default 0\n"
		"             0  no absorption\n"
  		"             1  according to Madau, Lyman-transitions(LT) to 4th order\n"
  		"             2  according to Madau, LT to 40th order following Meiksin 2005 for >4th order\n"
		"             3  Lyman transitions Meiksin 2005, Photo-electric absorption according to Madau\n"
		"             4  following Meiksin 2005\n"
		"             5  following Meiksin 2007\n"
		"             6  as 5 but without Lyman-Limit systems\n"
		"             7  as 6 but without optically thin systems",		
  		"An empirical correction to the apperant magnitudes of the K-band due to Zamorani - default no",
  		"Use photometric data if no error given (using minError values) (if set to no -> discard entry) - default no",
  		"Cutting of (1) or searching for a better fit for (2) entries with M_B magnitudes out of range - default 0",  		
  		"Lower limit of B band magnitude used for option --mb-mode  - default -22.5",
  		"Higher limit of B band magnitude used for option --mb-mode - default -13.0",
  		"The filename of the B-filter, used for B-band magnitude selection and in the output - default B.res",
  		"Pliantness, larger values allow template shapes to be changed stronger in the template correction - default 1.0",
  		"Regularization parameter, smaller values enforce a smoother gradient of the improved templates - default 0.1",
  		"The gradient restriction above is actually used - default no",
  		"Added to chi^2 (look also at --chi2-mult) if best fitting template is out of allowed redshift range - default 0",
  		"Multiplied with chi^2 (after adding --chi2-add) if best fitting template is out of allowed redshift range - default 1",
  		"Put templates and filters to a mesh of fixed resolution, increases speed - default no",
  		"Use linear instead of logarithmic spacing in wavelength for the template and filter mesh - default no",  		
  		"The minimal wavelength (in Angstroem) of the template/filter mesh - default 500",
  		"The maximal wavelength (in Angstroem) of the template/filter mesh - default 55000",
  		"The spacing of the template/filter mesh close to its smallest wavelengths - default 2",
  		"Normalize all templates to B-filter, should be used if linearly interpolating templates - default no",  		 
  		"Fix redshift to values from catalog, fit only type - default no",
  		"The number of iterative template correction steps - default 3",  		
  		"Run code in catalog correction mode - default no",  		
  		"Apply derived corrections to catalog - default no",
  		"Number of iterations of the catalog correction (calc+apply) - default 0",
		"Obtain filter band magnitude residual from fit with all filters bands (including itself) - default no", 
		"Factor by which the rel. error of respective filter is temporarily increased within catalog correction - default 4",
  		"Print the K-correction for the used templates - default no",
  		"Filename with binning information for each filter needed for catalog correction - default catalogCorrection.def",
  		"Base of filename for catalog correction data - default catalogCorrection",
  		"Filename with redshift binning information needed for template improvement - default templateImprovement.def",
  		"Run code in template improvement mode - default no",
  		"Iteration Mode: start always from original templates (0), use result from last iteration (1) - default 0 ",  		
  		"Temp.Improv: sigma and rho are rescaled with sqrt(grid spacing) to remove dependence on mesh resolution - default no",
  		"Temp.Improv: the squared flux errors are rescaled by the number of filter bands - default no",
  		"Temp.Improv: sigma, rho are scaled with the template flux avoiding over-corrections in low-flux regions - default 0\n"
  		"             0  rho and sigma do not change with flux of the unmodified template\n"
  		"             1  rho and sigma scale as sqrt(template flux)\n"
  		"             2  rho and sigma scale proportional to the template flux",
  		"Temp.Improv: Load sigma and rho (pliantness) from file(s) and use them in the template improvement step - default no",
  		"Temp.Improv: Save sigma and rho (pliantness) to a file - default no",
  		"Temp.Improv: Name of the file which contains sigma (first column) and rho (second column) - default rhoSigma.dat",
  		"Temp.Improv: Name of the file into which sigma and rho are written - default rhoSigma",
  		"Use a maximum likelihood approach (template fitting) to determine red shifts - default no",
  		"Use bayesian method (calculate prior and use it) to determine red shifts more accurately - default no",
  		"Smooth prior after iterative determination, smoothing strength depends on --smootht and --smoothz - default no",
  		"FWHM of rectangular smoothing kernel per template(!) in template space - default 0.05",
  		"FWHM of rectangular smoothing kernel in red shift space - default 0.03",
  		"Smoothing mode for prior in redshift space: 0 - fixed, 1 - adaptive, 2 - adaptive & template dependent - default 0",
  		"FWHM or FWHM/lambda (see --smooth-mode) of rectangular smoothing kernel for filters - default 0.",
  		"FWHM or FWHM/lambda (see --smooth-mode) of rectangular smoothing kernel for templates - default 0.",
  		"Below this wavelength the template is not smoothed (e.g. to retain sharp Ly-alpha break) - default 913", 
		"Options --smooth-filter and --smooth-template specify FWHM (0) or FWHM/lambda (1) - default 0",
  		"Number of interpolations in magnitudes (in log-flux space) - default 0",  		
  		"Number of interpolations in flux space - default 0",  	
  		"Flux in photons (0) or energy (1) per frequency - default 0",
  		"Catalog entries are fluxes not magnitudes - default no",
  		"Print original errors (not modified by e.g. lower error bounds) when catalog is saved - default no",  		
  		"Catalog is saved in the original format, just flux values / errors replaced - default no",
  		"Catalog entries with smaller magnitude (but larger than -99) produce a warning - default 5",
  		"Catalog entries with larger magnitude (but smaller than 99) produce a warning - default 35",
  		"Use only entries (in catalogCorr., templateImpr.) with a chi2 value within the percentile level - default 0.95",
  		"Use only entries (in catalogCorr., templateImpr., Bayes.) with a lower reduced-chi2 value - default 10",   
  		"Calculated fluxes read from file (-x) - default no",
  		"Calculated fluxes saved into file (-x) - default no",
  		"Prior read from file (-p) - default no",
  		"Calculated prior saved into file (-p) - default no",
  		"Prior saved after each iterative step (option available iff --save-prior set, saved into path -O) - default no",  		
  		"Calculate prior iteratively (start with flat or loaded prior) - default no",
  		"Use a prior in redshift only (taking a prior in z and t (0) or taking best template) (1) - default 0",
  		"The (maximum) number of iteration steps during the prior calculation - default 5",
  		"Print table of template IDs and corresponding names - default no",
  		"Prints out files depending on the argument (this option may be given several times): - default no\n"
  		"       besttemplate ... for each catalog entry print best fitted template\n"
  		"       templates    ... for each catalog entry print all fitted templates\n"
  		"       templates2   ... for each catalog entry print all fitted templates (just red shifted, no absorption)\n"
  		"       fluxes       ... print fluxes in each band of each catalog entry\n"
  		"       residual	 ... prints residual (in mag) for best fitted templates",  		  		  	
  		"Prints the likelihood for each entry in the catalog (option may be given several times): - default no \n"
  		"       full         ... for each catalog entry print complete likelihood function\n"
  		"       t            ... for each catalog entry print summed over red shift likelihood function\n"
  		"       z            ... for each catalog entry print summed over type likelihood function\n"
  		"       pz			 ... for each catalog entry print summed over type likelihood function in percentiles\n"
  		"       pzt			 ... (needs pz) print best fit template type for associated redshift from the percentile file",
  		"Prints the posterior for each entry in the catalog (option may be given several times): - default no \n"
  		"       full         ... prints the complete posterior for each entry in one file\n"
  		"       t            ... prints the marginalized posterior (over red shift) for each entry in one file\n"
  		"       z            ... prints the marginalized posterior (over types) for each entry in one file\n"
  		"       pz			 ... for each catalog entry print marginalized posterior (over types) in percentiles\n"
  		"       pzt			 ... (needs pz) print best fit template type for associated redshift from the percentile file",  		
  		"Prints likelihood / posterior of each entry into its own file - default no",
  		"Print out some data files - default no\n"
  		"       filters      ... prints all filters\n"
  		"       templates    ....prints all templates",
  		"Level of verbosity: quiet (0), normal (1), more (2), still more(3), most (4) - default 1",  		  		
  		"List of contributors (not necessarily complete)", 
  		"Gives this short help",
  		"Gives a short description of how to use the programm",		
  	};
  	  	
	// read in option arguments from command line
	while (optind < argc)
	{
		int index = -1;		
		struct option * opt = 0;
		int result = getopt_long(argc,argv,"hf:F:t:T:c:C:d:i:l:o:O:B:v:x:X:p:P:z:Z:D:a:m:M:sbukr",long_options,&index);
		
		if (result == -1) break; // end of list
		
		switch (result)
		{
			case 'B': // same as --base
				strcpy(base,optarg);
				strcpy(this->outputpath,base);
				strcpy(this->filterpath,base);
				strcpy(this->templatepath,base);
				strcpy(this->catalogpath,base);				
				strcpy(this->fluxpath,base);
				strcpy(this->priorpath,base);
				break;
			case 'f': // same as --filter 
				strcpy(filterfilename,optarg);
				break;
			case 'F': // same as --filterpath
				strcpy(this->filterpath,optarg);
				break;
			case 't': // same as --template 
				strcpy(templatefilename,optarg);
				break;
			case 'T': // same as --templatepath
				strcpy(this->templatepath,optarg);
				break;			
			case 'c': // same as --catalog
				strcpy(nameCatalog,optarg);
				break;
			case 'C': // same as --catalogpath
				strcpy(this->catalogpath,optarg);
				break;			
			case 'd': // same as --catalogdef
				strcpy(this->nameDefFile,optarg);
				break;			
			case 'o': // same as --outputbase
				strcpy(this->outputbase,optarg);				
				break;
			case 'O': // same as --outputpath
				strcpy(this->outputpath,optarg);
				break;								
			case 'x': // same as --fluxbase
				strcpy(this->fluxbase,optarg);
				break;
			case 'X': // same as --fluxpath
				strcpy(this->fluxpath,optarg);
				break;
			case 'p': // same as --priorbase
				strcpy(this->priorbase,optarg);
				break;
			case 'P': // same as --priorpath
				strcpy(this->priorpath,optarg);
				break;
			case 'z': // same as --zmin
				zmin=atof(optarg);				
				break;
			case 'Z': // same as --zmax
				zmax=atof(optarg);
				break;
			case 'D': // same as --dz
				dz=atof(optarg);
				break;
			case 'k': // same as --calcLikelihood
				calcLikelihood=1;				
				break;			
			case 'b': // same as --calcBayesian
				calcBayesian=1;
				break;				
			case 'i': // same as --log-interpolation
				logInterpolation=atoi(optarg);				
				break;
			case 'I': // same as --log-interpolation-mode
				logInterpolationMode=atoi(optarg);
				break;
			case 'l': // same as --lin-interpolation
				linInterpolation=atoi(optarg);
				break;
			case 'L': // same as --lin-interpolation-mode
				linInterpolationMode=atoi(optarg);
				break;
			case 'a': // same as --ig-absorption
				igAbsorption=igAbsorption=atoi(optarg);				
				break;
			case 'm': // same as --mb-low
				mbLow=atof(optarg);
				break;
			case 'M': // same as --mb-high
				mbHigh=atof(optarg);
				break;				
			case 'r': // same as --rightz
				rightZMode=1;
				break;
			case 's': // same as --show-table
				showTable=1;
				break;
			case 'v': // same as --verbose
				verboseLevel=atoi(optarg);
				break;			
			case 0 : // parameters without abbreviation
				opt = (struct option *)&(long_options[index]);
				if (strcmp(opt->name,"flux-type")==0) {
					if (strcmp(optarg,"photon-per-frequency")==0 ||
						strcmp(optarg,"0")==0) {
						fluxType=0;
					} else if (strcmp(optarg,"energy-per-frequency")==0 ||
							   strcmp(optarg,"1")==0) {
						fluxType=1;
					}
				} 
				else if (strcmp(opt->name,"calc-catalog-correction")==0)
					calcCatalogCorrection=1;
				else if (strcmp(opt->name,"apply-catalog-correction")==0)
					applyCatalogCorrection=1;
				else if (strcmp(opt->name,"iterations-catalog-correction")==0)
					iterationsCatalogCorrection=atoi(optarg);
				else if (strcmp(opt->name,"single-fit")==0)
				  	singleFit=1;
				else if (strcmp(opt->name,"downgrading-factor")==0)
				  	downgradingFactor=atof(optarg);
				else if (strcmp(opt->name,"kcorrection")==0)
					kcorrection=1;
				else if (strcmp(opt->name,"catalog-correction-def")==0)
					strcpy(catalogCorrectionDef,optarg);
				else if (strcmp(opt->name,"catalog-correction-base")==0)
					strcpy(catalogCorrectionBase,optarg);
				else if (strcmp(opt->name,"template-improvement-def")==0)
					strcpy(templateImprovementDef,optarg);
				else if (strcmp(opt->name,"improve-template")==0)
					improveTemplate=1; 
				else if (strcmp(opt->name,"improve-template-mode")==0)
					improveTemplateMode=atoi(optarg);									
				else if (strcmp(opt->name,"omegaM")==0)
					omegaM=atof(optarg);
				else if (strcmp(opt->name,"omegaL")==0)
					omegaL=atof(optarg);
				else if (strcmp(opt->name,"hubble")==0)
					hubble=atof(optarg);					
				else if (strcmp(opt->name,"smooth-limit-low")==0)
					smoothLimitLow=atof(optarg);					
				else if (strcmp(opt->name,"rescale-with-mesh")==0)
					rescaleWithMesh=1;
				else if (strcmp(opt->name,"rescale-with-bands")==0)
					rescaleWithBands=1;
				else if (strcmp(opt->name,"rescale-with-flux")==0) {
					rescaleWithFlux=atoi(optarg);
					if (rescaleWithFlux>2) 
					  rescaleWithFlux=2;
					else if (rescaleWithFlux<0)
					  rescaleWithFlux=0;  	
				}
				else if (strcmp(opt->name,"load-rho-sigma")==0)
					loadRhoSigma=1;
				else if (strcmp(opt->name,"save-rho-sigma")==0)
					saveRhoSigma=1;
				else if (strcmp(opt->name,"rho-sigma-load-base")==0)
					strcpy(rhoSigmaLoadBase,optarg);
				else if (strcmp(opt->name,"rho-sigma-save-base")==0)
					strcpy(rhoSigmaSaveBase,optarg);							
				else if (strcmp(opt->name,"use-mesh")==0)
					useMesh=1;	
				else if (strcmp(opt->name,"linear-mesh")==0)
					linearMesh=1;
				else if (strcmp(opt->name,"mesh-lambda-min")==0)
					meshLambdaMin=atof(optarg);	
				else if (strcmp(opt->name,"mesh-lambda-max")==0)
					meshLambdaMax=atof(optarg);
				else if (strcmp(opt->name,"mesh-lambda-res")==0)
					meshLambdaRes=atof(optarg);
				else if (strcmp(opt->name,"normalize-templates")==0)
					normalizeTemplates=1;							
				else if (strcmp(opt->name,"chi2-add")==0) {					
					chi2Add=atof(optarg);	
					if (chi2Add<0) {
						WARNING("The option --chi2-add only supports non-negative values. Given value is set to zero!");
						chi2Add=0.;
					}
				} else if (strcmp(opt->name,"chi2-mult")==0) {					
					chi2Mult=atof(optarg);	
					if (chi2Mult<1) {
						WARNING("The option --chi2-mult only supports values 1 or greater. Given value is set to 1!");
						chi2Mult=1.;
					}
				} 								
				else if (strcmp(opt->name,"sigma")==0)
					sigma=atof(optarg);
				else if (strcmp(opt->name,"rho")==0)
					rho=atof(optarg);
				else if (strcmp(opt->name,"punish-gradient")==0)
					punishGradient=1;
				else if (strcmp(opt->name,"correction-steps")==0)
					correctionSteps=atoi(optarg);			
				else if (strcmp(opt->name,"log-zBin")==0)
					binZType=1;
				else if (strcmp(opt->name,"k-band-correction")==0)
					kBandCorrection=1;
				else if (strcmp(opt->name,"mb-mode")==0)
					mbMode=atoi(optarg);
				else if (strcmp(opt->name,"b-filter-name")==0)
					strcpy(BFilterName,optarg);
				else if (strcmp(opt->name,"allow-missing-error")==0)
					allowMissingError=1;
				else if (strcmp(opt->name,"flux-entries")==0)
					fluxEntries=1;	
				else if (strcmp(opt->name,"preserve-errors-output")==0)
					preserveErrorsOutput=1;
				else if (strcmp(opt->name,"preserve-catalog-format")==0)
					preserveCatalogFormat=1;
				else if (strcmp(opt->name,"mag-catalog-low")==0)
					magCatalogLow=atof(optarg);
				else if (strcmp(opt->name,"mag-catalog-high")==0)				
					magCatalogHigh=atof(optarg);				
				else if (strcmp(opt->name,"chi2-percentile-cut")==0) {				
					percCut=atof(optarg);			
					if (percCut<0)
					  percCut=0.;
					else if (percCut>1)
					  percCut=1.;
				} else if (strcmp(opt->name,"chi2-reduced-cut")==0) {				
					chi2ReducedCut=atof(optarg);			
					if (chi2ReducedCut<0)
					  chi2ReducedCut=0.;					
				} else if (strcmp(opt->name,"load-flux")==0)
					loadFlux=1;
				else if (strcmp(opt->name,"save-flux")==0)
					saveFlux=1;						
				else if (strcmp(opt->name,"load-prior")==0)
					loadPrior=1;
				else if (strcmp(opt->name,"prior-load-base")==0)
					strcpy(priorLoadBase,optarg);
				else if (strcmp(opt->name,"save-prior")==0)
					savePrior=1;							
				else if (strcmp(opt->name,"save-prior-iterative")==0)
					savePriorIterative=1;					
				else if (strcmp(opt->name,"calc-prior")==0)
					calcPrior=1;
				else if (strcmp(opt->name,"calc-prior-mode")==0)
					calcPriorMode=atoi(optarg);
				else if (strcmp(opt->name,"max-iterations")==0)
					maxIterations=atoi(optarg);										
				else if (strcmp(opt->name,"smoothprior")==0)
					smoothPrior=1;
				else if (strcmp(opt->name,"smootht")==0)
					smoothT=atof(optarg);				
				else if (strcmp(opt->name,"smoothz")==0)
					smoothZ=atof(optarg);
				else if (strcmp(opt->name,"smoothz-mode")==0)
					smoothZMode=atoi(optarg);
				else if (strcmp(opt->name,"smooth-filter")==0)
					smoothFilter=atof(optarg);
				else if (strcmp(opt->name,"smooth-template")==0)
					smoothTemplate=atof(optarg);
				else if (strcmp(opt->name,"smooth-mode")==0)
					smoothTFMode=atoi(optarg);				
				else if (strcmp(opt->name,"dump")==0) {								
					if (strcmp(optarg,"besttemplate")==0)
						dump[0]=1;
					else if (strcmp(optarg,"templates")==0)
						dump[1]=1;
					else if (strcmp(optarg,"templates2")==0)
						dump[2]=1;	
					else if (strcmp(optarg,"fluxes")==0)
						dump[3]=1;
					else if (strcmp(optarg,"residual")==0)
						dump[4]=1;
					else 
						WARNING("unknown argument of --dump");
				} else if (strcmp(opt->name,"likelihood")==0) {		
					if (strcmp(optarg,"full")==0)
						likelihood[0]=1;
					else if  (strcmp(optarg,"t")==0)
						likelihood[1]=1;
					else if (strcmp(optarg,"z")==0)
						likelihood[2]=1;
					else if (strcmp(optarg,"pz")==0)
						likelihood[3]=1;
					else if (strcmp(optarg,"pzt")==0)
						likelihood[4]=1;
					else
						WARNING("unknown argument of --likelihood");		
				} else if (strcmp(opt->name,"posterior")==0) {		
					if (strcmp(optarg,"full")==0)
						posterior[0]=1;
					else if  (strcmp(optarg,"t")==0)
						posterior[1]=1;
					else if (strcmp(optarg,"z")==0)
						posterior[2]=1;
					else if (strcmp(optarg,"pz")==0)
						posterior[3]=1;
					else if (strcmp(optarg,"pzt")==0)
						posterior[4]=1;
					else
						WARNING("unknown argument of --posterior");		
				} else if (strcmp(opt->name,"single")==0) {
					single=1;
				} else if (strcmp(opt->name,"print-data")==0) {
					if (strcmp(optarg,"filters")==0)
						printData[0]=1;
					else if (strcmp(optarg,"templates")==0)
						printData[1]=1;					
					else 
						WARNING("unknown argument of --print-data");	
				} else if (strcmp(opt->name,"contrib")==0) {
					cout << " " << PHOTOZ << " has been developed originally by Robert Feldmann 2005/06" << endl;
					cout << " Please refer to Feldmann et al.(2006), MNRAS, 372, 565" << endl; 
					cout << " Additional modifications, developing & testing since 2006: " << endl;
					cout << " M. Carollo, R. Feldmann, P. Oesch, C. Porciani, C. Scarlata" << endl;
					exit(0);
				}
				break;
			case 'u': // same as --usage
				cout << "Usage of the program:" << endl;
				cout << "You need the following files to start with" << endl;
				cout << "filter.conf, template.conf, catalog.def and a catalog file." << endl;
				cout << "Please look into the documentation for further information." << endl;
				cout << "Details on the algorithms can be found in arXiv:astro-ph/0609044" << endl;
				exit(0);
				break;
			case 'h': // same as --help								
			default:  // unknown parameter
			    StartHeader *sh = new StartHeader();
			    sh->printHeader();
			    delete sh;						    
			    cout << "ZEBRA is distributed under the terms of the GNU Public License (GPL)" << endl;
			    cout << "It is provided 'as is' without express or implied warranty" << endl;			    			    			        	
				cout << "usage: " << argv[0] << " [ option [argument] ]^*" << endl;
				cout << "short_option long_option [has_argument?] description - default" << endl;
				for (int i=0;i<numOptions;i++) {
					if (long_options[i].val!=0) 
						cout << "-" << (char)long_options[i].val;
					else 
						cout << "  ";
					cout << " --" << long_options[i].name;
					if (long_options[i].has_arg==required_argument)
						cout << " argument";
					cout << "  " << option_help[i] << endl;
				}
				exit(0);	
		}
	}
	
	// set output handler
	if (m_out)
		m_out->set(verboseLevel);
	
	// print Logo	
	if (verboseLevel>0) {
		sh = new StartHeader();
		sh->printHeader();
		delete sh;
	}
	
	// checks and implications
	if (rightZMode && calcBayesian) {	// contradicting
		cout << "Cannot use options: --rightz/-r and --calcBayesian/-b simultaneously" << endl;
		exit(0);
	}	
	
	if (linearMesh==0) { 
	  if (meshLambdaMin<=0.1) {
		WARNING("The provided minimum mesh wavelength is too short - set it to 0.1 Angstroem");
		meshLambdaMin=0.1;
	  }
        } else {
          if (meshLambdaMin<0) {
      	    WARNING("The provided minimum mesh wavelength is negative - set it to 0");
      	    meshLambdaMin=0;
          }
	}
		
	if (strcmp(nameDefFile,"")==0) 
		sprintf(nameDefFile,"%s.def",nameCatalog);
	
	// open conf file
	sprintf(filename,"%s/%s",catalogpath,filterfilename);
	filterfile = new ConfFile(filename);
	filterfile->setOutputHandler(m_out);
	filterfile->read();
	filterfile->printTotalStatus();
	filterfile->getEntries(filterEntries,filterEntriesNr,filterLines);			
	
	// use entries to get: names, number of filters
	numFilters=0;
	for (int i=0;i<filterLines;i++) {
		if (filterEntriesNr[i]<2 || atoi(filterEntries[i][1])==1) {
			numFilters++;
		}
	}
	allocArray2(nameFilters,numFilters,MAX_LENGTH);
	numFilters=0;
	for (int i=0;i<filterLines;i++) {
		if (filterEntriesNr[i]<2 || atoi(filterEntries[i][1])==1) {
			strcpy(nameFilters[numFilters++],filterEntries[i][0]);
		}
	}	
	// close conf file
	delete filterfile;
	
	// open conf files
	sprintf(filename,"%s/%s",catalogpath,templatefilename);	
	templatefile = new ConfFile(filename);
	templatefile->setOutputHandler(m_out);
	templatefile->read();
	templatefile->printTotalStatus();
	templatefile->getEntries(templateEntries,templateEntriesNr,templateLines);	
	
	if (templateLines==0) {
		sprintf(s,"No templates defined in %s",filename);
		EXIT(s);
	}
		
	freeArray2(nameTemplates,numTemplates);
	free(massToLightRatios);
	free(minZ);
	free(maxZ);
	freeArray2(rhoSigmaFiles,numTemplates);		
	
	// use entries to get: names, number of templates
	numTemplates=0;
	for (int i=0;i<templateLines;i++) {
		if (templateEntriesNr[i]>=1 && (templateEntriesNr[i]<2 || atoi(templateEntries[i][1])==1) ) {			
			numTemplates++;
		}
	}
	
	if (numTemplates==0) {			
		sprintf(s,"No templates defined in %s",filename);
		EXIT(s);
	}
		
	allocArray2(nameTemplates,numTemplates,MAX_LENGTH);
	allocArray1(massToLightRatios,numTemplates);
	allocArray1(minZ,numTemplates);
	allocArray1(maxZ,numTemplates);
	allocArray2(rhoSigmaFiles,numTemplates,MAX_LENGTH);
	
	// setting defaults
	for (int i=0;i<numTemplates;i++) {		
			massToLightRatios[i]=1;
			minZ[i]=TEMPLATE_MIN_Z;
			maxZ[i]=TEMPLATE_MAX_Z;	
			strcpy(rhoSigmaFiles[i],"");											
	}		
	
	numTemplates=0;
	for (int i=0;i<templateLines;i++) {
		if (templateEntriesNr[i]>=1 &&  (templateEntriesNr[i]==1 || atoi(templateEntries[i][1])==1 || strcmp(templateEntries[i][1],"x")==0) ) {			
			strcpy(nameTemplates[numTemplates],templateEntries[i][0]);
						
			if (templateEntriesNr[i]>2 && strcmp(templateEntries[i][2],"x")!=0)
			   minZ[numTemplates]=atof(templateEntries[i][2]);
			if (templateEntriesNr[i]>3 && strcmp(templateEntries[i][3],"x")!=0)
			   maxZ[numTemplates]=atof(templateEntries[i][3]);
			if (templateEntriesNr[i]>4 && strcmp(templateEntries[i][4],"x")!=0)
			   massToLightRatios[numTemplates]=atof(templateEntries[i][4]);
			if (templateEntriesNr[i]>5 && strcmp(templateEntries[i][5],"x")!=0)
			   strcpy(rhoSigmaFiles[numTemplates],templateEntries[i][5]);
			   			
			numTemplates++;
		}
	}	
	
	// confirm rightZ Mode
	if (rightZMode) {
		if (m_out) {
			m_out->println("Use spectroscopic redshifts if available (--rightz enabled)",1);
		} 
	} else {
		if (m_out) {
			m_out->println("Use (internally calculated) photometric redshifts instead of spectroscopic redshifts (--rightz disabled)",1);
		}
	}
	
	// confirm igAbsorption
	if (igAbsorption) {
		if (m_out) {
			if (igAbsorption==1)		   
				m_out->println("IG-Absorption according to Madau applied",1);
			else
				m_out->println("IG-Absorption according to Meiksin applied",1);
		}
	}
		
	// close conf file
	delete templatefile;		
}
