#include "NumericalData.h"
#include "ConfFile.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/
	
/* Classes
 */

/* class Spline
 */  
void SplineApprox::calcSecondDeriv() {
	int i,k;
	double p,qn,sig,un,*u;
	
	if (m_length<2) { /* need at least 2 points */
	  return;
	}
	
	allocArray1(this->secondDeriv,m_length);
	allocArray1(u,m_length-1);	
  	
	if (yp1 > 0.99e30)		// setting lower boundary condition
		secondDeriv[0]=u[0]=0.0;
	else {
		secondDeriv[0] = -0.5;
		u[0]=(3.0/(m_x[1]-m_x[0]))*
				    ((m_y[1]-m_y[0])/
			        (m_x[1]-m_x[0])-yp1);
	}
	
	// tridiagonal algorithm (decomposition loop)
	for (i=2;i<m_length;i++) {	
		sig=(m_x[i-1]-m_x[i-2])/(m_x[i]-m_x[i-2]);
		p=sig*secondDeriv[i-2]+2.0;
		secondDeriv[i-1]=(sig-1.0)/p;
		u[i-1]=(m_y[i]-m_y[i-1])/
		(m_x[i]-m_x[i-1]) - (m_y[i-1]-m_y[i-2])/
		(m_x[i-1]-m_x[i-2]);
		u[i-1]=(6.0*u[i-1]/(m_x[i]-m_x[i-2])-sig*u[i-2])/p;
	}
	if (ypn > 0.99e30)		// setting higher boundary condition
		qn=un=0.0;
	else {
		qn=0.5;
		un=(3.0/(m_x[m_length-1]-m_x[m_length-2]))*
		   (ypn-(m_y[m_length-1]-m_y[m_length-2])/
		   (m_x[m_length-1]-m_x[m_length-2]));
	}
	secondDeriv[m_length-1]=(un-qn*u[m_length-2])/
						   (qn*secondDeriv[m_length-2]+1.0);
	// tridiagonal algorithm (backsubstitution)
	for (k=m_length-1;k>=1;k--)	
		secondDeriv[k-1]=secondDeriv[k-1]*secondDeriv[k]+u[k-1];	
	freeArray1(u);	
}

void SplineApprox::getValues(int n,const double *x, double *y)
{
	int index,i,inc;
	int indexlo,indexhi;
	double h,b,a,xi;	

	if (m_length==0)
	  return; 
	
	indexlo=0;
	indexhi=1;

	for (i=0;i<n;i++) {
		xi=x[i];
		
		// now find index s.t. m_x[index]<= xi < m_x[index+1]
		// since I know function values and derivatives only there
		
		// first phase hunt upwards (know that xi is only increasing i)				
		inc=1;	// hunt increment			
		while (indexhi<m_length-1 && xi>m_x[indexhi]) {
			indexlo=indexhi;
			inc*=2; // i.e. times 2
			indexhi=indexlo+inc;								
		}	
		if (indexhi>=m_length)
			indexhi=m_length-1;				
		
		// now m_x[indexlo]<= xi <= m_x[indexhi]				
		
		if (m_x[indexhi]==xi) {
			y[i]=m_y[indexhi];
			continue;				
			
		} else {
			
			if (indexhi==indexlo) {
			  y[i]=m_y[indexhi];
			  continue;
			}
			// now m_x[indexlo] <= xi < m_x[indexhi]
			
			// bisection phase
			while (indexhi-indexlo > 1) { 						  
				index=(indexhi+indexlo) >> 1; // i.e. divided by 2
				if (m_x[index] > xi) indexhi=index;
				else indexlo=index;
			}					
							
			h=m_x[indexhi]-m_x[indexlo];
			if (h == 0.0) {
			   WARNING("x-values in interpolation object are not distinct");
			   y[i]=m_y[indexhi];
			   continue;
			}	
						
			a=(m_x[indexhi]-xi)/h;
			b=(xi-m_x[indexlo])/h;
			y[i]=a*m_y[indexlo]+b*m_y[indexhi]+((a*a*a-a)*secondDeriv[indexlo]
	 	  		 +(b*b*b-b)*secondDeriv[indexhi])*(h*h)/6.0;									
		}
	}	

}

void SplineApprox::getValues(int n,const double *x, double *y, int *outofbounds)
{
	if (m_length<1)
	  return;
	
	for (int i=0;i<n;i++) {
		if (x[i]<m_x[0])
			outofbounds[i]=-1;
		else if (x[i]>m_x[m_length-1])
			outofbounds[i]=1;
		else
			outofbounds[i]=0;
	}	
	getValues(n,x,y);			
}	

double SplineApprox::getValue(double x)
{
	int klo,khi,k;
	double h,b,a;

        if (m_length<1)
	  return 0;
	  
	klo=0;
	khi=m_length-1;
	
	while (khi-klo > 1) {
		k=(khi+klo)/2;
		if (m_x[k] > x) khi=k;
		else klo=k;
	}
	
	//cout << "Bkhi=" << khi << " klo=" << klo << endl;
	
	if (khi==klo) 
	  return m_y[khi];
			
	h=m_x[khi]-m_x[klo];
	if (h == 0.0) {
		EXIT("All values have to be pairwise distinct");
		exit(1);
	}
	a=(m_x[khi]-x)/h;
	b=(x-m_x[klo])/h;
	return (a*m_y[klo]+b*m_y[khi]+((a*a*a-a)*secondDeriv[klo]
	   +(b*b*b-b)*secondDeriv[khi])*(h*h)/6.0);
}

double SplineApprox::getValue(double x, int & outofbounds)
{
	if (m_length<1)
	  return 0;
	  
	if (x<m_x[0])
		outofbounds=-1;
	else if (x>m_x[m_length-1])
		outofbounds=1;
	else
		outofbounds=0;		
	return getValue(x);				
}		
 
// determines first (smallest index) x value s.t. y=f(x)
// "found" tells you whether at least one such value exists

/* !!! uses still linear interpolation -> only approximation */
double SplineApprox::getXValueRight(double y, int startIndex, int & foundIndex)
{
    int index,found;
    
    if (m_length<2) {
      foundIndex=-1;
      if (m_length==1)
        return m_x[0];
      return 0;
    }
    
    // determine first index i s.t. y[i]<= y < y[i+1]            
    index=startIndex;
    found=0;
    
    while (index<m_length-1) {    
		if ((m_y[index]<=y && m_y[index+1]>=y) || (m_y[index]>=y && m_y[index+1]<=y)) {
			found=1;
			break;					
		}
		index++;
	};
	
	if (found==0) {
		foundIndex=-1;
		return m_x[m_length-1];
	}			 		
	foundIndex=index;
		
	// (yb-ya)/(xb-xa)=(y-ya)/(x-xa) -> determine x
	return (y-m_y[index])/(m_y[index+1]-m_y[index])*(m_x[index+1]-m_x[index])+m_x[index];
}

// determines last (highest index) x value s.t. y=f(x)
// "found" tells you whether at least one such value exists

/* !!! uses still linear interpolation -> only approximation */
double SplineApprox::getXValueLeft(double y, int startIndex, int & foundIndex)
{
	int index,found;
	
	if (m_length<2) {
	  foundIndex=-1;
	  if (m_length==1)
	    return m_x[0];
	  return 0.;
	}
			       
 	index=startIndex-1;
	
    // determine last index i s.t. y[i]<= y < y[i+1]      
    found=0;
	while (index>=0) {		
		if ((m_y[index]<=y && m_y[index+1]>=y) || (m_y[index]>=y && m_y[index+1]<=y)) {
			found=1;
			break;		
		}
		index--;
	};
		
	if (found==0) {
		foundIndex=-1;
		return 0.;
	}			 		
	foundIndex=index;
		
	// (yb-ya)/(xb-xa)=(y-ya)/(x-xa) -> determine x
	return (y-m_y[index])/(m_y[index+1]-m_y[index])*(m_x[index+1]-m_x[index])+m_x[index];
}

// determines x value s.t. y=f(x)
// if the function is non-monotonic it chooses one possibility
// you have to make sure that at least one x value exists

/* !!! uses still linear interpolation -> only approximation */
double SplineApprox::getXValue(double y)
{
	int index,flag;
	
	if (m_length==0)
	  return 0;
	if (m_length==1)
	  return m_x[0];
	
    // determine index i s.t. y[i]<= y < y[i+1]
	Bisection *bi_y = new Bisection(this->m_length,this->m_y);
	index=bi_y->getIndex(y,flag);
	delete bi_y;
	
	if (flag==1)
		return m_x[index];
		
	// (yb-ya)/(xb-xa)=(y-ya)/(x-xa) -> determine x
	return (y-m_y[index])/(m_y[index+1]-m_y[index])*(m_x[index+1]-m_x[index])+m_x[index];
} 
    
// use the analytical antiderivative of the spline
// divide total x-range into intervals and integrate in each interval
double SplineApprox::integrate()
{
	double integral=0.0;
	double diffx;
	for (int i=0;i<m_length-1;i++) {
		diffx=m_x[i+1]-m_x[i];
		integral+=(m_y[i]+m_y[i+1])*diffx/2
				  -(secondDeriv[i]+secondDeriv[i+1])*diffx*diffx*diffx/24;
	}
	return integral;	
}

double SplineApprox::integrate(int index_a, int index_b)
{
	double integral=0.0;
	double diffx;
	for (int i=index_a;i<index_b;i++) {
		diffx=m_x[i+1]-m_x[i];
		integral+=(m_y[i]+m_y[i+1])*diffx/2
				  -(secondDeriv[i]+secondDeriv[i+1])*diffx*diffx*diffx/24; 
	}
	return integral;				
}

/* integration:
 * figuratively: .a......b. (each . stand for a supporting point, a and b given)
 * 1. check for specific cases a=b, b<a
 * 2. determine whether there is an index value inbetween a and b
 * 2.a) no: just linearly integrate between a and b
 * 2.b) yes: determine indices ha & lb, s.t. x(ha-1)<= a < x(ha), x(lb) < b <= x(lb+1)
 * 			 linearly integrate from a to ha and from lb to b
 * 			 integrate from ha to lb (we know ha <= lb)
 * 			 add things together
 */
 
/* this is only an approximation since at the borders I use a linear approximation */
double SplineApprox::integrate(double a, double b)
{
	double integral=0.0;
	int flag,ha,lb;
	Bisection * bi;
	
	if (m_length<2)
	  return 0;
	
	if (a==b)
		return 0.;
	if (a>b)
		return -integrate(b,a);
		
	bi = new Bisection(this->m_length,m_x);
	ha=bi->getIndex(a)+1;
	if (ha<m_length && m_x[ha]>=b) { // no index between a and b				
		integral=(b-a)*(getValue(a)+getValue(b))/2;
	} else {	   // there is (at least) one index between a and b						
		lb=bi->getIndex(b,flag);
		if (flag==1)
			lb--;												
		integral=((m_x[ha]-a)*(getValue(a)+m_y[ha])+(b-m_x[lb])*(getValue(b)+m_y[lb]))/2;	
		integral+=integrate(ha,lb);
	}	
	delete bi;	
	return integral;				
}

/* class LinearApprox
 */
 
void LinearApprox::getValues(int n,const double *x, double *y)
{
	int index,i,inc;
	int indexlo,indexhi;
	double h,b,a,xi;	

        if (m_length<1)
	  return;

	indexlo=0;
	indexhi=1;

	for (i=0;i<n;i++) {
		xi=x[i];
		
		// now find index s.t. m_x[index]<= xi < m_x[index+1]
		// since I know function values and derivatives only there
		
		// first phase hunt upwards (know that xi is only increasing i)				
		inc=1;	// hunt increment			
		while (indexhi<m_length-1 && xi>m_x[indexhi]) {
			indexlo=indexhi;
			inc = inc << 1; // i.e. times 2
			indexhi=indexlo+inc;								
		}	
		if (indexhi>=m_length)
			indexhi=m_length-1;				
		
		// now m_x[indexlo]<= xi <= m_x[indexhi]				
		
		if (m_x[indexhi]==xi) {
			y[i]=m_y[indexhi];
			continue;				
			
		} else {
			
			if (indexlo==indexhi) {
			  y[i]=m_y[indexhi];
			  continue;
			}
			// now m_x[indexlo] <= xi < m_x[indexhi]
			
			// bisection phase
			while (indexhi-indexlo > 1) { 						  
				index=(indexhi+indexlo) >> 1; // i.e. divided by 2
				if (m_x[index] > xi) indexhi=index;
				else indexlo=index;
			}					
							
			h=m_x[indexhi]-m_x[indexlo];
			if (h == 0.0) {
				WARNING("Different x-values have to be distinct");
				y[i]=m_y[indexhi];
			}
			a=(m_x[indexhi]-xi)/h;
			b=(xi-m_x[indexlo])/h;
			y[i]=a*m_y[indexlo]+b*m_y[indexhi];
		}
	}	
}

void LinearApprox::getValues(int n,const double *x, double *y, int *outofbounds) 
{
        if (m_length<1)
	  return;

	for (int i=0;i<n;i++) {
		if (x[i]<m_x[0])
			outofbounds[i]=-1;
		else if (x[i]>m_x[m_length-1])
			outofbounds[i]=1;
		else
			outofbounds[i]=0;
	}	
	getValues(n,x,y);			
}	

double LinearApprox::getValue(double x)
{
	int klo,khi,k;
	double h,b,a;

        if (m_length<1)
	  return 0;

	klo=0;
	khi=m_length-1;
	
	while (khi-klo > 1) {
		k=(khi+klo) >> 1;
		if (m_x[k] > x) khi=k;
		else klo=k;
	}		
	
	if (khi==klo)
	  return m_y[khi];
			
	h=m_x[khi]-m_x[klo];
	if (h == 0.0) {
		EXIT("All values have to be pairwise distinct");		
		exit(1);
	}
	a=(m_x[khi]-x)/h;
	b=(x-m_x[klo])/h;
	return a*m_y[klo]+b*m_y[khi];		
}

double LinearApprox::getValue(double x, int & outofbounds)
{
	if (m_length<1)
	  return 0;

	if (x<m_x[0])
		outofbounds=-1;
	else if (x>m_x[m_length-1])
		outofbounds=1;
	else
		outofbounds=0;		
	return getValue(x);				
}

// determines first (smallest index) x value s.t. y=f(x)
// "found" tells you whether at least one such value exists
double LinearApprox::getXValueRight(double y, int startIndex, int & foundIndex)
{
	int index,found;

    if (m_length<2) {
      foundIndex=-1;
      if (m_length==1)
        return m_x[0];
      return 0;
    }

    // determine first index i s.t. y[i]<= y < y[i+1]    
    index=startIndex;
    found=0;
    
    //debugging invalid read!
    if(index<0) {
    	//cout << "warning index<0" << endl;
    	index=0;
    }
    
    while (index<m_length-1) {    
		if ((m_y[index]<=y && m_y[index+1]>=y) || (m_y[index]>=y && m_y[index+1]<=y)) {
			found=1;
			break;					
		}
		index++;
	};
	
	if (found==0) {
		foundIndex=-1;
		return m_x[m_length-1];
	}			 		
	foundIndex=index;
		
	// (yb-ya)/(xb-xa)=(y-ya)/(x-xa) -> determine x
	return (y-m_y[index])/(m_y[index+1]-m_y[index])*(m_x[index+1]-m_x[index])+m_x[index];
}

// determines last (highest index) x value s.t. y=f(x)
// "found" tells you whether at least one such value exists
double LinearApprox::getXValueLeft(double y, int startIndex, int & foundIndex)
{
	int index,found;
	
	if (m_length<2) {
	  foundIndex=-1;
	  if (m_length==1)
	    return m_x[0];
	  return 0;
	}
	
	if (m_length<=1) {
		foundIndex=-1;
		return 0.;		
	}
			        
 	index=startIndex-1; 
 	if (index>m_length-2)
 	  index=m_length-2;
 	   
 	// determine index i s.t. y[i]<= y < y[i+1]   
    found=0;
	while (index>=0) {		
		if ((m_y[index]<=y && m_y[index+1]>=y) || (m_y[index]>=y && m_y[index+1]<=y)) {
			found=1;
			break;		
		}
		index--;
	};
		
	if (found==0) {
		foundIndex=-1;
		return 0.;
	}			 		
	foundIndex=index;
		
	// (yb-ya)/(xb-xa)=(y-ya)/(x-xa) -> determine x
	return (y-m_y[index])/(m_y[index+1]-m_y[index])*(m_x[index+1]-m_x[index])+m_x[index];
}

// determines x value s.t. y=f(x)
// only works in monotonic increasing functions, yet
double LinearApprox::getXValue(double y)
{
	int index,flag;
	
	if (m_length<1)
	  return 0;
	if (m_length==1)
	  return m_x[0];
	
    // determine index i s.t. y[i]<= y < y[i+1]
	Bisection *bi_y = new Bisection(this->m_length,this->m_y);
	index=bi_y->getIndex(y,flag);
	delete bi_y;
		
	if (flag==1)
		return m_x[index];
		
	// (yb-ya)/(xb-xa)=(y-ya)/(x-xa) -> determine x
	return (y-m_y[index])/(m_y[index+1]-m_y[index])*(m_x[index+1]-m_x[index])+m_x[index];
}

double LinearApprox::integrate()
{
	double integral=0.0;
	double diffx;
	for (int i=0;i<m_length-1;i++) {
		diffx=m_x[i+1]-m_x[i];
		integral+=(m_y[i]+m_y[i+1])*diffx;				  
	}
	integral/=2;
	return integral;				
}

double LinearApprox::integrate(int index_a, int index_b)
{
	double integral=0.0;
	double diffx;
	for (int i=index_a;i<index_b;i++) {
		diffx=m_x[i+1]-m_x[i];
		integral+=(m_y[i]+m_y[i+1])*diffx;				  
	}
	integral/=2;
	return integral;				
}


/* integration:
 * figuratively: .a......b. (each . stand for a supporting point, a and b given)
 * 1. check for specific cases a=b, b<a
 * 2. determine whether there is an index value inbetween a and b
 * 2.a) no: just linearly integrate between a and b
 * 2.b) yes: determine indices ha & lb, s.t. x(ha-1)<= a < x(ha), x(lb) < b <= x(lb+1)
 * 			 linearly integrate from a to ha and from lb to b
 * 			 integrate from ha to lb (we know ha <= lb)
 * 			 add things together
 */

double LinearApprox::integrate(double a, double b)
{
	double integral=0.0;
	double diffx;
	int flag,ha,lb;
	Bisection * bi;
	
	if (m_length<2)
	  return 0;
	
	if (a==b)
		return 0.;
	if (a>b)
		return -integrate(b,a);
		
	bi = new Bisection(this->m_length,m_x);
	ha=bi->getIndex(a)+1;
	if (m_x[ha]>=b) // no index between a and b
		integral=(b-a)*(getValue(a)+getValue(b))/2;
	else {	   // there is (at least) one index between a and b
		lb=bi->getIndex(b,flag);
		if (flag==1)
			lb--;
		integral=((m_x[ha]-a)*(getValue(a)+m_y[ha])+(b-m_x[lb])*(getValue(b)+m_y[lb]));			
		for (int i=ha;i<lb;i++) {
			diffx=m_x[i+1]-m_x[i];
			integral+=(m_y[i]+m_y[i+1])*diffx;				  
		}
		integral/=2;
	}	
	delete bi;	
	return integral;				
}

/* class Bisection
 */
Bisection::Bisection(int length, double* x)
{
 	this->length=length;
 	this->xvalues=x;
}
 
// return an index i , such that xvalues[i]<= xval < xvalues[i+1]; 
int Bisection::getIndex(double xval)
{
 	int inew;
	int il=0;
	int ih=length;
	
	while (ih-il>1) {
		inew=(il+ih) >> 1;
		if (xval<xvalues[inew]) 
			ih=inew;
		else
			il=inew;
	}	
	return il; // now: xvalues[il]<= xval < xvalues[il+1=ih]; 
}

// return an index i , such that xvalues[i]<= xval < xvalues[i+1]; 
// the additional flag==1 shows that xvalues[i]==xval or xval < xvalues[0] (only if i==0);
int Bisection::getIndex(double xval,int &flag)
{
 	int inew;
	int il=0;
	int ih=length;
	
	if (length<1)
	  return -1;
	
	if (xval<xvalues[0]) {
		flag=1;
		return 0;
	}
	if (xval>xvalues[length-1]) {
		flag=0;
		return length-1;
	}
	
	while (ih-il>1) {
		inew=(il+ih) >> 1;
		if (xval<xvalues[inew]) 
			ih=inew;
		else
			il=inew;
	}
	if (xvalues[il]==xval)
		flag=1;
	else
		flag=0;
	return il; // now: xvalues[il]<= xval < xvalues[il+1=ih]; 
}

int Bisection::getNearestIndex(double xval)
{
	if (length<1)
	  return -1;
	
	int i=getIndex(xval);
	if ((xval-xvalues[i])<=(xvalues[i+1]-xval))
		return i;
	else
		return i+1;	
}
 
 /* class CharLine
  */
 
CharLine::CharLine()
{
  	line=NULL;
  	lineEntries=NULL;
  	entryNr=0;
}
  
CharLine::CharLine(const char* string)
{
  	CharLine();
  	this->length=strlen(string);
  	line=string;
}
  
CharLine::~CharLine()
{
  	freeArray2(lineEntries,entryNr);
}
  
void CharLine::set(const char* string)
{  	  	
	this->length=strlen(string);
  	line=string;  	  	  	  	
  	freeArray2(lineEntries,entryNr);
  	entryNr=0;
  	lineEntries=NULL;
}
  
  // splits input line into several entries separated by delimiters
  // delimiters contains a string which consists of
  // the used delimiters; it is terminated (as usual)
  // by '\0'
void CharLine::split(const char *delimiters)
{
	int lasti=0;
	int entryNr=0;		
	
	// determine number of entries
	for (int i=0;i<length;i++)
	{		
		
		// when line[i] delimiter
		// save current entry and remove 
		// remaining delimiters
		if (strchr(delimiters,line[i]))		
		{
			
			//for (int j=0;j<i-lasti;j++)
			//	lineEntry[entryNr][j]=line[lasti+j];
			//lineEntry[entryNr][i-lasti]='\0';
					
			if (lasti<i)
				entryNr++;				
			while (i+1<length && strchr(delimiters,line[i+1]))								
				i++;
			lasti=i+1;
		}					
	}
	
	// just to get the number of entryNr right for the end of the line
	if (lasti<length)
		entryNr++;
	
	this->entryNr=entryNr;
	
	// alloc memory
	allocArray2(this->lineEntries,entryNr,length+1);
	
	// read in entries
	lasti=0;
	entryNr=0;
	for (int i=0;i<length;i++)
	{
		// when line[i] delimiter
		// save current entry and remove 
		// remaining delimiters
		if (strchr(delimiters,line[i]))		
		{
				
			if (lasti<i) {
				for (int j=0;j<i-lasti;j++)
					this->lineEntries[entryNr][j]=line[lasti+j];
				this->lineEntries[entryNr][i-lasti]='\0';	
				entryNr++;
			}
			while (i+1<length && strchr(delimiters,line[i+1]))								
				i++;
			lasti=i+1;
		}			
	}
	// again, special case - end of line
	if (lasti<length) {
		for (int j=0;j<length-lasti;j++)
			this->lineEntries[entryNr][j]=line[lasti+j];
		this->lineEntries[entryNr][length-lasti]='\0';	
		entryNr++;
	}	
}
    
int CharLine::getEntryNumber()
{  	  		
	return this->entryNr;	
}
  
char **CharLine::getEntries()
{
  	return this->lineEntries;
}	


/* class xyData
 */
void xyData::make_interpol_obj() {
	if (! this->interpol_obj)	{
		if (approxCurveType==0)
			this->interpol_obj = new LinearApprox(this->length,this->x,this->y);
		else
			this->interpol_obj = new SplineApprox(this->length,this->x,this->y,0,0);
	}
}	


xyData::xyData()
{
	this->x=NULL;
	this->y=NULL;
	this->bi=NULL;
	this->interpol_obj=NULL;
	this->length=0;	
	this->approxCurveType=0;
}

xyData::xyData(int length,const double *x,const double *y)
{	
	this->x=NULL;
	this->y=NULL;
	this->bi=NULL;
	this->interpol_obj=NULL;
	this->approxCurveType=0;
	
	this->length=length;
	if (length>0) {
		allocArray1(this->x,length);
		allocArray1(this->y,length);
	}
	for (int i=0;i<length;i++) 
		this->x[i]=x[i];
	for (int i=0;i<length;i++) 
		this->y[i]=y[i];
}

xyData::xyData(xyData *d)
{	
	this->x=NULL;
	this->y=NULL;
	bi = NULL;				// cannot copy since would refer to data of d (pointers)
	interpol_obj = NULL;	// cannot copy since would refer to data of d (pointers)
	
	this->approxCurveType=d->approxCurveType;	
	this->length=d->length;
	if (length>0) {
		allocArray1(x,length);
		allocArray1(y,length);
	}
	for (int i=0;i<length;i++) {		
		x[i]=d->x[i];
		y[i]=d->y[i];
	}	
}

xyData::~xyData()
{
	freeArray1(this->x);
	freeArray1(this->y);
	if (this->bi!=NULL)
		delete this->bi;
	this->bi=NULL;
	if (this->interpol_obj!=NULL)
		delete this->interpol_obj;
	this->interpol_obj=NULL;
}

void xyData::resize()
{
	freeArray1(this->x);
	freeArray1(this->y);
	this->length=0;	
	if (this->bi!=NULL)
		delete this->bi;
	if (this->interpol_obj!=NULL)
		delete this->interpol_obj;
	this->bi=NULL;
	this->interpol_obj=NULL;	
}
void xyData::reset()
{
	if (this->bi!=NULL)
		delete this->bi;
	if (this->interpol_obj!=NULL)
		delete this->interpol_obj;	
	this->bi=NULL;
	this->interpol_obj=NULL;	
}

void xyData::copy(const xyData * data,int validLowLimit, int validHighLimit)
{	
	// clear all
	resize();	
		
	// copy
	this->approxCurveType=data->approxCurveType;	
	this->length=validHighLimit-validLowLimit+1;
	if (length>0) {
		allocArray1(this->x,this->length);
		allocArray1(this->y,this->length);
	}
	for (int i=0;i<this->length;i++) {		
		x[i]=data->x[i+validLowLimit];
		y[i]=data->y[i+validLowLimit];
	}
}

void xyData::set(int length,const double *x,const double *y)
{
	// clear all
	resize();	
	
	// set all	
	this->length=length;
	if (length>0) {
		allocArray1(this->x,length);
		allocArray1(this->y,length);
	}
	for (int i=0;i<length;i++) 
		this->x[i]=x[i];
	for (int i=0;i<length;i++) 
		this->y[i]=y[i];
	
}

void xyData::set(xyData *d)
{
	// clear all
	resize();	
		
	this->approxCurveType=d->approxCurveType;	
	this->length=d->length;
	if (length>0) {
		allocArray1(x,length);
		allocArray1(y,length);
	}
	for (int i=0;i<length;i++) {		
		x[i]=d->x[i];
		y[i]=d->y[i];
	}	
}

void xyData::set(int length,const double *x)
{
	// clear all
	resize();	
	
	// set all
	this->length=length;
	if (length>0) {
		allocArray1(this->x,length);
		callocArray1(this->y,length);
	}
	for (int i=0;i<length;i++) 
		this->x[i]=x[i];		
}

int xyData::calcBothInterpol(xyData *data2,int & numPoints,double* & xInter)
{
	double minx1,minx2,maxx1,maxx2;
	double minOverlap,maxOverlap;
	int minIndex1,minIndex2,maxIndex1,maxIndex2;
	int flag;
	int count;
	int havePoints1,havePoints2;
	double lastx1,lastx2;
	double *p1,*p2;				 	

	// calculate overlapping range of xvalues
	// only here an interpolation is useful		
	minx1=getMinX();
	minx2=data2->getMinX();
	maxx1=getMaxX();
	maxx2=data2->getMaxX();
	minOverlap = (minx1 > minx2) ? minx1 : minx2;
	maxOverlap = (maxx1 < maxx2) ? maxx1 : maxx2;
	
	if (maxOverlap<=minOverlap) {								
		return 2;
	}			
	
	// three cases
	// a) both data sets have points in overlap region
	// b) only data set 1 has points there
	// c) only data set 2 has points there
		
	// minIndex +1 since getIndex searches 
	// for an index i such that x[i]<=x<x[i+1]
	// if x[i]<x -> in order to avoid extrapolation I have to increase index by 1
	numPoints=0;
	havePoints1=0;
	havePoints2=0;
	flag=0;
	minIndex1=getIndex(minOverlap,flag); 
	if (flag==0) 	// x[i] < x
		minIndex1++;
	maxIndex1=getIndex(maxOverlap);
	if (minIndex1<=maxIndex1) {
		havePoints1=1;
		numPoints+=maxIndex1-minIndex1+1;		
	}
	flag=0;
	minIndex2=data2->getIndex(minOverlap,flag);
	if (flag==0)	// x[i] < x
		minIndex2++;
	maxIndex2=data2->getIndex(maxOverlap);
	if (minIndex2<=maxIndex2) {
		havePoints2=1;
		numPoints+=maxIndex2-minIndex2+1;	
	}	
	
	allocArray1(xInter,numPoints+1);	
	
	if (! havePoints1) {			// case c)
		p2=data2->getX()+minIndex2;
		for (int i=0;i<numPoints;i++) {
			xInter[i]=*p2;
			p2++;
		}
	} else if (! havePoints2) {		// case b)
		p1=this->getX()+minIndex1;
		for (int i=0;i<numPoints;i++) {
			xInter[i]=*p1;
			p1++;
		}
	} else {						// case a)		
		p1=this->getX()+minIndex1;
		p2=data2->getX()+minIndex2;				
		lastx1=*p1;
		lastx2=*p2;		
		xInter[0]=minOverlap;
		count=1;
		if (*p1==minOverlap)		
			p1++;
		else
			lastx1--;				
		if (*p2==minOverlap)
			p2++;		
		else
			lastx2--;	
		while (xInter[count-1]<maxOverlap) { // for a certain count xInter[count-1]==maxOverlap
			if (lastx1==*p1) { // ignore same x-value
				p1++;				
				continue;
			}
			if (lastx2==*p2) { // ignore same x-value
				p2++;				
				continue;
			}			
			if (*p1==*p2)  { // ignore same x-value
				lastx1=xInter[count]=*p1;			
				p1++;						
				p2++;						
			} else if (*p1<*p2) {
				lastx1=xInter[count]=*p1;		
				p1++;				
			} else {
				lastx2=xInter[count]=*p2;
				p2++;				
			}
			count++;			 // last three cases bring valid x-value
		}		
		numPoints=count;
	}		
	return 0;
}

xyData* xyData::operator * (const double val)
{	
	if (this->interpol_obj!=NULL)
		delete this->interpol_obj;
	this->interpol_obj=NULL;
	for (int i=0;i<this->length;i++)
		y[i]*=val;	
	return this;
}

xyData* xyData::operator * (xyData* data)
{	
	int numPoints,result;
	double *xInter,*yInter1,*yInter2;
		
	result=calcBothInterpol(data,numPoints,xInter);
	
	if (result>0)
		return NULL;
		
	allocArray1(yInter1,numPoints);
	allocArray1(yInter2,numPoints);
	
	this->calcYValues(numPoints,xInter,yInter1);
	data->calcYValues(numPoints,xInter,yInter2);
		
	// multiply
	for (int i=0; i<numPoints;i++)
		yInter1[i]*=yInter2[i];
		
	// clear memory and set new x,y entries;
	freeArray1(yInter2);
	this->resize();
	this->length=numPoints;
	this->x=xInter;
	this->y=yInter1;
					
	return this;
}

xyData* xyData::operator / (const double val)
{	
	if (this->interpol_obj!=NULL)
		delete this->interpol_obj;
	this->interpol_obj=NULL;
	for (int i=0;i<this->length;i++)
		y[i]/=val;	
	return this;
}

xyData* xyData::operator / (xyData* data)
{	
	int numPoints,result;
	double *xInter,*yInter1,*yInter2;
		
	result=calcBothInterpol(data,numPoints,xInter);
	
	if (result>0)
		return NULL;
		
	allocArray1(yInter1,numPoints);
	allocArray1(yInter2,numPoints);
	
	this->calcYValues(numPoints,xInter,yInter1);
	data->calcYValues(numPoints,xInter,yInter2);
		
	// divide
	for (int i=0; i<numPoints;i++)
		yInter1[i]/=yInter2[i];
		
	// clear memory and set new x,y entries;
	freeArray1(yInter2);
	this->resize();
	this->length=numPoints;
	this->x=xInter;
	this->y=yInter1;
					
	return this;
}

xyData* xyData::operator + (const double val)
{	
	if (this->interpol_obj!=NULL)
		delete this->interpol_obj;
	this->interpol_obj=NULL;
	for (int i=0;i<this->length;i++)
		y[i]+=val;	
	return this;
}

xyData* xyData::operator + (xyData* data)
{	
	int numPoints,result;
	double *xInter,*yInter1,*yInter2;
		
	result=calcBothInterpol(data,numPoints,xInter);
	
	if (result>0)
		return NULL;
		
	allocArray1(yInter1,numPoints);
	allocArray1(yInter2,numPoints);
	
	this->calcYValues(numPoints,xInter,yInter1);
	data->calcYValues(numPoints,xInter,yInter2);
		
	// add
	for (int i=0; i<numPoints;i++)
		yInter1[i]+=yInter2[i];
		
	// clear memory and set new x,y entries;
	freeArray1(yInter2);
	resize();
	this->length=numPoints;
	this->x=xInter;
	this->y=yInter1;
					
	return this;
}

xyData* xyData::operator - (const double val)
{	
	if (this->interpol_obj!=NULL)
		delete this->interpol_obj;
	this->interpol_obj=NULL;
	for (int i=0;i<this->length;i++)
		y[i]-=val;	
	return this;
}

xyData* xyData::operator - (xyData* data)
{	
	int numPoints,result;
	double *xInter,*yInter1,*yInter2;
		
	result=calcBothInterpol(data,numPoints,xInter);
	
	if (result>0)
		return NULL;
		
	allocArray1(yInter1,numPoints);
	allocArray1(yInter2,numPoints);
	
	this->calcYValues(numPoints,xInter,yInter1);
	data->calcYValues(numPoints,xInter,yInter2);
		
	// substract
	for (int i=0; i<numPoints;i++)
		yInter1[i]-=yInter2[i];
		
	// clear memory and set new x,y entries;
	freeArray1(yInter2);
	resize();
	this->length=numPoints;
	this->x=xInter;
	this->y=yInter1;
					
	return this;
}

void xyData::setY(xyData *data)
{
	int numPoints,result;
	double *xInter,*yInter2;
		
	result=calcBothInterpol(data,numPoints,xInter);
	
	if (result>0)
		return;
		
	allocArray1(yInter2,numPoints);
	
	data->calcYValues(numPoints,xInter,yInter2);		
		
	// clear memory and set new x,y entries;
	resize();
	this->length=numPoints;
	this->x=xInter;
	this->y=yInter2;							
}

void xyData::setY(double *y)
{										
	for (int i=0;i<length;i++)
		this->y[i]=y[i];				
}

void xyData::setY(double y)
{
	for (int i=0;i<length;i++)
		this->y[i]=y;	
}

void xyData::calcYValues(int numPoints,const double *x, double *y)
{
	make_interpol_obj();
		
	this->interpol_obj->getValues(numPoints,x,y);		
}

void xyData::calcYValues(int numPoints,const double *x, double *y, int* outofbounds)
{
	make_interpol_obj();
		
	this->interpol_obj->getValues(numPoints,x,y,outofbounds);				
}

double xyData::calcYValue(double x)
{
	make_interpol_obj();
	
	return this->interpol_obj->getValue(x);		
}

double xyData::calcYValue(double x, int & outofbounds)
{
	make_interpol_obj();
		
	return this->interpol_obj->getValue(x,outofbounds);		
}

double xyData::getMinY() const
{
	double min=DBL_MAX;
	for (int i=0;i<this->getlength();i++) 
		if (min>this->y[i])
			min=this->y[i];
	return min;			
}

double xyData::getMaxY() const
{
	double max=DBL_MIN;
	for (int i=0;i<this->getlength();i++) 
		if (max<this->y[i])
			max=this->y[i];
	return max;			
}

double xyData::getMinX() const
{
	return this->x[0];
}

double xyData::getMaxX() const
{
	return this->x[length-1];	
}

int xyData::getIndex(const double & x)
{	
	if (this->bi==NULL) 
		this->bi = new Bisection(this->length,this->x);
	return bi->getIndex(x);
}

int xyData::getIndex(const double & x,int & flag)
{	
	if (this->bi==NULL) 
		this->bi = new Bisection(this->length,this->x);
	return bi->getIndex(x,flag);	
}

int xyData::getNearestIndex(const double & x)
{	
	if (this->bi==NULL) 
		this->bi = new Bisection(this->length,this->x);
	return bi->getNearestIndex(x);
}

double* xyData::getX() const
{
	return this->x;
}

double* xyData::getY() const
{
	return this->y;	
}

void xyData::getCopyX(double* & x) const
{
	for (int i=0;i<this->length;i++)
		x[i]=this->x[i];	
}

void xyData::getCopyY(double* & y) const
{
	for (int i=0;i<this->length;i++)
		y[i]=this->y[i];
}

void xyData::invertXY()
{
	double *aux;
	aux=x;
	x=y;
	y=aux;	
}

double xyData::getXValue(double y)	// returns the (first) x-value (interpolated), s.t. f(x)=y
{
	make_interpol_obj();
	
	return this->interpol_obj->getXValue(y);			
}

double xyData::getXValueRight(double y, int startIndex, int & found) 
{
	make_interpol_obj();
	
	return this->interpol_obj->getXValueRight(y,startIndex,found);			
}

double xyData::getXValueRight(double y, int & found)
{	
	return this->getXValueRight(y,0,found);		
}


double xyData::getXValueLeft(double y, int startIndex, int & found) 
{
	make_interpol_obj();
	
	return this->interpol_obj->getXValueLeft(y,startIndex,found);					
}

double xyData::getXValueLeft(double y, int & found)  // returns the last x-value (interpolated), s.t. f(x)=y 
{
	return this->getXValueLeft(y,this->length-1,found);					
}

double xyData::sum(int a, int b)
{
	double sum;
	
	if (a<0) 
		a=0;
	if (b>length-1)
		b=length-1;
	
	sum=0;
	for (int i=a;i<b;i++)
		sum+=y[i];
		
	return sum;	
}

/* simple trapezoid integration
 */
 
double xyData::integrateSimple()
{
	double dx,ymean,integral;
	double *xj,*yj;
	
	yj=this->y;
	xj=this->x;	
	integral=0.0;
	for (int i=0;i<this->length-1;i++)
	{
		dx=-xj[0];
		xj++;	
		dx+=xj[0];	
		ymean=yj[0];
		yj++;
		ymean+=yj[0];		 
		integral+=ymean*dx;					
	}		
	integral/=2;
	return integral;
}
 
double xyData::integrate()
{
	make_interpol_obj();
	
	return interpol_obj->integrate();		
}

double xyData::integrate(int index_a, int index_b) 
{
	make_interpol_obj();
		
	return interpol_obj->integrate(index_a, index_b);		
	
}

double xyData::integrate(double a, double b) 
{
	make_interpol_obj();	
	
	return interpol_obj->integrate(a,b);		
	
}

// cumulates the y-values
void xyData::cumulate()
{
	make_interpol_obj();
	
	double *new_y;
	
	allocArray1(new_y,this->length);
	new_y[0]=0;
	for (int i=0;i<this->length-1;i++)
		new_y[i+1]=new_y[i]+interpol_obj->integrate(i,i+1);
	freeArray1(y);
	y=new_y;
	reset();
}

// these calculates effectively an (summed) average over the range from -fwhm/2 to +fwhm/2
// \sum_{z=x-fwhm/2}^{x+fwhm/2} f(z) s(x-z)   (do it for all x values)

void xyData::convolveRectangularSum(double fwhm)
{
	double *y2;
	double hwhm,aux;
	int lo,hi,flag;
	
	// the rectangle lies between -hwhm <= x < hwhm
	hwhm=fwhm/2;
	// make a copy of values
	allocArray1(y2,length);
	for (int i=0;i<length;i++)
		y2[i]=y[i];
	
	for (int i=0;i<length;i++) {
			
		// get integration bounds
		lo=getIndex(x[i]-hwhm,flag);
		if (flag==0)
			lo++;
		hi=getIndex(x[i]+hwhm,flag);
		if (flag==1)
			hi--;							
		
		if (lo>=hi)	// no points (or just one) found for executing convolution 
			continue;
		
		aux=0;
		for (int j=lo;j<=hi;j++) 	// summing over all these points (height of rectangle is 1)
			aux+=y2[j];
		aux/=(hi-lo+1);			// divide by number of data points lying in the rectangle
		y[i]=aux;
	}
	freeArray1(y2);
}

// convolve with a gaussian up to the 3 sigma range
void xyData::convolveGaussian(double sigma)
{
	xyData *data;
	double *f,*n,*y_conv;
	int minIndex,maxIndex,numIndex;
	double aux;
	int aux2;
	
	
	data = new xyData();
	allocArray1(y_conv,this->length);
	
	for (int t=0;t<this->length;t++) {
		
		// rigorous approach
		//if ((x[t]-3*sigma<x[0]) || (x[t]+3*sigma>x[length-1])) {
		// more practical
		/*
		if ((x[t]-sigma<x[0]) || (x[t]+sigma>x[length-1])) {
			y_conv[t]=y[t];
			continue;
		}
		*/		
		
		minIndex=getIndex(x[t]-3*sigma);
		maxIndex=getIndex(x[t]+3*sigma);		
		numIndex=maxIndex-minIndex+1;	
		if (numIndex<2) {
			y_conv[t]=y[t];
			continue;	
		}
				
		allocArray1(f,numIndex);
		allocArray1(n,numIndex);
		// multiply
		for (int i=minIndex;i<=maxIndex;i++) {			
			aux=(x[t]-x[i])/sigma;
			aux2=i-minIndex;
			f[aux2]=y[i]*exp(-aux*aux/2);
			n[aux2]=exp(-aux*aux/2);
		}	
		// integrate
		data->set(numIndex,x+minIndex,f);
		y_conv[t]=data->integrate();
		// normalize
		data->set(numIndex,x+minIndex,n);
		y_conv[t]/=data->integrate();
		freeArray1(f);
		freeArray1(n);
	}
	
	freeArray1(y);
	y=y_conv;
	
	delete data;
}

// convolve with a gaussian up to the 3 sigma range but use variable sigma for each point
void xyData::convolveGaussian(double *sigma)
{
	xyData *data;
	double *f,*n,*y_conv;
	int minIndex,maxIndex,numIndex;
	double aux;
	int aux2;
	
	
	data = new xyData();
	allocArray1(y_conv,this->length);
	
	for (int t=0;t<this->length;t++) {				
		
		minIndex=getIndex(x[t]-3*sigma[t]);
		maxIndex=getIndex(x[t]+3*sigma[t]);		
		numIndex=maxIndex-minIndex+1;	
		if (numIndex<2) {
			y_conv[t]=y[t];
			continue;	
		}
				
		allocArray1(f,numIndex);
		allocArray1(n,numIndex);
		// multiply
		for (int i=minIndex;i<=maxIndex;i++) {			
			aux=(x[t]-x[i])/sigma[t];
			aux2=i-minIndex;
			f[aux2]=y[i]*exp(-aux*aux/2);
			n[aux2]=exp(-aux*aux/2);
		}	
		// integrate
		data->set(numIndex,x+minIndex,f);
		y_conv[t]=data->integrate();
		// normalize
		data->set(numIndex,x+minIndex,n);
		y_conv[t]/=data->integrate();
		freeArray1(f);
		freeArray1(n);
	}
	
	freeArray1(y);
	y=y_conv;
	
	delete data;
}

// these calculates effectively an (integral) average over the range from -fwhm/2 to +fwhm/2
// \int_{z=x-fwhm/2}^{x+fwhm/2} f(z) dz /  \int_{z=x-fwhm/2}^{x+fwhm/2} dz (do it for all x values)
// fwhm ... is FWHM if smoothMode==0
//      ... is FWHM/x if smoothMode==1

void xyData::average(double fwhm,double lowlimit,int smoothMode)
{
	double *y2;
	double hwhm,aux;
	int lo,hi,flag;
	
	if (length<=1)
	  return;
	
	// make a copy of values
	allocArray1(y2,length);
	for (int i=0;i<length;i++)
		y2[i]=y[i];
	
	if (smoothMode==0) {
	
	  // the rectangle lies between -hwhm <= x < hwhm
	  hwhm=fwhm/2;
	
	  for (int i=0;i<length;i++) {
			
		if (x[i]-hwhm<lowlimit)
			continue;
			
		// get integration bounds	
		lo=getIndex(x[i]-hwhm,flag);		
		if (flag==0)
			lo++;
		hi=getIndex(x[i]+hwhm,flag);
		if (flag==1)
			hi--;							
		
		if (lo>=hi)	// no points (or just one) found for executing convolution 
			continue;
		
		aux=0;
		for (int j=lo;j<hi;j++) 	// calculating \int_{z=xlo}^{xhi} f(z) dz
			aux+=(y2[j]+y2[j+1])*(x[j+1]-x[j]);
		aux/=2.0;	
		aux/=(x[hi]-x[lo]);			// dividing by \int_{z=xlo}^{xhi} dz 
		y[i]=aux;
	  }
	  
	} else {
	
	  for (int i=0;i<length;i++) {
		
		hwhm=fwhm/2;	
			
		if (x[i]*(1-hwhm)<lowlimit)
			continue;
			
		// get integration bounds	
		lo=getIndex(x[i]*(1-hwhm),flag);		
		if (flag==0)
			lo++;
		hi=getIndex(x[i]*(1+hwhm),flag);
		if (flag==1)
			hi--;							
		
		if (lo>=hi)	// no points (or just one) found for executing convolution 
			continue;
		
		aux=0;
		for (int j=lo;j<hi;j++) 	// calculating \int_{z=xlo}^{xhi} f(z) dz
			aux+=(y2[j]+y2[j+1])*(x[j+1]-x[j]);
		aux/=2.0;	
		aux/=(x[hi]-x[lo]);			// dividing by \int_{z=xlo}^{xhi} dz 
		y[i]=aux;
	  }
	
	}
	
	freeArray1(y2);
}

// calculates an index j, such that \int_{i=0}^{j}y[i] <= (1-conflevel)/2 < \int_{i=0}^{j+1}y[i]
// (but the index is always between 0 and length-1)
int xyData::getPercentileLow(double conflevel)
{	
	return getPercentileLow(conflevel, 1);
}

// calculates an index j, such that \int_{i=0}^{j}y[i]/norm <= (1-conflevel)/2 < \int_{i=0}^{j+1}y[i]/norm
// (but the index is always between 0 and length-1)
int xyData::getPercentileLow(double conflevel, double norm)
{	
	double integral=0.0;
	double diffx;
	double limit=(1-conflevel);	// note here should be a devision by 2, but since the same should
								// be done with the integral value -> drop both -> faster
	limit*=norm;
				
	for (int i=0;i<length-1;i++) {
		diffx=x[i+1]-x[i];
		integral+=(y[i]+y[i+1])*diffx;			
		if (integral>limit)
			return i;
	}		
	
	return length-1;
}

// calculates an index j, such that \int_{i=0}^{j}y[i]/norm <= (1-conflevel)/2 < \int_{i=0}^{j+1}y[i]/norm
// (but the index is always between 0 and length-1)
// index and integral are used as input & output values !!
int xyData::getPercentileLow(double conflevel, double norm, int & index, double & integral)
{	
	double aux=2*integral;
	double diffx;
	double limit=(1-conflevel);	// note here should be a devision by 2, but since the same should
								// be done with the integral value -> drop both -> faster
								
	limit*=norm;
		
	int i;			
	for (i=index;i<length-1;i++) {
		diffx=x[i+1]-x[i];
		aux+=(y[i]+y[i+1])*diffx;			
		if (aux>limit) 
			break;
	}
	integral=aux/2;
	index=i;
	
	return i;
}

// calculates an index j, such that \int_{i=0}^{j}y[i]/norm <= (1-conflevel)/2 < \int_{i=0}^{j+1}y[i]/norm
// for histograms
int xyData::getPercentileLowHist(double conflevel)
{		
	double limit=(1-conflevel)/2.;
	int i;
	
	double norm=0;		
	for (i=0;i<length;i++) {
		norm+=y[i];
	}
	limit*=norm;
	
	double aux=0.;				
	for (i=0;i<length;i++) {
	    aux+=y[i];
		if (aux>limit) 
			break;
	}
	
	return i;
}

// calculates an index j, such that \int_{i=j}^{n}y[i] <= (1-conflevel)/2 < \int_{i=j-1}^{n}y[i]
// (but the index is always between 0 and length-1)
int xyData::getPercentileHigh(double conflevel)
{
	return getPercentileHigh(conflevel,1);
}

// calculates an index j, such that \int_{i=j}^{n}y[i]/norm <= (1-conflevel)/2 < \int_{i=j-1}^{n}y[i]/norm
// (but the index is always between 0 and length-1)
int xyData::getPercentileHigh(double conflevel,double norm)
{
	double integral=0.0;
	double diffx;
	double limit=(1-conflevel);	// note here should be a devision by 2, but since the same should
								// be done with the integral value -> drop both -> faster	
	limit*=norm;			
									
	for (int i=length-2;i>=0;i--) {
		diffx=x[i+1]-x[i];
		integral+=(y[i]+y[i+1])*diffx;			
		if (integral>limit)
			return i+1;			
	}
	
	return 0;
}

// calculates an index j, such that \int_{i=j}^{n}y[i]/norm <= (1-conflevel)/2 < \int_{i=j-1}^{n}y[i]/norm
// (but the index is always between 0 and length-1)
int xyData::getPercentileHigh(double conflevel,double norm, int & index, double & integral)
{
	double aux=2*integral;
	double diffx;
	double limit=(1-conflevel);	// note here should be a devision by 2, but since the same should
								// be done with the integral value -> drop both -> faster	
	limit*=norm;			
		
	int i;								
	for (i=index-1;i>=0;i--) {
		diffx=x[i+1]-x[i];
		aux+=(y[i]+y[i+1])*diffx;			
		if (aux>limit)
			break;
	}
	
	integral=aux/2;
	index=i+1;
		
	return i+1;
}

// calculates an index j, such that \int_{i=j}^{n}y[i]/norm <= (1-conflevel)/2 < \int_{i=j-1}^{n}y[i]/norm
// for histograms
int xyData::getPercentileHighHist(double conflevel)
{		
	double limit=(1-conflevel)/2.;
	int i;
	
	double norm=0;		
	for (i=0;i<length;i++) {
		norm+=y[i];
	}
	limit*=norm;
					
	double aux=0.;				
	for (i=length-1;i>=0;i--) {
	    aux+=y[i];
		if (aux>limit) 
			break;
	}
	
	return i;
}

int xyData::checkStrictMonotonicityX() {
        if (length<2)
	  return 1;
	  
	double xmin=x[0];
	for (int i=1;i<length;i++) {
	  if (x[i]<=xmin)
	    return 0;
	  xmin=x[i];
	}
	
	return 1;
}

void xyData::sort()
{	
	struct xyPair *dataPair;
	allocArray1(dataPair,length);
	
	// put data into that new structure
	for (int i=0;i<length;i++) {
		dataPair[i].x=this->x[i];
		dataPair[i].y=this->y[i];
	}		
	
	// sort
	qsort(dataPair,length,sizeof(xyPair),compDataPair);
	
	// take it from that structure
	for (int i=0;i<length;i++) {
		this->x[i]=dataPair[i].x;
		this->y[i]=dataPair[i].y;
	}
	
	freeArray1(dataPair);
}

void xyData::sortY()
{	
	struct xyPair *dataPair;
	allocArray1(dataPair,length);
	
	// put data into that new structure
	for (int i=0;i<length;i++) {
		dataPair[i].x=this->y[i];
		dataPair[i].y=this->x[i];
	}		
	
	// sort
	qsort(dataPair,length,sizeof(xyPair),compDataPair);
	
	// take it from that structure
	for (int i=0;i<length;i++) {
		this->x[i]=dataPair[i].y;
		this->y[i]=dataPair[i].x;
	}
	
	freeArray1(dataPair);
}

void xyData::print()
{
		
	cout << "# length=" << length << endl;
	for (int i=0;i<this->length;i++) {		
		cout << this->x[i] << " " << this->y[i] << endl;
	}	
}

void xyData::write(const char* filename)
{
	ofstream ofile;
	
	ofile.open(filename);
	if (! ofile.good()) {
		cerr << "Could not open output file " << filename << endl;
	}
	ofile << "# length=" << length << endl;
	for (int i=0;i<this->length;i++) {		
		ofile << this->x[i] << " " << this->y[i] << endl;
	}
	ofile.close();
	ofile.clear();
}

void xyData::append(const char* filename)
{
	ofstream ofile;
	
	ofile.open(filename,ios::app);
	if (! ofile.good()) {
		cerr << "Could not open output file " << filename << endl;
	}
	ofile << "# length=" << length << endl;
	for (int i=0;i<this->length;i++) {		
		ofile << this->x[i] << " " << this->y[i] << endl;
	}
	ofile.close();
	ofile.clear();
}

/*
 * class xyErrorData
 */

void xyErrorData::set(int length,const double *x)
{
	xyData::set(length,x);
	freeArray1(ex);
 	freeArray1(ey);
	if (length>0) {		
		callocArray1(this->ex,length);
		callocArray1(this->ey,length);
	}	
}

void xyErrorData::set(int length,const double *x,const double *y)
{
	xyData::set(length,x,y);
	freeArray1(ex);
 	freeArray1(ey);	
	if (length>0) {
		callocArray1(this->ex,length);
		callocArray1(this->ey,length);
	}	
}

void xyErrorData::set(int length,const double* x,const double* y, 
 				      const double *error_x, const double *error_y) { 			
 	xyData::set(length,x,y);
 	freeArray1(ex);
 	freeArray1(ey);
 	if (length>0) {
 		allocArray1(ex,length);
 		allocArray1(ey,length);
 	}
 	for (int i=0;i<length;i++) {
 		ex[i]=error_x[i];
 		ey[i]=error_y[i];	 	
 	}				 				
}

void xyErrorData::set(xyErrorData *d)
{
	xyData::set(d);
	freeArray1(ex);
 	freeArray1(ey);			
	if (length>0) {		
		allocArray1(ex,length);
		allocArray1(ey,length);
	}
	for (int i=0;i<length;i++) {				
		ex[i]=d->ex[i];
		ey[i]=d->ey[i];
	}	
}

void xyErrorData::setY(double *y) 
{
	xyData::setY(y);
}

void xyErrorData::setY(double y) 
{
	xyData::setY(y);
}

void xyErrorData::setY(xyErrorData *data)
{
	int numPoints,result;
	double *xInter,*yInter2;
		
	result=calcBothInterpol(data,numPoints,xInter);
	
	if (result>0)
		return;
		
	allocArray1(yInter2,numPoints);
	
	data->calcYValues(numPoints,xInter,yInter2);		
		
	// clear memory and set new x,y entries;
	resize();
	this->length=numPoints;
	this->x=xInter;
	this->y=yInter2;							
}

void xyErrorData::setEX(double y) 
{
	for (int i=0;i<length;i++)
		ex[i]=y;
}	

void xyErrorData::setEX(double *y) 
{
	for (int i=0;i<length;i++)
		ex[i]=y[i];
}

void xyErrorData::setEY(double y) 
{
	for (int i=0;i<length;i++)
		ey[i]=y;
}

void xyErrorData::setEY(double *y) 
{
	for (int i=0;i<length;i++)
		ey[i]=y[i];
}

// calculate estimated <X> = 1/n sum x_i
double xyErrorData::getMeanX() 
{ 			 			 			 			
 	double aux=0.;
 			
 	for (int i=0;i<length;i++)
 		aux+=x[i]; 		 			 		 				
 	return aux/length;
}
 		
// calculate estimated <X> = 1/n sum x_i	
// data with a x-value above or below the (1-level)/2 percentile is clipped 		 	
double xyErrorData::getMeanX(double level) 
{ 			
 	double aux=0.;
 			
 	if (level>1)
 		level=1;
 	if (level<0)
 		level=0;
 	int lowIndex=int((1.-level)/2*length+0.5);
 	int highIndex=length-lowIndex-1;
 			
 	sort();
 				
 	for (int i=lowIndex;i<=highIndex;i++)
 		aux+=x[i];	 			 	
 				
 	// note if e.g. level=0 -> lowIndex>highIndex -> return NAN				
 	return aux/(highIndex+1-lowIndex);
}

// calculate estimated <X> = sum_i(x_i/sigma^2_i)/sum_i(1/sigma^2_i) 		
double xyErrorData::getWeightedMeanX() 
{ 			 			 			 			
 	double aux=0.;
 	double aux2=0.;
 			
 	for (int i=0;i<length;i++)
 		aux+=x[i]/(ex[i]*ex[i]); 		 			
 	for (int i=0;i<length;i++)
 		aux2+=1/(ex[i]*ex[i]); 				 				
 	return aux/aux2;
}
 		
// calculate estimated <X> = sum_i(x_i/sigma^2_i)/sum_i(1/sigma^2_i)  		
// data with a x-value above or below the (1-level)/2 percentile is clipped 		 	
double xyErrorData::getWeightedMeanX(double level) 
{ 			
 	double aux=0.;
 	double aux2=0.;
 			
 	if (level>1)
 		level=1;
 	if (level<0)
 		level=0;
 	int lowIndex=int((1.-level)/2*length+0.5);
 	int highIndex=length-lowIndex-1;
 			
 	sort();
 				
 	for (int i=lowIndex;i<=highIndex;i++)
 		aux+=x[i]/(ex[i]*ex[i]);	 			
 	for (int i=lowIndex;i<=highIndex;i++)
 		aux2+=1/(ex[i]*ex[i]);		 	
 				
 	// note if e.g. level=0 -> lowIndex>highIndex -> return NAN				
 	return aux/aux2;
}

// calculate estimated <Y> = 1/n sum y_i
double xyErrorData::getMeanY() 
{ 			 			 			 			
 	double aux=0.;
 			
 	for (int i=0;i<length;i++)
 		aux+=y[i]; 		 			 		 				
 	return aux/length;
}
 		
// calculate estimated <X> = 1/n sum y_i
// data with a y-value above or below the (1-level)/2 percentile is clipped 		 	
double xyErrorData::getMeanY(double level) 
{ 			
 	double aux=0.;
 			
 	if (level>1)
 		level=1;
 	if (level<0)
 		level=0;
 	int lowIndex=int((1.-level)/2*length+0.5);
 	int highIndex=length-lowIndex-1;
 			
 	sortY();
 				
 	for (int i=lowIndex;i<=highIndex;i++)
 		aux+=y[i];	 			 	
 				
 	// note if e.g. level=0 -> lowIndex>highIndex -> return NAN				
 	return aux/(highIndex+1-lowIndex);
}
 		
// calculate estimated <Y> = sum_i(y_i/sigma^2_i)/sum_i(1/sigma^2_i) 		
double xyErrorData::getWeightedMeanY() 
{
 	double aux=0.;
 	double aux2=0.;
 			
 	for (int i=0;i<length;i++)
 		aux+=y[i]/(ey[i]*ey[i]); 		 			
 	for (int i=0;i<length;i++)
 		aux2+=1/(ey[i]*ey[i]); 				 				
 	return aux/aux2;
}
 		
// calculate estimated <Y> = sum_i(y_i/sigma^2_i)/sum_i(1/sigma^2_i) 		
// data with a y-value above or below the (1-level)/2 percentile is clipped
double xyErrorData::getWeightedMeanY(double level) 
{
 	double aux=0.;
 	double aux2=0.;
 			
 	if (level>1)
 		level=1;
 	if (level<0)
 		level=0;
 	int lowIndex=int((1.-level)/2*length+0.5);
 	int highIndex=length-lowIndex-1;
 			 			
 	sortY();
 			
 	for (int i=lowIndex;i<=highIndex;i++)
 		aux+=y[i]/(ey[i]*ey[i]); 		 			
 	for (int i=lowIndex;i<=highIndex;i++)
 		aux2+=1/(ey[i]*ey[i]); 				 				
 				
 	// note if e.g. level=0 -> lowIndex>highIndex -> return NAN
 	return aux/aux2;
}

// calculate median of y 
double xyErrorData::getMedianY() 
{ 			 			 			 			
 	double median=0.;
 	
 	if (length==0)
 	  return 0;
 	  
 	std::vector<double> y_vec(length);
 	for (int i=0;i<length;i++)
 	  y_vec[i]=y[i];
 	std::sort(y_vec.begin(),y_vec.end());
    if (length % 2 != 0) {
    	median =  *(y_vec.begin()+y_vec.size()/2);
    } else {
       	median = (*(y_vec.begin()+y_vec.size()/2-1) + *(y_vec.begin()+y_vec.size()/2))/2.;
    }
    return median;
}

// calculate median of y 
void xyErrorData::getPercY(double & median, double & lowPerc, double & highPerc, double lowPercLimit, double highPercLimit) 
{ 			 			 			 			
 	median=0.;
 	lowPerc=0.;
 	highPerc=0.;
 	
 	if (length==0)
 	  return;
 	  
 	std::vector<double> y_vec(length);
 	for (int i=0;i<length;i++)
 	  y_vec[i]=y[i];
 	std::sort(y_vec.begin(),y_vec.end());
    
    if (length % 2 != 0) {
    	median =  *(y_vec.begin()+y_vec.size()/2);
    } else {
       	median = (*(y_vec.begin()+y_vec.size()/2-1) + *(y_vec.begin()+y_vec.size()/2))/2.;
    }
    lowPerc=*(y_vec.begin()+int(y_vec.size()*lowPercLimit));
    highPerc=*(y_vec.begin()+int(y_vec.size()*highPercLimit));
        
    return;
}

// calculate sigma of y from clipping outliers at clippingLevel-sigma level, selfconsistently  
double xyErrorData::getClippedSigmaY(double clippingLevel) 
{ 	 
   if (length==0)
     return DBL_MAX;
 
   double median = getMedianY();     
    
   std::vector<double> y_new(length);
   
   bool haschanged=true;   
   int previous_lineIndex=0;
        
   int iteration=0; 	
   double currSigma=DBL_MAX;
      
   while (haschanged==true) {   	   	
   	 
   	 int lineIndex=0;
   	 if (iteration>0) {   	   	 
   	   for (int i=0;i<length;i++) {
         if ( fabs(y[i]-median) > clippingLevel*currSigma)
           continue;
         
         y_new[lineIndex]=y[i];
         lineIndex++;
   	   }
   	 } else {
   	   std::vector<double> y_tmp(length);
   	   // determine distance to median
   	   for (int i=0;i<length;i++) {
   	     y_tmp[i]=fabs(y[i]-median);   	     
   	   }
   	   // order w.r.t distance to median
   	   std::sort(y_tmp.begin(),y_tmp.end());
   	   // keep only the closest half   	   	  
   	   lineIndex=(int)(length/2)+1;
   	   // re-add median just to be able to substract it back later
   	   for (int i=0;i<lineIndex;i++) {
   	   	 y_new[i]=y_tmp[i]+median;
   	   }   	        	   	 
   	 }   	    	
   	 
   	 double aux=0;
   	 for (int i=0;i<lineIndex;i++)
 		aux+=(y_new[i]-median)*(y_new[i]-median);
 		
 	 if (lineIndex>1)	
 	   currSigma = sqrt(aux/(lineIndex-1));
 	 else
 	   currSigma = DBL_MAX;
 	 
 	 if (iteration > 1 && lineIndex==previous_lineIndex) {
 	 	haschanged=false;
 	 } 	  	
 	 
 	 previous_lineIndex=lineIndex;
 	 iteration++; 	  
   }    
   
   return currSigma;
}

// calculate sigma from data
// S(<X>)=sqrt(1/(n-1)sum_i(x_i-x_mean)^2) 		
double xyErrorData::getSigmaX() 
{ 
 	double aux=0.; 			
 	double meanX = this->getMeanX();
 			
 	for (int i=0;i<length;i++)
 		aux+=(x[i]-meanX)*(x[i]-meanX);
 	return sqrt(aux/length);	
}
 		
// calculate sigma from data
// S(<X>)=sqrt(1/(n-1)sum_i(x_i-x_mean)^2)
// data with a x-value above or below the (1-level)/2 percentile is clipped 		
double xyErrorData::getSigmaX(double level) 
{  			
 	if (level>1)
 		level=1;
 	if (level<0)
 		level=0;
 	int lowIndex=int((1.-level)/2*length+0.5);
 	int highIndex=length-lowIndex-1; 	
 			
 	sort();
 			
 	double aux=0.; 			
 	double meanX = this->getMeanX(level);
 			
 	for (int i=lowIndex;i<=highIndex;i++)
 		aux+=(x[i]-meanX)*(x[i]-meanX);
 	return sqrt(aux/(highIndex+1-lowIndex));			
} 	

// calculated sigma of weighted average
// S(<X>)=sqrt(1/sum_i(1/sigma(x_i)^2))
double xyErrorData::getWeightedSigmaX() 
{ 
 	double aux=0.; 			
 		
 	for (int i=0;i<length;i++)
 		aux+=1/(ex[i]*ex[i]); 			 				
 	return sqrt(1/aux);		 			 			
}
 		
// calculated sigma of weighted average
// S(<X>)=sqrt(1/sum_i(1/sigma(x_i)^2))
// data with a x-value above or below the (1-level)/2 percentile is clipped 		
double xyErrorData::getWeightedSigmaX(double level) 
{ 
 			
 	if (level>1)
 		level=1;
 	if (level<0)
 		level=0;
 	int lowIndex=int((1.-level)/2*length+0.5);
 	int highIndex=length-lowIndex-1;
 			
 	sort();
 			
 	double aux=0.; 			
 			
 	for (int i=lowIndex;i<=highIndex;i++)
 		aux+=1/(ex[i]*ex[i]); 			 				
 		return sqrt(1/aux);		 			 			
}
 	
// calculate sigma from data
// S(<Y>) = sqrt(1/(n-1)sum_i(y_i-y_mean)^2) 		
double xyErrorData::getSigmaY() 
{ 
 	double aux=0.; 			
 	double meanY = this->getMeanY();
 			
 	for (int i=0;i<length;i++)
 		aux+=(y[i]-meanY)*(y[i]-meanY);
 	return sqrt(aux/length);	
}
 		
// calculate sigma from data
// S(<Y>) = sqrt(1/(n-1)sum_i(y_i-y_mean)^2)
// data with a y-value above or below the (1-level)/2 percentile is clipped 		
double xyErrorData::getSigmaY(double level) 
{  			
 	if (level>1)
 		level=1;
 	if (level<0)
 		level=0;
 	int lowIndex=int((1.-level)/2*length+0.5);
 	int highIndex=length-lowIndex-1; 	
 			
 	sortY();
 			
 	double aux=0.; 			
 	double meanY = this->getMeanY(level);
 			
 	for (int i=lowIndex;i<=highIndex;i++)
 		aux+=(y[i]-meanY)*(y[i]-meanY);
 	return sqrt(aux/(highIndex+1-lowIndex));			
} 	
 		
// calculated sigma of weighted average
// S(<Y>)=sqrt(1/sum_i(1/sigma(y_i)^2)) 		
double xyErrorData::getWeightedSigmaY() 
{ 
 	double aux=0.; 			
 			
 	for (int i=0;i<length;i++)
 		aux+=1/(ey[i]*ey[i]); 			 				
 	return sqrt(1/aux);	
}
 		
// calculated sigma of weighted average
// S(<X>)=sqrt(1/sum_i(1/sigma(x_i)^2))
// data with a y-value above or below the (1-level)/2 percentile is clipped 		
double xyErrorData::getWeightedSigmaY(double level) 
{ 
 			
 	if (level>1)
 		level=1;
 	if (level<0)
 		level=0;
 	int lowIndex=int((1.-level)/2*length+0.5);
 	int highIndex=length-lowIndex-1;
 			
 	sortY();
 			
 	double aux=0.; 			
 			
 	for (int i=lowIndex;i<=highIndex;i++)
 		aux+=1/(ey[i]*ey[i]); 			 				
 	return sqrt(1/aux);		 			 			
}

void xyErrorData::print() {		
	cout << "# length=" << length << endl;
	for (int i=0;i<this->length;i++) {		
		cout << this->x[i] << " " << this->y[i] << " " 
			 << this->ex[i] << " " << this->ey[i] << endl;
	}			
}

void xyErrorData::write(const char* filename)
{
	ofstream ofile;
	
	ofile.open(filename);
	if (! ofile.good()) {
		cerr << "Could not open output file " << filename << endl;
	}
	ofile << "# length=" << length << endl;
	for (int i=0;i<this->length;i++) {		
		ofile << this->x[i] << " " << this->y[i] << " " << this->ex[i] << " " << this->ey[i] << endl;
	}
	ofile.close();
	ofile.clear();
}

void xyErrorData::append(const char* filename)
{
	ofstream ofile;
	
	ofile.open(filename,ios::app);
	if (! ofile.good()) {
		cerr << "Could not open output file " << filename << endl;
	}
	ofile << "# length=" << length << endl;
	for (int i=0;i<this->length;i++) {		
		ofile << this->x[i] << " " << this->y[i] << " " << this->ex[i] << " " << this->ey[i] << endl;
	}
	ofile.close();
	ofile.clear();
}

// allows to append only certain columns e.g. append(filename,0,0,1,1) only appends errors
void xyErrorData::append(const char* filename, int printX, int printY, int printEX, int printEY)
{
	ofstream ofile;
	
	ofile.open(filename,ios::app);
	if (! ofile.good()) {
		cerr << "Could not open output file " << filename << endl;
	}
	ofile << "# length=" << length << endl;
	for (int i=0;i<this->length;i++) {		
		if (printX)
			ofile << this->x[i] << " "; 
		if (printY)
			ofile << this->y[i] << " "; 
		if (printEX)	
			ofile << this->ex[i] << " ";
		if (printEY)
			ofile << this->ey[i];
		if (printX || printY || printEX || printEY)
			ofile << endl;
	}
	ofile.close();
	ofile.clear();		
}	


/* class RegressionData
 */
 
// do interpolation (using the adjusted options) for the binnedData
// the result is returned as correctionData
void RegressionData::interpolation(xyErrorData * correctionData) 
{
	double *aux_y;
	double aux;
	int minIndex;
	int maxIndex;
	int flag;	
	
	// minIndex is the smallest index of correctionData used for interpolation
	minIndex=correctionData->getIndex(binnedData->getX()[0],flag);			
		if (! flag)
		minIndex++;
	// maxIndex is the largest index of correctionData used for interpolation						
	maxIndex=correctionData->getIndex(binnedData->getX()[binnedData->getlength()-1]);							
			
	allocArray1(aux_y,correctionData->getlength());		
		
	// interpolation		
	if (minIndex<=maxIndex) {		
		switch (interMode) {				
			case 0:	// take mean of all bins (constant line)
					aux=binnedData->getWeightedMeanY();
					for (int i=minIndex;i<=maxIndex;i++)
						aux_y[i]=aux;
					break;
			case 1: // linear interpolation					
					binnedData->setApproxCurveType(0);
					binnedData->calcYValues(maxIndex-minIndex+1,
									correctionData->getX()+minIndex,aux_y+minIndex);
					break;																				
			case 2: // spline interpolation		
					binnedData->setApproxCurveType(1);								
					binnedData->calcYValues(maxIndex-minIndex+1,
									correctionData->getX()+minIndex,aux_y+minIndex);
					break;																
		}	
	}								
	// extrapolation	
	switch (extraMode) {				
		case 0:	// take mean of all bins (constant line)
				aux=binnedData->getWeightedMeanY();
				for (int i=0;i<minIndex;i++)
					aux_y[i]=aux;
				for (int i=maxIndex+1;i<correctionData->getlength();i++)
					aux_y[i]=aux;
				break;
		case 1: // linear extrapolation					
				binnedData->setApproxCurveType(0);
				if (minIndex>0)
					binnedData->calcYValues(minIndex,
										correctionData->getX(),aux_y);
				if (maxIndex+1<correctionData->getlength())
					binnedData->calcYValues(correctionData->getlength()-maxIndex-1,
										correctionData->getX()+maxIndex+1,aux_y+maxIndex+1);						
				break;												
		case 2: // spline extrapolation
				binnedData->setApproxCurveType(1);
				if (minIndex>0)
					binnedData->calcYValues(minIndex,
								correctionData->getX(),aux_y);
				if (maxIndex+1<correctionData->getlength())
					binnedData->calcYValues(correctionData->getlength()-maxIndex-1,
								correctionData->getX()+maxIndex+1,aux_y+maxIndex+1);						
				break;																
	}									
	correctionData->setY(aux_y);
	correctionData->setEX(0.);
	correctionData->setEY(0.);
	freeArray1(aux_y);		
	
}

/* Regression: Note, only y-error is used
 */

void RegressionData::regression(xyErrorData * data, xyErrorData * correctionData,gsl_vector * vC,
								double &c0,double &c1,gsl_matrix * mCov,
								double &cov00, double &cov01, double &cov11,
								double & chisq)
{
	double *aux_y;
	double *aux_ey;
	int minIndex;
	int maxIndex;
	int flag;	
	double extra_chisq;
	double prod_x;
	double xi;
	double *w;	
	double y_val;
	double y_err;
	
	// input: x,y,weight
	gsl_matrix *mX=NULL;
 	gsl_vector *vY=NULL;
 	gsl_vector *vW=NULL;
 	// input: positions for fit curve estimate
 	gsl_matrix *mX_est=NULL;
 	// parameter and covarianc
 	gsl_vector *extra_vC=NULL;
 	gsl_matrix *extra_mCov=NULL;
 	// output (estimated y and error) 	
 	gsl_vector *vY_est=NULL;
 	gsl_vector *vY_err=NULL; 	 	
	
	// minIndex is the smallest index of correctionData used for interpolation
	minIndex=correctionData->getIndex(minValue,flag);			
		if (! flag)
		minIndex++;
	if (minIndex<0)
		minIndex=0;
		
	// maxIndex is the largest index of correctionData used for interpolation						
	maxIndex=correctionData->getIndex(maxValue);						
	if (maxIndex>correctionData->getlength()-1)		
		maxIndex=correctionData->getlength()-1;
		
	allocArray1(aux_y,correctionData->getlength());		
	allocArray1(aux_ey,correctionData->getlength());		
	
	allocArray1(w,data->getlength());	
	double *auxp = data->getErrorY();
	for (int i=0;i<data->getlength();i++) {		
		w[i]=1.0/((*auxp)*(*auxp));
		auxp++;
	}
	
	// interpolation
	if (minIndex<=maxIndex) {		
		switch (interMode) {				
			case 0:	// constant regression								
					fit_wconst(w , 1 ,data->getY() ,1 ,data->getlength() , &c0 , &cov00 , &chisq);			
					for (int i=minIndex;i<=maxIndex;i++) {
						fit_const_est(c0,cov00,&y_val,&y_err);					
						aux_y[i]=y_val;
						aux_ey[i]=y_err;
					}					
					break;
			case 1: // linear regression
					gsl_fit_wlinear (data->getX(),1,w,1,data->getY(),1,data->getlength(),&c0,&c1,&cov00,&cov01,&cov11,&chisq);					
					auxp=correctionData->getX()+minIndex;
					for (int i=minIndex;i<=maxIndex;i++) {
						gsl_fit_linear_est (*auxp,c0,c1,cov00,cov01,cov11,&y_val,&y_err);			
						aux_y[i]=y_val;
						aux_ey[i]=y_err;
						auxp++;
					}
					break;
			default: // higher order regression
			 		mX = gsl_matrix_alloc (data->getlength(), interMode+1);
			 		vW = gsl_vector_alloc (data->getlength());
					vY = gsl_vector_alloc (data->getlength());								
  					  					  				  					
					for (int i = 0; i < data->getlength(); i++) {     					
						prod_x=1;
						xi=data->getX()[i];
           				for (int j=0; j<interMode+1;j++) {           				           					
      						gsl_matrix_set (mX, i, j, prod_x);
							prod_x*=xi;
           				}           				
      					gsl_vector_set (vY, i, data->getY()[i]);
						gsl_vector_set (vW, i, w[i]);
					}
      				{
    					gsl_multifit_linear_workspace * work = gsl_multifit_linear_alloc (data->getlength(), interMode+1);
    					gsl_multifit_wlinear (mX, vW, vY, vC, mCov, &chisq, work);
    					gsl_multifit_linear_free (work);
    				}
    				gsl_vector_free (vY);				
					gsl_vector_free (vW);	
					gsl_matrix_free (mX);
					
    				vY_est = gsl_vector_alloc (maxIndex+1-minIndex); 
					vY_err = gsl_vector_alloc (maxIndex+1-minIndex);  		
					
    				mX_est=gsl_matrix_alloc (maxIndex+1-minIndex,interMode+1);
    				for (int i = minIndex; i <= maxIndex; i++) {     					
						prod_x=1;
						xi=correctionData->getX()[i];
           				for (int j=0; j<interMode+1;j++) {           				           					
      						gsl_matrix_set (mX_est, i-minIndex, j, prod_x);
							prod_x*=xi;
           				}           				      					
					}
    				multifit_linear_est (mX_est, vC, mCov, vY_est, vY_err);
    				gsl_matrix_free(mX_est);    				
    				
    				for (int i=minIndex;i<=maxIndex;i++) {
						aux_y[i] = gsl_vector_get (vY_est, i-minIndex);
						aux_ey[i]= gsl_vector_get (vY_err, i-minIndex);
					}
					gsl_vector_free (vY_est);
					gsl_vector_free (vY_err);									
						
		}			    				
    }					  													
	// extrapolation	
	switch (extraMode) {				
		case 0:	// constant regression
				if (interMode!=extraMode)
					fit_wconst(w,1,data->getY(),1,data->getlength(),&c0,&cov00,&chisq);		
				for (int i=0;i<minIndex;i++) {		
					fit_const_est(c0,cov00,&y_val,&y_err);					
					aux_y[i]=y_val;
					aux_ey[i]=y_err;
				}
				for (int i=maxIndex+1;i<correctionData->getlength();i++) {
					fit_const_est(c0,cov00,&y_val,&y_err);				
					aux_y[i]=y_val;
					aux_ey[i]=y_err;
				}											
				break;
		case 1: // linear regression
				if (interMode!=extraMode)
					gsl_fit_wlinear (data->getX(),1,w,1,data->getY(),1,data->getlength(),&c0,&c1,&cov00,&cov01,&cov11,&chisq);
				auxp=correctionData->getX();
				for (int i=0;i<minIndex;i++) {	
					gsl_fit_linear_est (*auxp,c0,c1,cov00,cov01,cov11,&y_val,&y_err);			
					aux_y[i]=y_val;
					aux_ey[i]=y_err;
					auxp++;
				}
				auxp=correctionData->getX()+maxIndex+1;
				for (int i=maxIndex+1;i<correctionData->getlength();i++) {
					gsl_fit_linear_est (*auxp,c0,c1,cov00,cov01,cov11,&y_val,&y_err);			
					aux_y[i]=y_val;
					aux_ey[i]=y_err;
					auxp++;
				}
				break;
		default: // higher order regression													
				
				if (minIndex==0 && maxIndex==correctionData->getlength()-1)
					break;
						
				if (interMode!=extraMode) {
					mX = gsl_matrix_alloc (data->getlength(), extraMode+1);
					vY = gsl_vector_alloc (data->getlength());
					vW = gsl_vector_alloc (data->getlength());										
					extra_mCov = gsl_matrix_alloc (extraMode+1, extraMode+1);
					extra_vC = gsl_vector_alloc(extraMode+1);														
					  					  					  					
					for (int i = 0; i < data->getlength(); i++) {     					
						prod_x=1;
						xi=data->getX()[i];
           				for (int j=0; j<extraMode+1;j++) {           				           					
      						gsl_matrix_set (mX, i, j, prod_x);
							prod_x*=xi;
           				}           				
      					gsl_vector_set (vY, i, data->getY()[i]);
						gsl_vector_set (vW, i, w[i]);
					}
      				{
    					gsl_multifit_linear_workspace * work = gsl_multifit_linear_alloc (data->getlength(), extraMode+1);
    					gsl_multifit_wlinear (mX, vW, vY, extra_vC, extra_mCov, &extra_chisq, work);
    					gsl_multifit_linear_free (work);    					    					
    				}
    				
    				gsl_vector_free (vY);	
					gsl_vector_free (vW);	
					gsl_matrix_free (mX);
						
				} else {
					
					extra_mCov=mCov;
					extra_vC=vC;							    			
    			
				}
    			
    			// left side
    			if (minIndex>0) {
    				
	    			vY_est = gsl_vector_alloc (minIndex);
					vY_err = gsl_vector_alloc (minIndex);  
					
					mX_est=gsl_matrix_alloc (minIndex,extraMode+1);
    				for (int i = 0; i < minIndex; i++) {     					
						prod_x=1;
						xi=correctionData->getX()[i];
           				for (int j=0; j<extraMode+1;j++) {           				           					
      						gsl_matrix_set (mX_est, i, j, prod_x);
							prod_x*=xi;
	           			}           				      					
					}
    				
    				multifit_linear_est (mX_est, extra_vC, extra_mCov, vY_est, vY_err);
    				gsl_matrix_free(mX_est);    	
    			    				    							    				    				
	    			for (int i=0;i<minIndex;i++) {					
						aux_y[i] = gsl_vector_get (vY_est, i);
						aux_ey[i]= gsl_vector_get (vY_err, i);
					}
					
					gsl_vector_free (vY_est);
					gsl_vector_free (vY_err);
    			}
    				    			
    			// right side
    			if (maxIndex<correctionData->getlength()-1) {    			
    			
    				vY_est = gsl_vector_alloc (correctionData->getlength()-1-maxIndex);
					vY_err = gsl_vector_alloc (correctionData->getlength()-1-maxIndex);  
					
					mX_est=gsl_matrix_alloc (correctionData->getlength()-1-maxIndex,extraMode+1);
    				for (int i = maxIndex+1; i < correctionData->getlength(); i++) {     					
						prod_x=1;
						xi=correctionData->getX()[i];
           				for (int j=0; j<extraMode+1;j++) {           				           					
      						gsl_matrix_set (mX_est, i-maxIndex-1, j, prod_x);
							prod_x*=xi;
	           			}           				      					
					}
    				multifit_linear_est (mX_est, extra_vC, extra_mCov, vY_est, vY_err);    				
	    			gsl_matrix_free(mX_est);    				    		
    						    				
    				for (int i=maxIndex+1;i<correctionData->getlength();i++) {						
						aux_y[i] = gsl_vector_get (vY_est, i-maxIndex-1);
						aux_ey[i]= gsl_vector_get (vY_err, i-maxIndex-1);
					}
					
					gsl_vector_free (vY_est);
					gsl_vector_free (vY_err);								
				
    			}	
    									    								
				if (interMode!=extraMode) {
					gsl_matrix_free (extra_mCov);
					gsl_vector_free (extra_vC);								
				}					
	}								

	correctionData->setY(aux_y);
	correctionData->setEX(0.0);
	correctionData->setEY(aux_ey);
	freeArray1(aux_y);
	freeArray1(aux_ey);		
	freeArray1(w);
}

// allocates bins					
void RegressionData::createBins() {
	double delta;
			
	freeArray1(this->leftBorder);
	this->leftBorder=NULL;
			
	if (minValue>=maxValue) {
		this->numBins=0;						
	} else {
		allocArray1(this->leftBorder,numBins+1);			
		delta=(maxValue-minValue)/numBins;
		for (int i=0;i<=numBins;i++)
			leftBorder[i]=minValue+i*delta;
	}
						
	allocArray1(bins,numBins);
	for (int i=0;i<numBins;i++) {
		bins[i] = new xyErrorData();
	}																
}
		
// redirect *x and *y data into respective bins
void RegressionData::fillBins() {			
			
	int *numBinEntries;
	double **xBins;
	double **yBins;
	double **exBins;
	double **eyBins;
	callocArray1(numBinEntries,numBins);
	allocArray1(xBins,numBins);
	allocArray1(yBins,numBins);
	allocArray1(exBins,numBins);
	allocArray1(eyBins,numBins);
			
	// get number of entries in each of the bins
	for (int i=0;i<length;i++) {
		for (int b=0;b<numBins;b++) {
			if (x[i]>=leftBorder[b] && x[i]<leftBorder[b+1]) {						
				numBinEntries[b]++;
				break;			
			}
		}
	}								
	for (int b=0;b<numBins;b++) {
		if (numBinEntries[b]>0) {
			allocArray1(xBins[b],numBinEntries[b]);
			allocArray1(yBins[b],numBinEntries[b]);
			allocArray1(exBins[b],numBinEntries[b]);
			allocArray1(eyBins[b],numBinEntries[b]);
		} else {
			xBins[b]=NULL;
			yBins[b]=NULL;
			exBins[b]=NULL;
			eyBins[b]=NULL;
		}										
	}
			
	// fill bins
	for (int b=0;b<numBins;b++) {
		numBinEntries[b]=0;
	}
			
	for (int i=0;i<length;i++) {
		for (int b=0;b<numBins;b++) {
			if (x[i]>=leftBorder[b] && x[i]<leftBorder[b+1]) {						
				xBins[b][numBinEntries[b]]=x[i];
				yBins[b][numBinEntries[b]]=y[i];
				exBins[b][numBinEntries[b]]=ex[i];
				eyBins[b][numBinEntries[b]]=ey[i];
				numBinEntries[b]++;	
				break;		
			}
		}
	}
			
	for (int b=0;b<numBins;b++) {
		bins[b]->set(numBinEntries[b],xBins[b],yBins[b],exBins[b],eyBins[b]);
	}
			
	for (int b=0;b<numBins;b++) {
		freeArray1(xBins[b]);
		freeArray1(yBins[b]);
		freeArray1(exBins[b]);
		freeArray1(eyBins[b]);
	}
	freeArray1(xBins);
	freeArray1(yBins);
	freeArray1(exBins);
	freeArray1(eyBins);
	freeArray1(numBinEntries);
}

// selects (x,y,ex,ey) tupel from this according to minValue & maxValue limits		
int RegressionData::selectEntries() {
	
	double *xSel;
	double *ySel;
	double *exSel;
	double *eySel;
	int numSel;

	// get number of selected entries
	numSel=0;
	for (int i=0;i<length;i++) {
		if (x[i]>=minValue && x[i]<maxValue) {						
			numSel++;		
		}
	}		
			
	allocArray1(xSel,numSel);
	allocArray1(ySel,numSel);
	allocArray1(exSel,numSel);
	allocArray1(eySel,numSel);
	
	numSel=0;
	for (int i=0;i<length;i++) {		
		if (x[i]>=minValue && x[i]<maxValue) {						
			xSel[numSel]=x[i];
			ySel[numSel]=y[i];
			exSel[numSel]=ex[i];
			eySel[numSel]=ey[i];
			numSel++;
		}
	}
	
	if (binnedData)
		delete binnedData;
	binnedData = new xyErrorData(numSel,xSel,ySel,exSel,eySel);
	
	freeArray1(xSel);
	freeArray1(ySel);
	freeArray1(exSel);
	freeArray1(eySel);
	
	return numSel;
}		
		
// create binnedData object (from the individual bins)
int RegressionData::createBinnedData() {
			
	double *xBins;
	double *yBins;
	double *exBins;
	double *eyBins;
	int usableBins; // number of bins which contain at least minimumBinEntries entries	
			
	allocArray1(xBins,numBins);
	allocArray1(yBins,numBins);
	allocArray1(exBins,numBins);
	allocArray1(eyBins,numBins);			
	
	usableBins=0;		
	for (int b=0;b<numBins;b++) {		
		if (bins[b]->getlength()>=minimumBinEntries) {
			xBins[usableBins]=bins[b]->getMeanX(levelX);
			// don't care about error in x -> extension for future
			// exBins[numNonZeroBins]=bins[b]->getSigmaX(levelX);
			exBins[usableBins]=0.; 
									
			if (weightedY || bins[b]->getlength()<=weightedYLimit) {
				yBins[usableBins]=bins[b]->getWeightedMeanY(levelY);
				eyBins[usableBins]=bins[b]->getWeightedSigmaY(levelY);
			} else if (levelY<=0) {  // use medians and 3-sigma clipped standard deviations
				// old				
				///double median, lowPerc, highPerc;
				//bins[b]->getPercY(median,lowPerc,highPerc,0.159,0.841);
			    //yBins[usableBins]=median;
				//eyBins[usableBins]=((highPerc-median)+(median-lowPerc))/2.;
				
				// new
			    yBins[usableBins]=bins[b]->getMedianY();
				eyBins[usableBins]=bins[b]->getClippedSigmaY(3);
														
			} else {                 // use means and sigmas but clip
				yBins[usableBins]=bins[b]->getMeanY(levelY);
				eyBins[usableBins]=bins[b]->getSigmaY(levelY);
			}
			usableBins++;
		}
	}				
	
	if (binnedData)
		delete binnedData;											
	binnedData = new xyErrorData(usableBins,xBins,yBins,exBins,eyBins);
			
	freeArray1(xBins);
	freeArray1(yBins);
	freeArray1(exBins);
	freeArray1(eyBins);
	
	return usableBins;	
}

void RegressionData::getRegression(xyErrorData * correctionData, int n, 
								   double *param, double **cov, double & chisq)
{			
	gsl_matrix *inter_mCov;
 	gsl_vector *inter_vC;
 	
 	double c0,c1;
 	double cov00,cov01,cov11;


    if (interMode<0) {
      WARNING( "interMode < 0 is a yet untested feature. Be careful!!" );      
    }
 	
 	if (interMode>1) {
	 	inter_vC = gsl_vector_alloc (interMode+1);
  		inter_mCov = gsl_matrix_alloc (interMode+1, interMode+1);
 	} else {
 		inter_vC = NULL;
 		inter_mCov = NULL; 		 		
 	} 
								
	reset();
	
	if (n>abs(interMode+1))
		n=abs(interMode+1);
	
	if (interMode<0) {					
		createBins();
		fillBins();
		createBinnedData();
		interpolation(correctionData);		
		for (int i=0;i<n;i++) {
			param[i]=0;
			for (int j=0;j<n;j++) {
				cov[i][j]=0;
			}
		}
		chisq=0;
	
	} else {	
		
		if (numBins>0) {
			createBins();
			fillBins();
			int usableBins=createBinnedData();
			if (usableBins>interMode) { 
			  regression(binnedData,correctionData,inter_vC,c0,c1,inter_mCov,cov00,cov01,cov11,chisq);
			} else {			  
			  c0=0;
			  c1=1;
			  cov00=1;
			  cov01=0;
			  cov11=1;
			  chisq=0;	
			}						
		} else {
			int usableEntries=selectEntries();
			if (usableEntries>interMode) {
			  regression(binnedData,correctionData,inter_vC,c0,c1,inter_mCov,cov00,cov01,cov11,chisq);
			} else {
			  c0=0;
			  c1=1;
			  cov00=1;
			  cov01=0;
			  cov11=1;
			  chisq=0;				
			}												
		}
		
		if (interMode<=1) {
			param[0]=c0;
			cov[0][0]=cov00;
			if (interMode==1) {
				param[1]=c1;
				cov[0][1]=cov[1][0]=cov01;
				cov[1][1]=cov11;
			}
		} else {
			for (int i=0;i<n;i++) {
				param[i]=gsl_vector_get (inter_vC,i);
				for (int j=0;j<n;j++) {
					cov[i][j]=gsl_matrix_get(inter_mCov, i, j);
				}
			}
		}			
	}			
	
	if (interMode>1) {	
		gsl_vector_free (inter_vC);
		gsl_matrix_free (inter_mCov);		
	}
															
}		
		
/* class ErrorData
 */

ErrorData::ErrorData(const char* name)
{
	strcpy(this->name,name);
	this->value=NULL;
	this->error=NULL;
	this->invalid=NULL;
	writeWindowLow=0;
	writeWindowHigh=0;
}

ErrorData::~ErrorData()
{
	freeArray1(this->value);
	freeArray1(this->error);	
	freeArray1(this->invalid);
}

ErrorData* ErrorData::operator + (const double val)
{		
	for (int i=0;i<this->length;i++)
		value[i]+=val;	
	return this;
}

ErrorData* ErrorData::operator - (const double val)
{		
	for (int i=0;i<this->length;i++)
		value[i]-=val;	
	return this;
}

void ErrorData::set(int length,const double *values,const double* errors)
{
	
	freeArray1(this->value);
	freeArray1(this->error);
	freeArray1(this->invalid);
	allocArray1(this->value,length);
	allocArray1(this->error,length);
	allocArray1(this->invalid,length);
	this->length=length;
	this->writeWindowLow=0;
	this->writeWindowHigh=length;
	
	setValueAndError(values,errors);
}

void ErrorData::set(int length)
{
	freeArray1(this->value);
	freeArray1(this->error);
	freeArray1(this->invalid);
	allocArray1(this->value,length);
	allocArray1(this->error,length);	
	callocArray1(this->invalid,length);
	this->length=length;
	this->writeWindowLow=0;
	this->writeWindowHigh=length;
}

void ErrorData::setInvalid(char *invalid)
{	
	for (int i=0;i<writeWindowHigh-writeWindowLow;i++)
		this->invalid[i+writeWindowLow]=invalid[i];
}

void ErrorData::setValueAndError(const double* values,const double* errors) 
{			
	for (int i=0;i<writeWindowHigh-writeWindowLow;i++)
		this->value[i+writeWindowLow]=values[i];
	for (int i=0;i<writeWindowHigh-writeWindowLow;i++)
		this->error[i+writeWindowLow]=errors[i];	
}

void ErrorData::print()
{
	cout << name << ": Value Error Invalid" << endl;
	cout << "-------------------------------" << endl;
	for (int i=0;i<length;i++) 
		cout << value[i] << " " << error[i] << " " << int(invalid[i]) << endl;	
}

void ErrorDataOrigError::set(int length, const double* values,const double* errors, const double *orig_errors)
{
	ErrorData::set(length,values,errors);
	freeArray1(this->orig_error);	
	allocArray1(this->orig_error,length);
	setOrigError(orig_errors);		
}

void ErrorDataOrigError::setOrigError(const double *orig_errors) 
{		
	// contrary to *value and *error the pointer *orig_error is only allocated
	// if explicitely used to avoid waste of memory
	if (! this->orig_error)
		allocArray1(this->orig_error,this->length);
			
	for (int i=0;i<writeWindowHigh-writeWindowLow;i++)
		this->orig_error[i+writeWindowLow]=orig_errors[i];		
}

/* class FilterBand
 */

FilterBand::FilterBand(const char* name,int colID, int errorColID) : 
			ErrorDataOrigError(name)
{
	this->colID=colID;
	this->errorColID=errorColID;
	histAna=NULL;	
}

FilterBand::~FilterBand()
{
	if (histAna) 
		delete histAna;
}

void FilterBand::magToFlux() 
{			
	for (int i=0;i<length;i++) {
		value[i]=g_magToFlux(value[i]);
		error[i]=g_magToFluxError(error[i])*value[i];		
	}				
}

void FilterBand::fluxToMag()
{
	for (int i=0;i<length;i++) {
		if (value[i]<=0) {
			if (value[i]==0) {
				error[i]=-99;
				value[i]=99;
			} else {	
				error[i]=-99;
				value[i]=-99;
			}
		} else {
			error[i]=g_fluxToMagError(error[i]/value[i]);
			value[i]=g_fluxToMag(value[i]);			
		}
	}				
}

void FilterBand::errorAnalysis(int xBins, int xEntriesNr,
							   int errorBins, int errorEntriesNr)
{
	
	if (histAna) 
		delete histAna;
	histAna = new HistogramAnalysis();
	histAna->set(this->getLength(),this->getValue(),
				 this->getError(), 
				 xBins, xEntriesNr,
				 errorBins, errorEntriesNr);						  			
}

void FilterBand::writeErrorAnalysis(const char* filebase) {
	char filename[MAX_LENGTH];
	if (histAna) {
		sprintf(filename,"%s_%s",filebase,this->getName());
		histAna->write(filename);
	}
}

void FilterBand::readError(char *filebase) {
	char filename[MAX_LENGTH];
	if (histAna)
		delete histAna;
	histAna = new HistogramAnalysis();
	sprintf(filename,"%s_%s",filebase,this->getName());	
	if (histAna->read(filename)) 
		cout << "Cannot read error data for filter " << this->getName() << endl;
}

void FilterBand::multiplyError(double factor)
{
	double *error;
	error=this->getError();
	for (int i=0;i<this->length;i++)
		error[i]*=factor;
}

void FilterBand::floorError(double minError)
{
	double *error;
	double *value;
	double aux;
	
	error=this->getError();	// absolute error
	value=this->getValue();
	for (int i=0;i<this->length;i++) {
		aux=minError*value[i];
		if (error[i]<aux)
			error[i]=aux;
	}
}

void FilterBand::reshuffleGaussian() {	
	double *error;
	double *value;
	
	error=this->getError();	// absolute error
	value=this->getValue();
	
	for (int i=0;i<this->length;i++) {
		gaussian(value[i],value[i],error[i]);		
		if (value[i]<0) 
			value[i]=0.;
	}
}


void FilterBand::reshuffleGaussian(double error) {	
	double *value;
	
	value=this->getValue();
	for (int i=0;i<this->length;i++) {
		gaussian(value[i],value[i],error*value[i]);		
		if (value[i]<0) 
			value[i]=0.;
	}
}


void FilterBand::randomize(int errorMode, double minError) {	
	double *value,*error;
	int result=1;
	
	value=this->getValue();
	error=this->getError();	
	for (int i=0;i<this->getLength();i++) {
		result=histAna->randomize(value[i],error[i],errorMode,minError);
		if (result) break;
	}
	if (result)	
		cout << "Cannot randomize filter " << this->getName() << endl;
}

/* class HistogramAnalysis
 */
HistogramAnalysis::HistogramAnalysis()
{
	this->histogramX=NULL;
	this->histogramsError=NULL;
	this->missGaussData=0;
	this->missHistData=0;
	this->loaded=0;
}

HistogramAnalysis::~HistogramAnalysis()
{
	if (histogramX) {	
		if (histogramsError) {
			for (int i=0;i<this->histogramX->getBins();i++) 
				if (histogramsError[i])
					delete histogramsError[i];
			freeArray1(histogramsError);
		}
		delete histogramX;
	}
}

void HistogramAnalysis::set(int length, const double* values, 
					   const double *errors, 
					   int xBins, int xEntriesNr,									 
					   int errorBins, int errorEntriesNr)
{
	this->values=values;
	this->errors=errors;
	this->length=length;	
	this->xBins=xBins;
	this->xEntriesNr=xEntriesNr;
	this->errorBins=errorBins;
	this->errorEntriesNr=errorEntriesNr;	
	this->missGaussData=0;
	this->missHistData=0;
	
	this->makeHistograms();
	this->loaded=1;	
}

// errorMode =0   ..make analysis using histogram data
// errorMode =1   ..make analysis use only mean and sigma calculated from histograms
// errorMode =2	  ..same as errorMode=1 but does not change value
int HistogramAnalysis::randomize(double &x, double &error, int errorMode, double minError)
{
	int b;
	// give the binnumber b in histogramX corresponding to x
	if (loaded)
		b=histogramX->getBin(x);
	else 
		return 1;		
	
	// now obtain modified x and error values
	if (errorMode!=0 && missGaussData==0) {
		if (errorMode==1)		
			histogramsError[b]->gaussianError(x,error,minError);
		else
			histogramsError[b]->gaussianErrorOnly(x,error,minError);
	} else if (errorMode==0 && missHistData==0)
		histogramsError[b]->histogramError(x,error,minError);
	else 		
		return 1;
	
	return 0;
}

void HistogramAnalysis::makeHistograms()
{	
	xyData *data = new xyData(length, values, errors);
	double *xsorted,*ysorted;
	int currEntriesNr,prevEntriesNr;
	int posIndex;
	
	data->sort();	
	allocArray1(xsorted,data->getlength());
	allocArray1(ysorted,data->getlength());	
	data->getCopyX(xsorted);
	data->getCopyY(ysorted);
	// throw away x-values <= 0 (corresponds to +-99 mag in catalog)
	posIndex=0;
	while (xsorted[posIndex]<=0) {posIndex++;}			
	
	//for (int i=posIndex;i<length;i++)
	//	cout << "x=" << xsorted[i] << " error=" << ysorted[i] << endl;
	
	// create histogram for x-values	
	histogramX = new AdaptiveHistogram();
	histogramX->fill(this->length-posIndex,xsorted+posIndex,
					 this->xBins,this->xEntriesNr);
   							
	// create for each bin of the x-values a histogram of the error-values
	allocArray1(histogramsError,histogramX->getBins());	
	prevEntriesNr=0;
	for (int i=0;i<histogramX->getBins();i++) {
		currEntriesNr=histogramX->getBinEntries()[i];
		histogramsError[i] = new AdaptiveHistogram();
		histogramsError[i]->fill(currEntriesNr,
								ysorted+posIndex+prevEntriesNr,
								errorBins,errorEntriesNr);
		prevEntriesNr+=currEntriesNr;
	}


	delete data;
	freeArray1(xsorted);
	freeArray1(ysorted);
}

void HistogramAnalysis::write(char *basename)
{
	char filename[MAX_LENGTH];
	char **text=NULL;
	if (! this->loaded) 
		return;
		
	if (histogramX) {		
		allocArray2(text,histogramX->getBins()+1,MAX_LENGTH);
		strcpy(text[0]," mean(error) sigma(error)");
		if (histogramsError) {		
			for (int i=0;i<this->histogramX->getBins();i++) {
				if (histogramsError[i]) {
					sprintf(filename,"%s.e%d.hist",basename,i);
					histogramsError[i]->write(filename);
					sprintf(text[i+1],"%g",histogramsError[i]->getMean());
					sprintf(text[i+1],"%s %g",text[i+1],
							histogramsError[i]->getSigma());
				}
			}
		}		
		sprintf(filename,"%s.x.hist",basename);
		histogramX->write(filename,text);
	}
	freeArray2(text,histogramX->getBins()+1);
}

int HistogramAnalysis::read(char *basename)
{
	char filename[MAX_LENGTH];
	char **text,**entries;
	CharLine *charline;
	ifstream ifile;		
	int lines,result;
	
	// create histogram of x-values
	histogramX = new AdaptiveHistogram();
	sprintf(filename,"%s.x",basename);	
	result=histogramX->read(filename,lines,text);
	if (result==1) // no file containing the band bins found		
		return 1;
	else if (result==2) 	// cannot use gaussian error determination
		missGaussData=1;	// since mean and sigma values are missing						
		
	
	// create histograms of error-values 
	// (even if files do not exist)				
	allocArray1(histogramsError,histogramX->getBins());		
	for (int i=0;i<histogramX->getBins();i++) {
		sprintf(filename,"%s.e%d",basename,i);
		histogramsError[i] = new AdaptiveHistogram();
		result=histogramsError[i]->read(filename);
		if (result==1) // no error histogram files found
			missHistData=1;
	}
	// put sigma and error from .x.hist file into error histograms
	charline = new CharLine();
	for (int i=0;i<histogramX->getBins();i++) {		
		if (text[i]) {
			charline->set(text[i]);
			charline->split(" \t");			
			if (charline->getEntryNumber()>1) {
				entries=charline->getEntries();
				histogramsError[i]->setMean(atof(entries[0]));
				histogramsError[i]->setSigma(atof(entries[1]));
			} else {
				cout << "Warning: No mean or sigma entries for the respective"
						" error were found in file" << filename << " in line "
						<< i << endl;
			}
		}
	}
	delete charline;	
	freeArray2(text,lines);	
	this->loaded=1;	
	return 0;
}

/* class AdaptiveHistogram
 */
AdaptiveHistogram::AdaptiveHistogram()
{	
	this->binValue=NULL;
	this->binAverageValue=NULL;
	this->binHeight=NULL;
	this->leftBorder=NULL;
	this->binWidth=NULL;
	this->binEntries=NULL;
	this->values=NULL;	
	this->bisect=NULL;
}

AdaptiveHistogram::~AdaptiveHistogram()
{		
	if (bisect)
		delete bisect;
	freeArray1(values);
	freeArray1(binHeight);
	freeArray1(leftBorder);
	freeArray1(binWidth);
	freeArray1(binValue);
	freeArray1(binAverageValue);
	freeArray1(binEntries);		
}

// create this->bins bins, but make sure that in each bin there are
// at least entriesNr entries, i.e. the actual number of bins is 
// calculated appropriate

// main problem is here, that I have to make sure that same x-values
// come into the same bin -> sounds natural, but need to be superimposed 
void AdaptiveHistogram::fill(int length, const double* valuesIn,
							 int propbins,int entriesNr)
{					 									 									 		
	int currEntriesNr,index;
	float optimalEntries;
	
	// free memory
	if (bisect) {
		delete bisect;
		bisect=NULL;
	}
	freeArray1(binHeight);
	freeArray1(leftBorder);
	freeArray1(binWidth);
	freeArray1(binValue);
	freeArray1(binAverageValue);
	freeArray1(binEntries);		
		
	// copy entries
	this->length=length;	
	allocArray1(this->values,length);
	for (int i=0;i<length;i++)
		this->values[i]=valuesIn[i];	
	this->entriesNr=entriesNr;
	this->proposedBins=propbins;			
		
	// sorting values		
	qsort(values,length,sizeof(double),compDouble);
	
	this->min=values[0];
	this->max=values[length-1];				
			
	// fill binEntries and get at the same time
	// the number of bins
	currEntriesNr=0;
	index=0;
	allocArray1(binEntries,this->length/this->entriesNr+1);
	optimalEntries=this->length/((float)this->proposedBins);	
	while (index<length && currEntriesNr<length) 
	{
		// first fill in minimal number per bin
		if (currEntriesNr+this->entriesNr<=length) {
			binEntries[index]=this->entriesNr; 
			currEntriesNr+=this->entriesNr;
		} else {
			binEntries[index]=length-currEntriesNr; 
			currEntriesNr=length;	
		}
		
		// now fill such that optimal number per bin is reached
		//while( (currEntriesNr<optimalEntries*(index+1)) && 
		//	   (currEntriesNr<length) )
		while ( binEntries[index]<optimalEntries &&
				currEntriesNr<length )
		{
			binEntries[index]++;
			currEntriesNr++;			
		}
		
		// now fill entries with the same value
		// thus making sure that same values come into same bin		
		while ( (values[currEntriesNr-1]==values[currEntriesNr]) && 
				(currEntriesNr<length) ) 
		{
			binEntries[index]++;
			currEntriesNr++;
		}
		
		// now check that there are at least 2 different values left
		// and at least minimal-entry-number-many entries
		// otherwise all remaining data has to be filled into the
		// current bin
		if ( (values[currEntriesNr]==values[length-1]) ||
			 (currEntriesNr+this->entriesNr>length) ) {
			while (currEntriesNr<length) {
				binEntries[index]++;
				currEntriesNr++;
			}
		}
		index++;
	}
	// number of actual used bins <= this->proposedBins
	this->bins=index;	
	//cout << "bins=" << this->bins << endl;
				
	// allocate memory
	allocArray1(binHeight,bins);
	allocArray1(leftBorder,bins); 
	allocArray1(binWidth,bins);
	allocArray1(binValue,bins);
	allocArray1(binAverageValue,bins);
	
	//fill fields
	
	// set binLeftBorder and binAverageValue
	currEntriesNr=0;
	for (int b=0;b<bins;b++) {
		binAverageValue[b]=0;
		leftBorder[b]=values[currEntriesNr];
		for (int i=0;i<binEntries[b];i++) {
			binAverageValue[b]+=values[currEntriesNr];
			currEntriesNr++;
		}
		binAverageValue[b]/=binEntries[b];
	}
	
	// set binWidth
	for (int i=0;i<bins-1;i++) 
		binWidth[i]=leftBorder[i+1]-leftBorder[i];
	binWidth[bins-1]=max-leftBorder[bins-1];					
	
	// set binValue
	for (int i=0;i<bins;i++)
		binValue[i]=leftBorder[i]+binWidth[i]*0.5;		
	
	// set binHeight
	// normalize also by length, since integral shall give 1
	for (int i=0;i<bins;i++)
		binHeight[i]=binEntries[i]/(binWidth[i]*length);
		
	// calculate mean
	mean=0;
	for (int i=0;i<bins;i++) {
		mean+=binEntries[i]*binAverageValue[i];		
	}
	mean/=length;		
	
	// calculate mean from input
	meanInput=0;
	for (int i=0;i<length;i++)
		meanInput+=values[i];
	meanInput/=length;		
	
	// calculate sigma
	sigma=0;
	for (int i=0;i<bins;i++) {
		sigma+=binEntries[i]*(binAverageValue[i]-mean)*
							 (binAverageValue[i]-mean);		
	}
	sigma/=length;
	sigma=sqrt(sigma);		
	
	// calculate sigma from input
	sigmaInput=0;
	for (int i=0;i<length;i++) 
		sigmaInput+=(values[i]-mean)*(values[i]-mean);
	sigmaInput/=length;
	sigmaInput=sqrt(sigmaInput);		
}

void AdaptiveHistogram::write(const char* filename){
	write(filename,NULL);
}

void AdaptiveHistogram::write(const char* filename,char **text){
	ofstream ofile;
	ofile.open(filename);
	if (! ofile.good() ) {
		cerr << "Could not open output file " << filename << endl;
		exit(1);
	}		
		
	ofile << "#leftBorder binHeight binWidth binValue binAverageValue"
			 " binEntries ";
	if (text)
		ofile << text[0];
	ofile << endl;
	for (int i=0;i<bins;i++) {
		ofile << leftBorder[i] << " " << binHeight[i] <<" "<< binWidth[i]<< " "
			  << binValue[i] << " " << binAverageValue[i] << " "
			  << binEntries[i];
		if (text)
			ofile << " " << text[i+1];
		ofile << endl;
	}
	ofile.close();
	ofile.clear();
}

int AdaptiveHistogram::read(const char* basename)
{
	char **text;
	int length,result;
	
	text=NULL;
	result=read(basename,length,text);
	freeArray2(text,length);
	return result;
}

int AdaptiveHistogram::read(const char* basename, int & lines, char ** & text)
{
	char filename[MAX_LENGTH];
	char line[MAX_LENGTH];
	char **entries;
	int index,linecount,missingMeanSigma;
	ifstream ifile;
	CharLine *charline;
	
	// free memory
	if (bisect) {
		delete bisect;
		bisect=NULL;
	}
	freeArray1(binHeight);
	freeArray1(leftBorder);
	freeArray1(binWidth);
	freeArray1(binValue);
	freeArray1(binAverageValue);
	freeArray1(binEntries);		
	
	sprintf(filename,"%s.hist",basename);
	ifile.open(filename);
	if (! ifile.good()) {
		cout << "Could not find histogram file: " << filename << endl;		 
		return 1;
	}
		
	// read number of lines in file
	linecount=0;
	while (ifile.good()) {
		ifile.getline(line,MAX_LENGTH,'\n');
		if (strchr(line,'#'))
			continue;
		if (strlen(line)==0)
			continue;
		linecount++;
	}
	ifile.close();
	ifile.clear();	
	
	this->length=linecount;
	this->bins=linecount;
	this->proposedBins=linecount;
	this->entriesNr=1;
	// allocate memory
	allocArray1(binHeight,bins);
	allocArray1(leftBorder,bins); 
	allocArray1(binWidth,bins);
	allocArray1(binValue,bins);
	allocArray1(binAverageValue,bins);
	allocArray1(binEntries,bins);

	// read file	
	allocArray2(text,this->length,MAX_LENGTH);
	charline = new CharLine();
	index=0;
	missingMeanSigma=0;
	ifile.open(filename);
	while (ifile.good()) {
		ifile.getline(line,MAX_LENGTH,'\n');
		if (strchr(line,'#'))
			continue;
		if (strlen(line)==0)
			continue;
		charline->set(line);
		charline->split(" \t");
		if (charline->getEntryNumber()<6) {
			cerr << "File " << filename << " corrupt. Need more columns" 
				 << endl;
			delete charline;
			exit(1);
		}			
		entries=charline->getEntries();		
		leftBorder[index]=atof(entries[0]);
		binHeight[index]=atof(entries[1]);
		binWidth[index]=atof(entries[2]);
		binValue[index]=atof(entries[3]);
		binAverageValue[index]=atof(entries[4]);
		binEntries[index]=atoi(entries[5]);
		if (charline->getEntryNumber()>6) {
			sprintf(text[index],"%s %s",entries[6],entries[7]);
		} else {
			strcpy(text[index],"\0");
			missingMeanSigma=1;
		}
		index++;			
	}	
	delete charline;
	lines=this->length;
	if (missingMeanSigma==0) 
		return 0;
	else 
		return 2;
}

int AdaptiveHistogram::getBin(double x)
{
	if (! bisect)
		bisect = new Bisection(this->bins,leftBorder);
	return bisect->getIndex(x);
}

// creates a new value for x from the gaussian distribution with
// mean and sigma
void AdaptiveHistogram::gaussian(double &x,double mean, double sigma)
{	
	// use mean and sigma entries to obtain a new x value
	double x1,x2,y;
	
	// take two random variables from the interval (0,1)
	do {
		x1=rand()/(RAND_MAX+1.0);
		x2=rand()/(RAND_MAX+1.0);
	} while (x1==0 || x2==0);
	// box-muller transformation; y is now distributed according
	// to a nomalized gaussian (mean=0; deviation=1)
	y=sqrt(-2*log(x1))*cos(2*M_PI*x2); 
	// then the variable y2=y*sigma has standard deviation sigma
	y*=sigma;
	// add this error to x
	x=mean+y;
}

// Here the error is gaussian distributed with mean and sigma
// thus first determine a error value of x according to that
// distribution, then take a value of x according to a gaussian
// distribution with standard deviation of error
// minError restrictes the (relative!) error to be greater than minError
// normally you could call it with minError=0;
void AdaptiveHistogram::gaussianError(double &x, double &error, double minError)
{	
	double xold;
	gaussian(error, mean, sigma);	
	if (error<x*minError) // happens often if mean is not much greater than sigma
		error=x*minError;
	
	xold=x;
	gaussian(x, xold, error);
	if (x<error)		   // avoid negative fluxes
		x=error;
}

// same as above, but does not modify the original value 
// (just the error value is changed)
void AdaptiveHistogram::gaussianErrorOnly(const double &x, double &error, double minError)
{	
	gaussian(error, mean, sigma);	
	if (error<x*minError) // happens often if mean is not much greater than sigma
		error=x*minError;
}

void AdaptiveHistogram::histogramError(double &x, double &error, double minError)
{
	
}
	
/* class SimpleHistogram
 */

SimpleHistogram::SimpleHistogram(const std::vector<double> & entries, int bins)
{	
	this->bins=bins;	
	allocArray1(this->binValue,this->bins);
	allocArray1(this->binCount,this->bins);
	content=NULL;
	
	this->min=*min_element(entries.begin(),entries.end());
	this->max=*max_element(entries.begin(),entries.end());
		
	this->getSupport(entries);
	this->fill(entries);
}

SimpleHistogram::SimpleHistogram(const std::vector<double> & entries, int bins, double min, double max)
{	
	this->bins=bins;	
	allocArray1(this->binValue,this->bins);
	allocArray1(this->binCount,this->bins);
	content=NULL;	
	this->min=min;
	this->max=max;
	this->getSupport(entries);
	this->fill(entries);
}

SimpleHistogram::~SimpleHistogram()
{
	if (content)
		delete content;
	freeArray1(this->binValue);
	freeArray1(this->binCount);
}

// determines the x-values at the mid point of the bins
void SimpleHistogram::getSupport(const std::vector<double> & values) {
	
	
	// determine binSize (max and min lie in the middle of the first
	// and last bin, respectively)
	this->binWidth=(max-min)/(bins-1);
	
	// fill binValue array with the mid point values of the bins
	for (int i=0;i<this->bins;i++)
		binValue[i]=min+i*(this->binWidth);	
}

// fill given x-values into the right bins
void SimpleHistogram::fill(const std::vector<double> & values){
	int binIndex;
	
	for (int i=0;i<this->bins;i++)
		binCount[i]=0.;
	
	for (unsigned i=0;i<values.size();i++) {
		// find right binIndex (0..bins) for given x-value
		// -> don't use bisection, since bins are of equal width
		binIndex=(int)((values[i]-min)/binWidth+0.5);
		if (binIndex<0)
		  binIndex=0;
		if (binIndex>=this->bins)
		  binIndex=this->bins-1;
		binCount[binIndex]+=1.0;
	}
	
	// put bin data also in xyData structure, can use functionality
	// of writing to a file, integration, making splines, ...
	if (content)
		delete content;
	content = new xyData(this->bins,this->binValue,this->binCount);
}

// write Histogram to file
void SimpleHistogram::write(const char* filename){
	content->write(filename);
}

/* class storing z-values
 */
void BinningZ::set(int spacing, double zmin, double zmax, double dz)
{
	double dz_new;
	double zmin_log, zmax_log;
	
	resize();
	
	if (dz<1E-10)
	  dz=1E-10;	
			
	if (spacing==0)	{		
		if (fabs(zmax-zmin)<dz)
			m_length=1;
		else
			m_length=(int)((zmax-zmin)/dz+0.5)+1;
		if (m_length>1) 			
			dz_new=(zmax-zmin)/(m_length-1);
		else
			dz_new=0.;
		
		allocArray1(m_z,m_length);
		allocArray1(m_dz,m_length);
		m_z[0]=zmin;
		m_dz[0]=dz_new;
		for (int i=1;i<m_length;i++) {
			m_z[i]=m_z[i-1]+dz_new;		
			m_dz[i]=dz_new;		
		}
		
	} else {
		
		dz_new=log((1+zmin+dz)/(1+zmin));
		zmin_log=log(1+zmin);
		zmax_log=log(1+zmax);
		
		if (fabs(zmax-zmin)<dz)
			m_length=1;			
		else 
			m_length=(int)((zmax_log-zmin_log)/dz_new)+1;
		if (m_length>1) 
			dz_new=(zmax_log-zmin_log)/(m_length-1);
		else
			dz_new=0.;
				
		allocArray1(m_z,m_length);
		allocArray1(m_dz,m_length);		
		for (int i=0;i<m_length;i++) 
			m_z[i]=exp(zmin_log+i*dz_new)-1;										
		for (int i=0;i<m_length-1;i++)
			m_dz[i]=m_z[i+1]-m_z[i];
		m_dz[m_length-1]=exp(zmin_log+m_length*dz_new)-1-m_z[m_length-1];
	}			
}

void BinningZ::set(int length, double *z)
{
	m_length=length;
	allocArray1(m_z,m_length);
	allocArray1(m_dz,m_length);
	for (int i=0;i<m_length;i++) 
		m_z[i]=z[i];
	for (int i=0;i<m_length-1;i++)
		m_dz[i]=m_z[i+1]-m_z[i];
	m_dz[m_length-1]=m_dz[m_length-2];
}

void BinningZ::sort()
{	
	// sort
	qsort(m_z,m_length,sizeof(double),compDouble);
	
	for (int i=0;i<m_length-1;i++)
		m_dz[i]=m_z[i+1]-m_z[i];
	m_dz[m_length-1]=m_dz[m_length-2];
	
}

FluxRestriction::FluxRestriction(const BinningZ* binZ, double omegaM, double omegaL, double hubble)
{
	    this->omegaM=omegaM;
	    this->omegaL=omegaL;
	    this->hubble=hubble;
		dL=NULL;
		conv=NULL;		
		mbLow=-10;
		mbHigh=-27;
		mbLowFluxFast=g_magToFlux(mbLow);	
		mbHighFluxFast=g_magToFlux(mbHigh);
		m_binZ=new BinningZ();
		m_binZ->copy(binZ);
		calcDL();
}

FluxRestriction::~FluxRestriction()
{
	freeArray1(dL);
	freeArray1(conv);
	if (m_binZ)
		delete m_binZ;
}

void FluxRestriction::calcDL()
{
	double *iE=NULL;
	double *z2;
	double *z;	
	double dH = 2997.92458/hubble;
	BinningZ *binZ2;
	
	double omegaK = 1 - omegaL - omegaM;
	
	// calculate luminosity distance	
	// look eg Hogg, Distance measures in cosmology, astro-ph 9905116	
	z=m_binZ->getZ();
	
	binZ2 = new BinningZ();	
	binZ2->set(0, 0.0, z[m_binZ->getLength()-1], INTPOINTSDIFF);
	
	z2=binZ2->getZ();
	allocArray1(iE,binZ2->getLength());	
	for (int i=0;i<binZ2->getLength();i++) {
		iE[i]=1/sqrt(omegaM*(1+z2[i])*(1+z2[i])*(1+z2[i]) + omegaK*(1+z2[i])*(1+z2[i]) + omegaL);
	}
	
	xyData* data = new xyData(binZ2->getLength(),z2,iE);
	
	double *dC;
	allocArray1(dC,m_binZ->getLength());
	allocArray1(dL,m_binZ->getLength());
	for (int i=0;i<m_binZ->getLength();i++) 
		dC[i]=dH*data->integrate(0.,z[i]);		 
	if (omegaK>0) {	
	  for (int i=0;i<m_binZ->getLength();i++) 
		dL[i]=(1+z[i])*dH/sqrt(omegaK)*sinh(sqrt(omegaK)*dC[i]/dH);
	} else if (omegaK<0) {
	  for (int i=0;i<m_binZ->getLength();i++) 
		dL[i]=(1+z[i])*dH/sqrt(-omegaK)*sin(sqrt(-omegaK)*dC[i]/dH);			
	} else {
	  for (int i=0;i<m_binZ->getLength();i++)
		dL[i]=(1+z[i])*dC[i];
	}
	freeArray1(dC);
	
	allocArray1(conv,m_binZ->getLength());
	for (int i=0;i<m_binZ->getLength();i++) 
		conv[i]=(dL[i]*1E5)*(dL[i]*1E5)*(1+m_binZ->getZ()[i]);
		
	delete data;
	freeArray1(iE);
	delete binZ2;	
}

void FluxRestriction::getZLimit(double & zlimit,const double &flux)
{	
	// restriction for the appearent i-band flux
	if (g_fluxToMag(flux)<20)  // i.e. i-mag<20
		zlimit = ( zlimit < 1 ) ? zlimit : 1;
	if (g_fluxToMag(flux)<23) // i.e. i-mag<23
		zlimit = ( zlimit < 3 ) ? zlimit : 3;	
}

// precalculates iga for all wavelength > Lyman Limit
void IGAbsorption::setLambda(const xyData *data) 
{
	double *y;
	int flag, index;
	
	if (iga!=NULL)
		delete iga;		
	
	callocArray1(y,data->getlength());
	iga = new xyData();
	iga->set(data->getlength(),data->getX(),y);	
	freeArray1(y);
	
	index=iga->getIndex(ly[0],flag);	
	if (flag==0)
		index++;
	y=iga->getY();		
	
	for (int i=index;i<iga->getlength();i++)
		y[i]=1.0;						
	y=NULL;			
}

// multiplies data with transmittance iga
void IGAbsorption::absorb(xyData *data,double z) 
{
	if (absorptionMode<=2)
	  absorb_Madau(z,absorptionMode);
	else
	  absorb_Meiksin(z,absorptionMode);
	
	if (absorptionMode) {	
		// second: multiply data with current transmittance
		if (((*data) * iga)==NULL)
			EXIT("Could not include intergalactic absorption");		
	}
}

// similar to absorb but sets y-values of data 
// to the calculated optical depth epx(-\tau)
void IGAbsorption::getOpticalDepth(xyData *data,double z) 
{	
	if (absorptionMode<=2)
	  absorb_Madau(z,absorptionMode);
	else
	  absorb_Meiksin(z,absorptionMode);
	
	if (absorptionMode) {
		data->setY(iga);
	} else {
		data->setY(1);
	}
}
	
// multiplies data with transmittance iga
// compare: ApJ 441:18-27, 1995 March 1
// it first calculates for each wavelength in the rest-frame SED it attenuation
// and then multiplies the rest-frame SED with it
// mode=1: use only the first 4 Lyman-transitions
// mode=2: use the first numLines transitions, extend with norm from Meiksin 2005
// we recommend to use mode 2, although it still differs from the full (13-order) Madau solution
void IGAbsorption::absorb_Madau(double z, int mode) 
{
	int index1,index2, flag;
	double tau;
	double lambda_o,lambda_e;
	double *lambda,*y;			
	double lyL=912;	
	
	double A[4] = {3.6E-3, 1.7E-3, 1.2E-3, 9.3E-4}; // tau-norm for 1st-4th order
	double B[4] = {0.66, 0.47, 0.34, 0.26};         // tau-norm 5th-8th order relative to A[3]
	
	if (z!=lastz || lastz<0) {  // first: calculate transmittance 
				
		lambda=iga->getX();
		y=iga->getY();		
	
		// for given SED *data, calculate indices of Lyman-Limit and Lyman-alpha
		index1=iga->getIndex(ly[0],flag); // Lyman-alpha index
		if (flag==1)
			index1--;	
		index2=iga->getIndex(lyL,flag);   // Lyman-Limit index
		if (flag==0)
			index2++;				
			
	        // regard lambdas l: Lyman-limit(912) <= l <= Lyman-alpha(1216)
		// here only scattering from Lyman transitions
		for (int i=index2;i<=index1;i++) {	
			tau=0.;
			lambda_e=lambda[i];
			lambda_o=lambda[i]*(1+z);
			
			int l_numLines;		
			if (mode==1)           // use first four transitions
			  l_numLines=4;
			else
			  l_numLines=numLines; // use first numLines transitions, 
			                       // if necessary expand using Table 1 and Formula (4) 
					       // of Meiksin 2005
						
			for (int j=0;j<l_numLines;j++) {
			  // first four order terms are directly from Madau 2005
			  if (j<4) {						
			    if (lambda_e<=ly[j]) // only lower energy Lyman-lines can absorb
			      tau+=A[j]*pow(lambda_o/ly[j],3.46);
			    else
			      break;            // if ly[j]<lambda_e also ly[j+1]<lambda_e
			  //extrapolation of higher order series with the norm of Meiksin 2005
			  } else if (j<8) {     
			    if (lambda_e<ly[j])
                              tau+=B[j-4]*A[3]*pow(lambda_o/ly[j],3.46);
			    else
			      break;			  			  
			  } else {
			    if (lambda_e<ly[j])
                              tau+=720/(j+2.)/(j*j-1.0)*B[3]*A[3]*pow(lambda_o/ly[j],3.46);
			    else
			      break;			  
			  }
			}								
                        	    
			y[i]=exp(-tau);
		}
				
		// regard lambdas: 0 <= l < Lyman-Limit(912)
		// here scattering from Lyman transitions + 
		// strong absorption due to neutral hydrogen	
		double xc, xem;
		for (int i=0;i<index2;i++) {													
			tau=0.;
			lambda_o=lambda[i]*(1+z);
			
			int l_numLines;		
			if (mode==1)           // use first four transitions
			  l_numLines=4;
			else
			  l_numLines=numLines; // use first numLines transitions, 
			                       // if necessary expand using Table 1 and Formula (4) 
					       // of Meiksin 2005
					       
			for (int j=0;j<l_numLines;j++) {
			  // first four order terms are directly from Madau 2005
			  if (j<4)
			      tau+=A[j]*pow(lambda_o/ly[j],3.46);			    
			  //extrapolation of higher order series with the norm of Meiksin 2005
			  else if (j<8)			    
                              tau+=B[j-4]*A[3]*pow(lambda_o/ly[j],3.46);			  		  			  
			  else			    
                              tau+=720/(j+2.)/(j*j-1.0)*B[3]*A[3]*pow(lambda_o/ly[j],3.46);			   
			}										
		
			if (lambda_o<lyL)
			  xc=1;
			else
			  xc=lambda_o/lyL;
			xem=1+z;
														
			tau+=0.25*xc*xc*xc*(pow(xem,0.46)-pow(xc,0.46))+9.4*pow(xc,1.5)*(pow(xem,0.18)-pow(xc,0.18))-
			0.7*xc*xc*xc*(pow(xc,-1.32)-pow(xem,-1.32))-0.023*(pow(xem,1.68)-pow(xc,1.68));
			y[i]=exp(-tau);
		}
		
		lastz=z;
		
	}		
	
	// checking
	#if 0
	char s[MAX_LENGTH];
	sprintf(s,"m%d_z%g.dat",mode,z);
	this->iga->write(s);
	#endif			
}

// multiplies data with transmittance iga
// compare: astro-ph/0512435
// it first calculates for each wavelength in the rest-frame SED it attenuation
// and then multiplies the rest-frame SED with it
// all modes use Lyman-line attenuation down to numLines order following Meiksin 2005, they differ
// in the photoelectric absorption & Lyman-Limit treatment
// mode=3: use Madau photoelectric absorption & Lyman-Limit treatment
// mode=4: use Meiksin photoelectric absorption & Lyman-Limit treatment(from Meiksin 2005)
// mode=5: use Meiksin photoelectric absorption & Lyman-Limit treatment (Meiksin private Communications 2007)
// mode=6: use Meiksin photoelectric absorption & skip Lyman-Limit treatment
// mode=7: skip Meiksin photoelectric absorption & skip Lyman-Limit treatment
// we recommend to use mode=5
void IGAbsorption::absorb_Meiksin(double z, int mode) 
{
	int index1,index2, flag;
	double tau,tau_aux;
	double znPlus1;
	double lambda_o,lambda_e;
	double *lambda,*y;
	double *A;			
	double lyL=912;
	
	allocArray1(A,8);			
	A[0]=0;
	A[1]=0.348;
	A[2]=0.179;
	A[3]=0.109;
	A[4]=0.0722;
	A[5]=0.0508;
	A[6]=0.0373;
	A[7]=0.0283;			
	
	if (z!=lastz || lastz<0) {  // first: calculate transmittance 
				
		lambda=iga->getX();
		y=iga->getY();					        
		
		// for given SED *data, calculate indices of Lyman-Limit and Lyman-alpha
		index1=iga->getIndex(ly[0],flag);
		if (flag==1)
			index1--;	
		index2=iga->getIndex(lyL,flag);
		if (flag==0)
			index2++;
				
		// regard lambdas l: Lyman-Limit(912) <= l <= Lyman-alpha(1216)
		// here only scattering from Lyman transitions		
		for (int i=index2;i<=index1;i++) {
																														
			lambda_e=lambda[i];
			lambda_o=lambda[i]*(1+z);						

			znPlus1=lambda_o/ly[0];
			if (znPlus1<5) {
				tau=0.00211*pow(znPlus1,3.7);				
			} else {
				tau=0.00058*pow(znPlus1,4.5);
			}
			for (int j=1;j<numLines;j++) {
				if (lambda_e<ly[j]) {
					if (j<4 && znPlus1>4) {
						znPlus1=lambda_o/ly[j];
						tau_aux=A[j]*pow(0.25*(znPlus1),0.166666);												
					} else if (j<8) {
						znPlus1=lambda_o/ly[j];
						tau_aux=A[j]*pow(0.25*(znPlus1),0.333333);
					} else {
						znPlus1=lambda_o/ly[7];
						tau_aux=A[7]*pow(0.25*(znPlus1),0.333333)*720/
						     ( (j+2)*((j+2)*(j+2)-1) );
					}
					if (znPlus1<5) 
						tau_aux*=0.00211*pow(znPlus1,3.7);
					else
						tau_aux*=0.00058*pow(znPlus1,4.5);					
					tau+=tau_aux;						
				} else {
				  break;
				}
			}						
			y[i]=exp(-tau);
		}
				
		// regard lambdas l: 0 < l <= Lyman-Limit	
		// here scattering from Lyman transitions + photoelectric absorption by
		// neutral hydrogen (for the latter the Madau correction is used)
								
		double xc, xem;
		for (int i=0;i<index2;i++) {															
			
			lambda_o=lambda[i]*(1+z);									
			
			znPlus1=lambda_o/ly[0];
			if (znPlus1<5) {
				tau=0.00211*pow(znPlus1,3.7);				
			} else {
				tau=0.00058*pow(znPlus1,4.5);
			}		
			for (int j=1;j<numLines;j++) {	// need to include all the lines
				if (j<4 && znPlus1>4) {
					znPlus1=lambda_o/ly[j];
					tau_aux=A[j]*pow(0.25*(znPlus1),0.166666);												
				} else if (j<8) {
					znPlus1=lambda_o/ly[j];
					tau_aux=A[j]*pow(0.25*(znPlus1),0.333333);
				} else {
					znPlus1=lambda_o/ly[7];
					tau_aux=A[7]*pow(0.25*(znPlus1),0.333333)*720/
						     ( (j+2)*((j+2)*(j+2)-1) );
				}
				if (znPlus1<5) 
					tau_aux*=0.00211*pow(znPlus1,3.7);
				else
					tau_aux*=0.00058*pow(znPlus1,4.5);					
				tau+=tau_aux;															
			}
			
			if (mode<=3) { // photoelectric absorption according to Madau
				
			  if (lambda_o<lyL)
				xc=1;
			  else
				xc=lambda_o/lyL;						
			  xem=1+z;
										
			  tau+=0.25*xc*xc*xc*(pow(xem,0.46)-pow(xc,0.46))+9.4*pow(xc,1.5)*(pow(xem,0.18)-pow(xc,0.18))-0.7*xc*xc*xc*
			  (pow(xc,-1.32)-pow(xem,-1.32))-0.023*(pow(xem,1.68)-pow(xc,1.68));
			  
			} else {
			  double zlplus1 = lambda_o/912.0;
			
			  //contrib. by IGM
			  if (mode==4) { 			 // version of Meiksin 2005                            
                            tau+=0.805*pow(zlplus1,3.)*(1./zlplus1 - 1./(1+z));           
			  } else if (mode==5 || mode==6) {       // Meiksin private communications 2007
			    tau+=0.0755*pow(zlplus1,4.4)*(pow(zlplus1,-1.5) - pow(1+z,-1.5));
			  }
           
                          //calculating the contributions from LLSystems
			  if (mode<=5) {
                            double N0=0.25;
                            double beta=1.5;
                            double gamma=1.5;
           
                            double s1=0,s2=0;
                            for (int n=0; n<11; n++){
                              s1+=(beta-1)/(n+1-beta)*pow(-1.,n)/gsl_sf_fact(n);
                              if(n>0)
                               s2+=(beta-1)/(3*n-gamma-1)/(n+1-beta)*pow(-1.,n)/gsl_sf_fact(n)*(pow(1+z,gamma+1-3*n)*pow(zlplus1,3.0*n)-pow(zlplus1,gamma+1.0));
                            }
                            tau_aux = N0/(4+gamma-3.*beta)*(gsl_sf_gamma_inc(2.-beta,1.0)-exp(-1.)-s1)*(pow(1+z,-3.0*(beta-1)+gamma+1)*pow(zlplus1,3.0*(beta-1))-pow(zlplus1,gamma+1))- N0*s2;			               
                            tau+=tau_aux;              
			  }
			
			}								
			y[i]=exp(-tau);
		}
		
		lastz=z;
		
	}
	
	freeArray1(A);
	
	// checking
	#if 0
	char s[MAX_LENGTH];
	sprintf(s,"m%d_z%g.dat",mode,z);
	this->iga->write(s);
	#endif					
}

/* class Convolution2D
 */

void Convolution2D::rectangular(double fwhmX, double fwhmY)
{
	xyData *data;
	data = new xyData();
	double *field2,*fieldResult;		
	
		// convolute in y direction (actually just average, which is the same 
		// for a box filter)
		for (int nx=0;nx<Lx;nx++)	{
			data->set(Ly,y,field[nx]);
			data->average(fwhmY,0.,0);
			data->getCopyY(field[nx]);
		}	
		
		/*
		cout << "After y-smoothing" <<  endl;
		for (int nx=0;nx<Lx;nx++)	
			for (int ny=0;ny<Ly;ny++) 
				cout << "prior[" << nx << "][" << ny << "]=" << field[nx][ny] << endl;			
		*/
		
		allocArray1(field2,Lx);
		// convolute in x direction
		for (int ny=0;ny<Ly;ny++) {
			for (int i=0;i<Lx;i++)
				field2[i]=field[i][ny];			
			data->set(Lx,x,field2);
			data->average(fwhmX,0.,0);
			fieldResult=data->getY();			
			for (int i=0;i<Lx;i++)
				field[i][ny]=fieldResult[i];
		}					
		freeArray1(field2);
	delete data;
}

/* Functions
 */
 
/* helper functions
 */

int compDouble(const void * a,const void * b)
{	
	if (*(double*)a<*(double*)b) return -1;
	if (*(double*)a>*(double*)b) return 1;
	return 0;	
}

int compDataPair(const void * a,const void * b)
{	
	if ((*(xyPair*)a).x<(*(xyPair*)b).x) return -1;
	if ((*(xyPair*)a).x>(*(xyPair*)b).x) return 1;
	return 0;	
}
