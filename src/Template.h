#ifndef _TEMPLATE_H_
#define _TEMPLATE_H_

#include <cmath>
#include "LambdaDataSet.h"
#include "Filter.h"
#include "NumericalData.h"
#include "allocArray.h"
#include "globHeader.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/
 
using namespace std;

class Template : public LambdaDataSet
{	
private:
	int numFilters;
	Filter** filters;
	Filter *b_filter; 				// necessary to calculated normalized (to B band magnitude) flux		
	double m_bFlux;					// holds the flux through the B filter (rest frame) once calculated
	double m_bFluxNormalized;		// holds the normalized flux (to the B filter) through filter B
	BinningZ *m_binZ;
	IGAbsorption *m_iga;	
	double massToLightRatio;
	double minZ;					// minZ and maxZ give the range in redshift, for which the template
	double maxZ;					// is allowed to be used
	int havePrepared;
	
	const static int numPattern=2;	// number of pattern		
	Template** pattern;				// the pattern (original templates) used for interpolation
	double *weightsPattern;			// the weights of the given pattern
									// the weights have to sum up to 1
	const static int numOptions=3;	// number of allowed options
	int options[numOptions];		// following options are supported:
									// index name "description" default
									// 0 linear "log vs linear interpolation used" 0						
									// 1 igAbsorption "consider intergalactic absorption" 0
									// 2 fluxType "photon counts (0) vs energy(1)" 0

	void loadInterpol(); // calculates an interpolated template in log(flux)
						 // space from templates given by setInterpolData		
	void calc_IGA();	 // creates the IG_absorption object
	double intern_normalizeToFilter(Filter *filter,xyData *val); // return the normalization constant		
	
public:
	Template(char *name);
	Template(char *name, char *shortname);
	Template(Template *t);
	~Template();
	void resize();
	
	void load();
		
	void setInterpolData(Template **pattern,double* weightsPattern);
	void freeFlux();
		
	// gives template the information about filters used and the
	// range of redshifts for which a flux calculation will be done
	void prepareCalcFlux(int numFilters,Filter** filters,BinningZ *binZ);
#ifdef PARRAY							 
	void calcFlux(double** & flux); // calculates flux for all redshifts and filter bands
#else
	void calcFlux(double* flux);    // calculates flux for all redshifts and filter bands
#endif	
	double getFlux(int zi, int f); 	// calculates flux through filter band f at redshift z
	double getFlux(double z, int f);// the same, but uses redshift, not index of redshift, as input
	double getBFlux();				// calculates flux through B filter band (in rest frame)
									// \int filter(nu) template(nu) dnu or \int fil(nu) templ(nu)/nu dnu
	double getBFluxNormalized();	// calculates flux through B filter band (in rest frame)
									// \int filter(nu) template(nu) dnu / \int filter(nu) dnu or 
									// \int filter(nu) template(nu)/nu dnu / \int filter(nu)/nu dnu or 
	double getMassToLightRatio() { return massToLightRatio; };
	void getObservedSpectrum(int zi, xyData * data); // after redshifting and IG absorption and ... etc
	void getSpectrum(int zi, xyData * data);		 // after redshifting 
	void getPlainSpectrum(xyData * data);		 	 // just the plain spectrum (no redshifting etc) 
	IGAbsorption *getIGAbsorption() { return m_iga; }; 
	void setMinZ(double minZ) { this->minZ=minZ; };
	void setMaxZ(double maxZ) { this->maxZ=maxZ; };
	double getMinZ() {return minZ; };
	double getMaxZ() {return maxZ; };
	void setMassToLightRatio(double mtlr) { massToLightRatio=mtlr; };
	void setFluxType(int fluxType) { this->options[2]=fluxType; }
	void setLinear(int linear) { this->options[0]=linear; };
	void setigAbsorption(int igAbsorption) { this->options[1]=igAbsorption; };
	void setBFilter(Filter *filter) { this->b_filter=filter; };				
	
	void smooth(double fwhm, double smoothLimitLow, int smoothMode) {	// smoothes it with spectral resolution fwhm (if smoothMode==0)
	                                                                        // or with inverse resolving power fwhm/lambda (if smoothMode==1)
		if (! this->loaded) 
			load();
		
		this->values->average(fwhm,smoothLimitLow,smoothMode);
		
	}
	
	void normalizeToFilter_values(Filter *filter);
	void normalizeToFilter_meshed(Filter *filter);
	void normalizeToFilter(Filter *filter, xyData *data);
};

class TemplateOption {
	
protected:	
	int numBasicTemplates;			// number of templates without interpolating	
	char **nameTemplates;			// names of those basic templates
	char templatePath[MAX_LENGTH];	// where to find the basic templates
	
	// parameter for creation of each template
	int igAbsorption;
	int fluxType;
	double *massToLightRatios;
	double *minZ;
	double *maxZ;	
	Filter *B_filter;	
	
	// parameter for interpolation
	int logInterpolation;
	int logInterpolationMode;
	int linInterpolation;
	int linInterpolationMode;
	
public:		
	
	TemplateOption(){};
	
	TemplateOption(int numBasicTemplates, char** nameTemplates, char *templatePath) {			
		
		igAbsorption=0;
		fluxType=0;
		massToLightRatios=NULL;
		minZ=NULL;
		maxZ=NULL;
		B_filter=NULL;		
		logInterpolation=0;
		logInterpolationMode=0;
		linInterpolation=0;
		linInterpolationMode=0;
		
		this->numBasicTemplates=numBasicTemplates;
		allocArray2(this->nameTemplates,numBasicTemplates,MAX_LENGTH);
		for (int i=0;i<numBasicTemplates;i++) {
			strcpy(this->nameTemplates[i],nameTemplates[i]);
		}
		strcpy(this->templatePath,templatePath);
	}
		
	~TemplateOption() {
		freeArray2(nameTemplates,numBasicTemplates);
		freeArray1(massToLightRatios);
		freeArray1(minZ);
		freeArray1(maxZ);		
	}
	
	void copy(TemplateOption *t) {
		
		this->numBasicTemplates=t->numBasicTemplates;
		allocArray2(this->nameTemplates,numBasicTemplates,MAX_LENGTH);
		for (int i=0;i<numBasicTemplates;i++) {
			strcpy(this->nameTemplates[i],t->nameTemplates[i]);
		}
		strcpy(this->templatePath,t->templatePath);
		
		setIGAbsorption(t->igAbsorption);
		setFluxType(t->fluxType);
		setMassToLightRatios(t->massToLightRatios);
		setMinZ(t->minZ);
		setMaxZ(t->maxZ);
		setBFilter(t->B_filter);		
		setLogInterpolation(t->logInterpolation);
		setLinInterpolation(t->linInterpolation);
		setLogInterpolationMode(t->logInterpolationMode);
		setLinInterpolationMode(t->linInterpolationMode);		
	}	
		
	void setIGAbsorption(int igAbsorption) {
		this->igAbsorption=igAbsorption;
	}
	
	void setFluxType(int fluxType) {
		this->fluxType=fluxType;
	}
	
	void setMassToLightRatios(double *massToLightRatios) {
		allocArray1(this->massToLightRatios,numBasicTemplates);
		for (int i=0;i<numBasicTemplates;i++)
			this->massToLightRatios[i]=massToLightRatios[i];
	}
	
	void setMinZ(double *minZ) {
		allocArray1(this->minZ,numBasicTemplates);
		for (int i=0;i<numBasicTemplates;i++)
			this->minZ[i]=minZ[i];
	}
	
	void setMaxZ(double *maxZ) {
		allocArray1(this->maxZ,numBasicTemplates);
		for (int i=0;i<numBasicTemplates;i++)
			this->maxZ[i]=maxZ[i];
	}
	
	void setBFilter(Filter *B_filter) {
		this->B_filter=B_filter;
	}
	
	void setLogInterpolation(int logInterpolation) {
		this->logInterpolation=logInterpolation;
	}
	
	void setLogInterpolationMode(int logInterpolationMode) {
		this->logInterpolationMode=logInterpolationMode;
	}
	
	void setLinInterpolation(int linInterpolation) {
		this->linInterpolation=linInterpolation;
	}
	
	void setLinInterpolationMode(int linInterpolationMode) {
		this->linInterpolationMode=linInterpolationMode;
	}				
	
	int getIgAbsorption() {
		return igAbsorption;
	}
	
	int getFluxType() {
		return fluxType;
	}
	
	double *getMassToLightRatios() {
		return massToLightRatios;
	}
	
	double *getMinZ() {
		return minZ;
	}
	
	double *getMaxZ() {
		return maxZ;
	}
	
	Filter *getB_filter() {
		return B_filter;
	}	
	
	int getLogInterpolation() {
		return logInterpolation;
	}
	
	int getLogInterpolationMode() {
		return logInterpolationMode;
	}
	
	int getLinInterpolation() {
		return linInterpolation;
	}
	
	int getLinInterpolationMode() {
		return linInterpolationMode;
	}
	
	int getNumBasicTemplates() {
		return numBasicTemplates;
	}
	
	char **getNameTemplates() {
		return nameTemplates;
	}
	
	char *getTemplatePath() {
		return templatePath;
	}	
	
};

class TemplateBin
{	
protected:
	double zmin;					// minimal redshift for that bin
	double zmax;					// maximal redshift for that bin
	int numBasicTemplates;			// number of basic templates
	int numLogInterTemplates;		// number of log-interpolated templates 
	int numLinInterTemplates;		// number of lin-interpolated templates 		
	Template **origTemplates; 		// all templates (incl. interpolated ones; plus dust obscured ones)
	                                        // dyn. array (redshift bins) of pointers to templates; 
	Template **basicTemplates;		// just the basic templates (subset of origTemplates)
	Template **logInterTemplates;	// just the log-interpolated templates (subset of origTemplates)
	Template **linInterTemplates;	// just the lin-interpolated templates (subset of origTemplates)	
		
	TemplateOption *templateOption;	// contains parameters needed for creating templates
	
	OutputHandler *m_out;		
	
public:
	TemplateBin(TemplateOption *templateOption){
		
		this->templateOption = new TemplateOption();
		this->templateOption->copy(templateOption);		
		origTemplates=NULL;
		basicTemplates=NULL;
		logInterTemplates=NULL;
		linInterTemplates=NULL;				
		numBasicTemplates=0;
		numLogInterTemplates=0;
		numLinInterTemplates=0;				
		zmin=0;
		zmax=DBL_MAX;		
	};
	~TemplateBin(){		
		freeArray1(origTemplates);
		deleteBasicTemplates();
		deleteLogInterTemplates();
		deleteLinInterTemplates();		
		if (templateOption)
			delete templateOption;			
	};
	
	void resize() {
		freeArray1(origTemplates);
		deleteBasicTemplates();
		deleteLogInterTemplates();
		deleteLinInterTemplates();		
		if (templateOption)
			delete templateOption;		
	}

	void setOutputHandler(OutputHandler *m_out) {
		this->m_out=m_out;
	}		

	void setBasicTemplates(int numBasicTemplates,Template **basicTemplates) {
		deleteBasicTemplates();		
		this->numBasicTemplates=numBasicTemplates;
		allocArray1(this->basicTemplates,numBasicTemplates);
		for (int i=0;i<numBasicTemplates;i++)
			this->basicTemplates[i] = new Template(basicTemplates[i]);
	}

	void setZmin(double zmin) {
		this->zmin=zmin;
	}
	
	void setZmax(double zmax) {
		this->zmax=zmax;
	}

	void deleteBasicTemplates() {
		if (basicTemplates) {
			for (int i=0;i<numBasicTemplates;i++)
				if (basicTemplates[i])
					delete basicTemplates[i];
			freeArray1(basicTemplates);
		}
	}
	
	void deleteLogInterTemplates() {
		if (logInterTemplates) {
			for (int i=0;i<numLogInterTemplates;i++)
				if (logInterTemplates[i])
					delete logInterTemplates[i];
			freeArray1(logInterTemplates);
		}
	}
	
	void deleteLinInterTemplates() {
		if (linInterTemplates) {
			for (int i=0;i<numLinInterTemplates;i++)
				if (linInterTemplates[i])
					delete linInterTemplates[i];
			freeArray1(linInterTemplates);
		}
	}

	Template **getOrigTemplates() {
		return origTemplates;
	}
	
	Template *getOrigTemplate(int i) {
		return origTemplates[i];
	}
	
	int getNumOrigTemplates() {
		return numBasicTemplates+numLogInterTemplates+numLinInterTemplates;
	}
	
	Template **getBasicTemplates() {
		return basicTemplates;
	}
	
	int getNumBasicTemplates() {
		return numBasicTemplates;
	}
	
	int getNumLogInterTemplates() {
		return numLogInterTemplates;
	}
	
	int getNumLinInterTemplates() {
		return numLinInterTemplates;
	}

	/* 0 .. basic template, 1 .. log interpol template, 2 .. lin interpol template
	 * templateIndices are ordered as follows
	 * basic templates (B) & log-interpolations (L) form a continous sequence, i
	 * e.g., if 3 log-interp. templates: sequence 0(B), 1(L), 2(L), 3(L), 4(B), ...
         */		
	int getTemplateKind(int templateIndex) {
               
                if (templateIndex<numBasicTemplates+numLogInterTemplates) {
                    if (templateIndex%(templateOption->getLogInterpolation()+1)==0)
		       return 0;
		    else
		       return 1;   
		}
		return 2;														
	}
	
	/* returns the g factor (or 1-g if g<0.5), see Feldmann et al. 2006 
         */		
	double getBasicTemplateWeight(int templateIndex) {
	
		double weight;

		if (numLogInterTemplates==0 && numLinInterTemplates==0)
                  weight=1.;
		else {
                  if (templateIndex < numBasicTemplates+numLogInterTemplates) {
                    int index=templateIndex%(templateOption->getLogInterpolation()+1);
                    weight=index/(templateOption->getLogInterpolation()+1.0);
		  } else {
                    int index=(templateIndex-numBasicTemplates-numLogInterTemplates)%(templateOption->getLinInterpolation());
		    weight=(index+1.0)/(templateOption->getLinInterpolation()+1.0);
		  }
                  if (weight<0.5)
                    weight=1-weight;
		}
		return weight;		
	}
	
	// creates basic templates 
	// they are stored in origTemplateBin
	void createBasicTemplates();	
	
	// creates log-interpolated templates
	void createLogInterTemplates();			
	
	// creates lin-interpolated templates
	void createLinInterTemplates();		
	
	// fills origTemplate array with created basic & interpolated templates
	void fillOrigTemplates();		
	
	void createTemplates() {
		deleteBasicTemplates();
		deleteLogInterTemplates();
		deleteLinInterTemplates();				
			
		createBasicTemplates();
		createLogInterTemplates();
		createLinInterTemplates();		
		fillOrigTemplates();			
	}	
	
	// forces log-interpolated templates to be calculated from basic templates
	// whenever they are used in the future
	void resetLogInterTemplates() {
		for (int i=0;i<numLogInterTemplates;i++) 
			logInterTemplates[i]->unload();		
	}

	// forces log-interpolated templates to be calculated from basic templates
	// whenever they are used in the future
	void resetLinInterTemplates() {
		for (int i=0;i<numLinInterTemplates;i++) 
			linInterTemplates[i]->unload();		
	}
	
	void printTemplateTable();
	
	void writeAll(const char *path, const char *tag);				
	
	void writeBasic(const char *path, const char *tag);		
	
};

// this class takes control over all templates
// after creating them this class keeps track of each template and 
// finally deletes them

class TemplateContainer
{
private:
	int numTemplateBins;		
	TemplateBin ** templateBins;
	TemplateBin * origTemplateBin;	// contains basic + interpolated templates
	
	OutputHandler *m_out;		

	TemplateOption *templateOption;			
		
public:	

	TemplateContainer() {
				
		numTemplateBins=0;		
		templateBins=NULL;
		origTemplateBin=NULL;
		m_out=NULL;								
		templateOption=NULL;		
	}
	
	~TemplateContainer() {		
		if (templateBins) {
			for (int i=0;i<numTemplateBins;i++) {
				if (templateBins[i])
					delete templateBins[i];
			}
			freeArray1(templateBins);				
		}
		if (origTemplateBin) 
			delete origTemplateBin;			
		if (templateOption)
			delete templateOption;
	}		
	
	void setOutputHandler(OutputHandler *m_out) {
		this->m_out=m_out;
	}		
	
	void setOptions(TemplateOption *templateOption) {
		this->templateOption = new TemplateOption();
		this->templateOption->copy(templateOption);
	}				
	
	int getMinIndex(int binIndex) {
		int count;	
		if (binIndex==0)
			return 0;
		count=origTemplateBin->getNumOrigTemplates();
		for (int i=0;i<binIndex-1;i++) {			
			count+=templateBins[i]->getNumOrigTemplates();
		}
		return count;
	}
	
	double getMinPhysicalTemplateCode(int binIndex) {
		return 0.;		
	}
	
	double getMaxPhysicalTemplateCode(int binIndex) {
		if (binIndex==0)
			return origTemplateBin->getNumBasicTemplates()-1.;	
		else
			return templateBins[binIndex-1]->getNumBasicTemplates()-1.;
	}
	
	int getMinTemplateCode(int binIndex) {
		return 0;
	}
	
	int getMaxTemplateCode(int binIndex) {
		if (binIndex==0)
			return origTemplateBin->getNumOrigTemplates()-1;	
		else
			return templateBins[binIndex-1]->getNumOrigTemplates()-1;	
	}
	
	int getNumOrigTemplates() {
		return origTemplateBin->getNumOrigTemplates();
	}	
	
	int getNumOrigTemplates(int binIndex) {
		if (binIndex==0)
			return getNumOrigTemplates();
		else
			return templateBins[binIndex-1]->getNumOrigTemplates();
	}
	
	Template** getOrigTemplates() {
		return origTemplateBin->getOrigTemplates();
	}
	
	Template* getOrigTemplate(int i) {
		return origTemplateBin->getOrigTemplate(i);
	}
	
	Template* getTemplate(int binIndex, int i) {
		return templateBins[binIndex]->getOrigTemplate(i);
	}
	
	int getNumAllTemplates() {
		int count=origTemplateBin->getNumOrigTemplates();
		for (int i=0;i<numTemplateBins;i++) {
			count+=templateBins[i]->getNumOrigTemplates();
		}
		return count;
	}
		
	Template *getAllTemplate(int i) {
		int aux=i;
		if (aux<origTemplateBin->getNumOrigTemplates()) { // template in origTemplateBin
			return 	origTemplateBin->getOrigTemplate(aux);
		} 
		aux-=origTemplateBin->getNumOrigTemplates();		
		for (int j=0;j<numTemplateBins;j++) {
			if (aux<templateBins[j]->getNumOrigTemplates()) {
				return templateBins[j]->getOrigTemplate(aux);
			}
			aux-=templateBins[j]->getNumOrigTemplates();
		}
		return NULL;
	}	
					
	int getNumBasicTemplates() {
		return origTemplateBin->getNumBasicTemplates();
	}
	
	Template ** getBasicTemplates() {
		return origTemplateBin->getBasicTemplates();
	}
			
	int getNumTemplateBins() {
		return numTemplateBins;
	}		
			
	TemplateBin **getTemplateBins() {		
		return templateBins;
	}
				
	// copies templates from the origTemplateBin into all other bins of templateBins
	void createTemplateBins(int numTemplateBins, double *zmin, double *zmax);
	
	void resetLogInterTemplates(int binIndex) {
		if (binIndex==0)
			return;	// templates in origTemplateBin are never changed
		templateBins[binIndex-1]->resetLogInterTemplates();
	}

	void resetLinInterTemplates(int binIndex) {
		if (binIndex==0)
			return;	// templates in origTemplateBin are never changed
		templateBins[binIndex-1]->resetLinInterTemplates();
	}
	
	// creates origTemplates
	// according to the given parameters (i.e. especially according to interpolation scheme)
	void createTemplates() {
		if (! templateOption)
			EXIT("templateOption==NULL in class TemplateContainer");
		origTemplateBin = new TemplateBin(templateOption);
		origTemplateBin->setOutputHandler(m_out);
		origTemplateBin->createTemplates();
	}
	
	// prints a table of the contained templates
	void printTemplateTable() {
		origTemplateBin->printTemplateTable();
		for (int i=0;i<numTemplateBins;i++)
			templateBins[i]->printTemplateTable();
	}		
	
	void writeAll(const char *path) {
		origTemplateBin->writeAll(path,"orig");
		for (int i=0;i<numTemplateBins;i++)
			templateBins[i]->writeAll(path,"corr");			
	}
	
	void writeBasic(const char *path, const char *tag) {		
		char s[MAX_LENGTH];
		origTemplateBin->writeBasic(path,"orig");
		for (int i=0;i<numTemplateBins;i++) {
			sprintf(s,"corr%s",tag);
			templateBins[i]->writeBasic(path,s);			
		}
	}
	
	/* Indices:
	 * templateIndexGlobal: .. int, starts with 0 and increases until last template in last redshift bin
	 * templateIndex:       .. int, starts with 0 and increases until last template in orig bin
	 *                         ordering are 0 .. numBasicTemplates+numLogInterTemplates for basic+log-interp. templates,
	 *                         i.e., ordering is 0(B), 1(L), 2(L), 3(L), 4(B) for 2 basic(B) and 3-log-interp. templates
	 *                         and then numLogInterTemplates linear templates
	 * templateIndexBasic:     int, starts with 0 and increases 1 for each basic template
	 * physicalTemplateCode: .. double, runs from 0.0 (first basic template) to (numbasicTemplate-1.0)
	 *                          interpolated templates have non-integer values
	 */ 			
	
	// compute ``physical'' template index; basic templates have integer index, interp. templates have fractional index
	void getPhysicalTemplateCode_Orig(int binIndex, double * templateCode) {		
		int minPhysicalTemplateCode=0;
                
		TemplateBin *tb;
		
		if (binIndex==0)			
		       tb = origTemplateBin;
		else
		       tb = templateBins[binIndex-1];
		
		if (origTemplateBin->getNumOrigTemplates()>0)
		  templateCode[0] = minPhysicalTemplateCode;

                if (origTemplateBin->getNumOrigTemplates()>1) {
		         int maxPhysicalTemplateCode=tb->getNumBasicTemplates()-1;				

			 // basic + log templates 
			 int numBasicAndInterTemplates = tb->getNumBasicTemplates()+tb->getNumLogInterTemplates();
		         double delta=(maxPhysicalTemplateCode-minPhysicalTemplateCode)/(numBasicAndInterTemplates-1.0);
                         for (int i=0;i<numBasicAndInterTemplates; i++) {
			   templateCode[i]=minPhysicalTemplateCode+delta*i;				
		         }

			 // (basic +) lin templates
			 numBasicAndInterTemplates = tb->getNumBasicTemplates()+tb->getNumLinInterTemplates();
		         delta=(maxPhysicalTemplateCode-minPhysicalTemplateCode)/(numBasicAndInterTemplates-1.0);
			 int iLin = tb->getNumBasicTemplates()+tb->getNumLogInterTemplates(); // index of first lin. template
			 for (int i=0;i<numBasicAndInterTemplates; i++) {
			   if ( i%(templateOption->getLinInterpolation()+1) ) { // skip over basic templates
			     templateCode[iLin]=minPhysicalTemplateCode+delta*i;
			     iLin++;
			   }
		         }
		}
	}

	// projects to the original template bin
	int typeOfTemplate(int templateIndexGlobal) {
	        int templateIndex = templateIndexGlobal;
		if (templateIndex<origTemplateBin->getNumOrigTemplates()) 
			return templateIndex;
		templateIndex-=origTemplateBin->getNumOrigTemplates();
		for (int i=0;i<this->numTemplateBins;i++) {			
			if (templateIndex<templateBins[i]->getNumOrigTemplates()) 
				return templateIndex;
			templateIndex-=templateBins[i]->getNumOrigTemplates();
		}
		return -1;		
	}
	
	int binOfTemplate(int templateIndexGlobal) {
	        int templateIndex = templateIndexGlobal;
		if (templateIndex<origTemplateBin->getNumOrigTemplates()) 
			return 0;
		templateIndex-=origTemplateBin->getNumOrigTemplates();
		for (int i=0;i<this->numTemplateBins;i++) {			
			if (templateIndex<templateBins[i]->getNumOrigTemplates()) 
				return (i+1);
			templateIndex-=templateBins[i]->getNumOrigTemplates();
		}
		return -1;		
	}
				
	// returns the closest basic type to the type with orig type number		
	// the convention is that for an odd number of interpolations the middle
	// belongs to the basic template with the higher index	
	int getNearestBasicTemplateCode(int templateIndexGlobal) {
		
		int basicType1,basicType2;	
		getBasicTemplateCode(templateIndexGlobal, basicType1, basicType2);
		return basicType1;
	}
 		
	// returns the second closest basic type to the type with orig type number
	// only returns a sensible answer (>0) if templateIndexGlobal corresponds to interp. template
	int getSecondNearestBasicTemplateCode(int templateIndexGlobal) { 
		 
		int basicType1,basicType2;	
		getBasicTemplateCode(templateIndexGlobal, basicType1, basicType2);
		return basicType2;
	}
									
	/* returns the closest and second closest basic type for a given, possibly interpolated, template
	 * the return indices are of type templateIndexBasic */	
	void getBasicTemplateCode(int templateIndexGlobal, int &basicType1, int &basicType2) {	
		int aux;
		int interpolMode;
		int numBasicTemplates;
		int binIndex=binOfTemplate(templateIndexGlobal);
		int templateIndex=typeOfTemplate(templateIndexGlobal);
		int numLogInterpolations=templateOption->getLogInterpolation();
		int numLinInterpolations=templateOption->getLinInterpolation();		
		
		TemplateBin *tb;
		
		if (binIndex==0)			
		       tb = origTemplateBin;
		else
		       tb = templateBins[binIndex-1];
		
		numBasicTemplates=tb->getNumBasicTemplates();
		
		if (numLogInterpolations==0 && numLinInterpolations==0) {
			basicType1=templateIndex;
			basicType2=-1;
		} else if (templateIndex < tb->getNumBasicTemplates()+tb->getNumLogInterTemplates()) {
		   // templateIndex corresponds to basic or log. interp. template
		   basicType1 = (templateIndex + (numLogInterpolations+1)/2 ) / (numLogInterpolations+1);
		   if (templateIndex>basicType1*(numLogInterpolations+1))
			basicType2=basicType1+1;
		   else if (templateIndex<basicType1*(numLogInterpolations+1)) 
			basicType2=basicType1-1;
		   else
			basicType2=-1;	
		} else { 
		  // templateIndex corresponds to a linearly interpolated template; cannot be a basic template
		  int auxIndex = templateIndex - tb->getNumBasicTemplates() - tb->getNumLogInterTemplates();
		  basicType1 = (auxIndex + (numLinInterpolations+1)/2 ) / (numLinInterpolations);
		  if (auxIndex>=basicType1*numLinInterpolations)
			basicType2=basicType1+1;
		   else  
			basicType2=basicType1-1;
		}	
	}
	
	// tells whether templateIndexGlobal number corresponds to a basic type
	int getTemplateKind(int templateIndexGlobal) {		
		int binIndex=binOfTemplate(templateIndexGlobal);
		int templateIndex=typeOfTemplate(templateIndexGlobal);										
		if (binIndex==0)				
			return origTemplateBin->getTemplateKind(templateIndex);
		else 
			return templateBins[binIndex-1]->getTemplateKind(templateIndex);
	}
											
	// returns the weight assuming 2-point log-interpolation for the nearest basic-template
	void getBasicTemplateWeight(int templateIndexGlobal,double &weight) {
		int binIndex=binOfTemplate(templateIndexGlobal);
		int templateIndex=typeOfTemplate(templateIndexGlobal);		
		if (binIndex==0) 
			weight=origTemplateBin->getBasicTemplateWeight(templateIndex);
		else
			weight=templateBins[binIndex-1]->getBasicTemplateWeight(templateIndex);
	}
						
};

#endif //_TEMPLATE_H_
