#ifndef _ALLOCARRAY_H_
#define _ALLOCARRAY_H_

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

template <class T>
inline void allocArray1(T* &field, long size) {
  
  if (size<=0) {
    field=NULL;
    return;
  }
  
  field=(T*)malloc(size*sizeof(T));
  if (! field) {
    fprintf(stderr,"\nCould not allocate enough heap memory for class 1 array! Abort!\n");   
    exit(EXIT_FAILURE);
  }  
}    

template <class T>
inline void callocArray1(T* &field, long size) {
  
  if (size<=0) {
    field=NULL;
    return;  
  }
  
  field=(T*)calloc(size,sizeof(T));
  if (! field) {
    fprintf(stderr,"\nCould not allocate enough heap memory for class 1 array! Abort!\n");   
    exit(EXIT_FAILURE);
  }  
}   

template <class T>
inline void freeArray1(T* &field) {
  if (field)
  	free(field);
  field=NULL;
}

template <class T>
inline void allocArray2(T** &field, long size, int dim) {
long i;
  
  if (size<=0) {
    field=NULL;
    return;  
  }
  
  field=(T**)malloc(size*sizeof(T*));
  if (field==NULL) {
    fprintf(stderr,"\nCould not allocate enough heap memory for class 2 array! Abort!\n");
    exit(EXIT_FAILURE);
  }
  
  if (dim<=0) {    
    for (i=0;i<size;i++) {
      field[i]=NULL;     
    }
    return;
  } 
      
  field[0]=(T*)malloc(size*dim*sizeof(T));   
  if (field[0]==NULL) { 
    free(field); 
    fprintf(stderr,"\nCould not allocate enough heap memory for class 2 array! Abort!\n");
    exit(EXIT_FAILURE);
  }
          
  for (i=1;i<size;i++) {
    field[i]=field[0]+i*dim;     
  }
}  

template <class T>
inline void callocArray2(T** &field, long size, int dim) {
long i;

  if (size<=0) {
    field=NULL;
    return; 
  }
  
  field=(T**)calloc(size,sizeof(T*));
  if (field==NULL) {
    fprintf(stderr,"\nCould not allocate enough heap memory for class 2 array! Abort!\n");
    exit(EXIT_FAILURE);
  }
  
  if (dim<=0) {    
    for (i=0;i<size;i++) {
      field[i]=NULL;     
    }
    return;
  }
  
  field[0]=(T*)calloc(size*dim,sizeof(T));   
  if (field[0]==NULL) { 
    free(field); 
    fprintf(stderr,"\nCould not allocate enough heap memory for class 2 array! Abort!\n");
    exit(EXIT_FAILURE);
  }
        
  for (i=1;i<size;i++) {
    field[i]=field[0]+i*dim;     
  }
}

template <class T>
inline void freeArray2(T** &field,long size) {
  if (field) { 
    if (field[0]) {
      free(field[0]);
      field[0]=NULL;
    }
    free(field);
    field=NULL;
  }
} 

template <class T>
inline void allocArray3(T*** &field, long s1, long s2, long s3) {
long i,j;
  
  if (s1<=0) {
    field=NULL;
    return;
  }
  
  field=(T***)malloc(s1*sizeof(T**));
  if (field==NULL) {
    fprintf(stderr,"\nCould not allocate enough heap memory for class 3 array! Abort!\n");
    exit(EXIT_FAILURE);
  }
  
  if (s2<=0) {    
    for (i=0;i<s1;i++) {
      field[i]=NULL;     
    }
    return;
  }  
  
  field[0]=(T**)malloc(s1*s2*sizeof(T*));   
  if (field[0]==NULL) { 
    free(field); 
    fprintf(stderr,"\nCould not allocate enough heap memory for class 2 array! Abort!\n");
    exit(EXIT_FAILURE);
  }
  
  if (s3<=0) {    
    for (i=0;i<s1;i++)
      for (j=0;j<s2;j++)
        field[i][j]=NULL;    
    return;
  }
  
  field[0][0]=(T*)malloc(s1*s2*s3*sizeof(T));   
  if (field[0][0]==NULL) { 
    free(field[0]);
    free(field);
    fprintf(stderr,"\nCould not allocate enough heap memory for class 2 array! Abort!\n");
    exit(EXIT_FAILURE);
  }
  
  for (i=0;i<s1;i++) {
    field[i]=field[0]+i*s2;
    field[i][0]=field[0][0]+i*s2*s3;
    for (j=1;j<s2;j++) {
      field[i][j]=field[i][0]+j*s3;
    }
  }    
}

template <class T>
inline void callocArray3(T*** &field, long s1, long s2, long s3) {
long i,j;

  if (s1<=0) {
    field=NULL;
    return;
  }
  
  field=(T***)calloc(s1,sizeof(T**));
  if (field==NULL) {
    fprintf(stderr,"\nCould not allocate enough heap memory for class 3 array! Abort!\n");
    exit(EXIT_FAILURE);
  }
  
  if (s2<=0) {    
    for (i=0;i<s1;i++) {
      field[i]=NULL;     
    }
    return;
  }  
  
  field[0]=(T**)calloc(s1*s2,sizeof(T*));   
  if (field[0]==NULL) { 
    free(field); 
    fprintf(stderr,"\nCould not allocate enough heap memory for class 2 array! Abort!\n");
    exit(EXIT_FAILURE);
  }
  
  if (s3<=0) {    
    for (i=0;i<s1;i++)
      for (j=0;j<s2;j++)
        field[i][j]=NULL;    
    return;
  }
  
  field[0][0]=(T*)calloc(s1*s2*s3,sizeof(T));   
  if (field[0][0]==NULL) { 
    free(field[0]);
    free(field);
    fprintf(stderr,"\nCould not allocate enough heap memory for class 2 array! Abort!\n");
    exit(EXIT_FAILURE);
  }
  
  for (i=0;i<s1;i++) {
    field[i]=field[0]+i*s2;
    field[i][0]=field[0][0]+i*s2*s3;
    for (j=1;j<s2;j++) {
      field[i][j]=field[i][0]+j*s3;
    }
  }  
  
}

template <class T>
inline void freeArray3(T*** &field,long s1,long s2) {
   if (field) {
     if (field[0]) {
       if (field[0][0]) {
         free(field[0][0]);
	 field[0][0]=NULL;
       }
       free(field[0]);
       field[0]=NULL;
     }
     free(field);
     field=NULL;
   }
}     


#endif //_ALLOCARRAY_H_
