#ifndef _GLOBHEADER_H_
#define _GLOBHEADER_H_

#include <stdlib.h>
#include <string.h>

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

// zeropoint of AB magnitude system
#define ZEROPOINT 48.6

// integration of luminosity distance (in z space)
#define INTPOINTSDIFF 0.0001

// definition of " relative magnitude errors"
// 1 .. using P.Capak's definition of delta(mag) 
// 0 .. using a different one
#define MAGERRTYPE 1

// Possible names of the K-band (needed for Zamorani's K-band correction)
#define K_BAND_NAME1 "flamingos_Ks.res"
#define K_BAND_NAME2 "Ks_rf.res"

// name of program
#define PHOTOZ "zebra"
#define PHOTOZFULL "Zurich's Extra-galactic Bayesian Redshift Analyzer"

// version number (the string should be 21 characters long)
#define VERSION "1.10 (9/Feb/13)      "

// paper reference or e-mail
#define REFERENCE "zebra@phys.ethz.ch   "

// --- internal parameter ---

// if this variable is defined more run time information is printed
// used to find bottlenecks in the calculation
//#define MEASTIME 

// if this variable is set ZEBRA stores flux in 3-dim pointer array
// if not set ZEBRA stores flux in 1D-array (maybe faster, but untested)
#define PARRAY

// if this variable is defined, the verbosity is increased in the
// core routines (more if's). This of course slows down the code noticable
//#define FULLVERBOSE

// minimal number of filterbands (if there are less than the code
// refuses to make an estimate)
#define MINFILTERBANDS 3

// forward or backward iterating for the prior
// don't use this - just for testing purposes
#define INVERSEETA 0

// whether eta is saved in the prior calculation
#define SAVEETA 1

//minimal and maximal redshift of templates (default)
#define TEMPLATE_MIN_Z 0
#define TEMPLATE_MAX_Z 10

// the number of digits for redshifts in percentile statements
// used in giving the likelihood, prior, posterior
#define PERCENTILE_PRECISION 5

// number of catalog lines read simultaneously into memory
#define NUMBUNCHLINES 5000

// precision of the flux/error entries in the saved catalog file
#define OUTPUTPRECISION 6

// maximal string length
#define MAX_LENGTH 512

// column width in output files
#define MYSETW 13

//#if _GNU_SOURCE
#if 1
  // instead of non-portable exp10(x) use exp(LN10*x)
  #define LN10 2.3025851

  // this is the actual macro substituting exp10
  #define RFEXP10( val ) exp(LN10*(val))
#else
  #define RFEXP10( val ) exp10(val)
#endif

// add elapsed time differences
#define TIMEADD( val ) val += ( float ) ( ( end.tms_utime - start.tms_utime + \
                                         end.tms_stime - start.tms_stime  ) / 100.0 )
#define TIME( val ) val = ( float ) ( ( end.tms_utime - start.tms_utime + \
                                         end.tms_stime - start.tms_stime  ) / 100.0 )

#define TIMETOTAL( val ) val = ( float ) ( ( endTotal.tms_utime - startTotal.tms_utime + \
                                         endTotal.tms_stime - startTotal.tms_stime  ) / 100.0 )
         
#define CHECK( file, filename ) \
	if (! file.good()) { \
		cerr << "\n** Error: Could not open file " << filename << endl; \
		exit(1); \
	}              
	
#define EXIT( output ) \
	{ cerr << "\n**Error: " << output << endl; exit(1); }
	
#define WARNING( output ) \
	{ cerr << "\n**Warning: " << output << endl; }
	
#define WARNINGCONT( output ) \
	{ cerr << "** " << output << endl; }

#endif //_GLOBHEADER_H_
