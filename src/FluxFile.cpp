#include "FluxFile.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

#ifdef PARRAY
void FluxFile::load(double*** flux,int x, int y, int z)
{
	char s[MAX_LENGTH];
	read();
	
	// first line has to have 3 entries (the length of x, y and z)
	if (this->entrynumber[0]!=3) {		
		sprintf(s,"File %s has not the right format", this->filename);
		EXIT(s);
	}
	
	// check whether x y z have right length
	if (atoi(this->entries[0][0])!=x) {
		sprintf(s,"File %s has not the right format", this->filename);
		EXIT(s);		
	}	
	if (atoi(this->entries[0][1])!=y) {
		sprintf(s,"File %s has not the right format", this->filename);
		EXIT(s);		
	}
	if (atoi(this->entries[0][2])!=z) {
		sprintf(s,"File %s has not the right format", this->filename);
		EXIT(s);	
	}
	if (this->linenumber<x) {
		sprintf(s,"File %s has not the right format", this->filename);
		EXIT(s);	
	}
	
	// fill flux with entries
	// format: the values are just stored one after the other
	// usually each line contains all values for a given template
	int i=1;
	int j=0;
	for (int t=0;t<x;t++) {	
		for (int zi=0;zi<y;zi++) {
			for (int f=0;f<z;f++) {
				flux[t][zi][f]=atof(this->entries[i][j]);
				j++;
				if (j>=this->entrynumber[i]) {
					j=0;
					i++;
				}
			}
		}
	}		
}	 

void FluxFile::loadStepByStep(double*** flux,int x, int y, int z)
{
	ifstream infile;
	char s[MAX_LENGTH];
			
	resize();
	totallinenumber=0;
	totalheaderlinenumber=0;
	totalcolnumber=0;
			
	infile.open(this->filename);		
	if (! infile.good())
	{
		sprintf(s,"Could not open file %s",filename);
		EXIT(s);
	}
	
	std::string str;	
		
	//skip initial comment lines &
	//read header line
	int xx,yy,zz;
	do {
	  if (infile.good()) {
	    getline(infile,str);
	  } else {
	    std::cout << "problem while reading FluxFile! - It seems to contain no header and data lines" << std::endl;
	    exit(1);
	  }
	} while (str.find('#')!=string::npos);
	std::stringstream ss(str);
	ss >> xx >> yy >> zz;
				
	if(xx!=x || yy!=y || zz!=z) {
		cout << "FluxFile has wrong format! please check nr. of filters/templates/redshiftbins!" << endl;
		exit(1);
	}
	
	// fill flux with entries
	// format: the values are just stored one after the other
	// usually each line contains all values for a given template
	for (int t=0;t<x;t++) {	
		for (int zi=0;zi<y;zi++) {
			for (int f=0;f<z;f++) {
				infile >> flux[t][zi][f];
			}
		}
		if(! infile.good()) {
			cout << "problem while reading FluxFile!" << endl;
			exit(1);
		}
	}
	
				
	infile.clear();
	infile.close();
};

void FluxFile::save(double*** flux,int x, int y, int z)
{
	char s[MAX_LENGTH];
	ofstream ofile;
	ofile.open(this->filename);
	ofile.precision(8);
	if (! ofile.good())	{ // error while opening file
		sprintf(s,"Could not save file %s", this->filename);
		EXIT(s);	
	}
	ofile << "# Contains flux[template][red-shift][filter] data (eq. A3 in Feldmann et al 2006) in the following way" << endl;
	ofile << "# The first line prints number of templates T, number of redshifts Z and number of filters F" << endl;
	ofile << "# Each further lines stores the flux of one template as function of redshift and filter band" << endl; 
	ofile << "# The order corresponds to the order of active templates in the template configuration file" << endl;	
	ofile << "# The ith column (starting with 0) in each line corresponds to redshift index i/F (integer division) and " << endl;
	ofile << "# Filter index i%F, i.e. the filter indices are the fastest running indices." << endl; 
	ofile << x << " " << y << " " << z << endl;	
	for (int t=0;t<x;t++) {	
		for (int zi=0;zi<y;zi++) 
			for (int f=0;f<z;f++) 
				ofile << flux[t][zi][f] << " ";			
		ofile << endl;		
	}		
	ofile.close();
	ofile.clear();

}
#else
void FluxFile::load(double* flux,int x, int y, int z)
{
	char s[MAX_LENGTH];
	read();
	
	// first line has to have 3 entries (the length of x, y and z)
	if (this->entrynumber[0]!=3) {		
		sprintf(s,"File %s has not the right format", this->filename);
		EXIT(s);
	}
	
	// check whether x y z have right length
	if (atoi(this->entries[0][0])!=x) {
		sprintf(s,"File %s has not the right format", this->filename);
		EXIT(s);		
	}	
	if (atoi(this->entries[0][1])!=y) {
		sprintf(s,"File %s has not the right format", this->filename);
		EXIT(s);		
	}
	if (atoi(this->entries[0][2])!=z) {
		sprintf(s,"File %s has not the right format", this->filename);
		EXIT(s);	
	}
	if (this->linenumber<x) {
		sprintf(s,"File %s has not the right format", this->filename);
		EXIT(s);	
	}
	
	// fill flux with entries
	// format: the values are just stored one after the other
	// usually each line contains all values for a given template
	int i=1;
	int j=0;
	for (int ii=0;ii<x*y*z;ii++) {
	  flux[ii]=atof(this->entries[i][j]);
	  j++;
	  if (j>=this->entrynumber[i]) {
	    j=0;
	    i++;
	  }
	}			
}	 

void FluxFile::loadStepByStep(double* flux,int x, int y, int z)
{
	ifstream infile;
	char s[MAX_LENGTH];
			
	resize();
	totallinenumber=0;
	totalheaderlinenumber=0;
	totalcolnumber=0;
			
	infile.open(this->filename);		
	if (! infile.good())
	{
		sprintf(s,"Could not open file %s",filename);
		EXIT(s);
	}
	
	std::string str;	
	
	//skip initial comment lines &
	//read header line
	int xx,yy,zz;
	do {
	  if (infile.good()) {
	    getline(infile,str);
	  } else {
	    std::cout << "problem while reading FluxFile! - It seems to contain no header and data lines" << std::endl;
	    exit(1);
	  }
	} while (str.find('#')!=string::npos);
	std::stringstream ss(str);
	ss >> xx >> yy >> zz;
				
	if(xx!=x || yy!=y || zz!=z) {
		cout << "FluxFile has wrong format! please check nr. of filters/templates/redshiftbins!" << endl;
		exit(1);
	}	
	
	// fill flux with entries
	// format: the values are just stored one after the other
	// usually each line contains all values for a given template
	for (int ii=0;ii<x*y*z;ii++) {			
	  infile >> flux[ii];				  
	  if((ii+1)%(y*z)==0 && (! infile.good())) {
	    cout << "problem while reading FluxFile!" << endl;
	    exit(1);
	  }
	  
	}
					
	infile.clear();
	infile.close();
};

void FluxFile::save(double* flux,int x, int y, int z)
{
	char s[MAX_LENGTH];
	ofstream ofile;
	ofile.open(this->filename);
	ofile.precision(8);
	if (! ofile.good())	{ // error while opening file
		sprintf(s,"Could not save file %s", this->filename);
		EXIT(s);	
	}
	ofile << "# Contains flux[template][red-shift][filter] data (eq. A3 in Feldmann et al 2006) in the following way" << endl;
	ofile << "# The first line prints number of templates T, number of redshifts Z and number of filters F" << endl;
	ofile << "# Each further lines stores the flux of one template as function of redshift and filter band" << endl; 
	ofile << "# The order corresponds to the order of active templates in the template configuration file" << endl;	
	ofile << "# The ith column (starting with 0) in each line corresponds to redshift index i/F and " << endl;
	ofile << "# Filter index i%F, i.e. the filter indices run the fastest." << endl; 
	ofile << x << " " << y << " " << z << endl;
	for (int ii=0;ii<x*y*z;ii++) {		
	  ofile << flux[ii] << " ";			
	  if ((ii+1)%(y*z)==0)
	    ofile << endl;		
	}
	ofile.close();
	ofile.clear();
}
#endif

void PriorFile::load(double** prior)
{
	double *z;
	int numZ;
	char s[MAX_LENGTH];
	
	z=m_binZ->getZ();
	numZ=m_binZ->getLength();
	
	read();	
	
	// first line has to have 2 entries (the length of x, y)
	if (this->entrynumber[0]!=2) {
		sprintf(s,"File %s has not the right format", this->filename);
		EXIT(s);	
	}
	
	// check whether entries have right length
	if (atoi(this->entries[0][0])!=numTemplates) {
		sprintf(s,"File %s has not the right format", this->filename);
		EXIT(s);
	}
	if (atoi(this->entries[0][1])!=numZ) {
		sprintf(s,"File %s has not the right format", this->filename);
		EXIT(s);
	}
	if ( (this->linenumber-1) != numTemplates*numZ ) {
		sprintf(s,"File %s has not the right format", this->filename);
		EXIT(s);
	}
	if (this->colnumber!=3) {
		sprintf(s,"File %s has not the right format", this->filename);
		EXIT(s);
	}		
	
	// fill prior with entries
	// format: the values are just stored one after the other
	// usually each line contains all values for a given template	
	for (int t=0;t<numTemplates;t++) {	
		for (int zi=0;zi<numZ;zi++) {			
			#if 0
			if (atof(this->entries[t*numZ+zi+1][1])!=z[zi]) {
				sprintf(s,"File entry %s is not equal to %g in file %s", this->entries[t*numZ+zi+1][1],z[zi],
						this->filename);
				EXIT(s);
			}
			if (atof(this->entries[t*numZ+zi+1][0])!=t) {
				sprintf(s,"File entry %s is not equal to %d in file %s", this->entries[t*numZ+zi+1][0],t,
						this->filename);
				EXIT(s);
			}
			#endif
			prior[t][zi]=atof(this->entries[t*numZ+zi+1][2]);
		}
	}												
}	 

void PriorFile::save(double** prior)
{
	double *z;
	int numZ;
	char s[MAX_LENGTH];
	
	z=m_binZ->getZ();
	numZ=m_binZ->getLength();
	
	ofstream ofile;
	ofile.open(this->filename);
	
	ofile.precision(8);
	if (! ofile.good())	{ // error while opening file
		sprintf(s,"Could not save file %s", this->filename);
		EXIT(s);	
	}
		
	ofile << "# template redshift prior[template][redshift]" << endl;	
	ofile << numTemplates << " " << numZ << endl;
	for (int t=0;t<numTemplates;t++) 
		for (int zi=0;zi<numZ;zi++) 		
			ofile <<  t << " " << z[zi] << " " << prior[t][zi] << endl;						
	ofile.close();
	ofile.clear();
}

void PriorFile::load1D(double* prior)
{
	double *z;
	int numZ;
	char s[MAX_LENGTH];
	
	z=m_binZ->getZ();
	numZ=m_binZ->getLength();
	
	read();	
	
	// first line has to have 1 entries (the length of x)
	if (this->entrynumber[0]!=1) {
		sprintf(s,"File %s has not the right format", this->filename);
		EXIT(s);	
	}
	
	// check whether entries have right length	
	if (atoi(this->entries[0][0])!=numZ) {
		sprintf(s,"File %s has not the right format", this->filename);
		EXIT(s);
	}
	if ( (this->linenumber-1) != numZ ) {
		sprintf(s,"File %s has not the right format", this->filename);
		EXIT(s);
	}
	
	if (this->colnumber!=2) {
		sprintf(s,"File %s has not the right format", this->filename);
		EXIT(s);
	}		
	
	// fill prior with entries
	// format: the values are just stored one after the other		
	for (int zi=0;zi<numZ;zi++) {					
		prior[zi]=atof(this->entries[zi+1][1]);
	}												
}	 

void PriorFile::save1D(double* prior)
{
	double *z;
	int numZ;
	char s[MAX_LENGTH];
	
	z=m_binZ->getZ();
	numZ=m_binZ->getLength();
	
	ofstream ofile;
	ofile.open(this->filename);
	
	ofile.precision(8);
	if (! ofile.good())	{ // error while opening file
		sprintf(s,"Could not save file %s", this->filename);
		EXIT(s);	
	}
		
	ofile << "# redshift prior[best_template][redshift]" << endl;	
	ofile << numZ << endl;
	for (int zi=0;zi<numZ;zi++) 		
		ofile <<  z[zi] << " " << prior[zi] << endl;						
	ofile.close();
	ofile.clear();
}

void TableFile::load()
{	
	char s[MAX_LENGTH];
	resize();
	read();
	
	#if 0	
 		cout << "linenumber=" << linenumber << " colnumber=" << colnumber << endl;
 	#endif
	
	callocArray2(body,linenumber,colnumber);
	
	for (int i=0;i<linenumber;i++) {
		if (entrynumber[i]!=colnumber) {
			sprintf(s,"Number of columns (%d instead of %d at line %d) is not constant in file %s",
					entrynumber[i],colnumber,i,filename);
			WARNING(s);
		}		
		for (int j=0;j<entrynumber[i];j++)
			body[i][j]=atof(entries[i][j]);
	}			
}

void TableFile::load(int bunchlines)
{	
	char s[MAX_LENGTH];			
	
	resize();
	
	readFractionContinue(bunchlines);
	
	#if 0
		cout << "linenumber=" << linenumber << " colnumber=" << colnumber << endl;
	#endif
	
	callocArray2(body,linenumber,colnumber);
	
	for (int i=0;i<linenumber;i++) {
		if (entrynumber[i]!=colnumber) {
			sprintf(s,"Number of columns (%d instead of %d at line %d) is not constant in file %s",
					entrynumber[i],colnumber,i,filename);
			WARNING(s);
		}	
			
		for (int j=0;j<entrynumber[i];j++) {			
			body[i][j]=atof(entries[i][j]);			
		}
	}
}

void TableFile::saveAppend(char *filename)
{				
	unsigned int maxe;
	char saux[MAX_LENGTH];
	
	for (int i=0;i<linenumber;i++) {					
		// free memory of this line
		freeArray2(entries[i],entrynumber[i]);			
		// calculate maximal array length of entry in this line
		maxe=0;
		for (int j=0;j<entrynumber[i];j++) {
			sprintf(saux,"%.10g",body[i][j]);
			if (maxe<strlen(saux))
				maxe=strlen(saux);
		}				
		allocArray2(entries[i],entrynumber[i],maxe+1);	
		for (int j=0;j<entrynumber[i];j++) {
			sprintf(entries[i][j],"%.10g",body[i][j]);			
		}
	}
	
	ConfFile::saveAppend(filename);
}

