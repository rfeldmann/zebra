#include "DataSet.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/
 
DataSet::DataSet(char *name)
{
	allocArray1(this->name,strlen(name)+2);	
	allocArray1(this->shortname,strlen(name)+2);
	strcpy(this->name,name);		
	strcpy(this->shortname,name);
	head=NULL;
	comment=NULL;
	body=NULL;
	maxColValue=NULL;
	minColValue=NULL;
	m_out=NULL;
}

DataSet::DataSet(char *name, char *shortname)
{
	allocArray1(this->name,strlen(name)+2);	
	allocArray1(this->shortname,strlen(name)+2);
	strcpy(this->name,name);		
	strcpy(this->shortname,shortname);
	head=NULL;
	comment=NULL;
	body=NULL;
	maxColValue=NULL;
	minColValue=NULL;
	m_out=NULL;
}

DataSet::DataSet(DataSet *d) 
{
	allocArray1(name,strlen(d->name)+2);
	allocArray1(shortname,strlen(d->shortname)+2);	
	strcpy(name,d->name);
	strcpy(shortname,d->shortname);
	if (d->head==NULL)
		head=NULL;
	else {
		allocArray2(head,d->colnumber,MAX_LENGTH);
		for (int i=0;i<d->colnumber;i++)
			strcpy(head[i],d->head[i]);
	}
	if (d->comment==NULL)
		comment=NULL;
	else {
		allocArray2(comment,d->commentnumber,MAX_LENGTH);
		for (int i=0;i<d->commentnumber;i++)
			strcpy(comment[i],d->comment[i]);					
	}
	if (d->body==NULL)
		body=NULL;
	else {
		allocArray2(body,d->linenumber,d->colnumber);
		for (int i=0;i<d->linenumber;i++)
			for (int j=0;j<d->colnumber;j++)
				body[i][j]=d->body[i][j];
	}		
	if (d->maxColValue==NULL)
		maxColValue=NULL;
	else {
		allocArray1(maxColValue,d->colnumber);
		for (int i=0;i<d->colnumber;i++)
			maxColValue[i]=d->maxColValue[i];
	}
	if (d->minColValue==NULL)
		minColValue=NULL;
	else {
		allocArray1(minColValue,d->colnumber);
		for (int i=0;i<d->colnumber;i++)
			minColValue[i]=d->minColValue[i];
	}		
	colnumber=d->colnumber;
	linenumber=d->linenumber;
	commentnumber=d->commentnumber;
	loaded=d->loaded;
	m_out=d->m_out;
}

DataSet::~DataSet()
{
	DataSet::clear();
	free(this->name);	
	free(this->shortname);
}

int DataSet::getLineNumber()
{
	return this->linenumber;	
}

int DataSet::getColNumber()
{
	return this->colnumber;	
}

void DataSet::getColumn(double* entries, int colindex)
{
	for (int i=0;i<this->linenumber;i++)
		entries[i]=this->body[i][colindex];		
}

void DataSet::getLine(double* entries, int lineindex)
{
	for (int i=0;i<this->colnumber;i++)
		entries[i]=this->body[lineindex][i];		
}

double** DataSet::_accessBody()
{
	return this->body;
}

char** DataSet::_accessHead()
{
	return this->head;	
}

void DataSet::clear()
{
	freeArray2(this->head,this->colnumber);
	freeArray2(this->body,this->linenumber);
	freeArray2(this->comment,this->commentnumber);
	freeArray1(this->maxColValue);
	freeArray1(this->minColValue);
}

void DataSet::check(char *filename)
{
	fstream infile;
	int linenumber,colnumber,newline,oldcolnumber,commentnumber;
	char comment[MAX_LENGTH];
	char c;
	double d;
		
	infile.open (filename, fstream::in);
	if (! infile.good())
	{
		cerr << "Could not open file " << filename << endl;
		exit(0);
	}
	
	// get number of lines in file
	// linenumber is only increased for lines with data, 
	// there are finally 0..linenumber-1 lines in the file
	//
	// get number of columns within line
	// increase column number after each column
	// there are 0..colnumber-1 columns in the file 
	//
	// check whether number of columns is the same for all lines
	linenumber=0;
	colnumber=0;
	oldcolnumber=0;
	commentnumber=0;
	newline=1;	
	while (infile.get(c)) {				
		switch (c) {
			case '#':		
				infile.putback(c);
				infile.getline(comment,MAX_LENGTH,'\n');
				commentnumber++;
				newline=1;					
				break;
			case '\n':			 			
				newline=1;							
				if (linenumber > 1 && oldcolnumber!=colnumber) {
					cerr << "Number of columns not constant in file " 
						 << this->name << endl;
					cerr << "in line: " << linenumber << " column: " << colnumber << endl;
					exit(1);
				}
				oldcolnumber=colnumber;												
				break;			
			case ' ': break;
			case '\t': break;
			default: 				
				if (newline==1) { // first data in new line
					newline=0;	
					colnumber=0;					
					linenumber++;						
				}							
				infile.putback(c);
				infile >> d;
				colnumber++;										
		}
	}
	
	this->colnumber=colnumber;
	this->linenumber=linenumber;					
	this->commentnumber=commentnumber;
				
	infile.close();	
	infile.clear();		
}	
	
void DataSet::read(char *filename)
{
	fstream infile;
	int linenumber,colnumber,newline,commentnumber;
	char c;	
	double aux;
	int* firstData;
	
	// allocate enough memory	
	allocArray2(this->body,this->linenumber,this->colnumber);
	allocArray2(this->comment,this->commentnumber,MAX_LENGTH);
	allocArray1(this->maxColValue,this->colnumber);
	allocArray1(this->minColValue,this->colnumber);
	allocArray1(firstData,this->colnumber);
	
	for (int i=0;i<this->colnumber;i++) {
		maxColValue[i]=0.0;		// can only be greater 0								
		firstData[i]=1;			// minColValue is initialized in the loop					
	}
	
	// read data from file into this->body	
	infile.open (filename, fstream::in);
	if (! infile.good())
	{
		cerr << "Could not open file " << filename << endl;
		exit(0);
	}
	
	linenumber=0;
	colnumber=0;
	commentnumber=0;
	newline=1;	
	while (infile.get(c)) {				
		switch (c) {
			case '#':					// comments
				infile.putback(c);
				infile.getline(this->comment[commentnumber],MAX_LENGTH,'\n');
				commentnumber++;
				newline=1;					
				break;
			case '\n':			   		// newline
				newline=1;										
				break;					// whitespace
			case ' ': break;			// tabs
			case '\t': break;
			default: 					// otherwise is data
				if (newline==1) { // first data in new line
					newline=0;			
					colnumber=0;				
					linenumber++;
				}							
				infile.putback(c);	
				colnumber++;				
				infile >> aux;								
								
				if (firstData[colnumber-1]) {
					this->minColValue[colnumber-1]=aux;
					firstData[colnumber-1]=0;
				}
								
				if (aux>this->maxColValue[colnumber-1]) 
					this->maxColValue[colnumber-1]=aux;
				else if (aux<this->minColValue[colnumber-1])
					this->minColValue[colnumber-1]=aux;
				this->body[linenumber-1][colnumber-1]=aux;
					
		}
	}
	
	infile.close();	
	infile.clear();
	
	allocArray2(this->head,this->colnumber,MAX_LENGTH);
	for (int i=0;i<this->colnumber;i++) 
	  sprintf(this->head[i],"column_%d",i);  	
	  
	free(firstData); 	  
}

void DataSet::fill(int linenumber, int colnumber, char** head, double** body)
{
	this->linenumber=linenumber;
	this->colnumber=colnumber;
	for (int i=0;i<colnumber;i++)
		strcpy(this->head[i],head[i]);
	for (int i=0;i<linenumber;i++)
		for (int j=0;j<colnumber;j++)
			this->body[i][j]=body[i][j];			
}	

// prints name, header and columns
void DataSet::print()
{
	cout << "\n" << this->name << endl;
	cout << this->linenumber << " rows " << this->colnumber << " columns " 
		 << this->commentnumber << " commentlines" << endl;
	for (int i=0;i<this->commentnumber; i++) 
		cout << comment[i] << endl;					 
	for (int i=0;i<this->colnumber;i++)
		cout << head[i] << " ";
	cout << endl;
	for (int i=0;i<this->linenumber;i++) {
		for (int j=0;j<this->colnumber;j++)
			cout << this->body[i][j] << " ";
		cout << endl;
	}
	cout << endl;				
}
