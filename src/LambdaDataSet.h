#ifndef _LAMBDADATASET_H_
#define _LAMBDADATASET_H_

#include <cmath>
#include "DataSet.h"
#include "NumericalData.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

using namespace std;

// performes load-on-demand, ie. loads and fills "values" from file if needed
// once loaded it stays so until "unload" command 

class LambdaDataSet : public DataSet
{	
	
protected:	
	xyData *values;			// x: contains lambda
							// y: contains values of first column of body
	xyData *meshedValues;	// contains meshed version of values (coarse grid)	
	int validLowLimit; 		// low limit of valid lambda range (line index) 
	int validHighLimit;		// high limit of valid lambda range (line index)	
	int interpol; 			// whether is is a interpolated data set or
							// a "real" one (loaded from file)
	int loaded;				// flag: whether loaded	
	 	
	// writes x,y array to file	
	void writeValues(char *filename,int n, double *x, double *y); 
	
	// doing some integrations with the lamba-data set, using
	// the supporting points from data
	double getInteg(const xyData *data);
	double getIntegTimesLambda(const xyData *data);
	double getIntegTimesLambdaLambda(const xyData *data);
	double getIntegOverLambda(const xyData *data);
	double getIntegOverLambdaLambda(const xyData *data);	
	
	void calcLambdaMesh(double lmin, double lmax, double resolution, int & meshlength, double * & mesh);	
	void calcLogLambdaMesh(double lmin, double lmax, double resolution, int & meshlength, double * & mesh);
	void fillLambdaMesh(double resolution, int length, double *mesh);
	void fillLambdaMesh(double resolution, int length, double *mesh, int extend);
	
	virtual void load();
	virtual void load(char* filename);			
		
public:
	LambdaDataSet(char* filename);
	LambdaDataSet(char* filename, char* shortname);
	LambdaDataSet(LambdaDataSet *l);
	virtual ~LambdaDataSet();
	
	void unload();
	
	void setValues(xyData *new_values) {
		if (values)
			delete values;
		if (new_values) {
			values = new xyData(new_values);
			this->validLowLimit=0;
			this->validHighLimit=new_values->getlength()-1;
		} else {
			values=NULL;
			this->validLowLimit=0;
			this->validHighLimit=0;
		}
		this->loaded=1;
	}
	
	void setMeshedValues(xyData *new_values) {
		if (meshedValues)
			delete meshedValues;
		if (new_values) {
			meshedValues = new xyData(new_values);
			this->validLowLimit=0;
			this->validHighLimit=new_values->getlength()-1;
		} else {
			meshedValues=NULL;
			this->validLowLimit=0;
			this->validHighLimit=0;
		}
	}	
	
	// often used normalization factors
	double getWavelengthNormalization() { 
		if (! this->loaded) 
			this->load();
		return this->getIntegTimesLambda(values);	
	}
	double getWavelengthNormalization(const xyData *data) {	
		if (! this->loaded) 
			this->load();		
		return this->getIntegTimesLambda(data);	
	}
	double getFrequencyNormalization()
	{	
		if (! this->loaded) 
			this->load();		
		return this->getIntegOverLambda(values);	
	}
	double getFrequencyNormalization(const xyData *data)
	{	
		if (! this->loaded) 
			this->load();	
		return this->getIntegOverLambda(data);	
	}
	double getStrangeNormalization()
	{	
		if (! this->loaded) 
			this->load();
		return this->getIntegOverLambdaLambda(values);
	}
	double getStrangeNormalization(const xyData *data)
	{	
		if (! this->loaded) 
			this->load();
		return this->getIntegOverLambdaLambda(data);
	}
	int isLoaded()
	{
		return this->loaded;
	}						
						
	int isInterpol() { return interpol; };					
																						
    // given set of n xvalues (stored in *x) obtain 
    // n interpol. y values (then stored in *y) [using splines]
	void getInterpolValues(int n,const double *x, double *y); 
												
	// same as above but if only one value is needed [using splines]									
	double getInterpolValue(double x);		
	
	// writes x,y array to file	
	void writeXYValues(char *filename); 		
	
	double getMaxLambda(); // returns lambda value for validHighLimit;
	double getMinLambda(); // return lambda value for validLowLimit;
	int getValidLowLimit();
	int getValidHighLimit();			
	void getxyData(xyData * & data);  		// get xvalues & yvalues	(copying)
	xyData *getxyData() { 	// just returns a pointer	
		if (! this->loaded) 
			this->load();		
		return values; 
	}
	
	void calcMeshedValues(double resolution);
	void calcMeshedValues(double lambdaMin, double lambdaMax, double resolution);
	void calcMeshedValues(double lambdaMin, double lambdaMax, double resolution, int extend);	
	void calcLogMeshedValues(double resolution);
	void calcLogMeshedValues(double lambdaMin, double lambdaMax, double resolution);
	void calcLogMeshedValues(double lambdaMin, double lambdaMax, double resolution, int extend);	
	xyData *getMeshedValues() { return meshedValues; };
		
};

#endif //_LAMBDADATASET_H_
