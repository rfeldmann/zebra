#include "Filter.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

// Pivot wavelength
// gives sqrt{\int dlambda t(lambda) lambda / \int{ dlambda*t(lambda)/lambda }}
double Filter::getPivotWavelength()
{	
	if (! this->loaded)
		this->load();
		
	return sqrt(this->getIntegTimesLambda(values)/this->getIntegOverLambda(values));	
}

// gives traditional averaged wavelength
double Filter::getAverageWavelength()
{	
	if (! this->loaded)
		this->load();
		
	return this->getIntegTimesLambda(values)/this->getInteg(values);
}

// gives an estimate of the filter width in lambda
// lambda_rms=sqrt{\int{(lambda-lambda0)^2 t(lambda) dlambda}/
//				   \int{ t(lambda) dlambda }}
double Filter::getRMSWidth()
{	
	if (! this->loaded)
		this->load();
	
	double lambda0=this->getAverageWavelength();
	double l2=this->getIntegTimesLambdaLambda(values);
	double l1=this->getIntegTimesLambda(values);
	double l0=this->getInteg(values);
	return sqrt( (l2-2*lambda0*l1+lambda0*lambda0*l0)/l0 );
}

double Filter::getFWHM()
{
	double *x,*y;
	double max;
	int minIndex=0;
	int maxIndex=0;
		
	if (! this->loaded)
		this->load();	
		
	max=this->values->getMaxY();
	y=this->values->getY();
	x=this->values->getX();
	for (int i=0;i<this->values->getlength();i++) { 
		if (y[i]>max/2) {
			minIndex=i;
			break;
		}
	}
	for (int i=this->values->getlength()-1;i>=0;i--) { 
		if (y[i]>max/2) {
			maxIndex=i;
			break;
		}
	}
	#if 0
	  cout << "minIndex=" << minIndex << " x[minIndex]=" << x[minIndex] << endl;
	  cout << "maxIndex=" << maxIndex << " x[maxIndex]=" << x[maxIndex] << endl;
	#endif
	
	if (this->values->getlength())
		return x[maxIndex]-x[minIndex];
	else
		return 0;
}

double Filter::getMax()
{
	if (! this->loaded)
		this->load();	
	
	return this->values->getMaxY();		
}

void Filter::getTransmission(xyData * data)
{
	getxyData(data);			
}
