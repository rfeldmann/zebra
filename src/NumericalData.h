#ifndef _NUMERICALDATA_H_
#define _NUMERICALDATA_H_

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <numeric>
#include <cfloat>
#include <algorithm>
#include <gsl/gsl_fit.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_sf_gamma.h>
#include "allocArray.h"
#include "globHeader.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

using namespace std;

/* necessary forward delcarations */
inline double g_fluxToMag(const double & flux);
inline double g_magToFlux(const double & mag);

/* classes
 */

// forward declaration of ConfFile, in order not to include ConfFile.h 
// which itself includes NumericalData.h (this file)
class ConfFile; // needed for DustObscuration


class AbstractApproxCurve	// abstract approximation class
{	
	public:		
		virtual void getValues(int n,const double *x, double *y) = 0;
		virtual void getValues(int n,const double *x, double *y, int *outofbounds) = 0;
		virtual double getValue(double x) = 0;
		virtual double getValue(double x, int & outofbounds) = 0;
		virtual double getXValue(double y) = 0;
		virtual double getXValueRight(double y, int startIndex, int & found) = 0;
		virtual double getXValueLeft(double y, int startIndex, int & found) = 0;
		virtual double integrate() = 0;
		virtual double integrate(int index_a, int index_b) = 0;
		virtual double integrate(double a, double b) = 0;
		virtual ~AbstractApproxCurve(){};
};

class ApproxCurve : public AbstractApproxCurve
{
	protected:
		double* m_x;		// contains supporting x-values (for efficiency only pointers stored)
		double* m_y;		// contains respective y-values (for efficiency only pointers stored)
		int m_length;
		
	public:
		ApproxCurve(int length,double *x, double *y) {
			if (length>0) {
			  m_x=x;
			  m_y=y;
			} else {
			  m_x=NULL;
			  m_y=NULL;
			}
			m_length=length;							
		}
		ApproxCurve(ApproxCurve *l) {
			if (l->m_length>0) {		
			  m_x=l->m_x;
			  m_y=l->m_y;
			} else {
			  m_x=NULL;
			  m_y=NULL;
			}
			m_length=l->m_length;
		}		
};


/* makes a cubic spline interpolation
 */
class SplineApprox : public ApproxCurve
{	
	private:				
		double yp1;				// left boundary condition
		double ypn;				// right boundary condition
		double* secondDeriv;	// contains 2nd deriv. of y-values at x-values
		void calcSecondDeriv(); // fills secondDeriv-array
													  		
	public:		
		
		SplineApprox(int length,double* x, double* y, double yp1, double ypn)
					: ApproxCurve(length,x,y) {				
			this->yp1=yp1;
			this->ypn=ypn;
			calcSecondDeriv();			
		}	
		
		SplineApprox(SplineApprox *s): ApproxCurve(s) {			
			this->yp1=s->yp1;
			this->ypn=s->ypn;	
			calcSecondDeriv();				
		}
			
		void getValues(int n,const double *x, double *y);	
		void getValues(int n,const double *x, double *y, int *outofbounds);
		double getValue(double x);
		double getValue(double x, int & outofbounds);
		double getXValue(double y);
		double getXValueRight(double y, int startIndex, int & found);
		double getXValueLeft(double y, int startIndex, int & found);
		double integrate();	
		double integrate(int index_a, int index_b);
		double integrate(double a, double b);				
		~SplineApprox() {
			freeArray1(secondDeriv);
		}
};

/* approximates just linear (straight lines)
 */

class LinearApprox: public ApproxCurve
{			
	public:
		LinearApprox(int length,double *x, double *y):ApproxCurve(length,x,y) {};
		LinearApprox(LinearApprox *l):ApproxCurve(l) {};
		
		void getValues(int n,const double *x, double *y);
		void getValues(int n,const double *x, double *y, int *outofbounds);		
		double getValue(double x);	
		double getValue(double x, int & outofbounds);
		double getXValue(double y);
		double getXValueRight(double y, int startIndex, int & found);
		double getXValueLeft(double y, int startIndex, int & found);
		double integrate();	
		double integrate(int index_a, int index_b);	
		double integrate(double a, double b);
		~LinearApprox(){};
};

/* calculates for an array xa of x-values and a given x-value x
 * the index i of the array xa, s.t. xa[i]<= x < xa[i+1]
 */
class Bisection
{
	private:
		int length;
		double* xvalues;

	public:
		Bisection(int length, double *x);
		Bisection(Bisection *b) {
			length=b->length;
			xvalues=b->xvalues;
		}
		~Bisection(){};
		int getIndex(double x);	
		int getIndex(double xval,int &flag); // flag==1 shows, that xa[i]==x
		int getNearestIndex(double x);	// returns i if |x[i]-x|<|x-x[i+1]| otherwise returns i+1
};

/* operations on char strings
 */

class CharLine
{
	private:
		const char* line;
		int entryNr;
		char **lineEntries;	
		int length;
	
	public:
		CharLine();
		CharLine(const char* string);		
		~CharLine();
  		void set(const char* string);
  		void split(const char *delimiters);
 		int getEntryNumber();
 		char **getEntries();	
};


/* contains a set of (x-value,y-value) tupels
 * the xvalues are assumed to be ordered (increasing)
 * (if not a sort method should be added)
 */
class xyData { 
	
	private:		
			
	protected:
 		int length;
 		double* x;
 		double* y;
 		Bisection *bi;
 		int approxCurveType;	// 0 linear
 								// 1 spline
 		AbstractApproxCurve *interpol_obj;
 		
 		int calcBothInterpol(xyData *data2,int & numPoints,double* & xInter);
 		void make_interpol_obj();
 		void resize();	// clean everything up and set to NULL, and 0
 		void reset();   // clean reactive memory
 		
 	public:
 		xyData();
 		xyData(int length,const double* x,const double* y);
 		xyData(xyData *d);
 		virtual ~xyData(); 		
 		virtual void copy(const xyData * data,int validLowLimit, int validHighLimit);
 		virtual void set(int length,const double* x,const double* y);
 		virtual void set(xyData *d);
 		virtual void set(int length,const double* x);
 		virtual void setY(xyData *data);	// sets the yvalues to interpolated values of data
 		virtual void setY(double *y);		// set the yvalues to the given values y[] 		
 		virtual void setY(double y);		// set all yvalues to y 		
 		xyData * operator * (xyData* data);
 		xyData * operator * (const double val);
 		xyData * operator / (xyData* data);
 		xyData * operator / (const double val);
 		xyData * operator + (xyData* data);
 		xyData * operator + (const double val); 		 		 		
 		xyData * operator - (xyData* data);
 		xyData * operator - (const double val); 	 		 	
 		void setApproxCurveType(int approxCurveType) {
 			reset();
 			this->approxCurveType=approxCurveType;
 		}
 		 		
 		// returns the y-values for given values x, if necessary it interpolates
 		// second version works the same but checkes whether it 
 		// extrapolates to the left (outofbounds==-1), to the right (outofbounds==1) or 
 		// interpolates (outofbounds==0)
 		void calcYValues(int numPoints,const double *x, double *y); 
 		void calcYValues(int numPoints,const double *x, double *y, int* outofbounds);
 		double calcYValue(double x); 		
 		double calcYValue(double x, int & outofbounds);
 		double getMinX() const;
 		double getMaxX() const;
 		double getMinY() const;
 		double getMaxY() const;
 		// for given x-value it determines an index i, s.t. xa[i]<= x < xa[i+1]
 		// flag==1 indicates xa[i] == x
 		int getIndex(const double & x);
 		int getIndex(const double & x,int & flag);
 		int getNearestIndex(const double & x);
 		double* getX() const;
 		double* getY() const;
 		int getlength() const { return this->length; };
 		void getCopyX(double* & x) const;
 		void getCopyY(double* & y) const;
 		void invertXY();	// interchanges role of x and y
 		double getXValue(double y);	// returns an x-value (interpolated), s.t. f(x)=y
 	                                                                        // found=-1 no x value found, found>0 means x[found]<x<x[found+1]
 		double getXValueRight(double y, int & found); 					// returns the first x-value (interpolated), s.t. f(x)=y
 		double getXValueRight(double y, int startIndex, int & found); 	// same as above but start with startIndex not with index 0
		double getXValueLeft(double y, int & found);  					// returns the last x-value (interpolated), s.t. f(x)=y 
		double getXValueLeft(double y, int startIndex,int & found);  	// same as above but start with startIndex not with index maxIndex
		double sum(int index_a, int index_b);
 		double integrateSimple();
 		double integrate(); 		
 		double integrate(int index_a, int index_b);
 		double integrate(double a, double b);
 		// transforms the y-values (assumed a pdf) into the cumulated prob. function
 		void cumulate();
 		void convolveRectangularSum(double fwhm);
 		void convolveGaussian(double sigma);
 		void convolveGaussian(double *sigma);
 		void average(double fwhm, double lowlimit,int smoothMode);
		
 		int getPercentileLow(double conflevel);
 		int getPercentileLow(double conflevel,double norm);
 		int getPercentileLow(double conflevel, double norm, int & index, double & integral);
 		int getPercentileLowHist(double conflevel);
 		int getPercentileHigh(double conflevel); 		
 		int getPercentileHigh(double conflevel,double norm);
 		int getPercentileHigh(double conflevel, double norm, int & index, double & integral);
 		int getPercentileHighHist(double conflevel); 		
 		
		int checkStrictMonotonicityX(); // returns 1 if x values are strictly monotonically increasing, otherwise 0
		
 		void yTimesx() { 
 			reset();
 			for (int i=0;i<this->length;i++)
 				y[i]*=x[i]; 		
 		};
 		void yOverx(){ 
 			reset();
 			for (int i=0;i<this->length;i++)
 				y[i]/=x[i]; 	 		 		
 		};
 		void xTimes(double val) {
 			reset();
 			for (int i=0;i<this->length;i++)
				x[i]*=val;		 			
 		};
 		void xOver(double val) {
 			reset();
 			for (int i=0;i<this->length;i++)
				x[i]/=val; 			
 		};		 		
 		void yPow(double val) {
 			reset();
 			for (int i=0;i<this->length;i++)
				y[i]=pow(y[i],val);
 		};	
 		void sort();
 		void sortY();
 		virtual void print();
 		virtual void write(const char *filename);
 		virtual void append(const char *filename);
 };

class xyErrorData : public xyData {
	
	protected:
		double* ex;
		double* ey; 				
		
		void resize() { // clean everything up
			xyData::resize();
			freeArray1(this->ex);
			freeArray1(this->ey);	
		}		
		
	public:	
		xyErrorData(){
			ex=NULL;
			ey=NULL;	
		};
		
 		xyErrorData(int length,const double* x,const double* y, 
 					const double *error_x,const double *error_y) : xyData(length,x,y) { 						
 			ex=NULL;
			ey=NULL;	
 			if (length>0) {
 				allocArray1(ex,length);
 				allocArray1(ey,length);
 			}
 			for (int i=0;i<length;i++)
 				ex[i]=error_x[i];	 		
 			for (int i=0;i<length;i++)
 				ey[i]=error_y[i];	 					
 		};
 					
 		xyErrorData(xyErrorData *d):xyData(d) {
 			ex=NULL;
			ey=NULL;	
 			if (length>0) {
 				allocArray1(ex,length);
 				allocArray1(ey,length);
 			}
 			for (int i=0;i<length;i++)
 				ex[i]=d->ex[i];	 		
 			for (int i=0;i<length;i++)
 				ey[i]=d->ey[i]; 				
 		} 		 		 			
 		
 		~xyErrorData(){
 			freeArray1(ex);
 			freeArray1(ey);
 		}
 		void set(int length,const double* x);
 		void set(int length,const double *x,const double *y);
 		void set(int length,const double* x,const double* y, 
 				 const double *error_x, const double *error_y);
 		void set(xyErrorData *d); 
 		void setY(double *y);		// set the yvalues to the given values y[] 		
 		void setY(double y);		// set all yvalues to y 	
 		void setY(xyErrorData *d);	
 		void setEX(double y);
 		void setEX(double *y);
 		void setEY(double y);
 		void setEY(double *y);
 		void copy(xyErrorData *d) { 
 			set(d); 
 		} 		 		 		
 	 			 		 		 		 	 		 		 		
 		double *getErrorX() {
 			return ex;
 		}
 		
 		double *getErrorY() {
 			return ey;
 		}

		// calculate estimated <X> = 1/n sum x_i
		double getMeanX();
		
		// calculate estimated <X> = 1/n sum x_i
 		// data with a x-value above or below the (1-level)/2 percentile is clipped 		 	
 		double getMeanX(double level);

		// calculate estimated <X> = sum_i(x_i/sigma^2_i)/sum_i(1/sigma^2_i) 		
		double getWeightedMeanX();
		
		// calculate estimated <X> = sum_i(x_i/sigma^2_i)/sum_i(1/sigma^2_i)  		
 		// data with a x-value above or below the (1-level)/2 percentile is clipped 		 	
 		double getWeightedMeanX(double level); 		 		
 		
 		// calculate estimated <Y> = 1/n sum y_i
		double getMeanY();
		
		 // calculate estimated <X> = 1/n sum y_i
		// data with a y-value above or below the (1-level)/2 percentile is clipped 		 	
		double getMeanY(double level);
 		
 		// calculate estimated <Y> = sum_i(y_i/sigma^2_i)/sum_i(1/sigma^2_i) 		
 		double getWeightedMeanY();
 		
 		// calculate estimated <Y> = sum_i(y_i/sigma^2_i)/sum_i(1/sigma^2_i) 		
 		// data with a y-value above or below the (1-level)/2 percentile is clipped
 		double getWeightedMeanY(double level);
 		
 		// calculate median of y  		
 		double getMedianY();
 		
 		// calculates median, lower and upper percentiles for given lowPercLimit and highPercLimit;
 		void getPercY(double & median, double & lowPerc, double & highPerc, double lowPercLimit, double highPercLimit);
 		
 		// calculate sigma of y from clipping outliers at clippingLevel-sigma level, selfconsistently  
        double getClippedSigmaY(double clippingLevel); 
 		
 		// calculated sigma from x-data
 		// S(<X>) = sqrt(1/(n-1)sum_i(x_i-x_mean)^2)
 		double getSigmaX();
 		
 		// calculated sigma from x-data
 		// S(<X>) = sqrt(1/(n-1)sum_i(x_i-x_mean)^2)
 		// data with a x-value above or below the (1-level)/2 percentile is clipped 		
 		double getSigmaX(double level);
 		
 		// calculated sigma of weighted average
		// S(<X>)=sqrt(1/sum_i(1/sigma(x_i)^2))
		double getWeightedSigmaX();
		
		// calculated sigma of weighted average
		// S(<X>)=sqrt(1/sum_i(1/sigma(x_i)^2))
		// data with a x-value above or below the (1-level)/2 percentile is clipped 		
		double getWeightedSigmaX(double level);
 		
 		// calculate sigma from data
 		// S(<Y>) = sqrt(1/(n-1)sum_i(y_i-y_mean)^2)
 		double getSigmaY();
 		
 		// calculate sigma from data
 		// S(<X>) = sqrt(1/(n-1)sum_i(y_i-y_mean)^2) 	
 		// data with a y-value above or below the (1-level)/2 percentile is clipped 		
 		double getSigmaY(double level);
 		
 		// calculated sigma of weighted average
		// S(<Y>)=sqrt(1/sum_i(1/sigma(y_i)^2)) 		
		double getWeightedSigmaY();
		
		// calculated sigma of weighted average
		// S(<X>)=sqrt(1/sum_i(1/sigma(x_i)^2))
		// data with a y-value above or below the (1-level)/2 percentile is clipped 		
		double getWeightedSigmaY(double level);
 		
 		void print(); 	 				 		
 		
		void write(const char* filename);
		void append(const char* filename);
		// allows to append only certain columns e.g. append(filename,0,0,1,1) only appends errors
		void append(const char* filename, int printX, int printY, int printEX, int printEY);				
};
 
class RegressionData : public xyErrorData  {
	protected:
		int numBins;
		double minValue;	// left border of interpolation
		double maxValue;    // right border of interpolation
		double *leftBorder;	//..left x-value of each bin
		
		int interMode;		// -2 spline interpolation (needs at least three bins i.e. numBins>2)
							// -1 linear interpolation (needs at least two bins i.e. numBins>1)
							//  0 regression Y = const
							//  1 regression Y = c_0 + c_1 * X
							// >1 regression Y = SUM_i c_i * X^i													
		int extraMode;		// just as interMode but says which mode should be used for extrapolation			
		int weightedY;		// 0 .. y-errors are calculated from given data itself.
							//		For this to work, numBins>0 is required. Otherwise
							//		the provided y-errors are used.
							// 1 .. provided y-errors are used for weighted averages
							//      and error estimation (note: this option should (only)
							//      be used if provided errors comprise all error sources)
							// Note: a similar option weightedX does not exist, since 
							//       x-errors are ignored
		int weightedYLimit; // sets the weightedY option to 1 for considered bin if number of
							// objects in that bin is weightedYLimit or less
		double levelX;		// [0,1] - keep only levelX % objects in each bin (to remove outliers)
							//		   used only for average and error calculation
		double levelY;		// [0,1] - keep only levelY % objects in each bin (to remove outliers)
							//		   used only for average and error calculation
		int minimumBinEntries; // bin is only used if it contains at least minimumBinEntries entries 
		
		xyErrorData **bins;	// pointer to each bin
		xyErrorData *binnedData;	// each point represents mean and sigma for each bin
								    // this information is then used in regression
		// allocates bins					
		void createBins();
		
		// redirect *x and *y data into respective bins
		void fillBins();
		
		// selects (x,y,ex,ey) tupel and creates binnedData object 
		// according to minValue & maxValue limits		
		int selectEntries();
		
		// create binnedData object (from the individual bins)
		int createBinnedData();
		
		// interpolation (using the adjusted options) for the binnedData
		// the result is returned as correctionData
		void interpolation(xyErrorData * correctionData);
		
		// regression (using the adjusted options) for data
		// the result is returned as correctionData
		void regression(xyErrorData * data, xyErrorData * correctionData, 
						gsl_vector * vC, double &c0, double &c1, 
						gsl_matrix * mCov, double &cov00, double &cov01, double &cov11,
						double & chisq);	

		void reset() {			
			freeArray1(leftBorder);
			leftBorder=NULL;			
			if (bins) {
				for (int i=0;i<numBins;i++) {
					if (bins[i])
						delete bins[i];
				}
				freeArray1(bins);
			}
			bins=NULL;			
			if (binnedData)
				delete binnedData;
			binnedData=NULL;						
		}																					
						
		// clean everything up				
        void resize() {	
        	xyErrorData::resize();
        	
			reset();
		}						
							
	public:
		RegressionData(){
			leftBorder=NULL;
			minValue=maxValue=0;
			interMode=0;
			extraMode=0;
			bins=NULL;
			binnedData=NULL;
			numBins=0;
			weightedY=0;
			weightedYLimit=1;
			levelX=1.0;
			levelY=0.954;
			minimumBinEntries=1;
			
		};
		~RegressionData(){
			freeArray1(leftBorder);			
			if (bins) {
				for (int i=0;i<numBins;i++) {
					if (bins[i])
						delete bins[i];
				}
				freeArray1(bins);
			}			
			if (binnedData)
				delete binnedData;					
		};				
							
		void setNumBins(int numBins) {
			reset();
			this->numBins=abs(numBins);
			
			if (this->numBins==1) {
				interMode=0;
				extraMode=0;
			}
			
			if (interMode==-2 && this->numBins<3) 
				interMode=-1;
			if (interMode==-1 && this->numBins<2)
				interMode=0;
			
			if (extraMode==-2 && this->numBins<3) 
				extraMode=-1;	
			if (extraMode==-1 && this->numBins<2)
				extraMode=0;			
			
		}
		
		void setLeft(double minValue) {
			reset();
			this->minValue=minValue;
		}
		
		void setRight(double maxValue) {
			reset();
			this->maxValue=maxValue;
		} 
		
		void setInterpolationMode(int mode) {
			this->interMode=mode;
		}
		
		void setExtrapolationMode(int mode) {
			this->extraMode=mode;
		}
		
		void setWeightedY(int weightedY) {
			this->weightedY=weightedY;
		}
		
		void setWeightedYLimit(int weightedYLimit) {
			this->weightedYLimit=weightedYLimit;
		}
		
		void setLevelX(double levelX) {
			this->levelX=levelX;
		}
		
		void setLevelY(double levelY) {
			this->levelY=levelY;
		}
		
		// only bins which contain >= minimumBinEntries entries are used
		void setMinimumBinEntries(int minimumBinEntries) {
		    this->minimumBinEntries = minimumBinEntries;
		}
						
		/* get regression fit: IN x-values in correction data
		 * 					   OUT y-values in correction data
		 * 					   OUT ey-values in correction data
		 * 					   OUT para - holds the fit parameter
		 * 					   OUT cov  - holds the covariance matrix
		 */
		void getRegression(xyErrorData * correctionData, int n, 
						   double * param, double **cov, double & chisq);
				
		xyErrorData* getBinnedData() {
			return binnedData;
		}
};

/* contains a set of values together with their errors
 * similiar to xyData but not ordered
 */
class ErrorData {
	protected:
		int length;
		double* value;
		double* error;
		char *invalid;
		char name[MAX_LENGTH];
		int writeWindowLow;
		int writeWindowHigh;			
	public:
		ErrorData(const char* name);
		ErrorData(const ErrorData *e) {
		  length=e->length;
		  if (length>0) {
		    allocArray1(value,length);
		    allocArray1(error,length);
		    allocArray1(invalid,length);
		    for (int i=0;i<length;i++) {
		      value[i]=e->value[i];
		      error[i]=e->error[i];
		      invalid[i]=e->invalid[i];
		    }
		  } else {
		    value=NULL;
		    error=NULL;
		    invalid=NULL;
		  }
		  strcpy(name,e->name);
		  writeWindowLow=e->writeWindowLow;
		  writeWindowHigh=e->writeWindowHigh;
		}
		~ErrorData();
		ErrorData * operator + (const double val); 
		ErrorData * operator - (const double val); 
		void set(int length, const double* values,const double* errors);
		void set(int length);	// just allocates memory		
		void print();
		int getLength() { return length; }
		double* getValue() { return value; }
		double* getError() { return error; }		
		char* getInvalid() { return invalid; }
		char *getName()	{ return name; }		
		
		void setWriteWindow(int writeWindowLow, int writeWindowSize){
			this->writeWindowLow=writeWindowLow;
			this->writeWindowHigh=writeWindowLow+writeWindowSize;
		}
		void setInvalid(char *invalid);
		void setValueAndError(const double* values,const double* errors);
};

class ErrorDataOrigError: public ErrorData {
	protected:
		double *orig_error;
	public:
		ErrorDataOrigError(const char *name) : ErrorData(name) {
			orig_error=NULL;
		}
		ErrorDataOrigError(const ErrorDataOrigError *e) : ErrorData(e) {
		  if (length>0 && e->orig_error!=NULL) {
		    allocArray1(orig_error,length);
		    for (int i=0;i<length;i++) {
		      orig_error[i]=e->orig_error[i];
		    }
		  } else {
		    orig_error=NULL;	
		  }
		}
		~ErrorDataOrigError() {
		 freeArray1(orig_error);	
		}		
		
		void set(int length, const double* values,const double* errors, const double *orig_errors);
		
		void setOrigError(const double *orig_errors);				
		
		double *getOrigError() { return orig_error;	};		
		
		void print() {
			cout << name << ": Value Error OrigError Invalid" << endl;
			cout << "-------------------------------" << endl;
			for (int i=0;i<length;i++) {
				cout << value[i] << " " << error[i];
				if (orig_error!=NULL)
					cout << " " << orig_error[i] << " ";
				else
					cout << " NULL ";
				cout << int(invalid[i]) << endl;		
			}
		}		
};

class ListArray
{
	protected:
		int numBins;
		double *leftBorder;
		xyData **bins;
	public:
		ListArray(){
			leftBorder=NULL;
			bins=NULL;
		};
		~ListArray(){
			if (bins) {
				for (int i=0;i<this->numBins;i++) {
					if (bins[i])
						delete bins[i];
				}
				freeArray1(bins);
			}
			freeArray1(leftBorder);		
		};
		
		void setBins(int numBins, double *border) {
					
			if (bins) {
				for (int i=0;i<this->numBins;i++) {
					if (bins[i])
						delete bins[i];
				}
				freeArray1(bins);
			}
			freeArray1(leftBorder);
					
			this->numBins=numBins;		
			allocArray1(leftBorder,numBins+1);
			allocArray1(bins,numBins);
			
			for (int i=0;i<numBins;i++)
				this->leftBorder[i]=border[i];
			this->leftBorder[numBins]=border[numBins];	// is actually right border of last bin			
		}
		
		void fillBin(int binIndex, int numBinEntries, double *xvalues, double *entries) {
			 if (bins[binIndex])
			 	delete bins[binIndex];
			 bins[binIndex] = new xyData(numBinEntries,xvalues,entries);
		}								
};

/* just 1D histograms
 */
class SimpleHistogram
{
	private:			
		int bins;
		double binWidth;
		double *binValue;
		double *binCount;
		xyData *content;
		double min;
		double max;		
		
		void getSupport(const std::vector<double> & values);		
		void fill(const std::vector<double> & values);				
	public:
		//SimpleHistogram(int length,const double* values,int bins);
		SimpleHistogram(const std::vector<double> & entries, int bins);
		SimpleHistogram(const std::vector<double> & entries, int bins, double min, double max);
		~SimpleHistogram();
		
		xyData * getContent() { return content; };
		void write(const char* filename);			
};

class AdaptiveHistogram
{
	private:
		int length;			//..number of entries
		double *values;		//..entries
		
		int bins;			//..actual number of bins for entries
		int proposedBins;   //..wanted number of bins for entries
		int entriesNr;		//..minimal number of entries in each bin
		int *binEntries;	//..actual number of entries in each bin
		double *binValue;	//..midpoint value of each bin
		double *binAverageValue;	//..averaged x-value for each bin
		double *binHeight;	//..weighted & normalized bin count
		double *leftBorder;	//..left x-value of each bin
		double *binWidth;	//..width of each bin
			
		Bisection * bisect; //..bisection object for fast indices
		double min;			//..minimal entry
		double max;			//..maximal entry
		
		double meanInput;	//..mean (calculated from the input values)
		double mean;		//..mean (calculated from the histogram)
		double sigmaInput;	//..sigma (calulated from the input values)
		double sigma;		//..sigma (calculated from the histogram)
			
	public:
		AdaptiveHistogram();
		AdaptiveHistogram(const AdaptiveHistogram *h) {
		  length=h->length;
		  if (length>0 && h->values!=NULL ) {
		    allocArray1(values,length);
		    for (int i=0;i<length;i++) 
		      values[i]=h->values[i];
		  } else {
		    values=NULL;
		  }
		  bins=h->bins;
		  proposedBins=h->proposedBins;
		  entriesNr=h->entriesNr;

		  binEntries=NULL;
		  binValue=NULL;
		  binAverageValue=NULL;
		  binHeight=NULL;
		  leftBorder=NULL;
		  binWidth=NULL;
		  
		  if (bins>0) {
		    if (h->binEntries!=NULL) {
		      allocArray1(binEntries,bins);
		      for (int i=0;i<bins;i++)
		        binEntries[i]=h->binEntries[i];
		    } 

		    if (h->binValue!=NULL) {
		      allocArray1(binValue,bins);
		      for (int i=0;i<bins;i++)
			binValue[i]=h->binValue[i];
		    }
		
		    if (h->binAverageValue!=NULL) {
		      allocArray1(binAverageValue,bins);
		      for (int i=0;i<bins;i++)
			binAverageValue[i]=h->binAverageValue[i];
		    } 

		    if (h->binHeight!=NULL) {
		      allocArray1(binHeight,bins);
		      for (int i=0;i<bins;i++)
			binHeight[i]=h->binHeight[i];
		    } 
		    
		    if (h->leftBorder!=NULL) {
		      allocArray1(leftBorder,bins);
		      for (int i=0;i<bins;i++)
			leftBorder[i]=h->leftBorder[i];
		    } 

		    if (h->binWidth!=NULL) {
		      allocArray1(binWidth,bins);
		      for (int i=0;i<bins;i++)
			binWidth[i]=h->binWidth[i];
		    }
		  }
			
		  if (h->bisect)
		    bisect = new Bisection(h->bisect);
		  min=h->min;
		  max=h->max;
		  meanInput=h->meanInput;
		  mean=h->mean;
		  sigmaInput=h->sigmaInput;
		  sigma=h->sigma;
		}

		~AdaptiveHistogram();		
	
		int getBins(){ return bins; }
		int *getBinEntries() { return binEntries; };
		
		void fill(int length,const double* values,int bins, int entriesNr);		
		void write(const char* filename);
		void write(const char* filename, char** text);
		int read(const char* filename);
		int read(const char* filename, int & length, char** & text);
						
		void setMean(double mean){ this->mean=mean; };
		void setSigma(double sigma){ this->sigma=sigma; };
		
		double getMean() { return mean; };
		double getMeanInput() { return meanInput; };
		double getSigma(){ return sigma; };
		double getSigmaInput(){ return sigmaInput; };		
		int getBin(double x);
		
		// creates new x from gaussian distribution with mean & sigma
		void gaussian(double &x, double mean, double sigma);		
		// calculates new x & error (given mean and sigma of error)
		// the error is at least minError
		void gaussianError(double &x, double &error, double minError);
		// same as above, but does not change value x
		void gaussianErrorOnly(const double &x, double &error, double minError);
		// calculates new x & error (using histogram entries)
		// the error is at least minError
		void histogramError(double &x, double &error, double minError);
};

/* does analysis of 1D data given with its error
 */
class HistogramAnalysis
{
	private:
		AdaptiveHistogram *histogramX;			//..histogram of x-data
		AdaptiveHistogram **histogramsError;	//..histograms of error-data
		int xBins;						//..bin number for x-data histogram
		int xEntriesNr;					//..minimum number of entries per bin
		int errorBins;					//..bin number for error-data histograms
										//  for each x-data bin there is a corr.
										//  error-data histogram
		int errorEntriesNr;				//..minimum number of entries per bin
		int length;						//..length of given values array
		const double *values;			//..values for which analysis is done
		const double *errors;			//..errors of the values
		int loaded;						//..HistogramAnalysis object is loaded 
		int missGaussData;
		int missHistData;
		
		void makeHistograms();		
		
	public:
		HistogramAnalysis();
		HistogramAnalysis(const HistogramAnalysis *h) {
		  if (h->histogramX) {
		    histogramX = new AdaptiveHistogram(h->histogramX);
		    if (h->histogramsError) {
		      allocArray1(histogramsError,histogramX->getBins());
		      for (int i=0;i<histogramX->getBins();i++) {
			if (h->histogramsError[i]) {
			  histogramsError[i] = new AdaptiveHistogram(h->histogramsError[i]);
			} else {
			  histogramsError[i]=NULL;
			}
		      }
		    } else {
		      histogramsError=NULL;
		    }
		  } else {
		    histogramX = NULL;
		    histogramsError = NULL;
		  }
		  xBins=h->xBins;
		  xEntriesNr=h->xEntriesNr;
		  errorBins=h->errorBins;
		  errorEntriesNr=h->errorEntriesNr;
		  length=h->length;
		  values=h->values;
		  errors=h->errors;
		  loaded=h->loaded;
		  missGaussData=h->missGaussData;
		  missHistData=h->missHistData;
		}
		
		~HistogramAnalysis();
		void set(int length, 			//..total number of entries
				 const double* values,	//..entries
				 const double* errors,  //..error entries
			     int xBins, 			//..number of bins for entries
				 int xEntriesNr,		//..minimal number in each bin
				 int errorBins, int errorEntriesNr); //..analogous
		int read(char *filebase);
		void write(char *filebase);								
		int randomize(double &x, double &error,int errorMode,double minError);		
};

/* containes filter data and the information to which column
 * from the given catalog the data corresponds
 */

class FilterBand : public ErrorDataOrigError {
	protected:
		int colID;
		int errorColID;
		HistogramAnalysis *histAna;
	public:
		FilterBand(const char* name,int colID, int errorColID);		
		FilterBand(const FilterBand *f) : ErrorDataOrigError(f) {
		  colID=f->colID;
		  errorColID=f->errorColID;
		  if (f->histAna) {
		    histAna = new HistogramAnalysis(f->histAna);
		  } else {
		    histAna = NULL;
		  }
		}
		~FilterBand();
		int getColID() { return colID; }
		int getErrorColID() { return errorColID; }
		void magToFlux();
		void fluxToMag();
		void errorAnalysis(int xBins, 		//..number of bins
						   int xEntriesNr,  //..but at least this number of 
						   					//  entries in each bin
						   int errorBins, 		//..analogous
						   int errorEntriesNr); //..analogous
		void writeErrorAnalysis(const char* filebase);
		void readError(char* filebase); 	//..create histAna from file		
		void multiplyError(double factor);  // multiply each relative flux errors by a constant factor
		void floorError(double minError);	// set each relative flux error to a minimum value
		void reshuffleGaussian();			// randomizes a catalog by just changing all fluxes by a value drawn from a gaussian
		void reshuffleGaussian(double error); 	// same as above but instead of using the absolute flux error it uses <error>
		void randomize(int errorMode,double minError);	//..does the randomization using
														//  histAna object
};

/* class storing z-values
 */
class BinningZ {
	protected: 		
		double* m_z;
		double* m_dz;
		int m_length;
	public:
		BinningZ() { 
			m_z=NULL;
			m_dz=NULL;
			m_length=0;				
		}
		BinningZ(BinningZ *b) {
			m_length=b->m_length;
			allocArray1(m_z,m_length);
			allocArray1(m_dz,m_length);
			for (int i=0;i<m_length;i++) {
				m_z[i]=b->m_z[i];		
				m_dz[i]=b->m_dz[i];
			}						
		}
		~BinningZ() { 
			resize(); 
		}
		void resize() { 
			freeArray1(m_z); 
			freeArray1(m_dz);
			m_z=NULL;
			m_dz=NULL;
			m_length=0;
		}
		void copy(const BinningZ *binZ) {
			resize();
			m_length=binZ->m_length;
			allocArray1(m_z,m_length);
			allocArray1(m_dz,m_length);
			for (int i=0;i<m_length;i++) {
				m_z[i]=binZ->m_z[i];		
				m_dz[i]=binZ->m_dz[i];
			}	
		}
		
		// function set: spacing =0 : linear in z, spacing=1 : linear in log(1+z)
		void set(int spacing, double zmin, double zmax, double dz);
		void set(int length, double *z);
		double getZ(int index) { 
			return m_z[index]; 
		}
		double* getZ() { 
			return m_z; 
		}
		double getdZ(int index) { 
			return m_dz[index]; 
		}
		double* getdZ() { 
			return m_dz; 
		}
		int getLength() {
			return m_length;
		}
		int getNumBins() {
			return m_length-1;
		}
		void sort();
};

class BinningZNumber : public BinningZ
{
	protected:
		int numEntriesTotal;
		int *numEntries;	
	public:
		
		BinningZNumber() : BinningZ() {
			numEntries=NULL;
			numEntriesTotal=0;
		};
		~BinningZNumber() { 
			freeArray1(numEntries); 
		};		
					
		void setNumEntries(int *numEntries) {
			freeArray1(this->numEntries);
			allocArray1(this->numEntries,m_length-1);
			int count=0;
			for (int i=0;i<m_length-1;i++) {
				count+=numEntries[i];
				this->numEntries[i]=numEntries[i];			
			}
			numEntriesTotal=count;
		}	
};

/* class OutputHandler
 */
class OutputHandler
{
	private:
		int m_verbosity;
	
	public:
		OutputHandler(){m_verbosity=0;};
		~OutputHandler(){};
		void set(int verbosity) {
			m_verbosity=verbosity;
		}
		void print(const char* string, int verbosity) {
			if (m_verbosity >= verbosity) cout << string;
			cout.flush();
		}
		void println(const char* string, int verbosity) {
			if (m_verbosity >= verbosity) cout << string << endl;
		}	
};

/* class FluxRestriction  //checks templates for consistency as a function of redshift
 * 						  
 */
class FluxRestriction
{
	private:
		double *dL;					// luminosity distance in Mpc
		double *conv;				// conversion factor: absolute flux = normalization * conv * flux
		BinningZ *m_binZ;			// contains the z values for which lum. distance is calculated
		double mbLow,mbHigh;		// limits for M_B cutting (magnitudes)
		double mbLowFluxFast,mbHighFluxFast; // limits for M_B cutting in flux units and normalized
											// as to minimizes computation effort
        double omegaM;
        double omegaL;
        double hubble;											
	
		void calcDL();

	public:
		FluxRestriction(const BinningZ* binZ, double omegaM, double omegaL, double hubble);
		~FluxRestriction();	
		
		void setMBLow(double mbLow) { 
			this->mbLow=mbLow;
			this->mbLowFluxFast=g_magToFlux(mbLow);
		}
		
		double getMBLow() { 
			return mbLow; 	
		};
		
		void setMBHigh(double mbHigh) {
			this->mbHigh=mbHigh;
			this->mbHighFluxFast=g_magToFlux(mbHigh);
		}
		
		double getMBHigh() { 
			return mbHigh; 
		};
				
		void getZLimit(double & zLimit,const double &flux);
		double *getDL() { return dL; };
		
		double getAbsoluteMagB(int zi, const double &normalization,const double &flux) {
			// calculate total magnitude in the B-band
			// factor 1E5 = 1Mpc/10pc (since dL measured in Mpc)
			double aux=normalization*(dL[zi]*1E5)*flux*(dL[zi]*1E5)*(1+m_binZ->getZ()[zi]);
			
			//debugging
			//cout << "dL[zi]: " << dL[zi] << endl;
			
			if (aux>0)
				return g_fluxToMag(aux);
			else
				return 99;
		}						

		// this works well but is probably not fast enough
		int cutting(int zi, const double &normalization,const double &flux) {		
			double M_B=getAbsoluteMagB(zi,normalization,flux);	
	
			if (M_B>mbHigh)
				return 1;
			if (M_B<mbLow)
				return 1;		
			return 0;
		};	
		
		// this gives the same but is much faster, need dL2, mbLowFluxFast, mbHighFluxFast
		int cuttingFast(int zi, const double &normalization,const double &flux) {						
			double flux_B=normalization*flux*conv[zi];
	
			if (flux_B<mbHighFluxFast || flux_B>mbLowFluxFast)
				return 1;						
			return 0;
		};
};

/* class IGAbsorption
 * calculates intergalactic absorption (following Madau APJ 441:18-27, 1995)
 */

class IGAbsorption
{
	private:
		xyData *iga;
		double lastz;
		double *ly;
		static const int numLines=40;
		int absorptionMode;
		
		// calculates transmittance using Madau's formulae
		void absorb_Madau(double z, int mode=2);
		// calculates transmittance using Meiksin's formulae
		void absorb_Meiksin(double z, int mode=5);
		
	public:
		IGAbsorption() {
			iga=NULL;
			lastz=-1;
			absorptionMode=1;
			allocArray1(ly,numLines);	
			// Balmer formula for Lyman series		
			for (int i=0;i<numLines;i++)
				ly[i]=1/( 0.00109678* (1-1.0/((i+2)*(i+2)) ) );			
		}
		IGAbsorption(IGAbsorption *i) {
			iga = new xyData(i->iga);
			lastz = i->lastz;
			absorptionMode=i->absorptionMode;
			allocArray1(ly,numLines);			
			// Balmer formula for Lyman series		
			for (int i=0;i<numLines;i++)
				ly[i]=1/( 0.00109678* (1-1.0/((i+2)*(i+2)) ) );
		}
			
		~IGAbsorption() {
			delete iga;
			freeArray1(ly);
		}

		// possible	mode = 0 no IG absorption				
		// 		mode = 1 following Madau, line blankening to 4th order
		//              mode = 2 following Madau, line blankening to numLines order following Meiksin 2005 for >4th order
		//              the following use the line blankening according to Meiksin (to numLines order)
		//		mode = 3 use Madau photoelectric absorption & Lyman-Limit treatment
		//              mode = 4 use Meiksin photoelectric absorption & Lyman-Limit treatment(from Meiksin 2005)		
		//              mode = 5 use Meiksin photoelectric absorption & Lyman-Limit treatment (private Communications 2007)	
		// 		mode = 6: use Meiksin photoelectric absorption & skip Lyman-Limit treatment
		// 		mode = 7: skip Meiksin photoelectric absorption & skip Lyman-Limit treatment
		// 		we recommend using mode=5
		void setAbsorptionMode(int absorptionMode) {
			this->absorptionMode=absorptionMode;
		}						
		
		// precalculates iga for all wavelength > Lyman Limit
		void setLambda(const xyData *data);
		
		// multiplies data with transmittance iga 
		// the actual attenuation scheme is depends on the
		// absorptionMode option
		void absorb(xyData *data,double z);		
		
		// similar to absorb but sets y-values of data 
		// to the calculated optical depth exp(-\tau)		
		void getOpticalDepth(xyData *data,double z);
};

class Bins
{
	int numBins;
	int logSpaced;
	double min;
	double max;
	double *centers;
	double *high;
	double *low;
	
	void logSpace(){
		if(min<0 || max<=0) {
			cout << "min<0 or max<=0 found when trying to calculate logspace! aborting.." << endl;
			exit(1);	
		}
		centers=new double[numBins];
		high=new double[numBins];
		low=new double[numBins];
		for (int i=0;i<numBins;i++) {
			if(min!=0) {
				centers[i]=pow(10,log10(min)+(log10(max)-log10(min))/(numBins-1)*i);
			}
			else {
				centers[i]=pow(10,log10(min+1)+(log10(max+1)-log10(min+1))/(numBins-1)*i - 1);
			}
		}
		for (int i=0;i<numBins;i++) {
			if (i==0) {
				low[i]=min;	
				high[i]=centers[i]+(centers[i+1]-centers[i])/2.0;
			}
			else if(i==numBins-1) {
				low[i]=high[i-1];
				high[i]=max;
			}
			else {
				low[i]=high[i-1];
				high[i]=centers[i]+(centers[i+1]-centers[i])/2.0;
			}			
		}
	};
	void linSpace(){
		centers=new double[numBins];
		high=new double[numBins];
		low=new double[numBins];
		for (int i=0;i<numBins;i++) {
			centers[i]=min+(max-min)/(numBins-1)*i;
		}
		for (int i=0;i<numBins;i++) {
			if (i==0) {	
				low[i]=min;
				high[i]=centers[i]+(centers[i+1]-centers[i])/2.0;
			}
			else if (i==numBins-1) {
				low[i]=high[i-1];
				high[i]=max;	
			}
			else {
				low[i]=high[i-1];
				high[i]=centers[i]+(centers[i+1]-centers[i])/2.0;	
			}
		}
	};
	
	public:
		Bins(int numBins,double min, double max, int logspaced){
			this->numBins=numBins;
			this->min=min;
			this->max=max;
			this->logSpaced=logspaced;
			this->centers=NULL;
			this->low=NULL;
			this->high=NULL;
			if(logspaced)
				logSpace();
			else
				linSpace();
		};	
		
		Bins(int numBins,double *centers) {
			this->numBins=numBins;
			this->centers = new double[numBins];
			this->low = new double[numBins];
			this->high = new double[numBins];
			for (int i=0;i<numBins;i++)	
				this->centers[i]=centers[i];
			for (int i=0;i<numBins;i++) {
				if (i==0) {
					low[i]=this->centers[0];
					high[i]=centers[i]+(centers[i+1]-centers[i])/2.0;
				}
				else if (i==numBins-1) {
					low[i]=high[i-1];
					high[i]=this->centers[numBins-1];
				}
				else {
					low[i]=high[i-1];	
					high[i]=centers[i]+(centers[i+1]-centers[i])/2.0;
				}				
			}
			this->min=this->centers[0];
			this->max=this->centers[numBins-1];
			this->logSpaced=0;
		};
		
		~Bins()
		 {
		 	if(centers) {
		 		delete [] centers;
		 		delete [] high;
		 		delete [] low;
		 	}
		 };

		 double *getCenters() { return centers; };
		 double *getHigh() {return high; };
		 double *getLow() {return low; };
		 int isLogSpaced() {return logSpaced; };
		 int getNumBins() {return numBins; };
};


class Convolution2D
{
	double *x;
	double *y;
	double **field;
	int Lx;
	int Ly;
	
	public:
		Convolution2D(){};
		~Convolution2D(){};
		void setField(int Lx,double* x,int Ly, double *y, double **field) {
			this->x=x;
			this->y=y;
			this->field=field;
			this->Lx=Lx;
			this->Ly=Ly;
		}
		void rectangular(double fwhmX, double fwhmY);
		double** getField(){
			return field;
		}
		
};

/* returns the chi^2 value s.t. 0.99% of a sample with random errors lies below 
 * and 1% above, for different degrees of freedom (0-50). The value for zero d.o.f. 
 * is not defined, so set it to (arbitrary) large value. No chi2 criterion is then 
 * applied.
 */

class Chi2InvTable
{
	double chi2inv[51];
	
	public:
		Chi2InvTable(){
			chi2inv[0]=1000;
			chi2inv[1]=6.6349;
			chi2inv[2]=9.21034;
			chi2inv[3]=11.3449;
			chi2inv[4]=13.2767;
			chi2inv[5]=15.0863;
			chi2inv[6]=16.8119;
			chi2inv[7]=18.4753;
			chi2inv[8]=20.0902;
			chi2inv[9]=21.666;
			chi2inv[10]=23.2093;
			chi2inv[11]=24.725;
			chi2inv[12]=26.217;
			chi2inv[13]=27.6882;
			chi2inv[14]=29.1412;
			chi2inv[15]=30.5779;
			chi2inv[16]=31.9999;
			chi2inv[17]=33.4087;
			chi2inv[18]=34.8053;
			chi2inv[19]=36.1909;
			chi2inv[20]=37.5662;
			chi2inv[21]=38.9322;
			chi2inv[22]=40.2894;
			chi2inv[23]=41.6384;
			chi2inv[24]=42.9798;
			chi2inv[25]=44.3141;
			chi2inv[26]=45.6417;
			chi2inv[27]=46.9629;
			chi2inv[28]=48.2782;
			chi2inv[29]=49.5879;
			chi2inv[30]=50.8922;
			chi2inv[31]=52.1914;
			chi2inv[32]=53.4858;
			chi2inv[33]=54.7755;
			chi2inv[34]=56.0609;
			chi2inv[35]=57.3421;
			chi2inv[36]=58.6192;
			chi2inv[37]=59.8925;
			chi2inv[38]=61.1621;
			chi2inv[39]=62.4281;
			chi2inv[40]=63.6907;
			chi2inv[41]=64.9501;
			chi2inv[42]=66.2062;
			chi2inv[43]=67.4593;
			chi2inv[44]=68.7095;
			chi2inv[45]=69.9568;
			chi2inv[46]=71.2014;
			chi2inv[47]=72.4433;
			chi2inv[48]=73.6826;
			chi2inv[49]=74.9195;
			chi2inv[50]=76.1539	;										
		}
		~Chi2InvTable(){}
		double getChi2(int dof) {
			return chi2inv[dof];	
		}		
};

/* structs
 */
 
struct xyPair {
	double x;
	double y;
}; 
 
/* functions
 */

// creates a new value for x from the gaussian distribution with
// mean and sigma
inline void gaussian(double &x,double mean,double sigma)
{	
	// use mean and sigma entries to obtain a new x value
	double x1,x2,y;
	
	// take two random variables from the interval (0,1)
	do {
		x1=rand()/(RAND_MAX+1.0);
		x2=rand()/(RAND_MAX+1.0);
	} while (x1==0 || x2==0);
	
	// box-muller transformation; y is now distributed according
	// to a nomalized gaussian (mean=0; deviation=1)
	y=sqrt(-2*log(x1))*cos(2*M_PI*x2); 
	// then the variable y*sigma has standard deviation sigma
	y*=sigma;
	// add this error to x
	x=mean+y;
}
		
/* This function computes the best-fit constnat regression 
 * coefficient c0 of the model Y = c_0 for the weighted datasets (x, y), 
 * two vectors of length n with strides xstride and ystride. The vector w, of length n 
 * and stride wstride, specifies the weight of each datapoint. The weight is the reciprocal 
 * of the variance for each datapoint in y.
 * The variance for the parameters c0 is estimated from weighted data and 
 * returned via the parameter cov00. The weighted sum of squares of the 
 * residuals from the best-fit line, \chi^2, is returned in chisq. 
 */
inline int fit_wconst (const double * w, const size_t wstride, 
			    const double * y, const size_t ystride, size_t n, 
			    double * c0, double * cov00, double * chisq)
{
	/* calculate best fit parameter c0 and its variance */
	double param=0.;
	double varia=0.;
	
	const double * y_aux = y;
	const double * w_aux = w;

	for (unsigned int i=0;i<n;i++) {
		param+=(*y_aux)*(*w_aux);
		varia+=*w_aux;
		y_aux+=ystride;
		w_aux+=wstride;
	}	
	*c0=param/varia;	
	*cov00=1/varia;
	
	/* calculate chisq */		
	*chisq=0;
	for (unsigned int i=0;i<n;i++) {
		*chisq+=(*y-*c0)*(*y-*c0)*(*w);
		y+=ystride;
		w+=wstride;
	}
	
	return 0;
}	
		    
/* This function uses the best-fit linear regression coefficient c0 and its estimated 
 * covariance cov00 to compute the fitted function y and its standard deviation y_err for 
 * the model Y = c_0 at the point x.
 */

inline int fit_const_est (double c0, double cov00, double * y, double * y_err)
{
	*y=c0;
	*y_err=sqrt(cov00);	
	return 0;	
} 			    

/* This function uses the best-fit linear regression coefficients c and their estimated 
 * covariance cov to compute the fitted function y and its standard deviation y_err for 
 * the model y = X c.
 */
inline int multifit_linear_est (const gsl_matrix * x, const gsl_vector * c, const gsl_matrix * cov, gsl_vector * y, gsl_vector * y_err)
{
	// calculate y = X * c
	gsl_blas_dgemv (CblasNoTrans, 1., x, c, 0., y);
	
	// calculate Cov(Y) = X Cov(C) X^T
	// using BLAS this is done efficiently in the following order
	// let A = cov, B = X
	// 1) calculate M = ( A B^T )^T = B A  ( A is symmetric)
	// 2) calculate N = B M^T 
	
	gsl_matrix *m = gsl_matrix_alloc (x->size1,cov->size2);
	gsl_matrix *n = gsl_matrix_alloc (x->size1,m->size1);
	
	gsl_blas_dsymm (CblasRight, CblasUpper, 1.0, cov, x, 0.0, m);
	gsl_blas_dgemm (CblasNoTrans, CblasTrans , 1.0, x, m, 0.0, n);
	
	// grep the diagonal elements which correspond to the variance
	gsl_vector_view v = gsl_matrix_diagonal(n);
	gsl_vector_memcpy (y_err, &v.vector);
	
	gsl_matrix_free (m);
	gsl_matrix_free (n);
	
	return 0;
}

/* comparison functions for sorting etc
 */
int compDouble(const void * a,const void * b);
int compDataPair(const void * a,const void * b);

/* 
 * Use m_AB  = -2.5 log10(f_i) - ZEROPOINT
 * where f_i is the flux in units dependent on the ZEROPOINT
 * if ZEROPOINT=48.6 -> flux units are ergs s^-1 cm^-2 Hz^-1
 * m_AB the given AB magnitude
 */
 
inline double g_fluxToMag(const double & flux)
{
	return -2.5*log10(flux)-ZEROPOINT;
	
	//error[i]=2.5*log10(1+error[i]/value[i]);
	//value[i]=-2.5*log10(value[i])-ZEROPOINT;
}

/*
 * A relative flux error has to be supplied
 */

inline double g_fluxToMagError(const double & fluxError)
{
	if (MAGERRTYPE==0)
		return 2.5*log10(1+fluxError);
	else // P.Capak's definition
		return fluxError*2.5/log(10.);
}

/* 
 * Calculate f_i from m_AB  = -2.5 log10(f_i) - ZEROPOINT
 * where f_i is the flux in units dependent on the ZEROPOINT
 * if ZEROPOINT=48.6 -> flux units are ergs s^-1 cm^-2 Hz^-1
 * m_AB the given AB magnitude
 */
 

inline double g_magToFlux(const double & mag)
{
	return RFEXP10( -(mag+ZEROPOINT)/2.5 );		
}

/*
 * A relative flux error is calculated	
 */

inline double g_magToFluxError(const double & magError)
{
	if (MAGERRTYPE==0)
		return 	(RFEXP10( magError/2.5 ) - 1 );
	else // P.Capak's definition
		return magError*log(10.)/2.5;		
}

inline int SplitString(string data, const string & delimiters, vector<string>  & arr)
{
	string item = "";
	arr.clear();
	int length = data.length();

	for(int i=0; i<length; i++)
	{
		if(delimiters.find(data[i])==string::npos ) // none of the delimiters
		{	
			item += data[i];			
		}
		else
		{			
			if (! item.empty()) 			  
			   arr.push_back(item);			
			item = "";
		}
	}	
	if(!item.empty()) 		
		arr.push_back(item);	
		
	int size = arr.size();
	return size;
}

inline double round(double value, int precision) {
	double aux=pow(10.,precision);
	return floor(value*aux+0.5)/aux;	
}

#endif //_NUMERICALDATA_H_
