#ifndef _BAYESSOLVER_H_
#define _BAYESSOLVER_H_

#include <sys/times.h>
#include <algorithm>
#include "globHeader.h"
#include "Template.h"
#include "Filter.h"
#include "Catalog.h"
#include "CommandLineHandler.h"
#include "float.h"
#include "FluxFile.h"
#include "Likelihood.h"
#include "BroadZBinning.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

class BayesSolver
{
protected:
	CommandLineHandler *clh;
	Template** templates;
	TemplateContainer *templateContainer;
	Filter** filters;
	Filter* B_filter;	
	Catalog* catalog;	
	OutputHandler *out;
	FluxRestriction *fluxRestriction;
	Likelihood *like;
	Prior *prior;	
	BinningZ *binZ;
#ifdef PARRAY	
	double ***flux;
#else
        double *flux;
#endif
	
	int numTemplates;
	int numFilters;		
				
	void createFilters(int numFilters, char** nameFilters);	
	void createCatalog();	
	void saveTemplate(char *filename,int t, int zi, double a);
	void printTemplates(const char *filebase);
	void saveObservedTemplate(char *filename,int t, int zi, double a);	
	void printFilters(const char *filebase);
	void normalizeTemplates(bool, bool);
	void templatesFiltersToMesh(int extend);// extend = 1:  templates are extended to grid 
											//              even if not given there (set to zero)
											// extend = 0:  templates are only meshed in the
											//				region where they are given
	void prepareA();
	void prepareAnoCatalog();
	void prepareB();	
	void checkTemplateRange();
	
	void calcFlux();
	void calcMaxLikelihood(const char *iterationInfix);
	void calcBayesian();
	void calcCatalogCorrection(const char *iterationInfix);		// calculates fit parameters for catalog corr.
	void applyCatalogCorrection(const char *iterationInfix);		// applies the catalog correction and saves 
															// catalog; 
															
															// interationInfix is an additional label 
															// for the input/output filenames
															
	void calcKcorrection();					                // calculating the K-correction
	void printAuxHeader(char *filename);
	void printAux(char *filename, int failed);
	void checkRhoSigmaFile(std::string filename, unsigned int basicTypeID);
	void printUseRhoSigmaFile(std::string filename, unsigned int basicTypeID);
	
public:
	BayesSolver(CommandLineHandler *clh);
	~BayesSolver();
	
	void setOutputHandler(OutputHandler *out) {
		this->out=out;
	}
	
	void improveTemplates();				// doing the automatic template correction
	void calcPhotoZ();						// normal run mode incl. ML, Bayesian, 
											//                       Catalog-correction, K-correction
};

#endif //_BAYESSOLVER_H_
