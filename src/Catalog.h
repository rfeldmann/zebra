#ifndef _CATALOG_H_
#define _CATALOG_H_

#include <cmath>
#include <getopt.h>
#include "globHeader.h"
#include "ConfFile.h"
#include "FluxFile.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

using namespace std;

class Catalog
{
protected:
	char *filename;					// name of loaded catalog file
	char *name;						
	char *shortname;
	char *defFileName;  			// this file defines which column 
									// corresponds with which filter etc
	int colnumber;					// there are 0..colnumber-1 columns present in catalog file
	int linenumber; 				// there are 0..linenumber-1 lines in the catalog file								
	int specZ;						// whether there are spec-z values in the catalog		
	int numFilterBands;				// number of filter bands		
	int numSpecZBands;				// number of redshift bands
	double *oneSigmaError;			// contains 1-sigma error for each filter band
	double *minError;				// contains mininum relative error for each filter band
	double *zp;						// contains zero-points offset for each band
	FilterBand **filterBands; 		// stores the filterband objects
	FilterBand **specZBands;		// stores spectroscopic redshifts
									// more than one column may be given	
	const static int numOptions=8;	// number of allowed options
	int options[numOptions];		// following options are supported:
									// index name "description" default						
									// 0 flux-type "photon count per frequency
									//              vs energy per frequency" 0								
									// 1 help      "shows available options"
									// 2 flux-Entries "entries in file in mag (0) 
									// 			   or fluxes (1)"
									// 3 flux-Entries-Save "whether writing out 
									//			   entries in mag (0) / fluxes (1)
									// 4 using Zamorani K-band correction (0) no (1) yes
									// 5 missing error columns are allowed (use the filter band) (0) no (1) yes
									// 6 when saved, use original errors (1) instead of the modified errors (0)
									// 7 when saved, save also irrelevant columns -> preserve catalog-table shape
    double magCatalogLow;			// code provokes warning if catalog value is smaller than this
	double magCatalogHigh;			// code provokes warning if catalog value is larger than this	
	OutputHandler *m_out; 			// deals with printing 	
	int loaded;						// whether load has been called;					

	// look through catalog file
	void check(char* filename);
	
	// read catalog file
	void read(char* filename);
									
	void setOptions(int defnumOptions, char** defOptions);	
	
	// transforms magnitudes into fluxes (within the filterBands)
	void magToFlux();
			
	// transform fluxes back to magnitudes (within the filterBands)
	void fluxToMag();		
	
	// add zero point offset
	void subtractZP();
	
	// changes fluxes and errors according to mininum error criterions etc
	void modifyFluxAndError(double * f, double * e, int entry, int band, char **filterBandMissing);
	
public:
	Catalog(const char* name,const char* defFileName);
	Catalog(const char* name,const char *shortname, const char* defFileName);   
	
	~Catalog();		
	
	void load();
	void load(char* filename);	
	
	void setOutput(OutputHandler *out) { m_out=out; }	
	void setFluxType(int fluxType) { options[0]=fluxType; };	
	void setFluxEntries(int fluxEntries) { options[2]=fluxEntries; };			
	void setFluxEntriesSave(int fluxEntriesSave) { options[3]=fluxEntriesSave; };
	void setkBandCorrection(int kBandCorrection) { options[4]=kBandCorrection; };
	void setAllowMissingError(int allowMissingError) { options[5]=allowMissingError; };
	void setPreserveErrorsOutput(int preserveErrorsOutput) { options[6]=preserveErrorsOutput; };
	void setPreserveCatalogFormat(int preserveCatalogFormat) { options[7]=preserveCatalogFormat; };
	void setMagCatalogLow(double magCatalogLow) { this->magCatalogLow=magCatalogLow;}
	void setMagCatalogHigh(double magCatalogHigh) { this->magCatalogHigh=magCatalogHigh;}
	
	char* getName(){ return name; }
	char* getShortName(){ return shortname; }
	int getLineNumber() { return linenumber; }

	int getColNumber() { return colnumber; }
	int getFluxType() { return options[0]; };
	double *getColFlux(int filterIndex);
	double *getColErrorFlux(int filterIndex);
	double *getColSpecZ(int specZIndex);
	double *getColErrorSpecZ(int specZIndex);	
	void getRowFlux(int lineIndex, double* row);
	void getRowErrorFlux(int lineIndex, double* row);
	void getRowInvalidFlux(int lineIndex, char *row);
	void getRowSpecZ(int lineIndex,double* row);
	double getRowSpecZ(int lineIndex,int row);
	void getRowErrorSpecZ(int lineIndex,double* row);
	
	int getnumFilterBands() { return numFilterBands;};
	FilterBand** getFilterBands() { return filterBands; }
	int getnumSpecZBands() { return numSpecZBands;};
	int isSpecZ() { return specZ; };
	int isLoaded() { return loaded; };			
	
	void readDefFile(int numFiltersIn,char** nameFiltersIn,
					 int & numFiltersOut,char** & nameFiltersOut);
	void errorAnalysis(int xBins,int xEntriesNr,
					   int errorBins,int errorEntriesNr);				 
	
	void writeErrorAnalysis(const char* path, const char * filenamebase);
	
	// multiply each relative flux errors by a constant factor
	void multiplyError(double factor);
			
	// set each relative flux error to a minimum value		
	void floorError(double minError);
	
	// set each relative flux error to a minimum value	for given filterband with index filterBandNr
	void floorError(double minError, int filterBandNr);
	
	// randomizes a catalog by just changing all fluxes by a value drawn from a gaussian
	// the sigma of the gaussian is taken to be the absolute flux error
	void reshuffleGaussian();
	
	// randomizes a catalog by just changing all fluxes by a value drawn from a gaussian
	// the sigma of the gaussian is given (error)
	void reshuffleGaussian(double error);
	
	// randomizes a catalog using data from files
	void randomize(char *errorpath, char* errorbase, int errorHistogram, double minError);
	
	// save a catalog
	void save(const char *outputpath,const char* outputbase);		
	
	// show member values
	void show();	
};

#endif //_CATALOG_H_
