#ifndef _FILTER_H_
#define _FILTER_H_

#include "LambdaDataSet.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

using namespace std;

class Filter: public LambdaDataSet
{
protected:			
		
public:
	Filter(char* name) : LambdaDataSet(name){};
	Filter(char* name, char* shortname) : LambdaDataSet(name,shortname){};
	virtual ~Filter(){};
			
	double getPivotWavelength();
	double getAverageWavelength();
	double getRMSWidth();	
	double getFWHM();
	double getMax();
	void getTransmission(xyData * data);
	
	void smoothRectangular(double fwhm, int smoothMode) {	// smoothes it with spectral resolution fwhm (if smoothMode==0)
	                                                        // or with inverse resolving power fwhm/lambda (if smoothMode==1)
		if (! this->loaded) 
			this->load();
			
		this->values->average(fwhm,0,smoothMode);
	}
	
	void smoothGaussian(double sigma) {	// smoothes it with a gaussian of standard deviation sigma
	
		if (! this->loaded) 
			this->load();
			
		this->values->convolveGaussian(sigma);
	}
};

#endif //_FILTER_H_
