#ifndef _COMMANDLINEHANDLER_H_
#define _COMMANDLINEHANDLER_H_

#include <getopt.h>
#include "ConfFile.h"
#include "globHeader.h"
#include "StartHeader.h"
 
/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/
 
class CommandLineHandler
{
private:
	int argc;
	char** argv;	
	
	OutputHandler *m_out;
	
	int   logInterpolation;
	int   logInterpolationMode;
	int   linInterpolation;
	int   linInterpolationMode;	
	int   fluxType;
	int   fluxEntries;  		// catalog entries in mags (0) or fluxes (1)
	int   preserveErrorsOutput;	// print original errors (unmodified) if catalog is saved
	int   preserveCatalogFormat;// just replaces catalog flux values & errors, do not modify shape of table
	int   verboseLevel; 		// 0=quiet, 1=normal, 2=more info, ... 
	int   numFilters;
	int   numTemplates;
	int   loadFlux;
	int   saveFlux;
	int   loadPrior;
	int   savePrior;
	int   savePriorIterative;	
	int   calcPrior;
	int   calcPriorMode;
	int   maxIterations;
	int   binZType;
	int   igAbsorption;
	int   kBandCorrection;
	int   mbMode;
	int   allowMissingError;	
	int   improveTemplate;
	int   improveTemplateMode;
	//int   dimensionlessParameterMode;
	int   rescaleWithMesh;
	int   rescaleWithBands;
	int   rescaleWithFlux;
	int   loadRhoSigma;
	int   saveRhoSigma;	
	double mbLow;
	double mbHigh;
	double magCatalogLow;
	double magCatalogHigh;
	double percCut;
	double chi2ReducedCut;
	double omegaM;
	double omegaL;
	double hubble;
	double smoothLimitLow;	
	int   punishGradient;
	int   useMesh;
	int   linearMesh;
	int   normalizeTemplates;
	int   rightZMode;
	int   correctionSteps;
	int   calcCatalogCorrection;
	int   applyCatalogCorrection;	
	int   iterationsCatalogCorrection;
	int   singleFit;
	int   kcorrection;
	int   showTable;
	int   calcBayesian;
	int   calcLikelihood;
	int   smoothPrior;
	int   single;
	int   *dump;
	int   *likelihood;
	int   *posterior;
	int   *printData;
	double chi2Add;
	double chi2Mult;
	double sigma;
	double rho;
	double smoothT;
	double smoothZ;
	int    smoothZMode;
	double smoothFilter;
	double smoothTemplate;
	int    smoothTFMode;
	double zmin,zmax,dz;
	double *massToLightRatios;
	double *minZ,*maxZ;
	double meshLambdaMin;
	double meshLambdaMax;
	double meshLambdaRes;
	double downgradingFactor;
	char  filterpath[MAX_LENGTH];
	char  templatepath[MAX_LENGTH];
	char  catalogpath[MAX_LENGTH];
	char  fluxpath[MAX_LENGTH];
	char  fluxbase[MAX_LENGTH];
	char  priorbase[MAX_LENGTH];
	char  priorLoadBase[MAX_LENGTH];
	char  priorpath[MAX_LENGTH];
	char  nameCatalog[MAX_LENGTH];
	char  nameDefFile[MAX_LENGTH];
	char  BFilterName[MAX_LENGTH];
	char  rhoSigmaLoadBase[MAX_LENGTH];
	char  rhoSigmaSaveBase[MAX_LENGTH];
	char** nameFilters;
	char** nameTemplates;
	char** rhoSigmaFiles;
	char catalogCorrectionDef[MAX_LENGTH];
	char catalogCorrectionBase[MAX_LENGTH];
	char templateImprovementDef[MAX_LENGTH];
	char outputbase[MAX_LENGTH];
	char outputpath[MAX_LENGTH];			
		
	static const int numOptions=98;
	
public:	
	CommandLineHandler(int argc, char** argv);
	virtual ~CommandLineHandler();
	void readCommandLine();	
	int getFluxType() { return fluxType; };
	int getFluxEntries() { return fluxEntries; };
	int getPreserveErrorsOutput() { return preserveErrorsOutput; };
	int getPreserveCatalogFormat() { return preserveCatalogFormat; };
	int getVerboseLevel() { return verboseLevel; };
	int getnumFilters() { return numFilters; };
	int getnumTemplates() { return numTemplates; };	
	int getLogInterpolation() { return logInterpolation; };
	int getLogInterpolationMode() { return logInterpolationMode; }
	int getLinInterpolation() { return linInterpolation; };
	int getLinInterpolationMode() { return linInterpolationMode; }
	int getloadFlux() { return loadFlux; };
	int getsaveFlux() { return saveFlux; };
	int getloadPrior() { return loadPrior; };
	int getsavePrior() { return savePrior; };
	int getsavePriorIterative() { return savePriorIterative; };	
	int getCalcPrior() { return calcPrior; };
	int getCalcPriorMode() { return calcPriorMode; };
	int getMaxIterations() { return maxIterations; };
	int getbinZType() { return binZType; };
	int getigAbsorption() { return igAbsorption; };
	int getKBandCorrection() { return kBandCorrection; };
	int getMBMode() { return mbMode; };
	int getAllowMissingError() { return allowMissingError; };	
	int getImproveTemplate() { return improveTemplate; };
	int getImproveTemplateMode() { return improveTemplateMode; };	
	int getRescaleWithMesh() { return rescaleWithMesh; };
	int getRescaleWithBands() { return rescaleWithBands; };
	int getRescaleWithFlux() { return rescaleWithFlux; };
	int getLoadRhoSigma() { return loadRhoSigma; };
	int getSaveRhoSigma() { return saveRhoSigma; };
	double getMBLow() { return mbLow; };
	double getMBHigh() { return mbHigh; };
	double getMagCatalogLow() { return magCatalogLow; };
	double getMagCatalogHigh() { return magCatalogHigh;};	
	double getPercCut() { return percCut; };
	double getChi2ReducedCut() { return chi2ReducedCut; };
	double getOmegaM() { return omegaM; };
	double getOmegaL() { return omegaL; };
	double getHubble() { return hubble; };
	double getSmoothLimitLow() { return smoothLimitLow; }	
	double getSmoothFilter() { return smoothFilter; }
	double getSmoothTemplate() { return smoothTemplate; }
	int    getSmoothTFMode() { return smoothTFMode; }
	double getChi2Add() { return chi2Add; };
	double getChi2Mult() { return chi2Mult; };
	double getSigma() { return sigma; }
	double getRho() { return rho; }
	int getPunishGradient() { return punishGradient; }
	int getUseMesh() { return useMesh; };
	int getLinearMesh() { return linearMesh; };
	int getNormalizeTemplates() { return normalizeTemplates; };
	int getRightZMode() { return rightZMode; }
	int getCorrectionSteps() { return correctionSteps; };	
	int getCalcCatalogCorrection() { return calcCatalogCorrection; }
	int getApplyCatalogCorrection() { return applyCatalogCorrection; }
	int getIterationsCatalogCorrection() { return iterationsCatalogCorrection; };
	int getSingleFit() { return singleFit; }
	int getKcorrection() { return kcorrection; };
	int getShowTable() { return showTable; };	
	int getCalcBayesian() { return calcBayesian; };
	int getCalcLikelihood() { return calcLikelihood; };
	int getSmoothPrior() { return smoothPrior; };
	int getSingle() { return single; };
	int *getDump() { return dump; };
	int *getLikelihood() { return likelihood; };
	int *getPosterior() { return posterior; };
	int *getPrintData() { return printData; };
	double getSmoothZ() { return smoothZ; };
	double getSmoothT() { return smoothT; };
	int getSmoothZMode() { return smoothZMode; };
	double getZmin() { return zmin; };
	double getZmax() { return zmax; };
	double getdZ() { return dz; };	
	double *getMassToLightRatios() { return massToLightRatios; };
	double *getMinZ() { return minZ; }
	double *getMaxZ() { return maxZ; }
	double getMeshLambdaMin() { return meshLambdaMin; };
	double getMeshLambdaMax() { return meshLambdaMax; };
	double getMeshLambdaRes() { return meshLambdaRes; };
	double getDowngradingFactor() { return downgradingFactor; }
	char **getnameFilters() { return nameFilters; };
	char **getnameTemplates() { return nameTemplates; };
	char **getRhoSigmaFiles() { return rhoSigmaFiles; };
	char *getnameDefFile() { return nameDefFile; };
	char *getnameCatalog() { return nameCatalog; };	
	char *getoutputpath() { return outputpath; };
	char *getoutputbase() { return outputbase; };
	char *getfilterpath() { return filterpath; };
	char *gettemplatepath() { return templatepath; }
	char *getcatalogpath() { return catalogpath; }
	char *getfluxpath() { return fluxpath; };
	char *getfluxbase() { return fluxbase; };
	char *getpriorbase() { return priorbase; };
	char *getpriorLoadBase() { return priorLoadBase; }
	char *getpriorpath() { return priorpath; };
	char *getCatalogCorrectionDef() { return catalogCorrectionDef; }
	char *getCatalogCorrectionBase() { return catalogCorrectionBase; }
	char *getTemplateImprovementDef() { return templateImprovementDef; }
	char *getBFilterName() { return BFilterName; };	
	char *getRhoSigmaLoadBase() { return rhoSigmaLoadBase; };
	char *getRhoSigmaSaveBase() { return rhoSigmaSaveBase; };
	
	void setDump(int i, int j) { dump[i]=j; }
	void setOutputHandler(OutputHandler *out) {
		m_out=out;
	}
};

#endif //_COMMANDLINEHANDLER_H_
