#ifndef _DATASET_H_
#define _DATASET_H_

#include <fstream>
#include <iostream>
#include <string>
#include "globHeader.h"
#include "allocArray.h"
#include "NumericalData.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

using namespace std;

class DataSet
{
protected:
    char* name;
    char* shortname;
	char** head;
	char** comment;
	double** body;	
	double* maxColValue;
	double* minColValue;
	int colnumber;		// there are 0..colnumber-1 columns present
	int linenumber; 	// there are 0..linenumber-1 lines in the file
	int commentnumber;  // there are 0..commentnumber-1 comment lines
	int loaded;			// whether load has been called;
	OutputHandler *m_out; // deals with printing 
		
	void check(char* filename);
	void read(char* filename);
	
public:
	DataSet(char *name);
	DataSet(char *name, char *shortname);	
	DataSet(DataSet *d);
	~DataSet();
	
	void setOutput(OutputHandler *out) {
		m_out=out;
	}
	char* getName() {
		return name;
	}
	char* getShortName() {
		return shortname;
	}
	void setName(char *name) {
		strcpy(this->name,name);
	}
	int getLineNumber();
	int getColNumber();
	void getColumn(double* entries, int colindex);
	void getLine(double* entries, int lineindex);
	double** _accessBody();		// direct connection to body array
	char** _accessHead();		// direct connection to head array
	void clear();	
	void fill(int linenumber,int colnumber,char** head,double** body);
	void print();
	void check() {
		check(name);
	}
	
	int getCommentNumber() { return this->commentnumber;};
	char** _accessComment() { return this->comment; };
};

#endif //_DATASET_H_
