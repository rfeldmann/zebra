#include <iostream>
#include "globHeader.h"
#include "allocArray.h"
#include "NumericalData.h"

using namespace std;

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/
					 					 
inline void getGaussian(double & y1, double & y2)
{
	// use mean and sigma entries to obtain a new x value
	double x1,x2;
	
	// take two random variables from the interval (0,1)
	do {
		x1=rand()/(RAND_MAX+1.0);
		x2=rand()/(RAND_MAX+1.0);
	} while (x1==0 || x2==0);
	// box-muller transformation; y1,y2 are now independently
    // distributed to a normalized gaussian (mean=0; deviation=1)
	y1=sqrt(-2*log(x1))*cos(2*M_PI*x2); 
	y2=sqrt(-2*log(x1))*sin(2*M_PI*x2);			
}
					 					 
int main(int argc, char **argv)
{	
	double *z;
	double norm;
	double aux;
	
	BinningZ * binZ;
	binZ = new BinningZ();
	binZ->set(1, 0.0, 8.0, 0.01);
	z=binZ->getZ();
	int numTemplates=37;
	
	
	ofstream ofile;
	ofile.open("gaussianPrior.dat");
	CHECK(ofile, "gaussianPrior.dat");
	ofile << "#2d gaussian Prior" << endl;
	ofile << numTemplates << " " << binZ->getLength() << endl;
	
	// mean and standard deviation
	double tm=numTemplates/2;
	double zm=(z[binZ->getLength()-1]-z[0])/2;
	double ts=numTemplates/4;
	double zs=(z[binZ->getLength()-1]-z[0])/4;
	
	double tm2=numTemplates/8+8;
	double zm2=(z[binZ->getLength()-1]-z[0])/3;
	double ts2=numTemplates/10;
	double zs2=(z[binZ->getLength()-1]-z[0])/25;
	
	double tm3=numTemplates/8+20;
	double zm3=(z[binZ->getLength()-1]-z[0])/3+3;
	double ts3=numTemplates/7;
	double zs3=(z[binZ->getLength()-1]-z[0])/15;
	
	for (int t=0;t<numTemplates;t++) {	
		norm=0;
		for (int zi=0;zi<binZ->getLength();zi++) {
			norm+=exp(-( (z[zi]-zm)*(z[zi]-zm)/(2*zs*zs)+(t-tm)*(t-tm)/(2*tm*tm)));
			norm+=exp(-( (z[zi]-zm2)*(z[zi]-zm2)/(2*zs2*zs2)+(t-tm2)*(t-tm2)/(2*tm2*tm2)));
			norm+=exp(-( (z[zi]-zm3)*(z[zi]-zm3)/(2*zs3*zs3)+(t-tm3)*(t-tm3)/(2*tm3*tm3)));
		}
	}
	
	for (int t=0;t<numTemplates;t++) {	
		for (int zi=0;zi<binZ->getLength();zi++) {	
			aux=exp(-( (z[zi]-zm)*(z[zi]-zm)/(2*zs*zs)+(t-tm)*(t-tm)/(2*ts*ts)));
			aux+=exp(-( (z[zi]-zm2)*(z[zi]-zm2)/(2*zs2*zs2)+(t-tm2)*(t-tm2)/(2*ts2*ts2)));
			aux+=exp(-( (z[zi]-zm3)*(z[zi]-zm3)/(2*zs3*zs3)+(t-tm3)*(t-tm3)/(2*ts3*ts3)));
			ofile << t << " " << z[zi] << " " << aux/norm << endl;
		}
	}
	
	ofile.close();
	ofile.clear();
}
