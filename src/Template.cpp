#include "Template.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

Template::Template(char* name) : LambdaDataSet(name)
{	
	this->numFilters=0;
	this->filters=NULL;
	this->b_filter=NULL;	
	
	this->m_bFlux=-1;
	this->m_bFluxNormalized=-1;	
	
	this->m_binZ=NULL;
	this->m_iga=NULL;
	this->massToLightRatio=1;
	this->minZ=TEMPLATE_MIN_Z;
	this->maxZ=TEMPLATE_MAX_Z;
	
	this->havePrepared=0;	
			
	this->pattern=NULL;
	this->weightsPattern=NULL;		
		
	// sets default values (0)
	for (int i=0;i<numOptions;i++) 
		this->options[i]=0;	
	
	this->interpol=0;
}

Template::Template(char* name, char *shortname) : LambdaDataSet(name,shortname)
{	
	this->numFilters=0;
	this->filters=NULL;
	this->b_filter=NULL;	
	
	this->m_bFlux=-1;
	this->m_bFluxNormalized=-1;	
	
	this->m_binZ=NULL;
	this->m_iga=NULL;
	this->massToLightRatio=1;
	this->minZ=TEMPLATE_MIN_Z;
	this->maxZ=TEMPLATE_MAX_Z;
	
	this->havePrepared=0;	
			
	this->pattern=NULL;
	this->weightsPattern=NULL;		
		
	// sets default values (0)
	for (int i=0;i<numOptions;i++) 
		this->options[i]=0;	
}

Template::Template(Template *t) : LambdaDataSet(t)
{
	numFilters=t->numFilters;
	filters=t->filters;
	b_filter=t->b_filter;	
	
	m_bFlux=t->m_bFlux;
	m_bFluxNormalized=t->m_bFluxNormalized;
	
	m_binZ=NULL;
	if (t->m_binZ)
		m_binZ = new BinningZ(t->m_binZ);
		
	m_iga=NULL;	
	if (t->m_iga)
		m_iga = new IGAbsorption(t->m_iga);		
	massToLightRatio = t->massToLightRatio;
	minZ = t->minZ;
	maxZ = t->maxZ;
	havePrepared = t->havePrepared;			
	
	pattern=NULL;
	if (t->pattern) {
		allocArray1(pattern,numPattern);
		for (int i=0;i<numPattern;i++) 
			pattern[i]=t->pattern[i];
	}
	
	weightsPattern=NULL;
	if (t->weightsPattern) {
		allocArray1(weightsPattern,numPattern);
		for (int i=0;i<numPattern;i++)
			weightsPattern[i]=t->weightsPattern[i];
	}
	
	for (int i=0;i<numOptions;i++) 
		options[i]=t->options[i];			
}

Template::~Template()
{
	resize();
}

void Template::resize()
{
	if (pattern)
		freeArray1(pattern);
	if (weightsPattern)
		freeArray1(weightsPattern);
	if (m_iga) {
		delete m_iga;		
		m_iga=NULL;
	}
	if (m_binZ) {
		delete m_binZ;	
		m_binZ=NULL;
	}	
	m_bFlux=-1;
	m_bFluxNormalized=-1;
	havePrepared=0;
	b_filter=NULL;
	filters=NULL;	
	for (int i=0;i<numOptions;i++) 
		options[i]=0;
	massToLightRatio=1;
}

/* REMOVE
void Template::setDustData(Template **pattern, int dustLaw, double ebmv) {
	this->pattern=(Template**)malloc(sizeof(Template*));
	this->pattern[0]=pattern[0];
	this->dustLaw=dustLaw;
	this->ebmv=ebmv;
	this->dustTemplate=1;
}
*/


void Template::setInterpolData(Template **pattern, double* weightsPattern)
{
	this->pattern=(Template**)malloc(numPattern*sizeof(Template*));
	this->weightsPattern=(double*)malloc(numPattern*sizeof(double));	
	for (int i=0;i<numPattern;i++) {		
		this->pattern[i]=pattern[i];
		this->weightsPattern[i]=weightsPattern[i];
	}		
	this->interpol=1;
}

void Template::prepareCalcFlux(int numFilters,Filter** filters,BinningZ *binZ)
{
	if (m_iga) {
		delete m_iga;
		m_iga=NULL;
	}
	
	if (m_binZ)
		delete m_binZ;
		
	this->numFilters=numFilters;
	this->filters=filters;
	m_binZ = new BinningZ();
	m_binZ->copy(binZ);	
	havePrepared=1;
}

double Template::getBFlux()
{
	xyData *data_b,*data_aux;
	
	if (m_bFlux>=0)
		return m_bFlux;
	
	if (! this->loaded)
		load();				
										
	data_b = new xyData();
	b_filter->getxyData(data_b);											
	data_aux = new xyData();
	data_aux->copy(values,0,values->getlength()-1);
	if (((*data_aux) * data_b)==NULL)
		EXIT("Could not normalize template to B band");
	if (options[2]==0) 
		data_aux->yTimesx();	
	m_bFlux=data_aux->integrate();		
	delete data_aux;
	delete data_b;
	return m_bFlux;
}

double Template::getBFluxNormalized()
{
	double norm;
	xyData *data_b,*data_aux;
	
	if (m_bFluxNormalized>=0)
		return m_bFluxNormalized;
	
	if (! this->loaded)
		load();				
									
	data_b = new xyData();
	b_filter->getxyData(data_b);											
	data_aux = new xyData();
	data_aux->copy(values,0,values->getlength()-1);
	if (((*data_aux) * data_b)==NULL)
		EXIT("Could not normalize template to B band");
	if (options[2]==0) {
		data_aux->yTimesx();	
		norm=b_filter->getFrequencyNormalization(data_aux);
	} else
		norm=b_filter->getStrangeNormalization(data_aux);			
	m_bFluxNormalized=data_aux->integrate()/norm;		
	delete data_aux;
	delete data_b;
	return m_bFluxNormalized;
}

double Template::intern_normalizeToFilter(Filter *filter,xyData *val)
{
	xyData *data,*data_aux;
	double flux_normalized;
	double norm;
	double pivot_wavelength;
	char s[MAX_LENGTH];
			
	data = new xyData();
	filter->getxyData(data);											
	data_aux = new xyData();
	data_aux->copy(val,0,val->getlength()-1);
	if (((*data_aux) * data)==NULL) {
		sprintf(s,"Could not normalize template to filter %s",filter->getName());		
		EXIT(s);
	}
	
	if (options[2]==0) {
		data_aux->yTimesx();	
		norm=filter->getFrequencyNormalization(data_aux);
	} else
		norm=filter->getStrangeNormalization(data_aux);			
	
	// flux normalization for nu-flux	
	flux_normalized=data_aux->integrate()/norm;		
	
	// but here flux is given as lambda-flux (flux per unit wavelength) ->
	pivot_wavelength=filter->getPivotWavelength();
	flux_normalized/=pivot_wavelength*pivot_wavelength;
		
	delete data_aux;
	delete data;
	
	return flux_normalized;				
}


void Template::normalizeToFilter(Filter *filter,xyData *val)
{
	char s[MAX_LENGTH];
	
	if (! this->loaded)
		load();			
		
	double flux_normalized=intern_normalizeToFilter(filter,val);
	if (((*val)/flux_normalized)==NULL) {
		sprintf(s,"Could not normalize template to filter %s",filter->getName());		
		EXIT(s);
	}			
	
	sprintf(s,"%s %s %g",getShortName(),filter->getName(),flux_normalized);
	this->m_out->println(s,2);	
}

void Template::normalizeToFilter_values(Filter *filter)
{
	if (! this->loaded)
		load();		
	
	normalizeToFilter(filter,this->values);	
}

void Template::normalizeToFilter_meshed(Filter *filter)
{
	if (! this->loaded)
		load();		
	if (this->meshedValues)
		normalizeToFilter(filter,this->meshedValues);		
}
	
void Template::loadInterpol()
{															
	xyData *data2=NULL;	
	double minZ1,minZ2,maxZ1,maxZ2;
	
	if (this->loaded)
		return;
							
	// make sure that templates are loaded
	for (int i=0;i<this->numPattern;i++)
		if (! (this->pattern[i])->isLoaded())
			(this->pattern[i])->load();										
	
	// get lambda - flux values from templates (pattern)				
	values = new xyData();
	data2 = new xyData();			
	(this->pattern[0])->getxyData(values);
	(this->pattern[1])->getxyData(data2);
	minZ1=this->pattern[0]->getMinZ();
	minZ2=this->pattern[1]->getMinZ();
	this->minZ=(minZ1 > minZ2) ? minZ1 : minZ2;
	maxZ1=this->pattern[0]->getMaxZ();
	maxZ2=this->pattern[1]->getMaxZ();
	this->maxZ=(maxZ1 < maxZ2) ? maxZ1 : maxZ2;	
	
	if (options[0]==0) { // log interpolation: values <= values^weight1 * data2^weight2
		
		// for log interpolation a normalization is not necessary								
		values->yPow(weightsPattern[0]);
		data2->yPow(weightsPattern[1]);
		if ( (*values) * data2 == NULL )
			EXIT("Could not multiply two data sets during log-interpolation");		
		m_out->println("Log-Templated multiplied",4);
		
	} else {	// linear interpolation: values <= values*weights1 + data2*weights2
		double norm1,norm2,aux;
				
		// normalize it firstly to the B bands flux of the spectrum of
		// a typical galaxy of the given type		
		norm1=this->pattern[0]->getBFlux()*this->pattern[0]->getMassToLightRatio();
		norm2=this->pattern[1]->getBFlux()*this->pattern[1]->getMassToLightRatio();				
		
		aux=weightsPattern[0]*norm1+weightsPattern[1]*norm2;				
						
		(*values) * (aux*weightsPattern[0]/norm1);
		(*data2) * (aux*weightsPattern[1]/norm2);		
		if ( (*values) + data2 == NULL )
			EXIT("Could not add two data sets during lin-interpolation");		
		m_out->println("Lin-Templated added",4);
		
	}
	delete data2;		
	this->colnumber=2;
	this->linenumber=values->getlength();			
	this->validLowLimit=0;
	this->validHighLimit=values->getlength()-1;							
			
	this->loaded=1;		
				
	return;
}

void Template::calc_IGA() {
	
	if (! this->loaded)
		load();			
		
	if (m_iga)
		delete m_iga;
	m_iga = new IGAbsorption();
	m_iga->setAbsorptionMode(this->options[1]);
	m_iga->setLambda(this->values);	// set lambda values to be the same as in this->values;
}
	

void Template::getObservedSpectrum(int zi, xyData * data)
{
	double *z;	
	
	if (! this->loaded)
		load();			
		
	z=m_binZ->getZ();	
														
	// copy template data			
	data->copy(values,this->validLowLimit,this->validHighLimit);					
			
	// incorporate intergalactic absorption (following Madau APJ 441:18-27, 1995)
	if (options[1]) {
		if (! m_iga)
			calc_IGA();
		m_iga->absorb(data,z[zi]);			
	}
			
	// red shift template spectrum
	data->xTimes(1+z[zi]);		
}

void Template::getSpectrum(int zi, xyData * data)
{
	double *z;	
	
	if (! this->loaded)
		load();			
		
	z=m_binZ->getZ();	
														
	// copy template data			
	data->copy(values,this->validLowLimit,this->validHighLimit);				
			
	// red shift template spectrum
	data->xTimes(1+z[zi]);		
}

void Template::getPlainSpectrum(xyData * data)
{	
	if (! this->loaded)
		load();			
								
	// copy template data			
	data->copy(values,this->validLowLimit,this->validHighLimit);								
}

double Template::getFlux(double z, int f) 
{
	int zi;
	
	// map z to nearest redshift we have in our grid
	Bisection *bi = new Bisection(m_binZ->getLength(),m_binZ->getZ());
	zi=bi->getNearestIndex(z);	
	delete bi;			
	
	return getFlux(zi, f);
}

double Template::getFlux(int zi, int f)
{
	xyData *data;	
	double norm;
	double *z;	
	double result;
	
	if (! this->loaded)
		load();
	
	z=m_binZ->getZ();	
														
	// copy template data			
	data = new xyData();	
	data->copy(values,this->validLowLimit,this->validHighLimit);					
	
	//cout << ">> template <<" << endl;
	//data->print();
			
	// incorporate intergalactic absorption (following Madau APJ 441:18-27, 1995)
	if (options[1]) {
		if (! m_iga)
			calc_IGA();
		m_iga->absorb(data,z[zi]);				
	}
	
	//cout << ">> with IGA <<" << endl;
	//data->print();
			
	// red shift template spectrum
	data->xTimes(1+z[zi]);			
	
	//cout << ">> red shifted <<" << endl;
	//data->print();		
				
	// check whether filter is contained in templates lambda range
	// if not then no flux is calculated
	if (data->getMinX()>filters[f]->getxyData()->getMinX() || 
		data->getMaxX()<filters[f]->getxyData()->getMaxX()) {			
			//cout << "RETURN -1: filters[f]->getxyData()->getMinX()=" << filters[f]->getxyData()->getMinX() 
			//	 << " filters[f]->getxyData()->getMaxX()= " << filters[f]->getxyData()->getMaxX() 
			//	 << " data->getMinX()=" << data->getMinX() << " data->getMaxX()=" << data->getMaxX() << endl;
			delete data;	 
			return -1;			
	}								
			
	// multiply filter and template
	// note: there has to be an overlap since filter is
	// 		 actually contained in template (even stronger cond.)
	if (((*data) * filters[f]->getxyData())==NULL)
		EXIT("Could not multiply filter and template");		
			
	// if energy flux -> data is fine
	// if photon flux -> multiply data with lambda						
	if (options[2]==0)	
		data->yTimesx();
				
	//cout << ">> multiplied with filter <<" << endl;
	//data->print();	
				
	// integrate data and normalize it  
	if (options[2]==0) 
		norm=filters[f]->getFrequencyNormalization(data);
	else
		norm=filters[f]->getStrangeNormalization(data);
	result=data->integrate()/norm;					
	
	
	//cout << "integral=" << data->integrate() << " norm=" << norm << " result=" << result << endl;
	
	delete data;		
	
	return result;
}

void Template::load()
{
	char s[MAX_LENGTH];
		
	if (this->loaded)
		return;	
		
	sprintf(s,"loading Template: %s",this->name);
	m_out->println(s,2);
	m_out->print("Trying to load .. ",3);		
	if (this->interpol==0)
		LambdaDataSet::load();
	else {
		loadInterpol();			
		#if 0
			// print interpolated templates
			char filename[MAX_LENGTH];
			sprintf(filename,"./%s.sed",this->getName());
			this->writeXYValues(filename);
		#endif	
	}
		
	/* add a low-lambda point to the templates, in order to allow a wider redshift range
	double *x,*y;
	double *xp,*yp;
	allocArray1(x,values->getlength()+1);
	allocArray1(y,values->getlength()+1);
	xp=x+1;
	yp=y+1;
	values->getCopyX(xp);
	values->getCopyY(yp);
	x[0]=0.0181502051820;
	y[0]=0;
	xyData *data = new xyData(values->getlength()+1,x,y);
	freeArray1(x);
	freeArray1(y);
	delete values;	
	values = data;
	*/	
	validLowLimit=0;
	validHighLimit=values->getlength()-1;
								
	m_out->println("done",3);
	sprintf(s,"ValidLow= %d",this->validLowLimit);
	m_out->println(s,3);
	sprintf(s,"ValidHigh= %d",this->validHighLimit);
	m_out->println(s,3);
		
	if (options[1]) {			
		calc_IGA();
	}
}				

#ifdef PARRAY
void Template::calcFlux(double ** & flux)
{				
	int zlength=m_binZ->getLength();
	if (! havePrepared)
		EXIT("Template has to be prepared before call of calcFlux");												
	
	if (! this->loaded)
		load();						
	
	for (int zi=0;zi<zlength;zi++) 
	  for (int f=0;f<this->numFilters;f++) 		
	    flux[zi][f]=getFlux(zi,f);				
}
#else
void Template::calcFlux(double * flux)
{				
	int zlength=m_binZ->getLength();
	if (! havePrepared)
		EXIT("Template has to be prepared before call of calcFlux");												
	
	if (! this->loaded)
		load();						
	
	for (int zi=0;zi<zlength;zi++) 
	  for (int f=0;f<this->numFilters;f++) 		
	    flux[zi*numFilters+f]=getFlux(zi,f);
}
#endif


void TemplateContainer::createTemplateBins(int numTemplateBins, double *zmin, double *zmax) 
{
	// delete old template bins
	if (templateBins) {
		for (int i=0;i<this->numTemplateBins;i++) {
			if (templateBins[i])
				delete templateBins[i];
		}
		freeArray1(templateBins);				
	}
	
	if (! templateOption)
			EXIT("templateOption==NULL in class TemplateContainer");
			
	// create template bins	
	this->numTemplateBins=numTemplateBins;
	allocArray1(templateBins,numTemplateBins);
	for (int i=0;i<numTemplateBins;i++) {
		templateBins[i] = new TemplateBin(templateOption);
		templateBins[i]->setOutputHandler(m_out);
		templateBins[i]->setZmin(zmin[i]);
		templateBins[i]->setZmax(zmax[i]);
	}
	
	// get basic templates from original bin (no z-restriction)
	Template **basicTemplates;
	int numBasicTemplates=origTemplateBin->getNumBasicTemplates();	
	basicTemplates=origTemplateBin->getBasicTemplates();
	
	// set these as basic template for all other bins
	for (int i=0;i<numTemplateBins;i++) 
		templateBins[i]->setBasicTemplates(numBasicTemplates,basicTemplates);
		
	// construct interpolated templates for that red shift bin
	for (int i=0;i<numTemplateBins;i++) {
		templateBins[i]->createLogInterTemplates();
		templateBins[i]->createLinInterTemplates();
	}

	// fill template tables
	for (int i=0;i<numTemplateBins;i++) 	
		templateBins[i]->fillOrigTemplates();
}

void TemplateBin::createBasicTemplates() {
		
	int counter;		
	char s[MAX_LENGTH];			
		
	// delete old basic templates
	deleteBasicTemplates();	
				
	// get number of basic templates 
	this->numBasicTemplates=templateOption->getNumBasicTemplates();	
	
	// some output
	sprintf(s,"Creating %d basic template(s)",numBasicTemplates);
	if (m_out)	
		m_out->println(s,1);			
		
	// create (normal) templates	
	counter=0;
	allocArray1(basicTemplates,numBasicTemplates);
	for (int i=0;i<numBasicTemplates;i++) {
		sprintf(s,"%s/%s",templateOption->getTemplatePath(),templateOption->getNameTemplates()[i]);
		basicTemplates[i] = new Template(s,templateOption->getNameTemplates()[i]);	
		basicTemplates[i]->setOutput(m_out);
		basicTemplates[i]->setigAbsorption(templateOption->getIgAbsorption());
		basicTemplates[i]->setFluxType(templateOption->getFluxType());
		basicTemplates[i]->setBFilter(templateOption->getB_filter());			
		
		basicTemplates[i]->setMassToLightRatio(templateOption->getMassToLightRatios()[i]);
		basicTemplates[i]->setMinZ(templateOption->getMinZ()[i]);
		basicTemplates[i]->setMaxZ(templateOption->getMaxZ()[i]);
		sprintf(s,"Template %d: %s created",counter,basicTemplates[i]->getName());
		m_out->println(s,3);
		counter++;
		
		basicTemplates[i]->load();				
	}
}

void TemplateBin::createLogInterTemplates() {
		
	char tempname[MAX_LENGTH+MAX_LENGTH];	
	double tweights[2];
	Template *pattern[2];
	int counter;
	int numTemplates;			
	char s[MAX_LENGTH];			
		
	// 	delete old log-interpolated templates
	deleteLogInterTemplates();		
		
	// calculate total number of log-interpolated templates 	
	numTemplates=0;
	if (templateOption->getLogInterpolation()) {
		if (templateOption->getLogInterpolationMode()==0)
			numTemplates+=templateOption->getLogInterpolation()*(numBasicTemplates-1);
		else
			numTemplates+=templateOption->getLogInterpolation()*((numBasicTemplates-1)*numBasicTemplates)/2;			
	}
	this->numLogInterTemplates=numTemplates;
	
	// some output
	sprintf(s,"Creating %d log-interpolated template(s)",numLogInterTemplates);
	if (m_out)	
		m_out->println(s,1);	
	
	if (numLogInterTemplates) {
		// create log interpolated templates 
		counter=0;
		allocArray1(logInterTemplates,numLogInterTemplates);
		if (templateOption->getLogInterpolation()) {									
			if (templateOption->getLogInterpolationMode()==0) { 		// between adjacent templates
				for (int t0=0;t0<numBasicTemplates-1;t0++) {
					pattern[0]=basicTemplates[t0];
					pattern[1]=basicTemplates[t0+1];		
					for (int i=0;i<templateOption->getLogInterpolation();i++) {
						tweights[1]=(i+1.0)/(templateOption->getLogInterpolation()+1.0);
						tweights[0]=1-tweights[1];													
						sprintf(tempname,"%s^(%g)*%s^(%g)",templateOption->getNameTemplates()[t0],
								tweights[0],templateOption->getNameTemplates()[t0+1],tweights[1]);					
						logInterTemplates[counter] = new Template(tempname);
						logInterTemplates[counter]->setOutput(m_out);
						logInterTemplates[counter]->setInterpolData(pattern, tweights);					
						logInterTemplates[counter]->setLinear(0);
						logInterTemplates[counter]->setigAbsorption(templateOption->getIgAbsorption());
						logInterTemplates[counter]->setFluxType(templateOption->getFluxType());
						logInterTemplates[counter]->setBFilter(templateOption->getB_filter());													
												
						sprintf(s,"Template %d: %s created",counter,logInterTemplates[counter]->getName());
						m_out->println(s,3);					
						
						logInterTemplates[counter]->load();
						
						counter++;
					}
				}																	
			} else {										// between each pair of templates
				for (int t0=0;t0<numBasicTemplates;t0++) {
					for (int t1=t0+1;t1<numBasicTemplates;t1++) {
						pattern[0]=basicTemplates[t0];
						pattern[1]=basicTemplates[t1];		
						for (int i=0;i<templateOption->getLogInterpolation();i++) {
							tweights[1]=(i+1.0)/(templateOption->getLogInterpolation()+1.0);
							tweights[0]=1-tweights[1];														
							sprintf(tempname,"%s^(%g)*%s^(%g)",templateOption->getNameTemplates()[t0],
									tweights[0],templateOption->getNameTemplates()[t1],tweights[1]);					
							logInterTemplates[counter] = new Template(tempname);
							logInterTemplates[counter]->setOutput(m_out);
							logInterTemplates[counter]->setInterpolData(pattern, tweights);						
							logInterTemplates[counter]->setLinear(0);
							logInterTemplates[counter]->setigAbsorption(templateOption->getIgAbsorption());
							logInterTemplates[counter]->setFluxType(templateOption->getFluxType());
							logInterTemplates[counter]->setBFilter(templateOption->getB_filter());													
													
							sprintf(s,"Template %d: %s created",counter,logInterTemplates[counter]->getName());
							m_out->println(s,3);	
							
							logInterTemplates[counter]->load();
											
							counter++;
						}
					}
				}				
			}					
		}
	}
	
}	

void TemplateBin::createLinInterTemplates() 
{	
	char tempname[MAX_LENGTH+MAX_LENGTH];	
	double tweights[2];
	Template *pattern[2];
	int counter;
	int numTemplates;			
	char s[MAX_LENGTH];			
	
	// 	delete old lin-interpolated templates
	deleteLinInterTemplates();		
	
	// calculate total number of lin-interpolated templates 	
	numTemplates=0;
	if (templateOption->getLinInterpolation()) {
		if (templateOption->getLinInterpolationMode()==0)
			numTemplates+=templateOption->getLinInterpolation()*(numBasicTemplates-1);
		else
			numTemplates+=templateOption->getLinInterpolation()*((numBasicTemplates-1)*numBasicTemplates)/2;			
	}								
	this->numLinInterTemplates=numTemplates;
	
	// some output
	sprintf(s,"Creating %d lin-interpolated template(s)",numLinInterTemplates);
	if (m_out)	
		m_out->println(s,1);	
	
	if (numLinInterTemplates>0) {	
		// create lin-interpolated templates
		// each lin interpolated templates need the B band filter for normalization reasons	
		counter=0;
		allocArray1(linInterTemplates,numLinInterTemplates);
		if  (templateOption->getLinInterpolation()) {
																								if (templateOption->getLinInterpolationMode()==0) { 		// between adjacent templates
				for (int t0=0;t0<numBasicTemplates-1;t0++) {
					pattern[0]=basicTemplates[t0];
					pattern[1]=basicTemplates[t0+1];		
					for (int i=0;i<templateOption->getLinInterpolation();i++) {
						tweights[1]=(i+1.0)/(templateOption->getLinInterpolation()+1.0);
						tweights[0]=1-tweights[1];													
						sprintf(tempname,"%s*(%g)+%s*(%g)",templateOption->getNameTemplates()[t0],
								tweights[0],templateOption->getNameTemplates()[t0+1],tweights[1]);					
						linInterTemplates[counter] = new Template(tempname);
						linInterTemplates[counter]->setOutput(m_out);
						linInterTemplates[counter]->setInterpolData(pattern, tweights);					
						linInterTemplates[counter]->setLinear(1);
						linInterTemplates[counter]->setigAbsorption(templateOption->getIgAbsorption());
						linInterTemplates[counter]->setFluxType(templateOption->getFluxType());
						linInterTemplates[counter]->setBFilter(templateOption->getB_filter());													
												
						sprintf(s,"Template %d: %s created",counter,linInterTemplates[counter]->getName());
						m_out->println(s,3);					
						
					        linInterTemplates[counter]->load();
						
						counter++;
					}
				}																	
			} else {						        // between each pair of origTemplates
				for (int t0=0;t0<numBasicTemplates;t0++) {
					for (int t1=t0+1;t1<numBasicTemplates;t1++) {
						pattern[0]=basicTemplates[t0];
						pattern[1]=basicTemplates[t1];							
						for (int i=0;i<templateOption->getLinInterpolation();i++) {
							// linear weights (not so useful since not equally spaced in log frequency)
							//tweights[1]=(i+1.0)/(linInterpolation+1.0);
							//tweights[0]=1-tweights[1];	
							if (templateOption->getLinInterpolation()==1) // just one linear interpolated template
								tweights[1]=0.5;
							else 				
								tweights[1]=pow(0.01,1-i*(templateOption->getLinInterpolation()-2.0)/
								((templateOption->getLinInterpolation()-1.0)*(templateOption->getLinInterpolation()-1.0)));										
							tweights[0]=1-tweights[1];								
							sprintf(tempname,"%s*(%g)+%s*(%g)",templateOption->getNameTemplates()[t0],
									tweights[0],templateOption->getNameTemplates()[t1],tweights[1]);					
							linInterTemplates[counter] = new Template(tempname);
							linInterTemplates[counter]->setOutput(m_out);
							linInterTemplates[counter]->setInterpolData(pattern, tweights);						
							linInterTemplates[counter]->setLinear(1);
							linInterTemplates[counter]->setigAbsorption(templateOption->getIgAbsorption());
							linInterTemplates[counter]->setFluxType(templateOption->getFluxType());
							linInterTemplates[counter]->setBFilter(templateOption->getB_filter());													
												
							sprintf(s,"Template %d: %s created",counter, linInterTemplates[counter]->getName());
							m_out->println(s,3);	
							
							linInterTemplates[counter]->load();
											
							counter++;
						}
					}
				}				
			}			
		}
	}		
	
}

void TemplateBin::fillOrigTemplates()
{
	allocArray1(origTemplates,numBasicTemplates+numLogInterTemplates+numLinInterTemplates);	    
	
	for (int i=0;i<numBasicTemplates+numLogInterTemplates;i++) {
	   if (i%(templateOption->getLogInterpolation()+1)==0)
	    	origTemplates[i]=basicTemplates[i/(templateOption->getLogInterpolation()+1)];
	   else
	    	origTemplates[i]=logInterTemplates[i-i/(templateOption->getLogInterpolation()+1)-1];
	}
	for (int i=0;i<numLinInterTemplates;i++)
	    	origTemplates[i+numBasicTemplates+numLogInterTemplates]=linInterTemplates[i];
}

void TemplateBin::printTemplateTable()
{			
	char s[MAX_LENGTH];
	int numTemplates=numBasicTemplates+numLogInterTemplates+numLinInterTemplates;
	
	sprintf(s,"#----------------Templates: Bin minZ=%g maxZ=%g",zmin,zmax);
	m_out->println(s,1);
	m_out->println("#Template_ID     template_name      minZ      maxZ",1);
	m_out->println("#-------------------------------------------------",1);	
	if (numTemplates==0) {
		m_out->println("none",1);
	} else {
		for (int i=0;i<numTemplates;i++) {
			sprintf(s,"%d    %s",i,origTemplates[i]->getName());
			m_out->println(s,1);
		}
	}	
}

void TemplateBin::writeAll(const char *path, const char *tag)
{
	char s[MAX_LENGTH];
	for (int i=0;i<getNumOrigTemplates();i++) {
		if (zmax<1E20)			
			sprintf(s,"%s/%s_%s_zgt%g_zlt%g.dat",path,origTemplates[i]->getShortName(),tag,zmin,zmax);
		else 
			sprintf(s,"%s/%s_%s_zgt%g_zltinf.dat",path,origTemplates[i]->getShortName(),tag,zmin);
		origTemplates[i]->writeXYValues(s);
	}			
}

void TemplateBin::writeBasic(const char *path, const char *tag)
{
	char s[MAX_LENGTH];
	for (int i=0;i<this->numBasicTemplates;i++) {
		if (zmax<1E20)			
			sprintf(s,"%s/%s_%s_zgt%g_zlt%g.dat",path,basicTemplates[i]->getShortName(),tag,zmin,zmax);
		else 
			sprintf(s,"%s/%s_%s_zgt%g_zltinf.dat",path,basicTemplates[i]->getShortName(),tag,zmin);
		basicTemplates[i]->writeXYValues(s);
	}			
}
