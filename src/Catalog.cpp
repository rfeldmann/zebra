#include "Catalog.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

Catalog::Catalog(const char* name, const char* defFileName)
{	
	allocArray1(this->name,strlen(name)+2);	
	allocArray1(this->shortname,strlen(name)+2);	
	allocArray1(this->defFileName,strlen(defFileName)+2);
	strcpy(this->name,name);
	strcpy(this->shortname,name);	
	strcpy(this->defFileName,defFileName);
	filterBands=NULL;
	specZBands=NULL;	
	oneSigmaError=NULL;	
	minError=NULL;
	zp=NULL;
	m_out=NULL;
	colnumber=0;
	linenumber=0;
	numFilterBands=0;
	numSpecZBands=0;
	loaded=0;
	specZ=0;
	magCatalogLow=5;
	magCatalogHigh=35;
	// set default options
	// these option can be overruled by given global options
	// and options from the .def file (stronger)
	for (int i=0;i<numOptions;i++)
		options[i]=0;
}

Catalog::Catalog(const char* name, const char *shortname, const char* defFileName)
{	
	allocArray1(this->name,strlen(name)+2);	
	allocArray1(this->shortname,strlen(shortname)+2);	
	allocArray1(this->defFileName,strlen(defFileName)+2);
	strcpy(this->name,name);
	strcpy(this->shortname,shortname);	
	strcpy(this->defFileName,defFileName);
	filterBands=NULL;
	specZBands=NULL;	
	oneSigmaError=NULL;	
	minError=NULL;
	zp=NULL;
	m_out=NULL;
	colnumber=0;
	linenumber=0;
	numFilterBands=0;
	numSpecZBands=0;
	loaded=0;
	specZ=0;
	magCatalogLow=5;
	magCatalogHigh=35;
	// set default options
	// these option can be overruled by given global options
	// and options from the .def file (stronger)
	for (int i=0;i<numOptions;i++)
		options[i]=0;
}

Catalog::~Catalog()
{	
	freeArray1(this->name);
	freeArray1(this->shortname);	
	freeArray1(this->defFileName);
	for (int i=0;i<this->numFilterBands;i++)
		delete filterBands[i];			
	for (int i=0;i<this->numSpecZBands;i++)
		delete specZBands[i];			
	freeArray1(filterBands);
	freeArray1(specZBands);		
	freeArray1(oneSigmaError);
	freeArray1(minError);
	freeArray1(zp);
}

void Catalog::modifyFluxAndError(double * f, double * e, int entry, int band, char **filterBandMissing )
{		
	if (options[2]==0) { 			// entries in catalog file given in magnitudes		
									
		if (*f<=-99) {							// if magnitude not given for that object, remove object					
			filterBandMissing[entry][band]=1;
		} else if (*f>=99) {					// if magnitude not detectable 		  			 
			if (oneSigmaError[band]>0 && *e > 0 && *e < 99) { // if 1-sigma magnitude given and error is valid: 										
				*f=oneSigmaError[band];	          // .. set filter band it to one-Sigma magnitude and use given error
			} else {
				filterBandMissing[entry][band]=1; // .. otherwise: discard filter band 
			}								
		} else {								  // magnitude is ok
		  	if (*e <= 0 || *e >= 99) {			  // if error is not ok:
		  	  if (minError[band]>0)				
		  	    *e = minError[band];              // use minimum error if available
		  	  else
		  	    filterBandMissing[entry][band]=1; // otherwise: discard filter band 			  				  	
		  	}		  					
		}
		
		if (minError[band]>0 && *e<minError[band]) {	// set magnitude error to be at least minError (minimum magnitude error)				
			*e=minError[band];
		}
								
	} else {			 			// entries in catalog file given in fluxes	
		if (*f<=0) {
			if (oneSigmaError[band]>0) {
				*f=oneSigmaError[band];	// .. set it to one-Sigma flux
				*e=oneSigmaError[band];	// .. same for the error
			} else {							
				filterBandMissing[entry][band]=1;
			}
		}
				
		if (oneSigmaError[band]>0 && *e<oneSigmaError[band])
			*e=oneSigmaError[band];
				
		if (*e<=0) 
			if (oneSigmaError[band]>0)
				*e=oneSigmaError[band];		
			else 												
				*e=0.005;
					
		if (*e<=(*f)*minError[band]) // set absolute error to at least minError*flux
			*e=(*f)*minError[band];
	}			
}


/* this functions does three things
 * a) it reads the cosmos file
 * b) it filters entries which are not "good" according to some criterion
 * c) it fill the filterBands with all remaining data
 * Later a transformation of the data (into mag or fluxes) may place
 */
 
void Catalog::read(char *filename)
{
		
	fstream infile;	
	double *filterColumn,*errorColumn;
	double **addErrorCol;
	double **body;
	int colIndex,errorColIndex;		
	int currlinenumber;
	char **filterBandMissing;
	char *invalidColumn;
	char s[MAX_LENGTH];
		
	TableFile *tablefile;
	tablefile = new TableFile(filename);
	// total number of lines in the catalog and maximal number of columns
	tablefile->getQuickLineColnumber(filename,this->linenumber,this->colnumber);
	tablefile->setOutputHandler(this->m_out);
	
	// allocate internal memory of filterBands
	for (int b=0;b<numFilterBands;b++)
		filterBands[b]->ErrorData::set(this->linenumber);
	// allocate internal memory of specZ bands
	for (int b=0;b<numSpecZBands;b++) {		
		specZBands[b]->ErrorData::set(this->linenumber);		
	}		
	
	int cumbunchnumber=0;
	while (! tablefile->readComplete()) {
		
		tablefile->load(NUMBUNCHLINES);				// read only numLinesRead lines at a time
					
		currlinenumber=tablefile->getlinenumber();  // number of lines in the bunch, should equal
													// NUMBUNCHLINES unless for the last bunch		
	
		allocArray2(body,currlinenumber,this->colnumber);
		tablefile->getbody(body);	
					
		// records entries with some filters missing	
		callocArray2(filterBandMissing,currlinenumber,this->numFilterBands);	
		
		allocArray1(filterColumn,currlinenumber);
		allocArray1(errorColumn,currlinenumber);		
		callocArray1(invalidColumn,currlinenumber);		
	
		allocArray1(addErrorCol,numFilterBands);
		for (int c=0;c<numFilterBands;c++) 
			if (filterBands[c]->getErrorColID()<0)
				allocArray1(addErrorCol[c],currlinenumber);	
				
		// set absolute error at least to be oneSigmaError
		// for that oneSigmaError has to be declared in the .def file
		// set relative error at least to minError
		// for that minError has to be declared in the .def file
		// also identify the missing filter bands of some entries
				
		double *e,*f;						
		for (int i=0;i<currlinenumber;i++) {
			for (int c=0;c<numFilterBands;c++) {
												
				f=&(body[i][filterBands[c]->getColID()]);
				if (filterBands[c]->getErrorColID()>=0)
					e=&(body[i][filterBands[c]->getErrorColID()]);
				else { // no filterBand error given -> decide depending on the error mode				
					e=&addErrorCol[c][i];				
					*e=-99;								
				}					
				modifyFluxAndError(f,e,i,c,filterBandMissing);																	
			}	
		}			
	
		#if 1
		// do minor corrections to the data which passes
		if (options[4]==1) { // do Zamorani's K-band correction
				
			// identify K band
			int kbandfound=0;
			int kbandIndex=0;
			char kbandName[MAX_LENGTH];
			for (int b=0;b<numFilterBands;b++)	{
				if ( strcmp(filterBands[b]->getName(),K_BAND_NAME1)==0) {
					kbandfound=1;
					kbandIndex=b;
					strcpy(kbandName,K_BAND_NAME1);
					break;
				}
				if ( strcmp(filterBands[b]->getName(),K_BAND_NAME2)==0) {
					kbandfound=1;
					kbandIndex=b;
					strcpy(kbandName,K_BAND_NAME2);
					break;
				}						
			}		
			if (! kbandfound) {
				sprintf(s,"Could not do Zamorani K-band correction: No K-band with name: %s",kbandName);
				this->m_out->println(s,1);
			} else {
				sprintf(s,"Apply Zamorani K-band correction: Found K-band with name: %s",kbandName);
				this->m_out->println(s,1);
			}
		
			
			if (kbandfound) {		
				for (int i=0;i<this->linenumber;i++) {						
					f=&(body[i][filterBands[kbandIndex]->getColID()]);
					*f=*f-0.13*(*f-19.8);
				}
			}
		}
		#endif
		
		// now fill the filterBands	
		for (int b=0;b<numFilterBands;b++)	{
			colIndex=filterBands[b]->getColID();
			errorColIndex=filterBands[b]->getErrorColID();	
			bool flag_warning_low_filterColumn=false;
			bool flag_warning_high_filterColumn=false;
				
			for (int i=0;i<currlinenumber;i++) {			
				if ((int)filterBandMissing[i][b]) {
					invalidColumn[i]=1;	
					filterColumn[i]=-1;
					errorColumn[i]=0;					
				} else {							
					invalidColumn[i]=0;
					filterColumn[i]=body[i][colIndex];							
					if (errorColIndex<0)
						errorColumn[i]=addErrorCol[b][i];
					else
						errorColumn[i]=body[i][errorColIndex];
						
					// check for strange filter values;	
					if (filterColumn[i] < magCatalogLow && filterColumn[i]>-99)
					  flag_warning_low_filterColumn=true;					
				    else if (filterColumn[i] > magCatalogHigh && filterColumn[i]<99)
				      flag_warning_high_filterColumn=true;			      				      
				}
			}				
			// change only lines ranging from cumbunchnumber to cumbunchnumber+currlinenumber
				
			// give warning of strange filterColumn			
			if (flag_warning_low_filterColumn) {
				sprintf(s,"In filter-band %s: Some filter values are low (<%g but >-99)\nPlease use -99 or 99 to mark catalog entries as non-measured or non-detected, respectively or\nchange the low-magnitude limit with --mag-catalog-low!",
				       filterBands[b]->getName(),magCatalogLow);
				WARNING(s);			
			} 
			if (flag_warning_high_filterColumn) {
				sprintf(s,"In filter-band %s: Some filter values are large (>%g but <99)\nPlease use -99 or 99 to mark catalog entries as non-measured or non-detected, respectively or\nchange the high-magnitude limit with --mag-catalog-high!",
				        filterBands[b]->getName(),magCatalogHigh);
				WARNING(s);
			}		
								 		
			// fill values and modified errors
			filterBands[b]->setWriteWindow(cumbunchnumber,currlinenumber);
			filterBands[b]->setValueAndError(filterColumn,errorColumn);
			filterBands[b]->setInvalid(invalidColumn);
								
			// fill original errors into filterbands if --preserveErrorsOutput set
			if (options[6]) {
				for (int i=0;i<currlinenumber;i++) {
					if ((int)filterBandMissing[i][b]) {		
						errorColumn[i]=-99;
					} else {							
						if (errorColIndex<0)
							errorColumn[i]=-99;
						else {
							// get original (unmodified) catalog entries
							errorColumn[i]=tablefile->getbody()[i][errorColIndex];			
						}
					}
				}										
				filterBands[b]->setOrigError(errorColumn);				
			}							
		}					
				
		// now fill the specZBands	
		for (int b=0;b<numSpecZBands;b++) {
			colIndex=specZBands[b]->getColID();
			errorColIndex=specZBands[b]->getErrorColID();		
			for (int i=0;i<currlinenumber;i++) {			
				invalidColumn[i]=0;
				filterColumn[i]=body[i][colIndex];
				if (errorColIndex==-1)
					errorColumn[i]=0;
				else
					errorColumn[i]=body[i][errorColIndex];		
			}		
			specZBands[b]->setWriteWindow(cumbunchnumber,currlinenumber);
			specZBands[b]->setValueAndError(filterColumn,errorColumn);		
			specZBands[b]->setInvalid(invalidColumn);
		}
		
		freeArray1(filterColumn);
		freeArray1(errorColumn);	
		freeArray1(invalidColumn);
		
		freeArray2(filterBandMissing,currlinenumber);	
		freeArray2(body,currlinenumber);	
		
		for (int c=0;c<numFilterBands;c++) 
			if (filterBands[c]->getErrorColID()<0)
				freeArray1(addErrorCol[c]);
		freeArray1(addErrorCol);
		
		cumbunchnumber+=currlinenumber;						
	}
	tablefile->printTotalStatus();			
	delete tablefile;	
}

void Catalog::subtractZP()
{
	for (int b=0;b<numFilterBands;b++)			
		*(filterBands[b])-zp[b];	
}

void Catalog::load()
{
	this->load(this->name);
}

void Catalog::load(char *filename)
{
	
	this->filename=filename;
	
	if (this->loaded)
		return;		

	// load catalog, select entries and fill filterBands	
	read(filename);												
	
	// subtract zero point offset
	subtractZP();
	
	#if 0
	for (int f=0;f<numFilterBands;f++)	{
		filterBands[f]->print();				
	}
	#endif	
		
	// change from magnitudes to fluxes if option is set				
	magToFlux();		
	
	#if 0
	for (int f=0;f<numFilterBands;f++)	{
		filterBands[f]->print();				
	}
	#endif	
		
	// confirm that catalog is loaded						
	this->loaded=1;	
}

// transforms magnitudes into fluxes
void Catalog::magToFlux() {
	if (options[2]==0) {
		m_out->print("Transform magnitudes into fluxes ... ",2);		
		for (int f=0;f<numFilterBands;f++) {				
			filterBands[f]->magToFlux();
		}	
		m_out->println("done",2);
	}
}

// transforms fluxes into magnitudes
void Catalog::fluxToMag() {
	if (options[3]==0) {
		m_out->print("Transform fluxes into magnitudes ... ",2);		
		for (int f=0;f<numFilterBands;f++) {				
			filterBands[f]->fluxToMag();
		}	
		m_out->println("done",2);
	}
}

double *Catalog::getColFlux(int filterIndex)
{
	return this->filterBands[filterIndex]->getValue();
}
	
double *Catalog::getColErrorFlux(int filterIndex)
{	
	return this->filterBands[filterIndex]->getError();
}

double *Catalog::getColSpecZ(int specZIndex)
{
	return this->specZBands[specZIndex]->getValue();
}

double *Catalog::getColErrorSpecZ(int specZIndex)
{
	return this->specZBands[specZIndex]->getError();
}

void Catalog::getRowFlux(int lineIndex,double* row)
{
	for (int f=0;f<numFilterBands;f++) 
		row[f]=filterBands[f]->getValue()[lineIndex];					
}

void Catalog::getRowErrorFlux(int lineIndex, double* row)
{
	for (int f=0;f<numFilterBands;f++) 
		row[f]=filterBands[f]->getError()[lineIndex];		
}

void Catalog::getRowInvalidFlux(int lineIndex, char *row)
{
	for (int f=0;f<numFilterBands;f++) 
		row[f]=filterBands[f]->getInvalid()[lineIndex];	
}

void Catalog::getRowSpecZ(int lineIndex, double* row)
{
	for (int f=0;f<numSpecZBands;f++)
		row[f]=specZBands[f]->getValue()[lineIndex];	
}

double Catalog::getRowSpecZ(int lineIndex, int rowIndex)
{
	return specZBands[rowIndex]->getValue()[lineIndex];
}

void Catalog::getRowErrorSpecZ(int lineIndex, double* row)
{
	for (int f=0;f<numSpecZBands;f++)
		row[f]=specZBands[f]->getError()[lineIndex];	
}

void Catalog::setOptions(int defOptionEntries, char** defOptions)
{	
	// read options from defOptions array
	static const struct option long_options[]=
	{		               						
		{ "flux-type", required_argument, 0 , 0 },
		{ "help",  no_argument, 0, 'h' }, 		
        {0}
  	};
  	static const char* option_help[]=
  	{  	
  		"flux in photons (0) or energy (1) per frequency - default 0", 	
  		"gives this short help" 		 		 		  		
  	};
  	  	  	
	// read in option arguments from command line
	optind=1;
	while (optind < defOptionEntries)
	{
		int index = -1;		
		struct option * opt = 0;
		int result = getopt_long(defOptionEntries,defOptions,"h",long_options,&index);
		
		if (result == -1) break; // end of list
		
		switch (result)
		{			
			//case 'v': // same as --verbose
			//	verboseLevel=atoi(optarg);
			//	break;			
			case 0 : // parameters without abbreviation
				opt = (struct option *)&(long_options[index]);
				if (strcmp(opt->name,"flux-type")==0) {
					if (strcmp(optarg,"photon-per-frequency")==0 ||
						strcmp(optarg,"0")==0) {
						options[0]=0;
					} else if (strcmp(optarg,"energy-per-frequency")==0 ||
							   strcmp(optarg,"1")==0) {
						options[0]=1;
					}
				}
				break;
			case 'h': // same as --help								
			default:  // unknown parameter
				cerr << "specifications of options in .def file: " << endl;
				cerr << "<option_name> O [<option_value>]" << endl;
				for (int i=0;i<numOptions;i++) {
					if (long_options[i].val!=0) 
						cerr << "-" << (char)long_options[i].val;
					else 
						cerr << "  ";
					cerr << " --" << long_options[i].name;
					if (long_options[i].has_arg==required_argument)
						cerr << " argument";
					cerr << "  " << option_help[i] << endl;
				}
				exit(0);	
		}
	}						
}

void Catalog::readDefFile(int numFiltersIn,char** nameFiltersIn,
						  int & numFiltersOut,char** & nameFiltersOut)
{
	// read .def file and obtain the information 
	// which column corresponds to which band or banderror		
	char*** defEntries;
	int*	defEntriesNr;
	int 	defEntryLines;
	char	**defnameFilters,**defnameErrorFilters;
	char    **defnameSpecZ,**defnameErrorSpecZ;
	int 	defnumFilters,defnumErrorFilters;
	int 	defnumSpecZ,defnumErrorSpecZ;
	int 	*defColFilters,*defColErrorFilters;
	int		*defColSpecZ,*defColErrorSpecZ;
	int 	found,foundj;
	int		*filterMapping,*errorMapping,*errorMappingSpecZ;	
	int 	defOptionEntries;
	char	** defOptions;
	double  *def1sigmaFilters;
	double  *defminError;
	double  *defZPFilters;
	char    s[MAX_LENGTH];
	
	ConfFile *deffile = new ConfFile(this->defFileName);
	deffile->read();	
	deffile->getEntries(defEntries,defEntriesNr,defEntryLines);
	
	
	// get number of filters, errors, redshift columns defined in .def file
	defnumFilters=0;
	defnumErrorFilters=0;
	defnumSpecZ=0;
	defnumErrorSpecZ=0;
	defOptionEntries=1;
	for (int i=0;i<defEntryLines;i++) {		
		if (defEntriesNr[i]<2)
			EXIT("Entries in .def file does not seem to have the right syntax / columns missing.");
		if (strcmp(defEntries[i][1],"F")==0) // filter entry found
			defnumFilters++;
		else if (strcmp(defEntries[i][1],"dF")==0) // filter error entry found
			defnumErrorFilters++;
		else if (strcmp(defEntries[i][1],"Z")==0) // specZ entry found
			defnumSpecZ++;
		else if (strcmp(defEntries[i][1],"dZ")==0) // specZ entry found
			defnumErrorSpecZ++;
		else if (strcmp(defEntries[i][1],"O")==0) { // option found
			if (defEntriesNr[i]>2)
				defOptionEntries+=2;
			else
				defOptionEntries++;
		}
		// other entry types are ignored
	}
	
	// first checks
	if (defnumFilters==0) {
		cerr << "No filter found in the .def file" << endl;
		exit(1);
	}
	if (defnumErrorFilters==0) {
		cerr << "No filter error found in the .def file" << endl;
		exit(1);
	}			
	if (defnumFilters!=defnumErrorFilters) {
		cerr << "Error in .def file: Number of filters and errors are "
				"not corresponding" << endl;
		exit(1);
	}
	
	if (defnumSpecZ>0) {
		if (m_out)
			m_out->println("Spectroscopic redshifts found",1);
		this->specZ=1;
	} else {
		if (m_out)
			m_out->println("No spectroscopic redshifts found",1);
		this->specZ=0;
	}
	if (defOptionEntries>1) {
		if (m_out)
			m_out->println("Options defined in .def file replace global options",1);
	}
								
	allocArray2(defnameFilters,defnumFilters,MAX_LENGTH);
	allocArray1(defZPFilters,defnumFilters);
	if (defnumErrorFilters>0) {
		allocArray2(defnameErrorFilters,defnumErrorFilters,MAX_LENGTH);
		allocArray1(defColErrorFilters,defnumErrorFilters);
		allocArray1(def1sigmaFilters,defnumErrorFilters);
		allocArray1(defminError,defnumErrorFilters);
	} else {
	   defnameErrorFilters=NULL;
	   defColErrorFilters=NULL;
	   def1sigmaFilters=NULL;
	   defminError=NULL;			
	}
	allocArray2(defnameSpecZ,defnumSpecZ,MAX_LENGTH);
	if (defnumErrorSpecZ>0) {
		allocArray2(defnameErrorSpecZ,defnumErrorSpecZ,MAX_LENGTH);
		allocArray1(defColErrorSpecZ,defnumErrorSpecZ);
	} else {
	   defnameErrorSpecZ=NULL;
	   defColErrorSpecZ=NULL;			
	}
	allocArray1(defColFilters,defnumFilters);	
	allocArray1(defColSpecZ,defnumSpecZ);
	allocArray2(defOptions,defOptionEntries,MAX_LENGTH);		
				
	// get all names and corresponding columns
	defnumFilters=0;
	defnumErrorFilters=0;
	defnumSpecZ=0;
	defnumErrorSpecZ=0;
	defOptionEntries=1;
	for (int i=0;i<defEntryLines;i++) {
		if (strcmp(defEntries[i][1],"F")==0) { // filter entry found
			if (defEntriesNr[i]<3)
				EXIT("Entries in .def file does not seem to have the right syntax / columns missing.");
			strcpy(defnameFilters[defnumFilters],defEntries[i][0]);
				// -1 in the following line since column number starts with 0
				// in the .def file, however, the numbering starts with 1
			defColFilters[defnumFilters]=atoi(defEntries[i][2])-1;
			defZPFilters[defnumFilters]=0.0;
			if (defEntriesNr[i]>3) {
				if (strcmp(defEntries[i][3],"x")!=0) {					
					sprintf(s,"Found zero-point offset in .def file: %s",defEntries[i][3]);
					this->m_out->println(s,2);
					defZPFilters[defnumFilters]=atof(defEntries[i][3]);
				}
			}			
			defnumFilters++;
		} else if (strcmp(defEntries[i][1],"dF")==0) { // error entry found			
			//if (defEntriesNr[i]<3)
			//	EXIT("Entries in .def file does not seem to have the right syntax / columns missing.");
			strcpy(defnameErrorFilters[defnumErrorFilters],defEntries[i][0]);
			
			defColErrorFilters[defnumErrorFilters]=-1;					
			if 	(defEntriesNr[i]>2 && strcmp(defEntries[i][2],"x")!=0)			
				defColErrorFilters[defnumErrorFilters]=atoi(defEntries[i][2])-1;
					
			defminError[defnumErrorFilters]=-1;
			def1sigmaFilters[defnumErrorFilters]=-1;
	
			if (defEntriesNr[i]>3) {
				if (strcmp(defEntries[i][3],"x")!=0) {					
					sprintf(s,"Found 1-sigma magnitudes in .def file: %s",defEntries[i][3]);
					this->m_out->println(s,2);
					def1sigmaFilters[defnumErrorFilters]=atof(defEntries[i][3]);
				}
				if (defEntriesNr[i]>4 && strcmp(defEntries[i][4],"x")!=0) {
					sprintf(s,"Found minimum magnitude errors in .def file: %s",defEntries[i][4]);
					this->m_out->println(s,2);					
					defminError[defnumErrorFilters]=atof(defEntries[i][4]);
				} 
			} 
			defnumErrorFilters++;
			
		} else if (strcmp(defEntries[i][1],"Z")==0) { // spectroscopic z-values
			if (defEntriesNr[i]<3)
				EXIT("Entries in .def file does not seem to have the right syntax / columns missing.");
			strcpy(defnameSpecZ[defnumSpecZ],defEntries[i][0]);
			defColSpecZ[defnumSpecZ]=atoi(defEntries[i][2])-1;
			defnumSpecZ++;
		} else if (strcmp(defEntries[i][1],"dZ")==0) { // error of z-values
			if (defEntriesNr[i]<3)
				EXIT("Entries in .def file does not seem to have the right syntax / columns missing.");
			strcpy(defnameErrorSpecZ[defnumErrorSpecZ],defEntries[i][0]);
			defColErrorSpecZ[defnumErrorSpecZ]=atoi(defEntries[i][2])-1;
			defnumErrorSpecZ++;
		} else if (strcmp(defEntries[i][1],"O")==0) { // option found
			if (defEntriesNr[i]<2)
				EXIT("Entries in .def file does not seem to have the right syntax / columns missing.");
			if (strlen(defEntries[i][0])==1)
				sprintf(defOptions[defOptionEntries],"-%s",defEntries[i][0]);
			else
				sprintf(defOptions[defOptionEntries],"--%s",defEntries[i][0]);			
			defOptionEntries++;
			if (defEntriesNr[i]>2) {
				sprintf(defOptions[defOptionEntries],"%s",defEntries[i][2]);
				defOptionEntries++;
			}
		}
		// other entries are ignored
	}		
	

	// check whether each filter has an associated error entry
	// also obtain a mapping from each filter to each corresponding error
	// ie errorMapping[i] give the line number of the error entry for the
	// filter entry at line i in the .def file		
	if (defnumErrorFilters>0) {
		allocArray1(errorMapping,defnumFilters);
		for (int i=0;i<defnumErrorFilters;i++) {
			found=0;
			foundj=0;
			for (int j=0;j<defnumFilters;j++) {
				if (strcmp(defnameErrorFilters[i],defnameFilters[j])==0)  {
					found++;	
					foundj=j;		
				}
			}
			if (found!=1) {
				cerr << "Error in .def file: filter and error description do "
						"not match" << endl;
				cerr << "Check your .conf and .def files" << endl;
				exit(1);
			} 
			errorMapping[foundj]=i;
		}	
	} else {
		errorMapping=NULL;
	}
		
	
	// errorMapping for spec-z values
	if (defnumErrorSpecZ>0) {
		allocArray1(errorMappingSpecZ,defnumSpecZ);
		for (int i=0;i<defnumSpecZ;i++) {
			found=0;
			foundj=0;
			for (int j=0;j<defnumErrorSpecZ;j++) {
				if (strcmp(defnameSpecZ[i],defnameErrorSpecZ[j])==0)  {
					found++;	
					foundj=j;		
				}
			}
			if (found==1) 		
				errorMappingSpecZ[i]=foundj;
			else
				errorMappingSpecZ[i]=-1;
		}			
	} else {
		errorMappingSpecZ=NULL;
	}
	
	// get number of filters which are both in .def file (defnameFilters)
	// and also in nameFiltersIn
	numFiltersOut=0;
	for (int i=0;i<defnumFilters;i++) {
		found=0;
		for (int j=0;j<numFiltersIn;j++) {
			if (strcmp(defnameFilters[i],nameFiltersIn[j])==0) 
				found=1;				
		}
		if (found==1)			
			numFiltersOut++;
	}	
	if (numFiltersOut==0) {
		cerr << "None of in the .def file defined filters are found" << endl;
		cerr << "Check spelling in .conf file and .def file" << endl;
		exit(1);
	}
	
	allocArray2(nameFiltersOut,numFiltersOut,MAX_LENGTH);
	allocArray1(filterMapping,numFiltersOut);
	
	// put names of matching filter names in nameFilterOut
	// filterMapping[i] gives for each filter entry in the
	// final filter list i the corresponding line number of the
	// filter entry in the .def file
	numFiltersOut=0;
	for (int i=0;i<defnumFilters;i++) {
		found=0;
		for (int j=0;j<numFiltersIn;j++) {
			if (strcmp(defnameFilters[i],nameFiltersIn[j])==0) 	{
				found=1;			
			}
		}
		if (found==1) {
			filterMapping[numFiltersOut]=i;
			strcpy(nameFiltersOut[numFiltersOut++],defnameFilters[i]);
		}
	}					
	
	// now create filterbands
	this->numFilterBands=numFiltersOut;
	filterBands=(FilterBand**)malloc(numFilterBands*sizeof(FilterBand*));
	for (int i=0;i<numFilterBands;i++) {
		if (defnumErrorFilters>0) {
			filterBands[i] = new FilterBand(nameFiltersOut[i],
							defColFilters[filterMapping[i]],
							defColErrorFilters[errorMapping[filterMapping[i]]]);
		} else {
			filterBands[i] = new FilterBand(nameFiltersOut[i],
							defColFilters[filterMapping[i]],
							-1);
		}
							
	}
	
	// now create "filterbands" for spectroscopic redshifts
	this->numSpecZBands=defnumSpecZ;
	specZBands=(FilterBand**)malloc(numSpecZBands*sizeof(FilterBand*));
	for (int i=0;i<numSpecZBands;i++) {
		// -1 in errorMappingSpecZ indicates no column in the catalog 
		// corresponds to error entry
		if (defnumErrorSpecZ>0) {
			specZBands[i] = new FilterBand(defnameSpecZ[i], defColSpecZ[i],
									defColErrorSpecZ[errorMappingSpecZ[i]]);
		} else {
				specZBands[i] = new FilterBand(defnameSpecZ[i], defColSpecZ[i],
									-1);						
		}
	}

	// copy 1 sigma errors
	freeArray1(this->oneSigmaError);
	allocArray1(this->oneSigmaError,numFilterBands);
	freeArray1(this->minError);
	allocArray1(this->minError,numFilterBands);
	freeArray1(this->zp);
	allocArray1(this->zp,numFilterBands);
	for (int i=0;i<numFilterBands;i++) 
		this->oneSigmaError[i]=def1sigmaFilters[errorMapping[filterMapping[i]]];
	for (int i=0;i<numFilterBands;i++)
		this->minError[i]=defminError[errorMapping[filterMapping[i]]];
	for (int i=0;i<numFilterBands;i++)
		this->zp[i]=defZPFilters[filterMapping[i]];

	// free memory
	freeArray1(filterMapping);
	freeArray1(defZPFilters);
	if (defnumErrorFilters) {
		freeArray1(errorMapping);
		freeArray2(defnameErrorFilters,defnumErrorFilters);
		freeArray1(defColErrorFilters);	
		freeArray1(def1sigmaFilters);
		freeArray1(defminError);
	}
	if (defnumErrorSpecZ) {
		freeArray1(errorMappingSpecZ);
		freeArray2(defnameErrorSpecZ,defnumErrorSpecZ);
		freeArray1(defColErrorSpecZ);
	}
	freeArray2(defnameFilters,defnumFilters);	
	freeArray2(defnameSpecZ,defnumSpecZ);	
	freeArray1(defColFilters);	
	freeArray1(defColSpecZ);
	
	delete deffile;

	strcpy(defOptions[0],"Options");	
	setOptions(defOptionEntries,defOptions);
	freeArray2(defOptions,defOptionEntries);		
}

// multiply each relative flux errors by a constant factor
void Catalog::multiplyError(double factor) {	
	for (int i=0;i<this->numFilterBands;i++) 		
		this->filterBands[i]->multiplyError(factor); 			
}	

// set each relative flux error to a minimum value		
void Catalog::floorError(double minError) {
	for (int i=0;i<this->numFilterBands;i++) 		
		this->filterBands[i]->floorError(minError); 				
}

// set each relative flux error to a minimum value	for given filterband with index filterBandNr
void Catalog::floorError(double minError, int filterBandNr)
{
	this->filterBands[filterBandNr]->floorError(minError); 				
}



// as parameter should be given filter or filternames
// then only this filters are used in the summation
void Catalog::errorAnalysis(int xBins,int xEntriesNr,
							int errorBins,int errorEntriesNr)
{
	for (int i=0;i<this->numFilterBands;i++)
		this->filterBands[i]->errorAnalysis(xBins,xEntriesNr,
											errorBins,errorEntriesNr);		
}

// write results of errorAnalysis into files
void Catalog::writeErrorAnalysis(const char* path, const char * filenamebase) {
	char filename[MAX_LENGTH];
	for (int i=0;i<this->numFilterBands;i++) {
		sprintf(filename,"%s/%s",path,filenamebase);
		this->filterBands[i]->writeErrorAnalysis(filename);
	}	
}

// randomizes a catalog by just changing all fluxes by a value drawn from a gaussian
// the sigma of the gaussian is taken to be the absolute flux error
void Catalog::reshuffleGaussian() {
	for (int i=0;i<this->numFilterBands;i++) 		
		this->filterBands[i]->reshuffleGaussian(); 			
}
	
// randomizes a catalog by just changing all fluxes by a value drawn from a gaussian
void Catalog::reshuffleGaussian(double error)
{
	// randomize each filterband 
	for (int i=0;i<this->numFilterBands;i++) 		
		this->filterBands[i]->reshuffleGaussian(error); 	
}

// randomizes a catalog using data from files
void Catalog::randomize(char *errorpath, char* errorbase, int errorMode, double minError)
{
	char filename[MAX_LENGTH];
	// fill HistogramAnalysis object in each filterBand	
	for (int i=0;i<this->numFilterBands;i++) {
		sprintf(filename,"%s/%s",errorpath,errorbase);
		this->filterBands[i]->readError(filename);
	}
	
	// randomize each filterband 
	for (int i=0;i<this->numFilterBands;i++) 		
		this->filterBands[i]->randomize(errorMode,minError); 	
}

// saves complete catalog
void Catalog::save(const char * outputpath, const char* outputname)
{
	ofstream ofile;
	char filename[MAX_LENGTH];
	
	sprintf(filename,"%s/%s",outputpath,outputname);
	ofile.open(filename);
	if (! ofile.good()) {
		cerr << "Could not save catalog file " << filename << endl;
		exit(1);
	}

	fluxToMag();
	
	// two output styles
	// a) only used filterbands & specZ bands in somewhat arbitrary order
	//    (the order is specified by the .def and .conf files)
	// b) reproduce the orginal catalog, but substitute the new values / errors
	//    unchanged columns and header informations are passed unmodified
	
	if (! options[7]) { // a)
		ofile << "# Catalog " << this->name << endl;
		ofile << "# ";
		for (int i=0;i<numFilterBands;i++)
			ofile << filterBands[i]->getName() << " ";			
		for (int i=0;i<numFilterBands;i++)
			ofile << "e(" << filterBands[i]->getName() << ") ";	
		for (int i=0;i<numSpecZBands;i++)
			ofile << specZBands[i]->getName() << " ";	
		for (int i=0;i<numSpecZBands;i++)
			ofile << "e(" << specZBands[i]->getName() << ") ";	
		ofile << endl;
	
		for (int i=0;i<linenumber;i++) {				
			for (int j=0;j<numFilterBands;j++) {
				if (filterBands[j]->getInvalid()[i]) 
					ofile << "-99 ";
				else 
					ofile << filterBands[j]->getValue()[i] << " ";
			}
			for (int j=0;j<numFilterBands;j++) {
				if (! options[6]) { // print modified errors
					if (filterBands[j]->getInvalid()[i])
						ofile << "-99 ";
					else
						ofile << filterBands[j]->getError()[i] << " ";
				} else {			// print original errors
					ofile << filterBands[j]->getOrigError()[i] << " ";	
				}
			}	
			for (int j=0;j<numSpecZBands;j++)
				ofile << specZBands[j]->getValue()[i] << " ";				
			for (int j=0;j<numSpecZBands;j++)
				ofile << specZBands[j]->getError()[i] << " ";		
			ofile << endl;
		}			
	} else { // b)		
		double **body;	
		int currlinenumber,
			colIndex,
			errorColIndex,
			entryIndex;
				
		TableFile *tablefile;
		tablefile = new TableFile(this->filename);		// load original catalog file (to obtain uninteresting columns)
														// the relevant/changed values are stored in the filterBands
		
		entryIndex=0;									// the line number of entry in catalog														// = iterations*NUMBUNCHLINES + currlinenumber						
		while (! tablefile->readComplete()) {
		
			tablefile->load(NUMBUNCHLINES);				// read only numLinesRead lines at a time
					
			currlinenumber=tablefile->getlinenumber();  // number of lines in the bunch, should equal
														// NUMBUNCHLINES but for the last bunch				
			body=tablefile->getbody();				
			
			for (int i=0;i<currlinenumber;i++) {
				for (int b=0;b<numFilterBands;b++) {
					colIndex=filterBands[b]->getColID();
					errorColIndex=filterBands[b]->getErrorColID();	
					if (filterBands[b]->getInvalid()[entryIndex]) 			
						body[i][colIndex]=-99;
					else									
						//body[i][colIndex]=round(filterBands[b]->getValue()[entryIndex],OUTPUTPRECISION);
						body[i][colIndex]=filterBands[b]->getValue()[entryIndex];
																				
					if (errorColIndex>0) {						
					  if (options[6]) // print original errors
						body[i][errorColIndex]=round(filterBands[b]->getOrigError()[entryIndex],OUTPUTPRECISION);
					  else {				
						if (filterBands[b]->getInvalid()[entryIndex])
							body[i][errorColIndex]=-99;
						else
							//body[i][errorColIndex]=round(filterBands[b]->getError()[entryIndex],OUTPUTPRECISION);
							body[i][errorColIndex]=filterBands[b]->getError()[entryIndex];					
					  }				
					}
					#if 0
					printf("flux:body[%d][%d]=%g\n",i,colIndex,body[i][colIndex]);	
					printf(" error:body[%d][%d]=%g\n",i,errorColIndex,body[i][errorColIndex]);			
					#endif
					
				}
				entryIndex++;
			}
			
			tablefile->saveAppend(filename);			// save it under new name
			
		}
		
		delete tablefile;	
	}	
	
	magToFlux();
}

// show member values
void Catalog::show()
{
	char s[MAX_LENGTH];
	this->m_out->println("Catalog:",3);
	sprintf(s,"name = %s",this->name);
	m_out->println(s,3);	
	sprintf(s,"defFileName = %s",this->defFileName);
	m_out->println(s,3);	
	sprintf(s,"colnumber = %d",this->colnumber);
	m_out->println(s,3);	
	sprintf(s,"linenumber = %d",this->linenumber);
	m_out->println(s,3);	
	sprintf(s,"specZ = %d",this->specZ);
	m_out->println(s,3);	
	sprintf(s,"numFilterBands = %d",this->numFilterBands);
	m_out->println(s,3);	
	sprintf(s,"numSpecZBands = %d",this->numSpecZBands);
	m_out->println(s,3);	
	for (int i=0;i<this->numFilterBands;i++) {
		sprintf(s,"oneSigmaError[%d] = %g ",i,this->oneSigmaError[i]);
		m_out->print(s,3);	
	}
	m_out->println("",3);
	for (int i=0;i<this->numFilterBands;i++) {
		sprintf(s,"minError[%d] = %g ",i,this->minError[i]);
		m_out->print(s,3);	
	}
	m_out->println("",3);
	for (int i=0;i<this->numFilterBands;i++) {
		sprintf(s,"Filterbands[%d]->name = %s ",i,this->filterBands[i]->getName());
		m_out->print(s,3);	
	}
	m_out->println("",3);
	for (int i=0;i<this->numSpecZBands;i++) {
		sprintf(s,"specZbands[%d]->name = %s ",i,this->specZBands[i]->getName());
		m_out->print(s,3);	
	}
	m_out->println("",3);
	for (int i=0;i<this->numOptions;i++) {
		sprintf(s,"options[%d] = %d ",i,this->options[i]);
		m_out->print(s,3);	
	}
}
