#ifndef _CONFFILE_H_
#define _CONFFILE_H_

#include <iostream>
#include <fstream>
#include <iomanip>
#include "globHeader.h"
#include "NumericalData.h"

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/

class ConfFile
{
protected:
	char* filename;	
	int linenumber;								// current read in number of lines
	int headerlinenumber;						// current read in number of header lines
	int colnumber;								// current maximal column number
	
	int totallinenumber;						// total read in number of lines
	int totalheaderlinenumber;						// total read in number of header lines
	int totalcolnumber;							// maximal column number (concerning all lines read so far)
	
	int *entrynumber;							// number of cols in current read-in 
												//   line 0..linenumber-1
	char*** entries;
		
	char** headerentries;
	
	char *entryType;							// stores the information about the type of the
												// entry (normal entryline (0) vs. headerline (1))
	int m_loaded;								// whether entries are loaded
		
	ifstream infileFrac;
	
	void resize();
	void closeFileHandle(); 					// close file handle infileFrac
	
	OutputHandler *m_out;
		
public:
	ConfFile(const char* filename);
	virtual ~ConfFile();		
	
	void read();								// read all lines from file 
												// file handle NOT kept open
												
	void readFraction(int bunchlines); 			// read first bunchlines lines from file
												// file handle (infileFrac) KEPT open
												// use readFractionContinue unless you want
												// to restart the reading
												
	void readFractionContinue(int bunchlines);  // read next bunchlines lines from file
												// file handle (infileFrac) KEPT open
												
	int readComplete();							// 1 if infileFrac is completely read
												// 0 otherwise											
	
	void write();
	void write(const char *headerline);
	void saveAppend(char *filename);			// save file under new name (append if file existing)
	
	void printCurrentStatus();					// prints a status line with infos about read in file
	void printTotalStatus();						// prints a status line with infos about read in file
		
	void setOutputHandler(OutputHandler *out) {
		m_out=out;
	}
	
	void setEntries(char *** entries,int * entrynumber, int linenumber, 
					char ** headerentries, int headerlinenumber);
	
	void getEntries(char *** & entries, int * & entrynumber, int & linenumber);
	void getHeaderEntries(char ** & headerentries, int & headerlinenumber );		
							
	int getcolnumber() { return colnumber; };
	int getlinenumber() { return linenumber; };
	int getheaderlinenumber() { return headerlinenumber; };
	// get linenumber and colnumber without actually filling ***entries
	void getQuickLineColnumber(const char *filename, int & linenumber, int & colnumber);
		
};

#endif //_CONFFILE_H_
