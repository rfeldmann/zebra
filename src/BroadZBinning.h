#ifndef BROADZBINNING_H_
#define BROADZBINNING_H_

#include "Template.h"
#include "NumericalData.h"
#include "Likelihood.h"
#include <fstream>
#include <sstream>

/****************************************************
 * ZEBRA Copyright (C) 2007 Robert Feldmann         *
 * please refer to Feldmann, et al., MNRAS, 2006    *
 * e-mail: zebra@phys.ethz.ch                       *
 * written 2005/2006/2007                           *
 *                                                  *
 * ZEBRA comes with ABSOLUTELY NO WARRANTY;         *
 * For conditions of distribution and use,          *
 * see the accompanying LICENSE file or             *
 * read the documentation                           *
 ****************************************************/
 
class FittedEntry
{
protected:
	double specZ;			// spectroscopic redshift from catalog but shifted to grid
	int bestT;				// "original" template corresponding to best fitting corrected template
	double template_norm;	// normalization factor of "original" template corresponding to best fitting corrected template
	double chi2;			// chi2 of "original" template corresponding to best fitting corrected template
	double chi2Tot;		    // chi2 of best fitting template (might be a corrected template)
	int valid;				// whether entry is valid (1) or not (0)
	int numUsedFilters;	    // number of valid filter bands for this entry
	int numFilters;			// number of filter bands, gives array size of flux_entries etc
	int *flux_valid;		// whether certain filter band can be used (1) or not (0)
	double *flux_entries;	// flux values (from catalog)
	double *flux_errors;	// flux errors (from catalog)
	
public:

	FittedEntry(double specZ, int bestT, double template_norm, double chi2, double chi2Tot, int valid, int numUsedFilters,
				int numFilters, double *flux_entries, double *flux_errors, int *flux_valid){
		this->specZ=specZ;
		this->bestT=bestT;
		this->template_norm=template_norm;
		this->chi2=chi2;
		this->chi2Tot=chi2Tot;
		this->valid=valid;
		this->numUsedFilters=numUsedFilters;
		this->numFilters=numFilters;
		if (valid && numFilters>0) {		
			allocArray1(this->flux_valid,numFilters);
			allocArray1(this->flux_entries,numFilters);
			allocArray1(this->flux_errors,numFilters);
			for (int i=0;i<numFilters;i++) {
				this->flux_valid[i]=flux_valid[i];
				this->flux_entries[i]=flux_entries[i];
				this->flux_errors[i]=flux_errors[i];
			}
		} else {
			this->flux_valid=NULL;
			this->flux_entries=NULL;
			this->flux_errors=NULL;
		}
	}
	
	FittedEntry(FittedEntry *entry) {
		 specZ=entry->specZ;
	     bestT=entry->bestT;
	     template_norm=entry->template_norm;
	     chi2=entry->chi2;
	     chi2Tot=entry->chi2Tot;
	     valid=entry->valid;
	     numFilters=entry->numFilters;
	     numUsedFilters=entry->numUsedFilters;
	     if (valid && numFilters>0) {		
			allocArray1(flux_valid,numFilters);
			allocArray1(flux_entries,numFilters);
			allocArray1(flux_errors,numFilters);
			for (int i=0;i<numFilters;i++) {
				flux_valid[i]=entry->flux_valid[i];
				flux_entries[i]=entry->flux_entries[i];
				flux_errors[i]=entry->flux_errors[i];
			}
		} else {
			flux_valid=NULL;
			flux_entries=NULL;
			flux_errors=NULL;
		}	     		
	}
	
	~FittedEntry(){
		freeArray1(flux_valid);
		freeArray1(flux_entries);
		freeArray1(flux_errors);
	};
	
	void print() {
		printSimple();
	    if (valid && numFilters>0) {
		  for (int i=0;i<numFilters;i++) {			
			cout << "    B=" << i << "/F=" << flux_entries[i] << "/E=" << flux_errors[i] << "/V=" << flux_valid[i] << " ";
		  }	    
		  cout << endl;
	    }
	}
	
	void printSimple() {		
		cout << " FittedEntry: specZ=" << specZ << " bestT=" << bestT << " template_norm=" << template_norm 
			 << " chi2=" << chi2 << " chi2Tot=" << chi2Tot << " valid=" << valid << " numUsedFilters=" << numUsedFilters << std::endl;	    
	}
			
	void appendToFile(std::ofstream & ofile) {		
		ofile << specZ << " " << bestT << " " << template_norm << " " << chi2 << " " << chi2Tot << " " << valid << " " << numUsedFilters << std::endl;		
	}
	
	int getBestT() {
		return bestT;
	}
	
	double getSpecZ() {
		return specZ;
	}		
	
	double getTemplateNorm() {
		return template_norm;
	}
	
	double getChi2() {
		return chi2;
	}
	
	double getChi2Tot() {
		return chi2Tot;
	}
	
	int getValid() {
		return valid;
	}	
	
	int getNumUsedFilters() {
	    return numUsedFilters;
	}
	
	double *getFlux() {
		return flux_entries;
	}
	
	double *getFluxErrors() {
		return flux_errors;
	}
	
	int *getFluxValid() {
		return flux_valid;
	}
	
	void setValid(int vadid) {
		this->valid=valid;
	}			
};

class BroadZBinning;
class ResidualRedshiftBin;
class ResidualRedshiftTypeBin;

class BroadZBinning
{
protected:	
	int iterationID;		// in which iteration are we?
	int numEntries;
	int numFilters;
	int minEntriesBin;		// minimal number of entries in each red shift bin
	double sigma;			// determines how much templates are allowed to change in the 
							// template correction step, the higher the sigma value the less the penalty 
							// for changing the shape of the template		
	double rho;				// determines how much improved templates are allowed to change their
							// shape locally w.r.t the old template, i.e. the relative gradient is minimized
	int punishGradient;		// if set to 0, rho=inf i.e. no gradient punishment
	int rescaleWithMesh;
	int rescaleWithBands;
	int rescaleWithFlux;
	int loadRhoSigma;					// whether rho and sigma is loaded
	int saveRhoSigma;					// whether rho and sigma is saved
	std::string rhoSigmaLoadBase;       // file name from which rho, sigma are loaded
	std::string rhoSigmaSaveBase;       // file name into which rho and sigma are saved
	char **rhoSigmaFiles;				// contains the rho-sigma filenames for individual templates
    double 	percCut;        // used-defined percentile cut; fits with chi2 greater than the one dictated by the percentile are cut
                            // the corresponding objects are then not used in the template improvement step
    double chi2ReducedCut;  // used-defined reduced-chi^2 cut; fits with reduced chi2 greater than the one dictated chi2ReducedCut
    						// are cut and the corresponding objects are not used in the template improvement step
	Filter **filters;
			
	TemplateContainer *templateContainer;
	
	BinningZ *binSpecZ;		// the red shift binning used for calculating the residuals (specZ is constrained to that grid)
	BinningZNumber *binZ;	// the binning in z-space for the correction of the templates
	
	FittedEntry **entries;
	ResidualRedshiftBin **residualRedshiftBins;	
	
	OutputHandler *m_out;		
	std::string outputpath;	
	int igAbsorption;		// which type of IG-Absorption is used
	
	void load_sigma2_from_file(std::string filename,double *sigma2,int length);
	void load_rho2_from_file(std::string filename,double *rho2,int length);
			
public:
	BroadZBinning(int numFilters, Filter **filters, BinningZ *binSpecZ, TemplateContainer *templateContainer) {
				
		this->numFilters=numFilters;	
		this->filters=filters;
		this->binSpecZ=binSpecZ;
		this->templateContainer=templateContainer;
		
		this->iterationID=0;
		this->numEntries=0;		
		this->sigma=1.0;
		this->rho=1.0;
		this->punishGradient=0;				
		this->minEntriesBin=50;
		this->binZ=NULL;
		this->entries=NULL;
		this->residualRedshiftBins=NULL;		
		this->m_out=NULL;
		this->entries=NULL;
		this->outputpath=".";
		
		rescaleWithMesh=0;
	    rescaleWithBands=0;
	    rescaleWithFlux=0;
	    
	    loadRhoSigma=0;
	    saveRhoSigma=0;
	    rhoSigmaLoadBase="";
	    rhoSigmaSaveBase="";
	    
	    this->percCut=1;
	    this->chi2ReducedCut=1e100;
	}		
				   				  			
	~BroadZBinning();
			
	void set(Likelihood *like);
	
	void set(OutputHandler *out) {
		m_out=out;
	}
	
	void setOutputpath(std::string outputpath) {
		this->outputpath = outputpath;
	}
	
	void setSigma(double sigma) {
		this->sigma=sigma;
	}
	
	void setRho(double rho) {
		this->rho=rho;
	}
	
	void setPunishGradient(int punishGradient) {
		this->punishGradient=punishGradient;
	}
	
	void setigAbsorption(int igAbsorption) {
		this->igAbsorption=igAbsorption;
	}
	
	//void setDimensionlessParameterMode(int dimensionlessParameterMode) {
	//	this->dimensionlessParameterMode=dimensionlessParameterMode;
	//}
	void setParameterModes(int a,int b,int c) {
		rescaleWithMesh=a;
		rescaleWithBands=b;
		rescaleWithFlux=c;
	}
	
	void setRhoSigmaLoad(int a, std::string s) {
		loadRhoSigma=a;
		rhoSigmaLoadBase=s;
	}
	
	void setRhoSigmaSave(int a, std::string s) {
	    saveRhoSigma=a;
	    rhoSigmaSaveBase=s;	
	}
	
	void setRhoSigmaFiles(char **rhoSigmaFiles) {
		this->rhoSigmaFiles=rhoSigmaFiles;
	}
			
	void setIterationID(int iterationID) {
		this->iterationID=iterationID;
	}
	
	// construct binZ object raising the requirement that at least minEntriesBin entries in each 
	// redshift bin
	void setMinEntriesBin(int minEntriesBin);
						
	// use the given binZ object	
	void setzBins(BinningZ *binZ);		
	
	void setPercCut(double percCut) {
		this->percCut = percCut;
	}
	
	void setChi2ReducedCut(double chi2ReducedCut) {
		this->chi2ReducedCut = chi2ReducedCut;
	}
	
	void correctTemplates();
	
	void correctTemplatesFromOrig();
};

class ResidualRedshiftBin : public BroadZBinning
{
protected:
	int binID;
	
	int numRedshiftTypeBins;
	ResidualRedshiftTypeBin **residualRedshiftTypeBins;	
	
	double minZ;
	double maxZ;
		
public:
  
	ResidualRedshiftBin(int numFilters, Filter **filters, BinningZ *binSpecZ, TemplateContainer *templateContainer, int binID, double minZ, double maxZ)
	                   :BroadZBinning(numFilters,filters,binSpecZ,templateContainer){
		this->binID=binID;	
		this->minZ=minZ;
		this->maxZ=maxZ;				
		numRedshiftTypeBins=0;
		residualRedshiftTypeBins=NULL;		
	}		
	
	virtual ~ResidualRedshiftBin();	
	
	virtual void setEntries(int numEntries, FittedEntry **entries);
		
	void correct();
};

class ResidualRedshiftTypeBin : public ResidualRedshiftBin
{
protected:
	int basicTypeID;	
	
	double calc_chi2(int gridLength, int numFilters, int numEntries, 
				 	 double *sk_new, double *sk_old, double *sigma2, 
				 	 double **flux_new, double **flux_meas, double **flux_error);
				 	 
	double calc_chi2(int gridLength, int numFilters, int numEntries, 
				 	 double *sk_new, double *sk_old, double *sigma2, double *rho,
				 	 double **flux_new, double **flux_meas, double **flux_error);
	
public:


	ResidualRedshiftTypeBin(int numFilters, Filter **filters, BinningZ *binSpecZ, TemplateContainer *templateContainer, int binID, double minZ, double maxZ, int basicTypeID):
							ResidualRedshiftBin(numFilters, filters, binSpecZ, templateContainer, binID, minZ, maxZ) {
		this->basicTypeID=basicTypeID;		
	};				
	
	~ResidualRedshiftTypeBin() {};	
	
	void setEntries(int numEntries, FittedEntry **entries);

	void print();
	
	void saveStats();
	
	void correct();
	
	// returns corrected template(s), at the moment just one corrected template is returned
	void getTemplates(int & numTemplates, Template ** & templates);
};

#endif /*BROADZBINNING_H_*/
