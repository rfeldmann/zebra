# type 'gnuplot < plotPrior.gp' (after calling the script callzebra_Bayes)
# you need to have gnuplot installed

set title 'The prior [marginalized over template types] as a function of redshift'
set xlabel 'redshift'
set ylabel 'prior (normalized)'

set output 'plotPrior.eps'
set terminal postscript color

plot \
'prior_z0.0_Z4.0_d0.0025_i5_I_l_L_P0.datMarginZ_l0.dat' w l title 'initial' 0, \
'prior_z0.0_Z4.0_d0.0025_i5_I_l_L_P0.datMarginZ_l1.dat' w l title 'iteration 1' 1, \
'prior_z0.0_Z4.0_d0.0025_i5_I_l_L_P0.datMarginZ_l2.dat' w l title 'iteration 2' 2, \
'prior_z0.0_Z4.0_d0.0025_i5_I_l_L_P0.datMarginZ_l3.dat' w l title 'iteration 3' 3, \
'prior_z0.0_Z4.0_d0.0025_i5_I_l_L_P0.datMarginZ_l3.dat' w l title 'iteration 4' 4, \
'prior_z0.0_Z4.0_d0.0025_i5_I_l_L_P0MarginZ.dat' w l title 'iteration 5 (final)' -1

set output
