#!/bin/bash

#########################
#			#
# Run ZEBRA in 		#
# template improvement	#
# mode			#
#			#
# by Robert Feldmann 	#
# 2005/06		#
#			#
#########################

#######################
# setting paramterers #
#######################

renice 19 -p $$

## PATHS ##
EXECPATH=../bin
CATALOGPATH=../examples/tempimprove
CATALOG=testCatalog.cat
CATALOGDEF=testCatalog.cat.def
OUTPUTPATH=../examples/tempimprove
OUTPUTBASE="tempImprove"
FILTERPATH=../examples/filters
TEMPLATEPATH=../examples/templates

## RED SHIFT LIMITS ##

ZMIN=0.0
ZMAX=4.0
DZ=0.0025
LOGZBIN=" --log-zBin"

## MESH RELATED
MESHLAMBDAMIN=" --mesh-lambda-min 500 "
MESHLAMBDAMAX=" --mesh-lambda-max 25000 "
MESHLAMBDARES=" --mesh-lambda-res 2 "

## INTERPOLATION ##

LININTERPOL=0
LOGINTERPOL=5

## M_B cut ##

MBMODE=" --mb-mode 2 "
MBLOW=" -m -24 "
MBHIGH=" -M -13 "

## Template correction options ##

RIGHTZ=" --rightz "
IMPROVETEMPLATE=" --improve-template "
TEMPIMPROVDEF=" --template-improvement-def templateImprovement.def "
CORRECTIONSTEPS=" --correction-steps 2 "
SIGMA=" --sigma 10 "
RHO=" --rho 1 "
PUNISHGRADIENT=" --punish-gradient "
RESCALEWITHMESH=" --rescale-with-mesh "
RESCALEWITHFLUX=" --rescale-with-flux 1 "
#RESCALEWITHBANDS=" --rescale-with-bands "

## OTHER OPTIONS ##
SMOOTHTEMPLATE=" --smooth-template 0.01 "
SMOOTHFILTER=" --smooth-filter 0.01 "
SMOOTHMODE=" --smooth-mode 1 "
PHOTONFLUX=" --flux-type 0"	# 0 photon 1 energy
IGABSORPTION=" -a 1 "
ALLOWMISSINGERROR=" --allow-missing-error "
VERBOSITY=" --verbose 2"

## TEMPLATE FLUX OPTIONS ##

#LOADFLUX=" --load-flux"
#SAVEFLUX=" --save-flux"
FLUXBASE="fluxfile"
FLUXPATH=$PWD

## PRIOR OPTIONS ##

#LOADPRIOR=" --load-prior "
SAVEPRIOR=" --save-prior "
#SAVEPRIORITER=" --save-prior-iterative "
CALCPRIOR=" --calc-prior "
CALCPRIORMODE=" --calc-prior-mode 0 "	# 0 prior(T,z)  1 prior(best_T,z)
MAXITERATIONS=" --max-iterations 100 "

PRIORBASE="prior_z${ZMIN}_Z${ZMAX}_d${DZ}_i${LOGINTERPOL}_I${LOGINTERPOLMODE}_l${LININTERPOL}_L${LININTERPOLMODE}_P${PHOTONFLUX}.dat"
PRIORPATH=$PWD/prior


########################
# better making a copy #
########################
if [ -f $OUTPUTPATH/$0 ]
then
  TEMPNAME=`mktemp`
  cp ./$0 $TEMPNAME
  cp ${OUTPUTPATH}/$0 ${OUTPUTPATH}/${0}.old
fi
cp $TEMPNAME $OUTPUTPATH/$0

#########################
# starting the C++ code #
#########################

$EXECPATH/zebra \
-C $CATALOGPATH \
-c $CATALOG \
-d $CATALOGDEF \
-O $OUTPUTPATH \
-o $OUTPUTBASE \
-X $FLUXPATH \
-x $FLUXBASE \
-P $PRIORPATH \
-p $PRIORBASE \
-F $FILTERPATH \
-T $TEMPLATEPATH \
-i $LOGINTERPOL \
-l $LININTERPOL \
-z $ZMIN \
-Z $ZMAX \
-D $DZ \
$LOGZBIN \
$PHOTONFLUX \
$FLUXENTRIES \
$IMPROVETEMPLATE \
$SIGMA \
$RHO \
$PUNISHGRADIENT \
$TEMPIMPROVDEF \
$CORRECTIONSTEPS \
$RIGHTZ \
$SHOWTABLE \
$MBMODE \
$MBLOW \
$MBHIGH \
$ALLOWMISSINGERROR \
$VERBOSITY \
$LOADFLUX \
$SAVEFLUX \
$LOADPRIOR \
$SAVEPRIOR \
$SAVEPRIORITER \
$CALCPRIOR \
$CALCPRIORMODE \
$MAXITERATIONS \
$ZAMORANI \
$IGABSORPTION \
$BAYESIAN \
$SMOOTHPRIOR \
$SMOOTHT \
$SMOOTHZ \
$SMOOTHFILTER \
$SMOOTHTEMPLATE \
$SMOOTHMODE \
$POSTERIOR1 \
$POSTERIOR2 \
$POSTERIOR3 \
$POSTERIOR4 \
$LIKELIHOOD1 \
$LIKELIHOOD2 \
$LIKELIHOOD3 \
$LIKELIHOOD4 \
$DUMP1 \
$DUMP2 \
$DUMP3 \
$DUMP4 \
$DUMP5 \
$SINGLE \
$USEMESH \
$RESCALEWITHMESH \
$RESCALEWITHFLUX \
$RESCALEWITHBANDS \
$MESHLAMBDAMIN \
$MESHLAMBDAMAX \
$MESHLAMBDARES
